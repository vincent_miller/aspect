package aspect.example;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectLauncher.run;
import static aspect.resources.Resources.createImage;

import java.awt.image.BufferedImage;

import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.render.Material;
import aspect.render.Texture;
import aspect.resources.Resources;
import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.SimplexNoise;
import aspect.util.Trig;
import aspect.util.Vector3;

public class RandomSphereTest extends Behavior {
    private Texture texture;
    private BufferedImage image;
    
    public static void main(String[] args) {
        run(800, 600, false, 60, new RandomSphereTest());
    }

    @Override
    public void onAdd() {
        image = createImage(400, 200);
        texture = Texture.create(image);
        Entity sphere = new Entity(Resources.sphere(1, 50, new Material(texture)));
        sphere.transform.setPosition(new Vector3(1.5f, 0, -4));
        mainWorld.add(sphere);
        
        BufferedImage image2 = createImage(800, 400);
        for (int i = 0; i < image2.getWidth(); i++) {
            for (int j = 0; j < image2.getHeight(); j++) {
                float yaw = i * Trig.FULL_CIRCLE / image2.getWidth();
                float pitch = j * Trig.HALF_CIRCLE / image2.getHeight();
                Vector3 dir = new Angles(yaw, pitch, 0).toVector(1.0f);
                
                float noise = SimplexNoise.Generate(dir.x * 0.01f, dir.y * 0.01f, dir.z * 0.01f);
                if (noise < -0.1f) {
                    image2.setRGB(i, j, java.awt.Color.BLUE.getRGB());
                } else {
                    image2.setRGB(i, j, java.awt.Color.GREEN.getRGB());
                }
            }
        }
        Texture texture2 = Texture.create(image2);
        Entity sphere2 = new Entity(Resources.sphere(1, 50, new Material(texture2)));
        sphere2.transform.setPosition(new Vector3(-1.5f, 0, -4));
        mainWorld.add(sphere2);
    }
    
    @Override
    public void update() {
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                float noise = SimplexNoise.Generate(i * 0.05f, j * 0.05f, Time.frameTime());
                noise = 0.5f + noise / 2.0f;
                image.setRGB(i, j, new java.awt.Color(noise, 1.0f - noise, 0.5f + noise / 2.0f).getRGB());
            }
        }
        
        texture.updateTexture(image);
    }
}

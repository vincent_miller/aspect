package aspect.example;

import static aspect.core.AspectLauncher.addKeyListener;
import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.addSkybox;
import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.clearRenderer;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.core.AspectRenderer.getPixelDepth;
import static aspect.core.AspectRenderer.postProcess;
import static aspect.core.AspectRenderer.projectVectorNorm;
import static aspect.core.AspectRenderer.raycast;
import static aspect.core.AspectRenderer.setBackground;
import static aspect.core.AspectRenderer.setLightingEnabled;

import java.awt.Canvas;
import java.awt.image.BufferedImage;
import java.io.File;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.entity.behavior.Behavior;
import aspect.event.KeyEvent;

import static aspect.resources.Resources.*;

import aspect.physics.ConvexCollider;
import aspect.physics.Hit;
import aspect.physics.Motion;
import aspect.render.Light;
import aspect.render.Material;
import aspect.render.Sun;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.resources.HeightmapTerrain;
import aspect.resources.Terrain;
import aspect.time.ExpirationTimer;
import aspect.time.Time;
import aspect.util.Color;
import aspect.util.Debug;
import aspect.util.Random;
import aspect.util.Vector2;
import aspect.util.Vector3;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class HeightMapTest extends Behavior {
    private Terrain terrain;
    private Player player;
    private Light sunlight;
    private ShaderProgram waters, flare;
    private ViewModel moon;
    private ViewModel water;
    private Material clouds;
    private ExpirationTimer ripple;
    private int ripples;
    private ViewModel sun;

    public static void main(String[] args) {
        //run(800, 600, false, 60, new HeightMapTest());
        run(60, new HeightMapTest());
    }

    @Override
    public void onAdd() {
        
        player = new Player(Vector3.zero());
        player.addBehavior(new Motion());
        Material mtl = new Material(loadTexture(new File("textures/ground.jpg")));
        terrain = new HeightmapTerrain(loadImage(new File("textures/heightmap.jpg")), new Vector3(0.25f, 20, 0.25f), mtl, 2.0f);
        mainWorld.add(new Entity(terrain));

        sunlight = new Light(new Vector3(1, 1, 0).normalize());
        sunlight.directional = true;
        
        clouds = new Material(loadTexture(new File("textures/clouds.png")), ShaderProgram.PHONG_VERTEX);
        clouds.diffuse = Color.BLACK;
        clouds.ambient = Color.BLACK;
        ViewModel c = sphere(1, 50, clouds);
        c.transform.setUp(Vector3.yAxis().negate());
        addSkybox(c);

        moon = sphere(0.1f, 50, new Material(loadTexture(new File("textures/moon.jpg"))));
        moon.transform.setPosition(sunlight.transform.position().negate());
        addSkybox(moon);

        setLightingEnabled(true);

        flare = new ShaderProgram(Shader.V_TEXTURE, loadShader(new File("shaders/sun-flare.frag"), Shader.Type.FRAGMENT));
        
        sun = new Sun(ShaderProgram.SUN, flare);
        sun.transform.setPosition(sunlight.transform.position());
        addSkybox(sun);

        Shader vert = loadShader(new File("shaders/water.vert"), Shader.Type.VERTEX);
        Shader frag = loadShader(new File("shaders/water.frag"), Shader.Type.FRAGMENT);
        waters = new ShaderProgram(vert, frag);
        Material.defaultShader = waters;

        water = rect(new Color(0, 0, 0.6f, 0.8f), terrain.getModelWidth(), terrain.getModelDepth());
        water.transform.setForward(Vector3.yAxis().negate());
        water.transform.setPosition(new Vector3(terrain.getModelWidth() / 2, 3, terrain.getModelDepth() / 2));

        for (int i = 0; i < 10; i++) {
            Vector2 vec = new Vector2(Random.Float(terrain.getModelWidth()), Random.Float(terrain.getModelDepth()));
            waters.setUniform("rpos[" + i + "]", vec);
        }

        waters.setUniform("num", 10);

        waters.setUniform("width", terrain.getModelWidth());
        waters.setUniform("height", terrain.getModelDepth());

        addKeyListener(this);
    }

    @Override
    public void keyEvent(KeyEvent evt) {
        if (evt.type && evt.key == Keyboard.KEY_SPACE) {
            Vector3 p = player.transform.position();
            float y = terrain.interpolateHeight(p.x, p.z) + 1;
            Vector3 normal = getSlopeNormal(p);
            float slope = Vector3.dot(normal, Vector3.yAxis());
            if (slope > 0.5f && p.y == y) {
                player.motion().velocity.y += 5;
            }
        }
    }

    private Vector3 getSlopeNormal(Vector3 position) {
        Vector3 scale = terrain.getScale();
        
        int x = (int) (position.x / scale.x);
        int y = (int) (position.z / scale.z);

        if (x > 0 && y > 0 && x < terrain.getDataWidth() && y < terrain.getDataDepth()) {

            return terrain.getNormal(x, y);
        } else {
            return Vector3.yAxis();
        }
    }

    @Override
    public void update() {
        Vector3 old = player.transform.position();
        player.update();
        Vector3 after = player.transform.position();
        Vector3 p = player.transform.position();
        float y = terrain.interpolateHeight(p.x, p.z) + 1;
        if (p.y <= y) {
            player.motion().velocity.y = 0;
            p.y = y;
        } else if (p.y > y) {
            float acc = -9.8f;
            if (p.y < water.transform.position().y) {
                acc = -1.0f;
            }
            player.motion().velocity.y += acc * Time.deltaTime();
        }
        player.transform.setPosition(p);
        
        Vector3 normal = getSlopeNormal(p);
        float slope = Vector3.dot(normal, Vector3.yAxis());
        if (slope < 0.5f && p.y == y) {
            Vector3 movement = after.minus(old);
            float proj = Vector3.dot(movement, normal.xz().asXZ());
            
            if (proj < 0) {
                 player.transform.setPosition(old);
            }
        }

        sunlight.transform.orbit(Vector3.zAxis(), Time.deltaTime() * 0.1f);
        moon.transform.orbit(Vector3.zAxis(), Time.deltaTime() * 0.1f);
        sun.transform.setPosition(sunlight.transform.position());
        flare.setUniform("water", player.transform.position().y < water.transform.position().y);

    }

    @Override
    public void render() {
        clouds.emissive = new Color(sunlight.transform.position().y);
        water.render();
    }
}

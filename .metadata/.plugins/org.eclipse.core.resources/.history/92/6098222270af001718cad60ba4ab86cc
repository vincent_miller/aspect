package aspect.example;

import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.core.AspectRenderer.setBackground;

import java.io.File;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.entity.behavior.Behavior;
import aspect.gui.Button;
import aspect.gui.CheckBox;
import aspect.gui.Container;
import aspect.gui.Counter;
import aspect.gui.FPSMonitor;
import aspect.gui.FlowLayout;
import aspect.gui.GUI;
import aspect.gui.RadioButton;
import aspect.gui.Rectangle;
import aspect.gui.Slider;
import aspect.gui.TextField;
import aspect.gui.TextLog;
import aspect.util.Color;
import aspect.util.Vector2;

public class GUITest extends Behavior {
    public static void main(String[] args) {
        run(800, 450, false, 60, new GUITest());
    }

    public void onAdd() {
        setBackground(Color.WHITE);
        
        Container all = new Container(new Vector2(getCanvasWidth(), getCanvasHeight()));
        all.setBackgroundEnabled(false);
        all.setLayout(new FlowLayout(10, 10, FlowLayout.HORIZONTAL, false));
        GUI.add(all);

        Container rect = new Container(new Vector2(385, 580));
        rect.setColor(GUI.backColor);
        rect.setLayout(new FlowLayout(10, 10, FlowLayout.VERTICAL, false));
        all.add(rect);

        TextLog log = new TextLog(new Vector2(385, 580));
        log.setColor(GUI.backColor);
        log.setTextSize(12);
        all.add(log);

        Button btn = new Button("Button 1");
        btn.setTextSize(24);
        btn.fitToText();
        btn.addListener(() -> {
            log.appendln("Button 1 pressed");
        });
        rect.add(btn);

        Button btn2 = new Button("Button 2");
        btn2.setTextSize(24);
        btn2.fitToText();
        btn2.addListener(() -> {
            log.appendln("Button 2 pressed");
        });
        rect.add(btn2);

        TextField field = new TextField(200);
        field.addListener(log);
        field.setClearOnSend(true);
        rect.add(field);

        Container checks = new Container(new FlowLayout(10, 0, FlowLayout.HORIZONTAL, false));
        checks.setBackgroundEnabled(false);
        CheckBox chk1 = new CheckBox();
        checks.add(chk1);
        CheckBox chk2 = new CheckBox();
        checks.add(chk2);
        CheckBox chk3 = new CheckBox();
        checks.add(chk3);
        rect.add(checks);

        chk1.addListener(() -> {
            log.append("Checkbox 1: " + chk1.checked());
        });
        
        chk2.addListener(() -> {
            log.append("Checkbox 2: " + chk2.checked());
        });
        
        chk3.addListener(() -> {
            log.append("Checkbox 3: " + chk3.checked());
        });

        Container rads = new Container(new FlowLayout(10, 0, FlowLayout.HORIZONTAL, false));
        rads.setBackgroundEnabled(false);
        RadioButton rad1 = new RadioButton();
        rads.add(rad1);
        RadioButton rad2 = new RadioButton();
        rads.add(rad2);
        RadioButton rad3 = new RadioButton();
        rads.add(rad3);
        RadioButton.group(rad1, rad2, rad3);
        rect.add(rads);
        
        rad1.addListener(() -> {
            log.append("Radio Button 1 selected");
        });
        
        rad2.addListener(() -> {
            log.append("Radio Button 2 selected");
        });
        
        rad3.addListener(() -> {
            log.append("Radio Button 3 selected");
        });
        
        Slider slider = new Slider(Slider.HORIZONTAL, 100);
        slider.addListener((value) -> {
            log.appendln("Slider " + (((int)(value * 100)) / 100.0f));
        });
        rect.add(slider);
        
    }
}

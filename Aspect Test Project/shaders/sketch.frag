#version 130

uniform bool useTexture;
uniform sampler2D tex;

uniform sampler2D depth;

in vec2 texcoord;

float func(float f) {
	return sin(f) + 0.1 * sin(10 * f) + 0.01 * sin(100 * f);
}

void main() {
    vec4 texColor = vec4(1.0);

    if (useTexture) {
        texColor = texture2D(tex, texcoord);
    }
    
    if (texColor.a == 0.0) {
        discard;
    }
    
    float a = texColor.g * 3.14;
    
    
	float dist = texture2D(depth, texcoord).r;
    
    vec2 t = texcoord;
    t = vec2(cos(a) * t.x - sin(a) * t.y, sin(a) * t.x + cos(a) * t.y);
    
    float d = abs(t.y - (func(t.x * 400 * texColor.r) * 2));
    
    float f;
	
	if (d < 0.8 * 1.0 - ((1 - dist) * 50f) * texColor.r && texColor.r > 0) {
		f = 0.0;
	} else {
		f = 1.0;
	}
	
	gl_FragColor = vec4(f, f, f, 1.0);
	
	
}
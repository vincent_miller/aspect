#version 130
#include <time.ase>

in vec2 texcoord;
in vec3 lightIntensity;
in vec4 color;

uniform vec2 rpos[10];
uniform int num;
uniform float width;
uniform float height;

float rng(float val, float l, float u) {
	return min(max(val, l), u);
}

void main() {
    vec4 texColor = color;
    
    for (int i = 0; i < num; i++) {
	    float d = distance(texcoord * vec2(width, height), rpos[i]);
	    float f = min(0.05 * pow(abs(sin(5 * d - 4 * Time.elapsed)), 30), 0.1);
	    texColor += vec4(f, f, f, 0);
    }
	
    gl_FragColor = texColor;
}
#version 430 core
#include <matrices.ase>

out float intensity;

layout (location = 0) in vec4 Vertex;

void main() {
	gl_Position = Matrices.projection * Matrices.modelview * vec4(Vertex.xyz, 1.0);
	intensity = Vertex.w;
}
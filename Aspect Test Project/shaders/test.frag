#version 130

uniform bool useTexture;
uniform sampler2D tex;

in vec2 texcoord;

void main() {
    vec4 texColor = vec4(1.0);

    //if (useTexture) {
        texColor = texture2D(tex, texcoord);
    //}
    
    //if (texColor.a == 0.0) {
    //    discard;
    //}
    
	gl_FragColor = vec4(texColor.rgb, 1);
}
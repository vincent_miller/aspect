#version 130
#include <lighting>
#include <matrices>
#include <vertex>
#include <time>

uniform bool useTexture;
uniform sampler2D tex;

out vec3 normal;
out vec4 color;

out float diffuse[{maxLights}];
out float dist[{maxLights}];

out vec3 position;
out vec2 texcoord;

float y(vec2 v) {
	float l = 10 * length(v) + 5;
	
	return 0.5 * sin(l - Time.elapsed * 10) / l;
	return 0;
}

float dy(vec2 v) {
	float l = 10 * length(v) + 5;
	
	return 5 * (l * cos(l - Time.elapsed * 10) - sin(l - Time.elapsed)) / (l * l);
	return 0;
}

void main() {

    vec4 pos = Matrices.view * (vec4(0, y(Vertex.xy), 0, 0) + (Matrices.model * Vertex));
    gl_Position = Matrices.projection * pos;
    position = pos.xyz;
    
    vec2 n = normalize(Vertex.xy) * -dy(Vertex.xy);
    
    normal = Matrices.normal * (vec3(n.x, n.y, 1) + 0.00001 * Normal);
    
    
    texcoord = TexCoord;
    color = Color;
	
	if (LightModel.enabled) {
	    dist = lightDistanceFloat(pos);
	    diffuse = lightDiffuseFloat(pos, normal);
    }
}
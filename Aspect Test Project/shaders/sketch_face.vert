#version 130
#include <matrices.ase>
#include <vertex.ase>

out vec4 color;
out vec4 world;

out vec4 up;
out vec4 right;

void main() {
    world = Matrices.model * Vertex;
    gl_Position = Matrices.projection * Matrices.view * world;
    
    color = Color;
}

#version 130
#include <time>

uniform bool useTexture;
uniform sampler2D tex;

in vec2 texcoord;

void main() {
    vec4 texColor = vec4(1.0);

    if (useTexture) {
        texColor = texture2D(tex, texcoord);
    }
    
    if (texColor.a == 0.0) {
        discard;
    }
    
	//gl_FragColor = texColor;
	
	float d = distance(gl_FragCoord.xy, vec2(960, 540));
	
	float f = 10f * sqrt(max(Time.elapsed, 0)) / d;
	
	texColor.b += f;
	texColor.r -= f;
	texColor.g -= f;
	
	gl_FragColor = texColor;
}
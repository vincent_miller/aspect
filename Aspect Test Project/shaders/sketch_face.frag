#version 130

in vec4 color;
in vec4 world;


bool funcf(float f) {
	return abs(10 * sin(5 * f) + 1.0 * sin(50 * f) + 0.1 * sin(500 * f) - f) > 0.2;
}

bool func(vec3 v) {
	return funcf(v.x) && funcf(v.y) && funcf(v.z);
}

void main() {
	bool f = func(world.xyz);
	
    if (f) {
    	gl_FragColor = vec4(0, 0, 0, 1);
	} else {
		gl_FragColor = vec4(1);
	}
}
package aspect.example;

import java.nio.FloatBuffer;
import java.util.Random;

import aspect.entity.behavior.Behavior;
import aspect.time.Time;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.util.Matrix4x4;
import aspect.util.Vector3;

import org.lwjgl.BufferUtils;

import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.getAspectRatio;
import static aspect.core.AspectRenderer.projectionMatrix;
import static aspect.core.AspectRenderer.viewMatrix;
import static aspect.render.shader.Shader.Type.*;
import static aspect.resources.Resources.getShader;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_COPY;
import static org.lwjgl.opengl.GL15.GL_READ_WRITE;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.GL_RGBA32F;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL31.GL_TEXTURE_BUFFER;
import static org.lwjgl.opengl.GL31.glTexBuffer;
import static org.lwjgl.opengl.GL42.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT;
import static org.lwjgl.opengl.GL42.glBindImageTexture;
import static org.lwjgl.opengl.GL42.glMemoryBarrier;
import static org.lwjgl.opengl.GL43.glDispatchCompute;

public class ParticleTest extends Behavior {

    private static final int PARTICLE_GROUP_SIZE = 128;
    private static final int PARTICLE_GROUP_COUNT = 8000;
    private static final int PARTICLE_COUNT = (PARTICLE_GROUP_SIZE * PARTICLE_GROUP_COUNT);
    private static final int MAX_ATTRACTORS = 64;
    private static final Random r = new Random();
    
    private float[] attractorMasses = new float[MAX_ATTRACTORS];
    
    private ShaderProgram compute;
    private ShaderProgram render;
    
    private int positionTexture;
    private int velocityTexture;
    
    private int positionBuffer;
    private int velocityBuffer;
    private int attractorBuffer;
    
    private int renderVAO;

    public static void main(String[] args) {

        run(1600, 900, false, 60, new ParticleTest());
    }

    private Vector3 randomVector(float min, float max) {
        Vector3 vec = new Vector3(r.nextFloat() - 0.5f, r.nextFloat() - 0.5f, r.nextFloat() - 0.5f);
        vec = vec.normalize();
        vec = vec.times(r.nextFloat() * (min - max) + min);
        return vec;
    }

    @Override
    public void onAdd() {
        Shader comp = getShader("shaders/particles.comp", COMPUTE);
        compute = new ShaderProgram(comp);
        
        renderVAO = glGenVertexArrays();
        glBindVertexArray(renderVAO);

        positionBuffer = glGenBuffers();
        velocityBuffer = glGenBuffers();

        FloatBuffer positions = BufferUtils.createFloatBuffer(PARTICLE_COUNT * 4);
        FloatBuffer velocities = BufferUtils.createFloatBuffer(PARTICLE_COUNT * 4);
        
        for (int i = 0; i < PARTICLE_COUNT; i++) {
            randomVector(-10.0f, 10.0f).store(positions);
            positions.put(r.nextFloat());
            randomVector(-0.1f, 0.1f).store(velocities);
            velocities.put(r.nextFloat());
        }
        
        positions.flip();
        velocities.flip();

        glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
        glBufferData(GL_ARRAY_BUFFER, positions, GL_DYNAMIC_COPY);
        glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, velocityBuffer);
        glBufferData(GL_ARRAY_BUFFER, velocities, GL_DYNAMIC_COPY);
        
        positionTexture = glGenTextures();
        velocityTexture = glGenTextures();
        
        glBindTexture(GL_TEXTURE_BUFFER, positionTexture);
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, positionBuffer);
        
        glBindTexture(GL_TEXTURE_BUFFER, velocityTexture);
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, velocityBuffer);
        
        compute.setUniform("B", randomVector(0.0f, 0.5f));
        
        Shader vert = getShader("shaders/render.vert", Shader.Type.VERTEX);
        Shader frag = getShader("shaders/render.frag", Shader.Type.FRAGMENT);
        render = new ShaderProgram(vert, frag);
    }
    
    @Override
    public void render() {
        
        
        compute.bind();
        compute.setUniform("Time.delta", Time.deltaTime() * 10);
        
        glBindImageTexture(0, velocityTexture, 0, false, 0, GL_READ_WRITE, GL_RGBA32F);
        glBindImageTexture(1, positionTexture, 0, false, 0, GL_READ_WRITE, GL_RGBA32F);
        
        glDispatchCompute(PARTICLE_GROUP_COUNT, 1, 1);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        
        projectionMatrix.set(Matrix4x4.perspective(45.0f, getAspectRatio(), 0.1f, 1000.0f));
        viewMatrix.loadIdentity();
        viewMatrix.translate(new Vector3(0, 0, -60.0f));
        //viewMatrix = viewMatrix.rotate(Vector3.yAxis(), frameTime() / 1000);
        
        render.bind();
        render.updateMatrices();
        
        glBindVertexArray(renderVAO);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        
        glDrawArrays(GL_POINTS, 0, PARTICLE_COUNT);
    }
}

package aspect.example;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.raycast;
import static aspect.core.AspectRenderer.setLightingEnabled;

import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.entity.behavior.Behavior;

import static aspect.resources.Resources.*;

import aspect.physics.ConvexCollider;
import aspect.physics.Hit;
import aspect.physics.Motion;
import aspect.render.Light;
import aspect.render.PointLight;
import aspect.render.ViewModel;
import aspect.util.Color;
import aspect.util.Debug;
import aspect.util.Vector3;

import org.lwjgl.input.Mouse;

public class RaycastTest extends Behavior {
    private Entity box;
    private Vector3 hit = Vector3.zero();
    private ViewModel hitmarker;

    public static void main(String[] args) {
        run(800, 600, false, 60, new RaycastTest());
    }

    @Override
    public void onAdd() {
        box = new Entity(box(Color.RED, 1.0f, 1.0f, 1.0f));
        box.addBehavior(ConvexCollider.box(1.0f, 1.0f, 1.0f));
        Player.create(Vector3.zero());

        PointLight light = new PointLight(new Vector3(5, 4, 8));
        setLightingEnabled(true);
        Light.ambient = new Color(0.25f);
        hitmarker = box(Color.BLUE, 0.1f, 0.1f, 0.1f);
        mainWorld.add(box);
        box.addBehavior(new Motion(new Vector3(1, 0, 0)));
    }

    @Override
    public void update() {
        if (Mouse.isButtonDown(0)) {
            Hit h = raycast(mainWorld, camera);
            
            if (h != null) {
                hit = h.position;
            }
            hitmarker.transform.setPosition(hit);
        }
    }

    @Override
    public void render() {
        hitmarker.render();
    }
}

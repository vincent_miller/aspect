package aspect.example;

import static aspect.core.AspectLauncher.run;
import static aspect.resources.Resources.getMaterial;
import static aspect.resources.Resources.loadAssets;

import java.io.FileNotFoundException;
import javax.script.ScriptException;

import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.resources.Resources;

public class JSTest extends Behavior {
    public static void main(String[] args) throws ScriptException, FileNotFoundException, NoSuchMethodException {
        run(800, 600, false, 60, new JSTest());
    }
    
    @Override
    public void onAdd() {
        loadAssets("prefabs/Assets.json");
        
        spawn("StoneBlock");
    }
}

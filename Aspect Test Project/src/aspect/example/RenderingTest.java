package aspect.example;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.addSkybox;
import static aspect.core.AspectRenderer.postProcess;
import static aspect.core.AspectRenderer.setBackground;
import static aspect.core.AspectRenderer.setCulling;
import static aspect.core.AspectRenderer.setLightingEnabled;
import static aspect.render.shader.Shader.Type.FRAGMENT;
import static aspect.render.shader.Shader.Type.VERTEX;
import static aspect.resources.Resources.box;
import static aspect.resources.Resources.getImage;
import static aspect.resources.Resources.getShader;
import static aspect.resources.Resources.getTexture;
import static aspect.resources.Resources.rect;

import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.entity.behavior.Behavior;
import aspect.render.AbstractMesh;
import aspect.render.Light;
import aspect.render.PointLight;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.shader.Shader;
import aspect.render.shader.Shader.Type;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.resources.terrain.HeightmapTerrain;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Vector3;

public class RenderingTest extends Behavior {

    public static void main(String[] args) {
        run(1600, 900, false, 60, new RenderingTest());
    }
    
    @Override
    public void onAdd() {
        setLightingEnabled(true);
        Light.ambient = new Color(0.25f);
        PointLight l = new PointLight();
        l.light.directional = true;
        l.transform.setPosition(new Vector3(1.0f, 0.1f, 1.0f));
        
        Material m = new Material(Color.WHITE, new ShaderProgram(getShader("shaders/newWater.vert", VERTEX), Shader.F_PHONG_FRAGMENT));
        m.specular = Color.WHITE;
        m.shininess = 10.0f;
        m.cull = true;
        
        MeshRenderer mesh = Resources.plane(new Color(0.0f, 0.5f, 1.0f, 0.8f), 20, 20, 500);
        mesh.material = m;
        
        Entity ground = new Entity(mesh);
        ground.transform.setForward(Vector3.yAxis().negate());
        ground.transform.setPosition(new Vector3(0, -2, 0));
        
        Player.create(Vector3.zero());
        
        Entity terrain = new Entity(new HeightmapTerrain(getImage("textures/output.png"), new Vector3(20f / 256f, 4f, 20f / 256f), new Material(getTexture("textures/ground.jpg")), 1));
        terrain.transform.setPosition(new Vector3(-10, -5.5f, -10));
        mainWorld.add(terrain);
        mainWorld.add(ground);
        
        //addSkybox(box(new Material(getTexture("textures/ground.jpg"), ShaderProgram.TEXTURE), 1, 1, 1));
    }

}

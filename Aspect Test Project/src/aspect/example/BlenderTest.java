package aspect.example;

import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.setLightingEnabled;
import static aspect.resources.Resources.getModel;

import java.io.File;

import aspect.entity.behavior.Behavior;
import aspect.gui.GUI;
import aspect.render.PointLight;
import aspect.render.ViewModel;
import aspect.time.Time;
import aspect.util.Vector3;

public class BlenderTest extends Behavior {
    private ViewModel dropship;
    
    @Override
    public void onAdd() {
        dropship = getModel("models/wmd_blue.obj", null);
        dropship.transform.moveForwardAbsolute(25.0f);
        
        PointLight l = new PointLight(new Vector3(1, 1, 1));
        l.light.directional = true;
        setLightingEnabled(true);
        
        GUI.setDrawFPS(true);
    }
    
    @Override
    public void update() {
        dropship.transform.yawLeft(Time.deltaTime());
    }
    
    @Override
    public void render() {
        dropship.render();
    }
    
    public static void main(String[] args) {
        run(800, 600, false, 60, new BlenderTest());
    }
}

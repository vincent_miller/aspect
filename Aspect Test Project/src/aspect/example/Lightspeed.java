package aspect.example;

import static aspect.core.AspectLauncher.addKeyListener;
import static aspect.core.AspectLauncher.run;
import static aspect.core.AspectRenderer.addSkybox;
import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.updateProjection;
import static aspect.resources.Resources.getShader;
import static aspect.resources.Resources.getTexture;
import static aspect.resources.Resources.sphere;
import static aspect.resources.Resources.sprite;
import static org.lwjgl.opengl.Display.setVSyncEnabled;

import java.io.File;

import aspect.core.AspectRenderer.ProjectionMode;
import aspect.entity.behavior.Behavior;
import aspect.event.KeyEvent;
import aspect.gui.GUI;
import aspect.gui.Rectangle;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Vector3;

import org.lwjgl.input.Keyboard;


public class Lightspeed extends Behavior {
    private ViewModel cockpit;
    
    public static void main(String[] args) {
        run(60, new Lightspeed());
        setVSyncEnabled(true);
    }
    
    @Override
    public void onAdd() {
        
        ViewModel s = sphere(new Material(getTexture("textures/bg_space2.png"), new ShaderProgram(Shader.V_TEXTURE, getShader("shaders/blueshift.frag", Shader.Type.FRAGMENT))), 1.0f, 50);
        s.transform.setForward(Vector3.zAxis());
        addSkybox(s);
        
        cockpit = sprite(getTexture("textures/bridge.png"), 1, 1);
        updateProjection(ProjectionMode.ORTHOGRAPHIC);
        
        GUI.add(new Rectangle(getTexture("textures/bridge.png")));
        
        Time.speed = 0.0f;
        
        addKeyListener(this);
    }
    
    @Override
    public void keyEvent(KeyEvent evt) {
        if (evt.key == Keyboard.KEY_SPACE) {
            Time.speed = 1.0f;
        }
    }
    
    @Override
    public void update() {
        camera.setScale(new Vector3(1, 1, (float)Math.pow(1.0f / (Time.frameTime() + 1), 2)));
        

        if (Time.frameTime() > 20.0f) {
            Time.speed = -1.0f;
        }
        
        if (Time.frameTime() < 0.0f) {
            Time.speed = 0.0f;
        }
    }
}

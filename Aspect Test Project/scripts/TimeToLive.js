import ExpirationTimer;
import Resources;

var timer;
var time;

function onAdd() {
	timer = new ExpirationTimer(time);
	timer.set();
}

function update() {
	if (timer.expired()) {
		entity.destroy();
	}
}
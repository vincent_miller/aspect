import aspect.input.Input;
import Time;

function update() {
	entity.transform.moveRight(Input.getAxis("X", true) * Time.deltaTime());
	entity.transform.moveForward(Input.getAxis("Y", true) * Time.deltaTime());
}
package spacegame;

import static aspect.core.AspectRenderer.camera;
import static aspect.resources.Resources.box;

import aspect.entity.Entity;
import aspect.entity.behavior.TimeToLive;
import aspect.physics.Motion;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Vector3;

public class Bullet extends Entity {
    private static ViewModel model;

    public Bullet(Vector3 position, Vector3 velocity) {
        Material.defaultShader = ShaderProgram.COLOR;
        
        if (model == null) {
            model = box(Color.YELLOW, 0.01f, 0.01f, 0.1f);
        }
        
        addBehavior(model.copy());
        Material.defaultShader = ShaderProgram.PHONG_VERTEX;
        addBehavior(new Motion(velocity));
        transform.setForward(velocity);
        transform.setPosition(position);
        addBehavior(new TimeToLive(1.0f));
        transform.setForward(velocity);
    }

    @Override
    public void render() {
        if (Vector3.distance2(transform.position(), camera.global().position()) < 50 * 50) {
            super.render();
        }
    }
}

package spacegame;

import static aspect.resources.Resources.ellipse;
import static aspect.resources.Resources.getTexture;

import java.io.File;

import aspect.entity.Entity;
import aspect.render.Billboard;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Vector3;

public class Smoke extends Entity {
    private Vector3 scale;
    private static ViewModel model;

    private ViewModel myModel;

    public Smoke(Vector3 pos, float size) {
        if (model == null) {
            Material m = new Material(getTexture("textures/smoke.png"), ShaderProgram.TEXTURE);
            model = ellipse(m, 1.0f, 1.0f, 20);
        }

        myModel = model.copy();
        scale = new Vector3(size);
        addBehavior(new Billboard(myModel));

        transform.setPosition(pos);

        onUpdate(() -> {
            scale.x -= 2.0f * size * Time.deltaTime();
            scale.y -= 2.0f * size * Time.deltaTime();
            scale.z -= 2.0f * size * Time.deltaTime();
            myModel.transform.setScale(scale);

            if (scale.x < 0) {
                destroy();
            }
        });
    }
}

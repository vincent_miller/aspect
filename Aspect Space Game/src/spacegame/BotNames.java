package spacegame;

import java.util.HashSet;

public class BotNames {
    private static HashSet<String> available = new HashSet<>();
    
    public static void load(String namesfile) {
        available.clear();
        
        String[] lines = namesfile.split("\n");
        for (String line : lines) {
            String name = line.trim();
            available.add(name);
        }
    }
    
    public static String take() {
        float f = 0;
        float inc = 1.0f / available.size();
        float rand = SpaceGame.r.nextFloat();
        
        for (String name : available) {
            if (rand <= f) {
                available.remove(name);
                return name;
            } else {
                f += inc;
            }
        }
        return "Unnamed";
    }
    
    public static void give(String name) {
        available.add(name);
    }
}

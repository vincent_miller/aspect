package spacegame;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectRenderer.camera;
import static aspect.resources.Resources.getModel;

import java.io.File;

import aspect.entity.Entity;
import aspect.entity.behavior.TimeToLive;
import aspect.physics.RigidBody;
import aspect.time.AbilityTimer;
import aspect.time.Time;
import aspect.util.Transform;
import aspect.util.Vector3;

import spacegame.ship.Ship;

public class Missile extends Entity {
    private Ship target;
    private boolean team;
    private Transform cam;
    private AbilityTimer smoke;

    public Missile(Vector3 position, Vector3 velocity, Ship target) {
        transform.setPosition(position);
        addBehavior(new RigidBody());
        rigidBody().collisions = false;
        rigidBody().velocity = velocity;
        addBehavior(getModel("models/missile.obj", null));
        this.target = target;
        cam = new Transform();
        cam.setParent(transform);
        cam.moveForward(-4);
        smoke = new AbilityTimer(0.0f, 0.025f);
    }

    @Override
    public void update() {
        super.update();
        
        if (target.dead()) {
            addBehavior(new TimeToLive(4.0f));
        }

        if (target != null) {
            rigidBody().velocity = rigidBody().velocity.rotateTowards(target.transform.position().minus(transform.position()), Time.deltaTime());
            transform.setForward(rigidBody().velocity);

            if (Vector3.distance2(transform.position(), target.transform.position()) < 9) {
                if (target.damage(100)) {
                    SpaceGame.log.accept("Player killed " + target);
                }
                SpaceGame.showHitMarker.set();
                mainWorld.add(new Explosion(transform.position(), 2.0f));
                destroy();
            }
        }

        if (smoke.use()) {
            mainWorld.add(new Smoke(transform.position().minus(transform.forward().times(1.5f)), 1.0f));
        }
    }
}

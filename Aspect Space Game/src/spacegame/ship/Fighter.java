/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spacegame.ship;

import aspect.entity.Entity;
import aspect.gui.GUI;
import aspect.gui.Rectangle;
import aspect.gui.RectangleBorder;
import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.physics.Hit;
import aspect.physics.RigidBody;
import aspect.render.ViewModel;
import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.core.AspectRenderer.getSelected;
import static aspect.core.AspectRenderer.projectPoint;
import static aspect.core.AspectRenderer.projectPointNorm;
import static aspect.core.AspectRenderer.raycast;
import static aspect.core.AspectRenderer.raycastSimple;
import aspect.time.AbilityTimer;
import aspect.time.Time;
import aspect.util.Color;
import aspect.util.Random;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;
import spacegame.BotNames;
import spacegame.Bullet;
import spacegame.Missile;
import spacegame.SpaceGame;
import spacegame.control.AIFighterControl;
import spacegame.control.PlayerFighterControl;

/**
 *
 * @author MillerV
 */
public class Fighter extends Ship {

    public final Transform thirdPersonView;
    public final Transform firstPersonView;
    public final Transform shoot;

    private PlayerFighterControl human;
    private AIFighterControl ai;

    private ViewModel normal;
    private ViewModel cockpit;

    public static boolean firstPerson = true;
    public boolean firing = false;
    private AbilityTimer fire;
    private AbilityTimer missile;
    public Fighter target;
    public static int numRedFighters = 0;
    public static int numBlueFighters = 0;
    private Rectangle hud;

    public Fighter(boolean team) {
        super(team, 150, 2.0f);

        fire = new AbilityTimer(0, 0.15f);
        missile = new AbilityTimer(0, 1.0f);

        normal = loadModel("fighter", 0.25f);
        addBehavior(normal);
        cockpit = loadModel("fighter_cockpit", 0.25f);
        addBehavior(cockpit);

        addBehavior(ConvexCollider.box(1.5f, 1.5f, 0.75f));

        thirdPersonView = new Transform();
        thirdPersonView.setPosition(new Vector3(0.0f, 1.0f, 4.0f));
        thirdPersonView.setParent(transform);

        firstPersonView = new Transform();
        firstPersonView.setPosition(new Vector3(0.0f, 0.0f, 0.65f));
        firstPersonView.setParent(transform);

        shoot = new Transform();
        shoot.setPosition(new Vector3(0.0f, 0.0f, -2.0f));
        shoot.setParent(transform);

        addBehavior(new RigidBody());
        rigidBody().collisions = false;

        ai = new AIFighterControl(this, 10, 10);
        human = new PlayerFighterControl(this);

        if (team == RED) {
            numRedFighters++;
        } else {
            numBlueFighters++;
        }

        setName(BotNames.take());

        hud = new RectangleBorder(new Vector2(20, 20));
        hud.setColor(team ? Color.BLUE : Color.RED);
        GUI.add(hud);
    }

    @Override
    public String toString() {
        if (useAI) {
            return "(BOT) " + getName();
        } else {
            return "Player";
        }
    }

    @Override
    public void onCollision(Collision c) {
        if (c.other instanceof Ship) {
            die();
            ((Ship) c.other).damage(100);
            destroy();
            SpaceGame.log.accept(this + " crashed");
        }
    }

    public void fireMissile() {
        if (missile.use()) {
            // mainWorld.add(new Missile(transform.position(),
            // transform.forward().times(30), target, team(), 20.0f));
            Ship[] t = new Ship[1];
            float[] dist2 = { Float.POSITIVE_INFINITY };

            mainWorld.forEach((entity) -> {
                if (entity instanceof Fighter) {
                    Fighter f = (Fighter) entity;
                    if (f.team() != team() && !f.dead()) {
                        Vector2 pos = projectPoint(entity.transform.position());
                        if (pos != null) {
                            float d = Vector2.distance2(pos, new Vector2(getCanvasWidth() / 2, getCanvasHeight() / 2));
                            if (d < dist2[0]) {
                                dist2[0] = d;
                                t[0] = f;
                            }
                        }
                    }
                }
            });

            if (t[0] != null) {
                mainWorld.add(new Missile(transform.position(), transform.forward().times(30), t[0]));
            }
        }
    }

    @Override
    public void update() {
        super.update();

        if (useAI) {
            Vector2 p = projectPoint(transform.position());
            if (p != null) {
                hud.setPosition(p.minus(new Vector2(10)));
                hud.setEnabled(true);
                normal.enabled = true;
            } else {
                hud.setEnabled(false);
                normal.enabled = false;
            }
        } else {
            hud.setEnabled(false);
        }

        if (!dead()) {
            if (useAI) {
                ai.update();
            } else {
                human.update();
            }

            if (fire.use()) {
                if (firing) {
                    Vector3 pos1 = transform.modelMatrix().transformPoint(new Vector3(0.7f, 0.0f, -1.0f));
                    Vector3 pos2 = transform.modelMatrix().transformPoint(new Vector3(-0.7f, 0.0f, -1.0f));
                    Vector3 vel = transform.forward().times(200.0f).plus(rigidBody().velocity);

                    mainWorld.add(new Bullet(pos1, vel));
                    mainWorld.add(new Bullet(pos2, vel));

                    Hit hit = raycast(mainWorld, shoot);

                    if (hit != null && hit.entity instanceof Ship && (!useAI || Random.Chance(0.2f))) {
                        Ship s = (Ship) hit.entity;
                        if (s.damage(20)) {
                            SpaceGame.log.accept(this + " killed " + s);
                        }
                        if (s instanceof Fighter) {
                            ((Fighter) s).target = this;
                        }
                        
                        if (!useAI) {
                            SpaceGame.showHitMarker.set();
                        }
                    }
                }

                if (useAI && (target == null || target.dead())) {
                    target = null;
                }
            }
        }
    }

    @Override
    public void die() {
        super.die();
        if (team() == RED) {
            numRedFighters--;
        } else {
            numBlueFighters--;
        }
        if (!getName().equals("Unnamed")) {
            BotNames.give(getName());
        }
    }

    @Override
    public void onRemove() {
        super.onRemove();
        GUI.remove(hud);
    }

    @Override
    public void render() {
        if (!useAI && firstPerson) {
            cockpit.enabled = true;
            normal.enabled = false;
            camera = firstPersonView;
        } else {
            if (!useAI) {
                camera = thirdPersonView;
            }
            cockpit.enabled = false;
            normal.enabled = true;
        }
        super.render();
    }
}

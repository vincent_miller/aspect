package spacegame.ship;

import java.io.File;

import aspect.physics.ConvexCollider;
import aspect.util.Vector3;

import spacegame.control.AIDestroyerControl;
import spacegame.control.PlayerCruiserControl;

public class Destroyer extends Cruiser {

    public Destroyer(boolean team) {
        super(25.0f, 100000, team, "destroyer", 1);
        
        turretModelBase = loadModel("cannon_base", 1);
        turretModelBarrels = loadModel("cannon_barrels", 1);
        engineModel = loadModel("engine", 1);
        
        addCannonTurret(new Vector3(-3.0f, 1.45f, 5.0f), Vector3.yAxis());
        addCannonTurret(new Vector3(3.0f, 1.45f, 5.0f), Vector3.yAxis());
        addCannonTurret(new Vector3(-6.0f, 1.45f, 5.0f), Vector3.yAxis());
        addCannonTurret(new Vector3(6.0f, 1.45f, 5.0f), Vector3.yAxis());

        addRotaryEngine(new Vector3(8.375f, 0.78125f, 4.5f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(-8.375f, 0.78125f, 4.5f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);
        addRotaryEngine(new Vector3(6.5f, -1.5f, -5.25f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(-6.5f, -1.5f, -5.25f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);

        addBehavior(ConvexCollider.model("models/destroyer.obj", new Vector3(1)));
        
        ai = new AIDestroyerControl(this);
    }

}


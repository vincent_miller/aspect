package spacegame.ship;

import static aspect.core.AspectRenderer.*;
import static aspect.core.AspectLauncher.*;
import static aspect.resources.Resources.*;

import java.io.File;

import aspect.entity.Entity;
import aspect.physics.Hit;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Color;
import aspect.util.Random;
import aspect.util.Transform;
import aspect.util.Vector3;

import spacegame.SpaceGame;

public class BeamTurret extends Turret {
    private final ViewModel beam;
    private float scale;
    private ShaderProgram beams;

    public BeamTurret(Cruiser ship, ViewModel base, ViewModel barrels, Vector3 position, Vector3 axis) {
        super(ship, base, barrels, 0.1f, position, axis);

        beams = new ShaderProgram(Shader.V_TEXTURE, getShader("shaders/beam.frag", Shader.Type.FRAGMENT));
        //beam = box(m, 1.0f, 0.05f, 0.05f);
        //beam.transform.setForward(Vector3.xAxis());
        //beam.transform.setParent(transform);
        
        beam = rect(new Material(beams), 0.03f, 1.0f);
        beam.transform.setUp(Vector3.zAxis().negate());
        beam.transform.setParent(barrels.transform);

        view.move(new Vector3(0.0f, 0.0f, -0.3f));
    }

    @Override
    protected void fire() {
        Entity hit = null;

        Hit h = raycast(mainWorld, view);
        if (h != null) {
            hit = h.entity;
            scale = Vector3.distance(transform.global().position(), h.position);
        } else {
            scale = 1000;
        }

        if (hit instanceof Ship) {
            if (((Ship) hit).damage(20 * Time.deltaTime())) {
                SpaceGame.log.accept("Frigate killed " + hit);
            }

            if (controlled()) {
                SpaceGame.showHitMarker.set();
            }
        }
    }

    private void renderBeam() {
        Vector3 viewVector = barrels.transform.globalViewMatrix().transformVector(beam.transform.global().position().minus(camera.global().position()).normalize());
        Vector3 newForward = viewVector.project2D(beam.transform.right(), beam.transform.forward()).normalize();
        beam.transform.setForward(newForward);
        //global.setRotation(Vector3.cross(newForward, global.forward()), global.forward(), newForward);

        //beam.transform.set(global);
        beam.render();
    }

    @Override
    public void render() {
        super.render();
        if (firing) {
            float d = (float) Math.pow(Vector3.distance(transform.global().position(), camera.global().position()), 0.1);
            float f = Random.Float(0, d);
            beams.setUniform("color", new Color(1.0f, f, 0));

            beam.transform.setScale(new Vector3(1, scale, 1));

            float pos = -(scale / 2) - 0.45f;

            beam.transform.setPosition(new Vector3(0.11f, 0.0f, pos));
            renderBeam();
            beam.transform.setPosition(new Vector3(-0.11f, 0.0f, pos));
            renderBeam();
        }
    }
}

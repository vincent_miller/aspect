/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spacegame.ship;

import aspect.physics.ConvexCollider;
import aspect.util.Vector3;

import java.io.File;

import spacegame.control.AICruiserControl;
import spacegame.control.AIFrigateControl;
import spacegame.control.SpawnFighters;

/**
 *
 * @author MillerV
 */
public class Carrier extends Cruiser {
    public Carrier(boolean team) {
        super(10.0f, 20000, team, "carrier", 1);
        
        engineModel = loadModel("engine", 1);

        addRotaryEngine(new Vector3(7.5f, 0.0f, -5.0f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(5.375f, 0.78125f, 5.0f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(-7.5f, 0.0f, -5.0f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);
        addRotaryEngine(new Vector3(-5.375f, 0.78125f, 5.0f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);
        
        addBehavior(ConvexCollider.model("models/carrier.obj", Vector3.one()));
        
        ai = new AICruiserControl(this);
        addBehavior(new SpawnFighters(1.0f, 15));
    }
}

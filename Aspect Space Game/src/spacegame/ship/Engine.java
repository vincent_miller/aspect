package spacegame.ship;

import static aspect.core.AspectRenderer.*;
import static aspect.resources.Resources.*;

import java.io.File;

import aspect.entity.Entity;
import aspect.render.Material;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Transform;
import aspect.util.Vector3;

public class Engine extends Entity {
    private final Exhaust exhaust;
    protected final Cruiser parent;
    protected float thrust = 0.0f;
    protected final float strength;

    public Engine(Cruiser parent, ViewModel viewModel, Vector3 position, Vector3 forward, Vector3 up, float strength, Vector3 offset) {
        super(viewModel.copy());

        this.parent = parent;
        this.exhaust = new Exhaust(offset);
        this.strength = strength;

        transform.setPosition(position);
        transform.setRotation(Vector3.cross(forward, up), up, forward);
        transform.setParent(parent.transform);
    }

    public void updateTorque(Vector3 desiredTorque) {
        Vector3 perfectDirection = Vector3.cross(desiredTorque, transform.position());
        updateThrust(perfectDirection);
    }

    public void updateThrust(Vector3 desiredDirection) {
        if (desiredDirection.mag() > 10) {
            desiredDirection = desiredDirection.normalize().times(10);
        }
        thrust = Math.max(Vector3.dot(transform.forward(), desiredDirection), 0);
        parent.rigidBody().impel(transform.global().forward().times(thrust * Time.deltaTime()), transform.global().position().minus(transform.parent().global().position()));
    }

    public Vector3 getMaxAngularThrust(Vector3 axis) {
        Vector3 direction = Vector3.cross(axis, transform.position());
        Vector3 thrust = getMaxThrust(direction);
        return Vector3.cross(thrust, transform.position());
    }

    public Vector3 getMaxThrust(Vector3 direction) {
        float thrust = Math.max(Vector3.dot(transform.forward(), direction), 0) * 10;
        return transform.forward().times(thrust);
    }

    public void cancelThrust() {
        thrust = 0.0f;
    }

    @Override
    public void render() {
        super.render();
        exhaust.render();
    }

    private class Exhaust {
        private final ViewModel model;
        private final Transform t;
        private final Vector3 offset;

        private Exhaust(Vector3 offset) {
            Shader vert = Shader.loadPrebuilt("texture.vert", Shader.Type.VERTEX);
            Shader frag = getShader("shaders/thrust.frag", Shader.Type.FRAGMENT);
            this.offset = offset;
            Material m = new Material(new ShaderProgram(vert, frag));
            model = rect(m, 2, 8);
            t = new Transform();
            t.setRotation(Vector3.xAxis().negate(), Vector3.yAxis(), Vector3.zAxis().negate());
            t.setParent(Engine.this.transform);
        }

        public void render() {

            if (thrust != 0) {
                float f = strength * thrust / 10.0f;
                
                Vector3 o = offset.copy();
                o.z = o.z + 4 * f;
                t.setPosition(o);

                t.setScale(new Vector3(strength, f, strength));
                Transform global = t.global();
                Vector3 viewVector = camera.global().position().minus(global.position()).normalize();
                Vector3 newForward = viewVector.project2D(global.up(), global.right()).normalize();

                global.setRotation(Vector3.cross(newForward, global.forward()), global.forward(), newForward);

                model.transform.set(global);
                model.render();
            }

        }
    }
}

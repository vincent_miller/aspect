package spacegame.ship;

import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.time.Time;
import aspect.util.Vector3;

public class BidirectionalEngine extends Engine {

    public BidirectionalEngine(Cruiser parent, MeshRenderer model, Vector3 position, Vector3 forward, Vector3 up, float strength) {
        super(parent, model, position, forward, up, strength, Vector3.zero());
    }

    @Override
    public void updateThrust(Vector3 desiredDirection) {
        if (desiredDirection.mag() > 10) {
            desiredDirection = desiredDirection.normalize().times(10);
        }
        thrust = Vector3.dot(transform.forward(), desiredDirection);
        parent.rigidBody().impel(transform.global().forward().times(thrust * Time.deltaTime()), transform.global().position().minus(transform.parent().global().position()));
    }

    @Override
    public Vector3 getMaxThrust(Vector3 direction) {
        float thrust = Vector3.dot(transform.forward(), direction) * 10;
        return transform.forward().times(thrust);
    }

}

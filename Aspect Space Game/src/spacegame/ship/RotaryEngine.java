package spacegame.ship;

import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.time.Time;
import aspect.util.Vector3;

public class RotaryEngine extends Engine {

    public RotaryEngine(Cruiser parent, MeshRenderer model, Vector3 position, Vector3 axis, Vector3 scale, float strength) {
        super(parent, model, position, Vector3.zAxis().negate(), Vector3.yAxis(), strength, new Vector3(1.0f, 0.0f, 2.0f).times(strength));
        
        transform.setRight(axis);
        transform.setScale(scale);
    }

    @Override
    public void updateThrust(Vector3 desiredDirection) {
        Vector3 achieveableDirection = desiredDirection.project2D(transform.up(), transform.forward());
        if (transform.rotate(transform.forward(), achieveableDirection, 10.0f * Time.deltaTime())) {
            super.updateThrust(desiredDirection);
        } else {
            cancelThrust();
        }
    }
    
    @Override
    public Vector3 getMaxThrust(Vector3 direction) {
        Vector3 achieveableDirection = direction.project2D(transform.up(), transform.forward()).normalize();
        float thrust = Math.max(Vector3.dot(direction, achieveableDirection), 0) * 10;
        return achieveableDirection.times(thrust);
    }
}

package spacegame.ship;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.resources.Resources.getModel;
import java.io.File;
import java.util.function.Predicate;

import aspect.entity.Entity;
import aspect.entity.behavior.TimeToLive;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.resources.ModelImportSettings;
import aspect.time.AbilityTimer;
import aspect.util.Color;
import aspect.util.Random;
import aspect.util.Vector3;

import spacegame.Explosion;
import spacegame.SpaceGame;

public abstract class Ship extends Entity {
    private float health;
    public final float maxHealth;
    private boolean dead = false;
    private float explosionRadius;
    private AbilityTimer explode;
    private static ModelImportSettings importRed = new ModelImportSettings();
    private static ModelImportSettings importBlue = new ModelImportSettings();
    
    static {
        importRed.addOverride("Highlight", new Color(0.33f, 0.0f, 0.0f));
        importBlue.addOverride("Highlight", new Color(0.0f, 0.0f, 0.33f));
    }

    protected boolean useAI = true;

    private boolean team;
    public static final boolean RED = false;
    public static final boolean BLUE = true;
    
    public static final float VIEW_RANGE = 50;
    public static final float VIEW_RANGE2 = VIEW_RANGE * VIEW_RANGE;

    public Ship(boolean team, float health, float explosionRadius) {
        this.health = health;
        this.maxHealth = health;
        this.explosionRadius = explosionRadius;
        this.explode = new AbilityTimer(0, 0.25f);
        this.team = team;
    }

    public boolean human() {
        return !useAI;
    }

    public Predicate<Ship> enemyFilter() {
        return (ship) -> aliveEnemy(ship) && Vector3.distance2(transform.position(), ship.transform.position()) < VIEW_RANGE2;
    }

    public boolean aliveEnemy(Ship ship) {
        return !(ship.team() == team || ship.dead);
    }

    public Predicate<Ship> allyFilter() {
        return (ship) -> aliveAlly(ship) && Vector3.distance2(transform.position(), ship.transform.position()) < VIEW_RANGE2;
    }

    public boolean aliveAlly(Ship ship) {
        return !(ship.team() != team || ship.dead);
    }

    protected MeshRenderer loadModel(String name, float scale) {

        ModelImportSettings settings = team ? importBlue : importRed;
        settings.transform.setScale(new Vector3(scale));
        return (MeshRenderer) getModel(name, "models/" + name + ".obj", settings);
    }

    protected ViewModel loadModelNeutral(String name, float scale) {
        return (MeshRenderer) getModel(name, "models/" + name + ".obj", new ModelImportSettings(scale));
    }

    public boolean damage(float damage) {
        health -= damage;
        if (health < 0 && !dead) {
            die();
            return true;
        } else {
            return false;
        }
    }

    public float health() {
        return health;
    }

    public boolean team() {
        return team;
    }

    public boolean dead() {
        return dead;
    }

    public void setHuman(boolean human) {
        useAI = !human;
    }

    @Override
    public void update() {
        super.update();

        if (!useAI) {
            SpaceGame.healthbar.setHealth(health / maxHealth);
        }

        if (dead && explode.use()) {
            mainWorld.add(new Explosion(Random.Vector3(collider().bounds()), explosionRadius));
        }
    }

    public void die() {
        if (!useAI) {
            SpaceGame.healthbar.setHealth(0);
        }

        dead = true;
        addBehavior(new TimeToLive(5));
        mainWorld.add(new Explosion(transform.position(), explosionRadius));
    }
}

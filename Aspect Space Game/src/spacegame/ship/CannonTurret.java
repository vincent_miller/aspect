package spacegame.ship;

import static aspect.core.AspectLauncher.mainWorld;

import aspect.render.ViewModel;
import aspect.time.AbilityTimer;
import aspect.util.Transform;
import aspect.util.Vector3;

import spacegame.CannonShell;

public class CannonTurret extends Turret {
    private AbilityTimer fire;
    private final Transform cannon;

    public CannonTurret(Cruiser ship, ViewModel base, ViewModel barrels, Vector3 position, Vector3 axis) {
        super(ship, base, barrels, 0.3f, position, axis);

        view.move(new Vector3(0.0f, 0.0f, -0.5f));

        cannon = new Transform();
        cannon.setPosition(new Vector3(0.275f, 0.0f, -1.07719f));
        cannon.setParent(this.barrels.transform);

        fire = new AbilityTimer(0.0f, 0.5f);
    }

    @Override
    protected void fire() {
        if (fire.use()) {
            mainWorld.add(new CannonShell(controlled(), cannon.global().position(), cannon.global().forward().times(100.0f)));
            Vector3 p = cannon.position();
            p.x *= -1;
            cannon.setPosition(p);
        }
    }
}

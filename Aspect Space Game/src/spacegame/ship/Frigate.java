/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spacegame.ship;

import aspect.physics.ConvexCollider;
import aspect.util.Vector3;

import java.io.File;

import spacegame.control.AIFrigateControl;
import spacegame.control.SpawnFighters;

/**
 *
 * @author MillerV
 */
public class Frigate extends Cruiser {
    public Frigate(boolean team) {
        super(10.0f, 40000, team, "frigate", 1);
        
        turretModelBase = loadModel("turret_base", 1);
        turretModelBarrels = loadModel("turret_barrels", 1);
        engineModel = loadModel("engine", 1);

        addRotaryEngine(new Vector3(7.5f, 0.0f, -5.0f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(5.375f, 0.78125f, 5.0f), Vector3.xAxis(), Vector3.one(), 1.0f);
        addRotaryEngine(new Vector3(-7.5f, 0.0f, -5.0f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);
        addRotaryEngine(new Vector3(-5.375f, 0.78125f, 5.0f), Vector3.xAxis(), new Vector3(-1.0f, 1.0f, 1.0f), 1.0f);

        addBeamTurret(new Vector3(1.25f, 1.45f, -2.5f), Vector3.yAxis());
        addBeamTurret(new Vector3(-1.25f, 1.45f, -2.5f), Vector3.yAxis());
        addBeamTurret(new Vector3(1.25f, 1.45f, 6.5f), Vector3.yAxis());
        addBeamTurret(new Vector3(-1.25f, 1.45f, 6.5f), Vector3.yAxis());
        addBeamTurret(new Vector3(3.0f, 0.66875f, -6.0f), Vector3.yAxis());
        addBeamTurret(new Vector3(-3.0f, 0.66875f, -6.0f), Vector3.yAxis());
        addBeamTurret(new Vector3(3.0f, -0.66875f, -6.0f), Vector3.yAxis().negate());
        addBeamTurret(new Vector3(-3.0f, -0.66875f, -6.0f), Vector3.yAxis().negate());
        
        addBehavior(ConvexCollider.model("models/frigate.obj", new Vector3(1)));
        
        ai = new AIFrigateControl(this);
    }
}

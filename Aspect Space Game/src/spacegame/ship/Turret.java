package spacegame.ship;

import aspect.entity.Entity;
import aspect.render.ViewModel;
import aspect.util.Angles;
import aspect.util.Transform;
import aspect.util.Vector3;

public class Turret extends Entity {
    public final Transform view;
    public boolean firing = false;
    protected final Cruiser ship;
    
    protected Entity barrels;

    public Turret(Cruiser ship, ViewModel base, ViewModel barrels, float offy, Vector3 position, Vector3 axis) {
        super(base.copy());
        this.barrels = new Entity(barrels.copy());
        this.barrels.transform.moveUp(offy);
        addChild(this.barrels);
        
        this.view = new Transform();
        this.ship = ship;

        Transform parent = new Transform();
        parent.setForward(ship.transform.forward().negate());
        parent.setUp(axis);
        parent.setPosition(position);
        parent.setParent(ship.transform);

        transform.setParent(parent);
        view.setParent(this.barrels.transform);
    }
    
    public boolean controlled() {
        return ship.human() && ship.human.controlIndex > 0 && ship.turrets.get(ship.human.controlIndex - 1) == this;
    }
    
    public void aimAt(Angles angles) {
        transform.setEulerAngles(new Angles(0, angles.yaw, 0));
        barrels.transform.setEulerAngles(new Angles(angles.pitch, 0, 0));
    }
    
    public Angles getAim() {
        Angles base = transform.eulerAngles();
        Angles barrel = barrels.transform.eulerAngles();
        return new Angles(barrel.pitch, base.yaw, 0);
    }

    @Override
    public void update() {
        super.update();
        if (firing) {
            fire();
        }
    }
    
    protected void fire() {
    }
}
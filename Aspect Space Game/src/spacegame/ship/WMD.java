package spacegame.ship;

import java.io.File;

import spacegame.control.AICruiserControl;
import spacegame.control.AIDestroyerControl;
import spacegame.control.PlayerCruiserControl;
import aspect.physics.ConvexCollider;
import aspect.util.Vector3;

public class WMD extends Cruiser {

    public WMD(boolean team) {
        super(4.0f, 100000, team, "wmd", 4.0f);

        engineModel = null;

        addEngine(new Vector3(0, 0, 28), Vector3.zAxis().negate(), Vector3.yAxis(), 3.0f);

        addBidirectionalEngine(new Vector3(0, 5, -22), Vector3.xAxis(), Vector3.zAxis(), 1.0f);
        addBidirectionalEngine(new Vector3(0, -5, -22), Vector3.xAxis(), Vector3.zAxis(), 1.0f);
        addBidirectionalEngine(new Vector3(5, 0, -22), Vector3.yAxis(), Vector3.zAxis(), 1.0f);
        addBidirectionalEngine(new Vector3(-5, 0, -22), Vector3.yAxis(), Vector3.zAxis(), 1.0f);
        
        /*
        addEngine(new Vector3(0, 5, -22), Vector3.yAxis().negate(), Vector3.zAxis(), 1.0f);
        addEngine(new Vector3(0, -5, -22), Vector3.yAxis(), Vector3.zAxis(), 1.0f);
        addEngine(new Vector3(5, 0, -22), Vector3.xAxis().negate(), Vector3.zAxis(), 1.0f);
        addEngine(new Vector3(-5, 0, -22), Vector3.xAxis(), Vector3.zAxis(), 1.0f);*/

        human = new PlayerCruiserControl(this, 50.0f);

        addBehavior(ConvexCollider.box(12, 12, 64));

        ai = new AICruiserControl(this);
    }

}

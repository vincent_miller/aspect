package spacegame.ship;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import static aspect.resources.Resources.*;
import static aspect.core.AspectLauncher.*;

import aspect.entity.Entity;
import aspect.physics.Collision;
import aspect.physics.RigidBody;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.time.Time;
import aspect.util.Transform;
import aspect.util.Vector3;

import spacegame.Explosion;
import spacegame.control.AICruiserControl;
import spacegame.control.CruiserControl;
import spacegame.control.EngineControl;
import spacegame.control.PlayerCruiserControl;
import static org.lwjgl.input.Keyboard.*;

public class Cruiser extends Ship {
    public final ArrayList<Engine> engines = new ArrayList<>();
    public final ArrayList<Turret> turrets = new ArrayList<>();

    public final EngineControl engineControl = new EngineControl(this);

    public MeshRenderer turretModelBase;
    public MeshRenderer turretModelBarrels;
    
    
    public MeshRenderer engineModel;

    public PlayerCruiserControl human = new PlayerCruiserControl(this, 20.0f);
    public AICruiserControl ai = new AICruiserControl(this);

    public Cruiser(float mass, float health, boolean team, String modelname, float scale) {
        super(team, health, 10);
        addBehavior(new RigidBody());
        addBehavior(loadModel(modelname, scale));
        rigidBody().mass = mass;
        rigidBody().setUniformMOI(20 * mass * mass);
        //rigidBody().collisions = false;
    }

    @Override
    public void onCollision(Collision c) {
        if (c.other instanceof Ship) {
            ((Ship) c.other).damage(100);
        }
        
        if (c.other instanceof Cruiser) {
            super.onCollision(c);
        }
    }

    public void addBeamTurret(Vector3 position, Vector3 up) {
        BeamTurret turret = new BeamTurret(this, turretModelBase, turretModelBarrels, position, up);
        addChild(turret);
        turrets.add(turret);
    }

    public void addCannonTurret(Vector3 position, Vector3 up) {
        CannonTurret turret = new CannonTurret(this, turretModelBase, turretModelBarrels, position, up);
        addChild(turret);
        turrets.add(turret);
    }

    public void addEngine(Vector3 position, Vector3 forward, Vector3 up, float strength) {
        Engine engine = new Engine(this, engineModel, position, forward, up, strength, Vector3.zero());
        addChild(engine);
        engines.add(engine);
    }
    
    public void addBidirectionalEngine(Vector3 position, Vector3 forward, Vector3 up, float strength) {
        Engine engine = new BidirectionalEngine(this, engineModel, position, forward, up, strength);
        addChild(engine);
        engines.add(engine);
    }

    public void addRotaryEngine(Vector3 position, Vector3 axis, Vector3 scale, float strength) {
        RotaryEngine engine = new RotaryEngine(this, engineModel, position, axis, scale, strength);
        addChild(engine);
        engines.add(engine);
    }

    @Override
    public void update() {
        super.update();

        if (!dead()) {
            ai.update();
            
            if (!useAI) {
                human.update();
            }
        } else {
            engineControl.cancelThrust();
        }

        if (rigidBody().velocity.mag2() > 100) {
            rigidBody().velocity = rigidBody().velocity.normalize().times(10);
        }

        if (rigidBody().angularVelocity.mag2() > 5 * 5) {
            rigidBody().angularVelocity = rigidBody().angularVelocity.normalize().times(5);
        }
    }
}

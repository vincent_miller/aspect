/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spacegame;

import java.io.File;

import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.render.Billboard;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Vector3;

import static aspect.resources.Resources.*;

import aspect.util.Color;

/**
 *
 * @author MillerV
 */
public class Explosion extends Entity {

    private Vector3 scale = Vector3.zero();
    private static ViewModel model;
    
    private ViewModel myModel;

    public Explosion(Vector3 pos, float size) {
        if (model == null) {
            Shader frag = getShader("shaders/explosion.frag", Shader.Type.FRAGMENT);
            Material m = new Material(new ShaderProgram(Shader.V_TEXTURE, frag));
            model = ellipse(m, 1.0f, 1.0f, 20);
        }

        myModel = model.copy();
        addBehavior(new Billboard(myModel));

        transform.setPosition(pos);

        addBehavior(new Behavior() {
            @Override
            public void update() {
                scale.x += 4.0f * size * Time.deltaTime();
                scale.y += 4.0f * size * Time.deltaTime();
                scale.z += 4.0f * size * Time.deltaTime();
                myModel.transform.setScale(scale);

                if (scale.x > size) {
                    Explosion.this.destroy();
                }
            }
        });
    }
}

package spacegame;

import static aspect.core.AspectLauncher.*;

import aspect.entity.Entity;
import aspect.entity.behavior.TimeToLive;
import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.physics.Motion;
import aspect.util.Vector3;

import spacegame.ship.Cruiser;
import spacegame.ship.Ship;

public class Projectile extends Entity {
    private final boolean player;
    private boolean explosive;
    private float damage;

    public Projectile(boolean player, Vector3 position, Vector3 velocity, float damage, boolean explosive) {
        this.player = player;
        this.explosive = explosive;
        this.damage = damage;
        addBehavior(new Motion(velocity));
        transform.setForward(velocity);
        transform.setPosition(position);
        addBehavior(ConvexCollider.point(Vector3.zero()));
    }

    @Override
    public void onCollision(Collision collision) {
        hit(collision.other);
    }

    private void hit(Entity entity) {
        if (entity instanceof Ship) {
            if (((Ship) entity).damage(damage)) {
                SpaceGame.log.accept("Destroyer killed " + entity);
            }
            
            if (player) {
                SpaceGame.showHitMarker.set();
            }
        }
        if (explosive) {
            mainWorld.add(new Explosion(transform.position(), 10.0f));
        }
        destroy();
    }
}

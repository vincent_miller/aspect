package spacegame;

import aspect.entity.Entity;
import aspect.entity.behavior.TimeToLive;
import aspect.render.Material;
import aspect.render.MultiModel;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Vector3;

import spacegame.ship.Ship;
import static aspect.resources.Resources.*;

public class CannonShell extends Projectile {
    private static ViewModel model;
    
    public CannonShell(boolean player, Vector3 position, Vector3 velocity) {
        super(player, position, velocity, 100, true);
        
        if (model == null) {
            Material m = new Material(Color.YELLOW);
            ViewModel model1 = pipe(m, 1.0f, 1.0f, 0.05f, 0.2f, 8);
            ViewModel model2 = ellipse(m, 0.1f, 0.1f, 8);
            model1.transform.setUp(Vector3.zAxis());
            model2.transform.setPosition(new Vector3(0, 0, 0.1f));
            model = new MultiModel(model1, model2);
        }
        
        addBehavior(model.copy());
        addBehavior(new TimeToLive(10.0f));
    }
}

package spacegame.control;

import spacegame.ship.Cruiser;

public interface AIBehavior {
    public void start(Cruiser ship);
    public void update(Cruiser ship);
    public boolean done(Cruiser ship);
}

package spacegame.control;

import aspect.core.Console;
import aspect.time.AbilityTimer;
import aspect.util.Trig;
import aspect.util.Vector3;

import spacegame.ship.Cruiser;

public class AIRotateTo implements AIBehavior {
    protected Vector3 destAngle;
    protected float power;
    protected Vector3 axis;
    protected boolean mode = false;
    
    public AIRotateTo(Vector3 angle, float power) {
        this.destAngle = angle.normalize();
        this.power = power;
        this.axis = Vector3.zero();
    }

    @Override
    public void start(Cruiser ship) {
        mode = false;
    }

    @Override
    public void update(Cruiser ship) {
        Vector3 forward;
        if (Vector3.dot(ship.transform.forward(), destAngle) > 0) {
            forward = ship.transform.forward();
        } else {
            forward = ship.transform.forward().negate();
        }
        
        Vector3 cross = ship.transform.globalViewMatrix().transformVector(Vector3.cross(forward, destAngle));
        
        if (!mode && Math.abs(cross.y) < 0.1f && ship.rigidBody().angularVelocity.mag2() < 0.01 * 0.01) {
            mode = true;
        }
        
        if (mode) {
            axis = Vector3.xAxis();
        } else {
            axis = Vector3.yAxis();
        }

        float angleFromDest = Vector3.dot(cross, axis);
        float vel = Vector3.dot(ship.transform.globalViewMatrix().transformVector(ship.rigidBody().angularVelocity), axis);
        Vector3 axisn = axis;
        if (vel < 0) {
            axisn = axisn.negate();
        }
        float acc = Vector3.dot(ship.engineControl.maxAngularAcceleration(axisn), axis);

        float t0 = -vel / acc;
        t0 += Trig.HALF_CIRCLE / 10.0f;
        float angleAtT0 = angleFromDest - vel * t0 + acc * t0 * t0 * 0.5f;

        if ((vel > 0) == (angleAtT0 > 0)) {
            ship.engineControl.updateTorque(axisn.times(10));
        } else {
            ship.engineControl.updateTorque(axisn.times(-10));
        }
    }

    @Override
    public boolean done(Cruiser ship) {
        if (!mode) {
            return false;
        }
        
        Vector3 forward;
        if (Vector3.dot(ship.transform.forward(), destAngle) > 0) {
            forward = ship.transform.forward();
        } else {
            forward = ship.transform.forward().negate();
        }
        
        Vector3 cross = ship.transform.globalViewMatrix().transformVector(Vector3.cross(forward, destAngle));
        
        return Math.abs(cross.x) < 0.1f && ship.rigidBody().angularVelocity.mag2() < 0.01 * 0.01;
    }

}

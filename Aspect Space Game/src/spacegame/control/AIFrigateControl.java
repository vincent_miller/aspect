package spacegame.control;

import aspect.entity.Entity;
import aspect.util.Vector3;

import spacegame.ship.Cruiser;
import spacegame.ship.Fighter;
import spacegame.ship.Ship;

public class AIFrigateControl extends AICruiserControl {

    public AIFrigateControl(Cruiser ship) {
        super(ship);
    }

    @Override
    protected Ship getTarget() {
        return ship.container.findNearest(Fighter.class, ship.transform.position(), (fighter) -> {
            return !(fighter.dead() || 
                    (fighter.team() == ship.team()) || 
                    Vector3.distance2(ship.transform.position(), fighter.transform.position()) > 100 * 100);
        });
    }

}

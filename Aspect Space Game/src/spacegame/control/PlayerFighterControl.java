package spacegame.control;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static org.lwjgl.input.Keyboard.KEY_A;
import static org.lwjgl.input.Keyboard.KEY_D;
import static org.lwjgl.input.Keyboard.KEY_S;
import static org.lwjgl.input.Keyboard.KEY_W;
import static org.lwjgl.input.Keyboard.isKeyDown;

import aspect.input.Input;
import aspect.input.Joystick;
import aspect.physics.RigidBody;
import aspect.time.Time;
import aspect.util.Trig;
import aspect.util.Vector3;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import spacegame.ship.Fighter;

public class PlayerFighterControl {
    private Joystick joystick;
    private Fighter f;

    public PlayerFighterControl(Fighter f) {
        this.f = f;
        
        if (Joystick.count() > 0) {
            joystick = Joystick.get(0);
            joystick.addButtonListener((evt) -> {
                System.out.println(evt.button + ": " + evt.type);
            });
        }
    }

    public void update() {
        Input.mouseMode = Input.MOUSE_POSITION;
        f.transform.yawLeft(-Input.getAxis("RX", 2) * 2 * Time.deltaTime());
        f.transform.pitchUp(Input.getAxis("RY", 2) * 2 * Time.deltaTime());

        f.transform.rollRight(Input.getAxis("X", true) * Trig.QUARTER_CIRCLE * Time.deltaTime());

        float thrust = Input.getAxis("Y", true) * 10;

        RigidBody rb = f.rigidBody();
        if (rb.velocity.mag2() > 400.0f) {
            rb.velocity = rb.velocity.normalize().times(20.0f);
        }
        
        f.firing = Mouse.isButtonDown(0);
        
        if (Mouse.isButtonDown(1)) {
            f.fireMissile();
        }

        f.rigidBody().impelLinear(f.transform.forward().times(thrust * Time.deltaTime()));
    }
}
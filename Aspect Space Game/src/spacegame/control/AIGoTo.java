package spacegame.control;

import aspect.util.Vector3;

import spacegame.ship.Cruiser;

public class AIGoTo implements AIBehavior {
    private AILookAt rotation;
    private AIMoveTo movement;
    private boolean turning = true;
    
    public AIGoTo(Vector3 point, float power) {
        rotation = new AILookAt(point, power);
        movement = new AIMoveTo(point, power);
    }
    
    @Override
    public void start(Cruiser ship) {
        rotation.start(ship);
        movement.start(ship);
    }

    @Override
    public void update(Cruiser ship) {
        if (rotation.done(ship)) {
            turning = false;
            movement.update(ship);
        } else {
            if (!turning) {
                turning = true;
                rotation.start(ship);
            }
            rotation.update(ship);
        }
    }

    @Override
    public boolean done(Cruiser ship) {
        return rotation.done(ship) && movement.done(ship);
    }

}

package spacegame.control;

import static aspect.core.AspectLauncher.mainWorld;

import java.util.Collection;
import java.util.Random;

import aspect.time.Time;
import aspect.util.Trig;
import aspect.util.Vector3;

import spacegame.SpaceGame;
import spacegame.ship.Fighter;

public class AIFighterControl {

    protected float maxSpeed;
    protected float acceleration;
    protected Vector3 dest;
    protected Fighter f;

    public AIFighterControl(Fighter f, float maxSpeed, float acceleration, Vector3 dest) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.dest = dest;
        this.f = f;
    }

    public AIFighterControl(Fighter f, float maxSpeed, float acceleration) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.dest = new Vector3(SpaceGame.r.nextFloat() * 100 - 50, SpaceGame.r.nextFloat() * 50 - 25, 0);
        this.f = f;
    }

    private Fighter pickTarget() {
        //return mainWorld.find(Fighter.class, f.enemyFilter());
        return mainWorld.findNearest(Fighter.class, f.transform.position(), f.enemyFilter());
    }

    public void update() {

        Collection<Fighter> enemies = mainWorld.findAll(Fighter.class, f.enemyFilter());
        Collection<Fighter> all = mainWorld.findAll(Fighter.class);
        // Collection<Fighter> allies = mainWorld.findAll(Fighter.class,
        // f.allyFilter());

        if (f.target == null || f.target.dead()) {
            f.target = pickTarget();
        }

        float evade = 0.0f;

        boolean attack = false;
        Vector3 evadeVector = null;

        for (Fighter fighter : enemies) {
            Vector3 fighterToThis = f.transform.position().minus(fighter.transform.position());
            Vector3 thisToFighter = fighterToThis.negate();
            Vector3 cross = Vector3.cross(fighterToThis.normalize(), fighter.transform.forward());
            float e = 1.0f / Math.max(Vector3.angle(fighterToThis, fighter.transform.forward()), 0.1f);
            if (e > evade) {
                if (Vector3.angle(thisToFighter, f.transform.forward()) < 0.2f) {
                    attack = true;
                    f.target = fighter;
                } else {
                    attack = false;
                }
                evade = e;
                evadeVector = Vector3.cross(fighterToThis, cross);
            }
        }

        for (Fighter fighter : all) {
            if (fighter != f) {
                Vector3 toFighter = fighter.transform.position().minus(f.transform.position());

                float e = 1000 / toFighter.mag2();
                if (e > evade) {
                    attack = false;
                    evade = e;
                    Vector3 cross1 = Vector3.cross(f.transform.forward(), toFighter);
                    if (cross1.mag2() == 0.0f) {
                        cross1 = Vector3.yAxis();
                    }
                    evadeVector = toFighter.negate();
                    // evadeVector = Vector3.cross(f.transform.forward(),
                    // cross1);
                }
            }
        }

        evade *= 2.0f - (f.health() / f.maxHealth);

        Vector3 angle;
        if ((attack || evade < 5) && f.target != null) {
            angle = f.target.transform.position().minus(f.transform.position());
            f.firing = true;
        } else if (evade > 5 && evadeVector != null) {
            angle = evadeVector;
            f.firing = false;
        } else {
            angle = dest.minus(f.transform.position());
            f.firing = false;
        }

        f.transform.rotate(f.transform.forward(), angle, 2.0f * Time.deltaTime());
        float dot = Math.max(Vector3.dot(f.transform.forward(), angle.normalize()), 0.0f);
        f.rigidBody().impelLinear(f.transform.forward().times(acceleration * dot));

        if (f.rigidBody().velocity.mag2() > maxSpeed * maxSpeed) {
            f.rigidBody().velocity = f.rigidBody().velocity.normalize().times(maxSpeed);
        }

        // f.transform.setForward(f.rigidBody().velocity);
        // f.rigidBody().velocity = f.rigidBody().velocity.rotateTowards(angle,
        // 2.0f * Time.deltaTime());

    }
}

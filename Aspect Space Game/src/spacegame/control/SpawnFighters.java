package spacegame.control;

import static aspect.core.AspectLauncher.mainWorld;

import aspect.entity.behavior.Behavior;
import aspect.time.AbilityTimer;

import spacegame.ship.Fighter;
import spacegame.ship.Ship;

public class SpawnFighters extends Behavior {
    private final AbilityTimer spawn;
    private float side = 1;
    private final float distance;

    public SpawnFighters(float interval, float distance) {
        spawn = new AbilityTimer(interval, interval);
        this.distance = distance;
    }

    @Override
    public void update() {
        Ship s = (Ship) entity;
        if (spawn.use() && ((s.team() == Ship.BLUE && Fighter.numBlueFighters < 25) || (s.team() == Ship.RED && Fighter.numRedFighters < 25))) {
            Fighter f = new Fighter(((Ship) entity).team());
            f.transform.setPosition(entity.transform.position().plus(entity.transform.right().times(distance * side)));
            f.rigidBody().velocity = entity.transform.right().times(side * 10); 
            mainWorld.add(f);
            side *= -1;
            
        }
    }
}

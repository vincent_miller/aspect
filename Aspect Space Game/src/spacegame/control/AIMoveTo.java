package spacegame.control;

import aspect.core.Console;
import aspect.util.Color;
import aspect.util.Vector3;

import spacegame.ship.Cruiser;

public class AIMoveTo implements AIBehavior {
    private Vector3 point;
    private float power;

    public AIMoveTo(Vector3 point, float power) {
        this.point = point;
        this.power = power;
    }

    @Override
    public void start(Cruiser ship) {
    }

    private Vector3 getProjectedPoint(Cruiser ship) {
        return point.project2D(ship.transform.position(), ship.transform.forward(), ship.transform.up());
    }

    private Vector3 getProjectedVelocity(Cruiser ship) {
        return ship.rigidBody().velocity.project2D(ship.transform.forward(), ship.transform.up());
    }

    @Override
    public void update(Cruiser ship) {

        Vector3 p = getProjectedPoint(ship);

        Vector3 toTarget = p.minus(ship.transform.position());

        Vector3 vel = getProjectedVelocity(ship).negate();

        if (toTarget.mag2() > 1) {
            vel = vel.plus(toTarget);
        }

        Vector3 local = ship.transform.globalViewMatrix().transformVector(vel);
        ship.engineControl.updateThrust(local.times(power));
    }

    @Override
    public boolean done(Cruiser ship) {
        Vector3 p = getProjectedPoint(ship);

        return Vector3.distance2(ship.transform.position(), p) < 3 && getProjectedVelocity(ship).mag2() < 0.2f * 0.2f;
    }
}

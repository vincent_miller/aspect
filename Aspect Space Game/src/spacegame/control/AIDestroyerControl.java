package spacegame.control;

import aspect.entity.Entity;
import aspect.util.Vector3;

import spacegame.ship.Cruiser;
import spacegame.ship.Fighter;
import spacegame.ship.Ship;

public class AIDestroyerControl extends AICruiserControl {

    public AIDestroyerControl(Cruiser ship) {
        super(ship);
        
        directives.add(new AILookAt(new Vector3(0, -20, 0), 10));
    }
    
    @Override
    protected Ship getTarget() {
        return ship.container.findNearest(Cruiser.class, ship.transform.position(), (fighter) -> !(fighter.dead() || (fighter.team() == ship.team())));
    }

}

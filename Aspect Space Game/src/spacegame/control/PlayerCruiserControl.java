package spacegame.control;

import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;
import static org.lwjgl.input.Keyboard.*;

import org.lwjgl.input.Mouse;

import spacegame.ship.Cruiser;
import spacegame.ship.Turret;

import aspect.util.Angles;
import aspect.util.Transform;
import aspect.util.Trig;
import aspect.util.Vector3;

public class PlayerCruiserControl extends CruiserControl {
    public int controlIndex = 0;
    private Angles angles = new Angles(0, 180, 0);
    private final float distance;

    public PlayerCruiserControl(Cruiser ship, float distance) {
        super(ship);
        this.distance = distance;

        addKeyListener((evt) -> {
            if (evt.type) {
                if (evt.key == KEY_2) {
                    controlIndex++;
                } else if (evt.key == KEY_1) {
                    controlIndex--;
                    if (controlIndex < 0) {
                        controlIndex = ship.turrets.size();
                    }
                }
            }
        });
    }

    @Override
    public void update() {
        if (controlIndex == 0) {
            updateEngines();
        } else if (controlIndex > 0 && controlIndex <= ship.turrets.size()) {
            updateTurret(ship.turrets.get(controlIndex - 1));
        } else {
            controlIndex = 0;
        }
    }

    protected void updateEngines() {

        float x = Mouse.getDX() * 0.01f;
        float y = Mouse.getDY() * 0.01f;

        angles.pitch += y;
        angles.yaw += x;

        Transform t = new Transform();
        t.setEulerAngles(angles);
        t.setPosition(angles.toVector(distance).negate());
        t.setParent(ship.transform);
        
        camera = t;

        if (isKeyDown(KEY_W)) {
            ship.engineControl.updateTorque(Vector3.xAxis().negate());
        } else if (isKeyDown(KEY_S)) {
            ship.engineControl.updateTorque(Vector3.xAxis());
        } else if (isKeyDown(KEY_A)) {
            ship.engineControl.updateTorque(Vector3.zAxis());
        } else if (isKeyDown(KEY_D)) {
            ship.engineControl.updateTorque(Vector3.zAxis().negate());
        } else if (isKeyDown(KEY_Q)) {
            ship.engineControl.updateTorque(Vector3.yAxis());
        } else if (isKeyDown(KEY_E)) {
            ship.engineControl.updateTorque(Vector3.yAxis().negate());
        } else if (isKeyDown(KEY_LSHIFT)) {
            ship.engineControl.updateThrust(Vector3.zAxis().times(-10));
        } else if (isKeyDown(KEY_LCONTROL)) {
            ship.engineControl.updateThrust(Vector3.zAxis().times(10));
        } else {
            ship.engineControl.cancelThrust();
        }

    }

    protected void updateTurret(Turret turret) {
        Angles angles = turret.getAim();

        float x = Mouse.getDX() * 0.005f;
        float y = Mouse.getDY() * 0.005f;

        angles.pitch += y;
        angles.yaw += x;

        angles.pitch = Math.max(angles.pitch, 0);
        angles.pitch = Math.min(angles.pitch, Trig.QUARTER_CIRCLE);

        turret.aimAt(angles);

        camera = turret.view;
        
        turret.firing = Mouse.isButtonDown(0);
    }
}

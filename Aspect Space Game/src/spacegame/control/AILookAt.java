package spacegame.control;

import aspect.util.Vector3;

import spacegame.ship.Cruiser;

public class AILookAt extends AIRotateTo {
    private Vector3 point;

    public AILookAt(Vector3 point, float power) {
        super(Vector3.zero(), power);
        this.point = point;
    }
    
    @Override
    public void update(Cruiser ship) {
        destAngle = getAngle(ship);
        super.update(ship);
    }
    
    private Vector3 getAngle(Cruiser ship) {
        return point.minus(ship.transform.position()).normalize();
    }

}

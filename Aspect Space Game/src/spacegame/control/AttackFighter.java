package spacegame.control;

import static aspect.core.AspectLauncher.mainWorld;

import aspect.time.Time;
import aspect.util.Vector3;

import spacegame.ship.Fighter;

public class AttackFighter extends AIFighterControl {
    
    public AttackFighter(Fighter f, float maxSpeed, float acceleration) {
        super(f, maxSpeed, acceleration);
    }
    
    @Override
    public void update() {
        
        if (f.target == null || f.target.dead()) {
            f.target = mainWorld.findNearest(Fighter.class, f.transform.position(), (fighter) -> !fighter.dead() && (fighter.team() != f.team()));
        }
        
        if (f.rigidBody().velocity.mag() > maxSpeed) {
            f.rigidBody().velocity = f.rigidBody().velocity.normalize().times(maxSpeed);
        }
        
        if (f.target != null) {
            
            Vector3 toTarget = f.target.transform.position().minus(f.transform.position());
            
            f.transform.rotate(f.transform.forward(), toTarget, Time.deltaTime());
            
            Vector3 targetn = toTarget.normalize();
            
            //return targetn.times(acceleration).plus(entity.transform.right().times(acceleration * 0.25f).plus(entity.transform.up().times(acceleration * 0.25f)));
        } else {
            //return Vector3.zero();
        }
    }
}

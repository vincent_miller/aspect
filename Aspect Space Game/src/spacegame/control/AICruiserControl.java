package spacegame.control;

import static aspect.core.AspectRenderer.*;

import java.util.LinkedList;
import java.util.Queue;

import aspect.entity.Entity;
import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.Trig;
import aspect.util.Vector3;

import static aspect.core.AspectLauncher.*;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

import spacegame.ship.Cruiser;
import spacegame.ship.Fighter;
import spacegame.ship.Ship;
import spacegame.ship.Turret;

public class AICruiserControl extends CruiserControl {
    protected Queue<AIBehavior> directives = new LinkedList<>();
    private Ship target;

    public AICruiserControl(Cruiser ship) {
        super(ship);
    }

    @Override
    public void update() {
        if (target == null || target.dead()) {
            target = getTarget();
        }
        
        attack(target);

        if (!ship.human() || ship.human.controlIndex != 0) {
            AIBehavior b = directives.peek();
            if (b != null) {
                b.update(ship);
                if (b.done(ship)) {
                    directives.poll();
                }
            } else {
                ship.engineControl.cancelThrust();
            }
        }
    }

    public void addDirective(AIBehavior d) {
        directives.add(d);
    }

    protected Ship getTarget() {
        Ship target = ship.container.findNearest(Ship.class, ship.transform.position(), (fighter) -> !(fighter.dead() || (fighter.team() == ship.team())));
        return target;
    }

    protected void attack(Entity entity) {
        for (int i = 0; i < ship.turrets.size(); i++) {
            Turret turret = ship.turrets.get(i);
            
            if (!ship.human() || ship.human.controlIndex != i + 1) {
                if (entity != null) {
                    turret.transform.resetAngles();

                    Vector3 localPosition;
                    if (entity.rigidBody() != null) {
                        localPosition = turret.transform.globalViewMatrix().transformPoint(entity.transform.position().plus(entity.rigidBody().velocity.times(Time.deltaTime())));
                    } else {
                        localPosition = turret.transform.globalViewMatrix().transformPoint(entity.transform.position());
                    }

                    Vector3 attackVector = localPosition;

                    Angles a = attackVector.dir();
                    if (a.pitch > 0) {
                        turret.aimAt(a);

                        if (ship.collider().raycast(turret.view.global().position(), turret.view.global().forward()) != null) {
                            turret.firing = false;
                        } else {
                            turret.firing = true;
                        }
                    } else {
                        turret.firing = false;
                    }
                } else {
                    turret.firing = false;
                }
            }
        }
    }
}

package spacegame.control;

import aspect.entity.behavior.Behavior;

import spacegame.ship.Cruiser;

public class CruiserControl extends Behavior {
    public boolean active = false;
    
    protected final Cruiser ship;
    
    public CruiserControl(Cruiser ship) {
        this.ship = ship;
    }
}

package spacegame.control;

import static aspect.core.AspectLauncher.addKeyListener;
import static org.lwjgl.input.Keyboard.KEY_A;
import static org.lwjgl.input.Keyboard.KEY_D;
import static org.lwjgl.input.Keyboard.KEY_E;
import static org.lwjgl.input.Keyboard.KEY_LCONTROL;
import static org.lwjgl.input.Keyboard.KEY_LSHIFT;
import static org.lwjgl.input.Keyboard.KEY_Q;
import static org.lwjgl.input.Keyboard.KEY_S;
import static org.lwjgl.input.Keyboard.KEY_SPACE;
import static org.lwjgl.input.Keyboard.KEY_T;
import static org.lwjgl.input.Keyboard.KEY_W;
import static org.lwjgl.input.Keyboard.isKeyDown;

import org.lwjgl.input.Mouse;

import spacegame.ship.Cruiser;
import spacegame.ship.Engine;

import aspect.entity.behavior.Behavior;
import aspect.event.KeyEvent;
import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.Transform;
import aspect.util.Trig;
import aspect.util.Vector3;

public class EngineControl extends Behavior {

    private final Cruiser ship;

    public EngineControl(Cruiser ship) {
        addKeyListener(this);
        this.ship = ship;
    }

    public void updateTorque(Vector3 torque) {
        for (Engine engine : ship.engines) {
            engine.updateTorque(torque);
        }
    }

    public void updateThrust(Vector3 thrust) {
        for (Engine engine : ship.engines) {
            engine.updateThrust(thrust);
        }
    }

    public void cancelThrust() {
        for (Engine engine : ship.engines) {
            engine.cancelThrust();
        }
    }
    
    public Vector3 maxAngularAcceleration(Vector3 normAxis) {
        Vector3 torque = Vector3.zero();
        for (Engine engine : ship.engines) {
            torque = torque.plus(engine.getMaxAngularThrust(normAxis));
        }
        return ship.rigidBody().inverseInertiaTensor.transform(torque);
    }
}
/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package spacegame;

import aspect.entity.behavior.Behavior;
import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;
import aspect.event.KeyEvent;
import aspect.gui.GUI;
import aspect.gui.Healthbar;
import aspect.gui.Rectangle;
import aspect.gui.TextLog;
import aspect.render.PointLight;
import aspect.render.Light;
import aspect.render.Material;
import aspect.render.Texture;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;

import static aspect.resources.Resources.*;
import static org.lwjgl.opengl.Display.setVSyncEnabled;
import aspect.time.ExpirationTimer;
import aspect.util.Color;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector4f;

import spacegame.control.AIGoTo;
import spacegame.ship.Carrier;
import spacegame.ship.Destroyer;
import spacegame.ship.Fighter;
import spacegame.ship.Frigate;
import spacegame.ship.Ship;
import spacegame.ship.WMD;

/**
 *
 * @author MillerV
 */
public class SpaceGame extends Behavior {

    private ViewModel skybox;

    public static float scale = 0.0f;
    private Ship controlled;

    public static Healthbar healthbar;
    public static Rectangle crosshair;
    public static Rectangle death;
    public static Rectangle hitMarker;
    public static TextLog log;

    private boolean playerTeam = Ship.BLUE;
    private ViewModel planet;
    //private ViewModel sun;
    //private Vector3 sunPosition;
    //private ShaderProgram post;
    //private ShaderProgram back;
    private static boolean testAI = false;

    public static final ExpirationTimer showHitMarker = new ExpirationTimer(0.1f);

    public static final Random r = new Random();

    @Override
    public void onAdd() {
        Mouse.setGrabbed(true);
        setLightingEnabled(true);
        //GUI.setDrawFPS(true);

        camera.reset();
        camera.setPosition(new Vector3(0, 10, 0));
        camera.setForward(Vector3.yAxis().negate());

        if (healthbar == null) {
            healthbar = new Healthbar(new Vector2(200, 50));
            healthbar.setPosition(new Vector2(20));
            healthbar.setEnabled(false);
            GUI.add(healthbar);

            death = new Rectangle(new Vector2(getCanvasWidth(), getCanvasHeight()));
            death.setEnabled(false);
            death.setColor(new Color(0.8f, 0.0f, 0.0f, 0.25f));
            GUI.add(death);

            log = new TextLog(new Vector2(0, getCanvasHeight() - 150), new Vector2(600, 150));
            log.setTextSize(16);
            log.setTextColor(Color.RED);
            log.setBackgroundEnabled(false);
            //GUI.add(log);

            hitMarker = new Rectangle(getTexture("textures/hitmarker.png"));
            hitMarker.setPosition(new Vector2(getCanvasWidth(), getCanvasHeight()).minus(hitMarker.size()).times(0.5f));
            GUI.add(hitMarker);
        }

        log.clear();
        Fighter.numRedFighters = 0;
        Fighter.numBlueFighters = 0;
        BotNames.load(loadTextFile("txt/botnames.txt"));

        if (testAI) {
            Fighter f1 = new Fighter(Ship.BLUE);
            f1.transform.setPosition(new Vector3(-20, -50, 30));
            f1.rigidBody().velocity = new Vector3(0, 0, -10);
            f1.setName("Blue 1");
            mainWorld.add(f1);

            /*
             * Fighter f4 = new Fighter(Ship.BLUE); f4.transform.setPosition(new
             * Vector3(20, -50, 30)); f4.rigidBody().velocity = new Vector3(0,
             * 0, -10); f4.name = "Blue 2"; mainWorld.add(f4);
             */

            Fighter f2 = new Fighter(Ship.RED);
            f2.transform.setPosition(new Vector3(-30, -50, -30));
            f2.rigidBody().velocity = new Vector3(0, 0, 10);
            f2.setName("Red 1");
            mainWorld.add(f2);

            /*
             * Fighter f3 = new Fighter(Ship.RED); f3.transform.setPosition(new
             * Vector3(30, -50, -30)); f3.rigidBody().velocity = new Vector3(0,
             * 0, 10); f3.name = "Red 2"; mainWorld.add(f3);
             */
        } else {

            Frigate frigate1 = new Frigate(Ship.BLUE);
            frigate1.transform.setPosition(new Vector3(-50, 0, 100));
            mainWorld.add(frigate1);

            frigate1.ai.addDirective(new AIGoTo(frigate1.transform.position().plus(frigate1.transform.forward().times(50).plus(frigate1.transform.right().times(50))), 10));
            frigate1.ai.addDirective(new AIGoTo(frigate1.transform.position().plus(frigate1.transform.forward().times(100)), 10));

            Frigate frigate2 = new Frigate(Ship.RED);
            frigate2.transform.setPosition(new Vector3(50, 0, -100));
            frigate2.transform.setRotation(Vector3.xAxis().negate(), Vector3.yAxis(), Vector3.zAxis());
            mainWorld.add(frigate2);

            frigate2.ai.addDirective(new AIGoTo(frigate2.transform.position().plus(frigate2.transform.forward().times(50).plus(frigate2.transform.right().times(50))), 10));
            frigate2.ai.addDirective(new AIGoTo(frigate2.transform.position().plus(frigate2.transform.forward().times(100)), 10));

            Destroyer destroyer1 = new Destroyer(Ship.BLUE);
            destroyer1.transform.setPosition(new Vector3(50, 0, 100));
            mainWorld.add(destroyer1);

            Destroyer destroyer2 = new Destroyer(Ship.RED);
            destroyer2.transform.setPosition(new Vector3(-50, 0, -100));
            mainWorld.add(destroyer2);

            Carrier carrier1 = new Carrier(Ship.BLUE);
            carrier1.transform.setPosition(new Vector3(0, 0, 150));
            mainWorld.add(carrier1);

            Carrier carrier2 = new Carrier(Ship.RED);
            carrier2.transform.setPosition(new Vector3(0, 0, -150));
            mainWorld.add(carrier2);

            /*
            WMD wmd1 = new WMD(Ship.BLUE);
            wmd1.transform.setPosition(new Vector3(0, 0, -150));
            mainWorld.add(wmd1);*/
        }

        crosshair = new Rectangle(getTexture("materials/crosshair.png"));
        crosshair.setPosition(new Vector2((getCanvasWidth() - crosshair.width()) / 2, (getCanvasHeight() - crosshair.height()) / 2));
        GUI.add(crosshair);

        Texture t = loadedTextures.get("skybox");
        if (t == null) {
            BufferedImage img = createImage(512, 512);
            Graphics2D g = img.createGraphics();
            g.setColor(java.awt.Color.WHITE);
            for (int i = 0; i < 500; i++) {
                int x = (int) (Math.random() * img.getWidth());
                int y = (int) (Math.random() * img.getHeight());

                g.fillOval(x, y, 1, 2);
            }
            g.dispose();
            t = Texture.create(img);
            loadedTextures.put("skybox", t);
        }

        PointLight light = new PointLight(Color.WHITE);
        light.light.directional = true;
        light.transform.setPosition(new Vector3(0.0f, 1.0f, 0.0f));
        Light.ambient = new Color(0.2f);

        planet = sphere(new Material(getTexture("textures/planet.jpg")), 1.0f, 50);
        planet.transform.setPosition(new Vector3(-1, -2, 1));

        //sunPosition = light.transform.position().times(20);

        skybox = box(new Material(t, ShaderProgram.TEXTURE), 1, 1, 1);
        addSkybox(skybox);
        addSkybox(planet);

        addKeyListener(this);

        addSkybox(new aspect.render.Sun(light.transform.position()));
    }

    @Override
    public void keyEvent(KeyEvent evt) {
        if (evt.type) {
            if (evt.key == Keyboard.KEY_F1) {
                Carrier c = mainWorld.find(Carrier.class, (carrier) -> carrier.team() == playerTeam);
                if (c != null) {
                    assumeControl(mainWorld.findNearest(Fighter.class, c.transform.position(), (ship) -> ship.team() == playerTeam && !ship.dead()));
                } else {
                    assumeControl(mainWorld.findBest(Fighter.class, (ship) -> ship.health(), (ship) -> ship.team() == playerTeam && !ship.dead()));
                }
            } else if (evt.key == Keyboard.KEY_F2) {
                assumeControl(mainWorld.find(Frigate.class, (ship) -> ship.team() == playerTeam));
            } else if (evt.key == Keyboard.KEY_F3) {
                assumeControl(mainWorld.find(Destroyer.class, (ship) -> ship.team() == playerTeam));
            } else if (evt.key == Keyboard.KEY_F4) {
                assumeControl(mainWorld.find(Carrier.class, (ship) -> ship.team() == playerTeam));
            } else if (evt.key == Keyboard.KEY_F5) {
                assumeControl(mainWorld.find(WMD.class, (ship) -> ship.team() == playerTeam));
            } else if (evt.key == Keyboard.KEY_V) {
                Fighter.firstPerson = !Fighter.firstPerson;
            }
        }
    }

    private void releaseControl() {
        if (controlled != null) {
            controlled.setHuman(false);
            controlled = null;
        }

        healthbar.setEnabled(false);
        camera = new Transform();
        camera.moveUp(10);
        camera.setForward(Vector3.yAxis().negate());
    }

    @Override
    public void update() {
        if (controlled != null && controlled.dead()) {
            death.setEnabled(true);
        } else {
            death.setEnabled(false);
        }

        hitMarker.setEnabled(!showHitMarker.expired());
    }

    public void assumeControl(Ship ship) {
        releaseControl();
        if (ship != null) {
            ship.setHuman(true);
            controlled = ship;
            healthbar.setEnabled(true);
        }
    }

    public static void main(String[] args) {
        run(1600, 900, false, 60, new SpaceGame());
        //run(60, new SpaceGame());
        setVSyncEnabled(true);
    }

}

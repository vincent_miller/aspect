#version 130

in vec2 texcoord;

void main() {
	float sub = pow(abs(0.5 - texcoord.x) * 2, 2);
    gl_FragColor = vec4(1.0, 0.1, sub * 2, texcoord.y - sub);
}
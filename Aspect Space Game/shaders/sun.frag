#version 130
#include <shapes.ase>

uniform sampler2D tex;
uniform vec2 sun;
uniform bool show;

in vec2 texcoord;

void main() {
	vec4 texcolor = texture(tex, texcoord);

	vec2 vec = vec2(0.5) - sun;

	float l = length(vec);
	if (show) {
		if (l < 1) {
			for (int i = 2; i < 5; i++) {
				vec2 point = vec * i * 0.5 + sun;
				float r = 0.05 + abs(i - 2) * 0.04;
				float d = distance(texcoord, point);

				//float mul = (r / d) / pow(max(8 * l, 1), 1);
				float mul = min(pow(0.2 / l, 4), 0.5);
				//float mul = 0.5;

				vec3 color = vec3(0.2, 0.5, 0.5);

				if (i == 2) {
					texcolor += vec4(color * fillGlow(circlef(point, r, texcoord)) * mul, 1);
					texcolor += vec4(color * fillGlow(circlef(point, 0.8 * r, texcoord)) * mul, 1);
				} else if (i == 3) {
					texcolor += vec4(color * fillGlow(circlef(point, r, texcoord)) * mul, 1);
				} else if (i == 4) {
					texcolor += vec4(color * fillGlow(hexagonf(point, r, texcoord)) * mul, 1);
				}

				//if (ellipse(point, r, texcoord)) {
					//if (hexagon(point, r, texcoord)) {
					//	texcolor += vec4(color, 1);
					//}
				//}
			}
		}
	}

	gl_FragColor = texcolor;
}
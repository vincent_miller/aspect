#version 130

in vec2 texcoord;

void main() {
	vec2 t = texcoord - vec2(0.5);
	float r = length(t);
	float f = pow(0.3 / (r - 0.7), 2);
	gl_FragColor = vec4(f, f * 0.6, 0.0, f);
}
#version 130

in vec2 texcoord;

uniform vec4 color;

void main() {
	float f = 0.3 / abs(texcoord.x - 0.5);
    gl_FragColor = color * f;
}
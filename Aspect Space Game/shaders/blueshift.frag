uniform float scale;
uniform sampler2D tex;

in vec2 texcoord;

void main() {
	vec4 texcolor = texture(tex, texcoord);
	texcolor.a = 1.0f;
	
	vec2 center = vec2(0.5, 0.5);
	vec2 disp = (center - texcoord) * 20;
	float dist2 = dot(disp, disp);
	float f = abs(scale / (disp.x * disp.y));
	f = max(f, 0.0);
	
	//texcolor.r -= f;
	//texcolor.g -= f;
	
	texcolor.b += f;
	
	gl_FragColor = texcolor;
}
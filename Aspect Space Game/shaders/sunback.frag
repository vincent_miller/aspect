#version 130

uniform vec2 sun;
uniform bool show;

in vec2 texcoord;

void main() {
	vec4 texcolor = vec4(0, 0, 0, 0);
	
	vec2 vec = vec2(0.5) - sun;
	
	float l = length(vec);
	if (show) {
		
		float d = distance(texcoord, sun);
		float f = (0.1 / d) / max(pow(l, 0.5), 0.25);
		texcolor += vec4(0.3f, 0.2 * f, 0.2 * f, f);
		//f = 0.02 / abs(d - 0.05);
		//texcolor += vec4(f, f, 0.3 * f, f);
	}
	
	gl_FragColor = texcolor;
}
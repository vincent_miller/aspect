/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.time;

/**
 *
 * @author MillerV
 */
public class Time {

    private static long totalTime = 0;

    private static long lastFrame;
    private static long thisFrame;

    public static float speed = 1;
    
    public static float maxDeltaTime = speed;

    public static void tick() {
        if (lastFrame != 0 && thisFrame != 0) {
            totalTime += (thisFrame - lastFrame) * speed;
        }

        lastFrame = thisFrame;
        thisFrame = System.nanoTime();
    }

    public static long deltaTimeMillis() {
        return (long) (((thisFrame - lastFrame) / 1000000) * speed);
    }

    public static float deltaTime() {
        
        float dt = (((thisFrame - lastFrame) / 1e9f) * speed);
        if ((maxDeltaTime > 0 && dt < maxDeltaTime) || (maxDeltaTime < 0 && dt > maxDeltaTime)) {
            return dt;
        } else {
            return 0;
        }
        
        //return 1.0f / 5000f;
    }
    
    public static void setDeltaTime(float dt) {
        thisFrame = lastFrame + (int)(dt * 1_000_000_000);
    }

    public static long frameTimeMillis() {
        return totalTime / 1000000;
    }

    public static float frameTime() {
        return totalTime / 1e9f;
    }

    public static long currentTimeMillis() {
        return System.nanoTime() / 1000000;
    }

    public static float currentTime() {
        return System.nanoTime() / 1e9f;
    }

    public static String formatTime() {
        long millis = frameTimeMillis();
        
        int days = (int) (millis / (24 * 3600_000));
        millis %= (24 * 3600_000);
        int hours = (int) (millis / 3600_000);
        millis %= 3600_000;
        int minutes = (int) (millis / 60_000);
        millis %= 60_000;
        int seconds = (int) (millis / 1000);
        millis %= 1000;

        return days + ":" + hours + ":" + minutes + ":" + seconds + ":" + millis;

    }
}

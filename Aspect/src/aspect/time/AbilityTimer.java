package aspect.time;


public class AbilityTimer {
    private float last;
    private float interval;
    
    public AbilityTimer(float initial, float interval) {
        this.last = Time.frameTime() + initial - interval;
        this.interval = interval;
    }
    
    public boolean use() {
        float current = Time.frameTime();
        if (current - last > interval) {
            last = current;
            return true;
        } else {
            return false;
        }
    }
    
    public void setInterval(float interval) {
        this.interval = interval;
        reset();
    }
    
    public float interval() {
        return interval;
    }
    
    public void reset() {
        last = Time.frameTime();
    }
    
    public void clear() {
        last = Time.frameTime() - interval;
    }
}

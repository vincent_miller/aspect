package aspect.time;

public class ExpirationTimer {
    private float expiration;
    private float lastSet;
    
    public ExpirationTimer(float expiration) {
        this.expiration = expiration;
        this.lastSet = Time.frameTime() - expiration;
    }
    
    public boolean expired() {
        return Time.frameTime() - lastSet > expiration;
    }
    
    public void set() {
        lastSet = Time.frameTime();
    }
    
    public void set(float expiration) {
        lastSet = Time.frameTime() - this.expiration + expiration;
    }
    
    public void clear() {
        lastSet = Time.frameTime() - expiration;
    }
    
    public void setExpiration(float expiration) {
        this.expiration = expiration;
    }
}

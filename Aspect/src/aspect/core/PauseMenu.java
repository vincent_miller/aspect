package aspect.core;

import org.lwjgl.input.Mouse;

import aspect.gui.Button;
import aspect.gui.Component;
import aspect.gui.Container;
import aspect.gui.GUI;
import aspect.gui.FlowLayout;
import aspect.util.Vector2;

import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;

public class PauseMenu {
    private static Container buttons;
    private static boolean grabMouse = false;
    
    public static void initialize() {
        Button quit = new Button("Quit Game");
        quit.addListener(() -> System.exit(0));
        Button resume = new Button("Resume Game");
        resume.addListener(() -> setPaused(false));
        Button console = new Button("Open Console");
        console.addListener(() -> {
            setPaused(false);
            Console.setEnabled(true);
        });
        Button restart = new Button("Restart Game");
        restart.addListener(() -> {
            restart();
            setPaused(false);
        });
        
        Component.makeUniformSize(quit, resume, console, restart);
        
        buttons = new Container(new FlowLayout(0, 10, FlowLayout.VERTICAL, true));
        buttons.add(resume);
        buttons.add(console);
        buttons.add(restart);
        buttons.add(quit);
        buttons.setBackgroundEnabled(false);
        buttons.setPosition(new Vector2((getCanvasWidth() - buttons.width()) / 2, 300));
        
        GUI.addOverlay(buttons);
    }
    
    public static void setEnabled(boolean enabled) {
        buttons.setEnabled(enabled && !Console.enabled());
        
        if (enabled) {
            grabMouse = Mouse.isGrabbed();
            Mouse.setGrabbed(false);
        } else {
            Mouse.setGrabbed(grabMouse);
        }
    }
}

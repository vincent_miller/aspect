package aspect.core;

import static aspect.core.AspectLauncher.addKeyListener;
import static aspect.core.AspectLauncher.addMouseListener;
import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectLauncher.restart;
import static aspect.core.AspectLauncher.setPaused;
import static aspect.core.AspectLauncher.setPrintFPS;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.core.AspectRenderer.getSelected;
import static aspect.core.AspectRenderer.setLightingEnabled;
import static aspect.core.AspectRenderer.setTexturesEnabled;
import static aspect.core.AspectRenderer.setWireframe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.function.Consumer;

import aspect.core.Console.LogLevel;
import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.gui.GUI;
import aspect.gui.Slider;
import aspect.gui.TextField;
import aspect.gui.TextLog;
import aspect.time.Time;
import aspect.util.Color;
import aspect.util.StringUtil;
import aspect.util.Vector2;
import aspect.util.Vector3;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Console {
    private static TextLog log;
    private static TextField field;
    private static Slider scroll;
    private static boolean pauseOnOpen = true;
    private static boolean grabMouse = false;
    private static HashMap<String, Consumer<String[]>> commands = new HashMap<>();
    private static LinkedList<OutItem> out = new LinkedList<>();
    private static Entity selected;
    private static HashMap<String, BindItem> binds = new HashMap<>();

    public static void initialize() {
        log = new TextLog(new Vector2(0, getCanvasHeight() - 300), new Vector2(getCanvasWidth(), 300));
        log.setTextSize(14);
        log.appendln("");
        GUI.addOverlay(log);
        field = new TextField(new Vector2(0, getCanvasHeight() - 350), getCanvasWidth());
        field.addListener(log);
        field.addListener((line) -> {
            int divider = line.indexOf(' ');
            try {
                if (divider == -1) {
                    command(line);
                } else {
                    String name = line.substring(0, divider);
                    String[] args = line.substring(divider + 1).split(" ");
                    command(name, args);
                }
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
                logheader(LogLevel.ERROR, ex);
            }
        });
        field.setClearOnSend(true);
        GUI.addOverlay(field);
        scroll = new Slider(new Vector2(getCanvasWidth() - 50, getCanvasHeight() - 250), Slider.VERTICAL, 200.0f);
        scroll.setValue(0.0f);
        scroll.addListener((value) -> {
            log.setScrollPosition(value);
        });
        GUI.addOverlay(scroll);

        setEnabled(false);
        setupCommands();

        for (OutItem item : out) {
            print(item.string, item.color);
        }

        GUI.addMouseListener((evt) -> {
            if (evt.button == 0 && evt.type && enabled()) {
                Vector2 point = new Vector2(evt.x, evt.y);
                if (!(field.globalBounds().contains(point) || scroll.globalBounds().contains(point))) {
                    selected = getSelected(mainWorld, evt.x, evt.y);
                    Console.log(LogLevel.INFO, "Selected entity: " + selected);
                    field.setSelected(true);
                }
            }
        });
        
        addKeyListener((evt) -> {
            BindItem item = binds.get(Keyboard.getKeyName(evt.key));
            if (evt.type && item != null) {
                command(item.command, item.args);
            }
        });
    }

    public static void command(String command, String... args) {
        if (commands.containsKey(command)) {
            commands.get(command).accept(args);
        } else {
            log(LogLevel.ERROR, "Unrecognized command: " + command);
        }
    }

    private static void setupCommands() {
        commands.put("quit", (args) -> System.exit(0));
        commands.put("restart", (args) -> restart());
        commands.put("lighting", (args) -> setLightingEnabled(StringUtil.getBoolean(args[0])));
        commands.put("textures", (args) -> setTexturesEnabled(StringUtil.getBoolean(args[0])));
        commands.put("wireframe", (args) -> setWireframe(StringUtil.getBoolean(args[0])));
        commands.put("showfps", (args) -> GUI.setDrawFPS(StringUtil.getBoolean(args[0])));
        commands.put("destroy", (args) -> {
            LinkedList<Entity> results = getEntities(args);
            for (Entity entity : results) {
                entity.destroy();
                if (entity == selected) {
                    selected = null;
                }
            }
            Console.log(LogLevel.INFO, "Destroyed " + results.size() + " entities.");
        });
        commands.put("info", (args) -> {
            LinkedList<Entity> results = getEntities(args);
            if (results.size() > 0) {
                log(LogLevel.INFO, "--- ENTITY INFO ---");
                log(LogLevel.INFO, "Results: " + results.size());
                log(LogLevel.INFO, "Simple Class Name: " + results.getFirst().getClass().getSimpleName());
                log(LogLevel.INFO, "Full Class Name: " + results.getFirst().getClass().getName());
                log(LogLevel.INFO, "Behaviors:");
                for (Behavior b : results.getFirst().getBehaviors()) {
                    log(LogLevel.INFO, "    " + b.getClass().getSimpleName());
                }
            }
        });
        commands.put("field", (args) -> {
            if (selected != null) {
                try {
                    Field f = selected.getClass().getField(args[0]);
                    log(LogLevel.INFO, "Value of field " + f.getName() + " on selected entity is " + f.get(selected));
                } catch (NoSuchFieldException | IllegalAccessException ex) {
                    logheader(LogLevel.ERROR, ex);
                }
            }
        });
        commands.put("create", (args) -> {
            Class<Entity> c = findClass(Entity.class, args[0]);
            try {
                Entity e = c.newInstance();
                mainWorld.add(e);
                if (args.length > 1) {
                    float x = StringUtil.getFloat(args[1]);
                    float y = StringUtil.getFloat(args[2]);
                    float z = StringUtil.getFloat(args[3]);
                    e.transform.setPosition(new Vector3(x, y, z));
                }
            } catch (Exception ex) {
                logheader(LogLevel.ERROR, ex);
            }
        });
        commands.put("bind", (args) -> {
            String key = args[0];
            String command = args[1];
            String[] newArgs = new String[args.length - 2];
            System.arraycopy(args, 2, newArgs, 0, newArgs.length);
            binds.put(key.toUpperCase(), new BindItem(command, newArgs));
        });
        commands.put("unbind", (args) -> binds.remove(args[0].toUpperCase()));
        commands.put("timescale", (args) -> Time.speed = StringUtil.getFloat(args[0]));
    }

    @SuppressWarnings("unchecked")
    private static <T> Class<T> findClass(Class<T> superClass, String name) {
        try {
            Class<?> temp = Class.forName(name);
            if (superClass.isAssignableFrom(temp)) {
                return (Class<T>) temp;
            }
        } catch (ClassNotFoundException ex) {
            for (Package pkg : Package.getPackages()) {
                try {
                    Class<?> temp = Class.forName(pkg.getName() + "." + name);
                    if (superClass.isAssignableFrom(temp)) {
                        return (Class<T>) temp;
                    }
                } catch (ClassNotFoundException ex1) {
                }
            }
        }

        return null;
    }

    private static LinkedList<Entity> getEntities(String[] args) {
        LinkedList<Entity> entities = new LinkedList<>();
        if (args.length == 0) {
            if (selected != null) {
                entities.add(selected);
            } else {
                log(LogLevel.ERROR, "Nothing selected");
            }
        } else if (args[0].equals("-c")) {
            Class<Entity> c = findClass(Entity.class, args[1]);
            if (c == null) {
                log(LogLevel.ERROR, "Class not found: " + args[1]);
            } else {
                mainWorld.forEach((entity) -> {
                    if (c.isInstance(entity)) {
                        entities.add(entity);
                    }
                });
            }
        } else {
            String name = args[0];
            mainWorld.forEach((entity) -> {
                if (entity.getName().equalsIgnoreCase(name)) {
                    entities.add(entity);
                }
            });
        }

        return entities;
    }

    public static void addCommand(String name, Consumer<String[]> action) {
        commands.put(name, action);
    }

    public static void setPauseOnOpen(boolean pauseOnOpen) {
        Console.pauseOnOpen = pauseOnOpen;
    }

    public static void setEnabled(boolean enabled) {
        log.setEnabled(enabled);
        field.setEnabled(enabled);
        scroll.setEnabled(enabled);

        if (pauseOnOpen) {
            setPaused(enabled);
        } else {
            if (enabled) {
                grabMouse = Mouse.isGrabbed();
                Mouse.setGrabbed(false);
            } else {
                Mouse.setGrabbed(grabMouse);
            }
        }

        if (enabled) {
            field.setSelected(true);
        }
    }

    public static boolean enabled() {
        return log.enabled();
    }

    public static void print(Object obj) {
        print(obj, GUI.outlineColor);
    }

    public static void println(Object obj) {
        print(obj + "\n");
    }

    public static void print(Object obj, Color color) {
        if (log == null) {
            out.add(new OutItem(String.valueOf(obj), color));
        } else {
            log.append(String.valueOf(obj), color);
        }
        System.out.print(obj);
    }

    public static void println(Object obj, Color color) {
        print(obj + "\n", color);
    }

    public static void println(Throwable thr) {
        println(thr, Color.RED);
    }

    public static void println(Throwable thr, Color color) {
        StringWriter s = new StringWriter();
        thr.printStackTrace(new PrintWriter(s));
        println(s, color);
    }
    
    public static void printheader(Throwable thr, Color color) {
        println(thr.getClass().getSimpleName() + ": " + thr.getMessage(), color);
    }
    
    public static void logheader(LogLevel level, Throwable thr) {
        log(level, thr.getClass().getSimpleName() + ": " + thr.getMessage());
    }

    public static void log(LogLevel level, String message) {
        println(level.header() + message, level.color);
    }

    public static void log(LogLevel level, String message, Throwable thr) {
        println(level.header() + message, level.color);
        println(thr, level.color);
    }

    public static void log(LogLevel level, Throwable thr) {
        print(level.header(), level.color);
        println(thr, level.color);
    }

    public static enum LogLevel {
        INFO("INFO", Color.BLACK), WARNING("WARNING", Color.ORANGE), ERROR("ERROR", Color.RED), DEBUG("DEBUG", Color.BLUE), SUCCESS("SUCCESS", new Color(0, 0.5f, 0));

        private String title;
        private Color color;

        private LogLevel(String title, Color color) {
            this.title = title;
            this.color = color;
        }

        private String header() {
            return "[" + title + "] ";
        }
    }

    private static class OutItem {
        private String string;
        private Color color;

        private OutItem(String string, Color color) {
            this.string = string;
            this.color = color;
        }
    }
    
    private static class BindItem {
        private String command;
        private String[] args;
        
        private BindItem(String command, String[] args) {
            this.command = command;
            this.args = args;
        }
    }
}

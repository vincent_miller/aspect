/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.core;

import aspect.util.Vector2;
import aspect.util.Vector3;
import aspect.physics.Hit;
import aspect.render.Material;
import aspect.render.RenderTexture;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.entity.Entity;
import aspect.resources.Resources;
import aspect.resources.modeling.Vertex;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.MatrixStack;
import aspect.util.Transform;
import aspect.util.Trig;
import aspect.world.World;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;

import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.projectionMatrix;
import static org.lwjgl.opengl.Display.*;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.vector.Vector4f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

/**
 * This is the utility class used to render the scene. Note that typically
 * Entity.draw() should be used to render 3D objects and setCameraViewpoint
 * should be used to control the camera.
 *
 * @author MillerV
 */
public class AspectRenderer {

    private static boolean resized = false;

    static LinkedList<Entity> skyboxes = new LinkedList<Entity>();

    private static boolean texturesEnabled = true;
    private static boolean wireframeOn = false;
    private static boolean lightingEnabled = false;

    protected static RenderTexture fbo;

    private static final Transform DEFAULT_CAMERA = new Transform();

    private static final ArrayList<Entity> selectionBuffer = new ArrayList<>(256);

    /**
     * The current camera viewpoint.
     */
    public static Transform camera = DEFAULT_CAMERA;

    public static MatrixStack modelMatrix = new MatrixStack();
    public static MatrixStack viewMatrix = new MatrixStack();
    public static MatrixStack projectionMatrix = new MatrixStack();
    
    static int width;
    static int height;

    /**
     * Change between 2D and 3D rendering modes. It is usually not necessary to
     * call this method yourself.
     *
     * @param mode
     *            the new RenderMode
     */
    public static void updateProjection(ProjectionMode mode) {
        glViewport(0, 0, getWidth(), getHeight());

        if (mode == ProjectionMode.PERSPECTIVE) {
            projectionMatrix.set(Matrix4x4.perspective(Trig.PI / 3.0f, getAspectRatio(), 0.01f, 1000));
            glEnable(GL_DEPTH_TEST);
        } else {
            projectionMatrix.set(Matrix4x4.orthographic(0, getWidth(), 0, getHeight()));
            glDisable(GL_DEPTH_TEST);
        }

        resized = false;
    }

    public static void onDisplayResize() {
        resized = true;
    }

    public static void setCulling(boolean allow) {
        if (allow) {
            glEnable(GL_CULL_FACE);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    public static void postProcess(ShaderProgram shader) {
        if (fbo == null) {
            fbo = new RenderTexture(getCanvasWidth(), getCanvasHeight(), camera);
        }

        fbo.postProcess(shader);
    }
    
    public static RenderTexture getFBO() {
        return fbo;
    }

    /**
     * Clears the color and depth buffers so that the scene can be redrawn. It
     * is not necessary to call this method yourself.
     */
    public static void clearRenderer() {
        clearRenderer(camera);
    }

    public static void clearRenderer(Transform viewpoint) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        viewMatrix.clear();
        viewMatrix.set(viewpoint.globalViewMatrix());
        modelMatrix.clear();
    }

    /**
     * Prints the OpenGL version currently supported.
     */
    public static void printGLVersion() {
        System.out.println("GPU Vendor: " + glGetString(GL_VENDOR));
        System.out.println("GPU Model: " + glGetString(GL_RENDERER));
        System.out.println("OpenGL Version: " + glGetString(GL_VERSION));
        System.out.println("GLSL Version: " + glGetString(GL_SHADING_LANGUAGE_VERSION));
        System.out.println();
    }

    public static float getAspectRatio() {
        return (float) width / (float) height;
    }

    /**
     * Get the height of the window.
     *
     * @return the window height in pixels
     */
    public static int getCanvasHeight() {
        return height;
    }

    /**
     * Get the width of the window.
     *
     * @return the window width in pixels
     */
    public static int getCanvasWidth() {
        return width;
    }

    /**
     * Get the x coordinate of the left window bound, relative to the left edge
     * of the screen.
     *
     * @return the x coordinate of the window
     */
    public static int getCanvasX() {
        return getX();
    }

    /**
     * Get the y coordinate of the upper window bound, relative to the upper
     * edge of the screen.
     *
     * @return the y coordinate of the window
     */
    public static int getCanvasY() {
        return getY();
    }

    public static Vector2 projectVectorNorm(Vector3 vector) {
        Vector3 position = camera.globalViewMatrix().transformVector(vector);
        Vector4f projected = projectionMatrix.current().transformPointFull(position);
        if (projected.z > 0) {
            Vector2 norm = new Vector2(projected.x / projected.w, projected.y / projected.w);
            return norm.plus(new Vector2(1.0f)).times(0.5f);
        } else {
            return null;
        }
    }

    public static Vector2 projectPointNorm(Vector3 point) {
        return projectVectorNorm(point.minus(camera.global().position()));
    }

    public static Vector2 projectVector(Vector3 vector) {
        Vector2 norm = projectVectorNorm(vector);
        if (norm != null) {
            norm.x *= getCanvasWidth();
            norm.y *= getCanvasHeight();
        }
        return norm;
    }

    public static Vector2 projectPoint(Vector3 point) {
        return projectVector(point.minus(camera.global().position()));
    }
    
    public static void pushMatrices() {
        modelMatrix.push();
        viewMatrix.push();
        projectionMatrix.push();
    }
    
    public static void pushIdentityMatrices() {
        pushMatrices();
        modelMatrix.set(Matrix4x4.identity());
        viewMatrix.set(Matrix4x4.identity());
        projectionMatrix.set(Matrix4x4.identity());
    }
    
    public static void popMatrices() {
        modelMatrix.pop();
        viewMatrix.pop();
        projectionMatrix.pop();
    }

    /**
     * Clear the depth buffer, meaning that any new objects will be drawn over
     * those already drawn, regardless of their depth.
     */
    public static void clearDepthBuffer() {
        glClear(GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Save the depth buffer into a IntBuffer, so that it can be restored later
     * on.
     *
     * @return the saved buffer
     * @see #restoreDepthBuffer(java.nio.IntBuffer)
     */
    public static IntBuffer saveDepthBuffer() {
        IntBuffer depth = BufferUtils.createIntBuffer(getWidth() * getHeight());
        glReadPixels(0, 0, getWidth(), getHeight(), GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, depth);
        return depth;
    }

    /**
     * Restore the depth buffer from a saved IntBuffer.
     *
     * @param depth
     *            the saved buffer
     * @see #saveDepthBuffer()
     */
    public static void restoreDepthBuffer(IntBuffer depth) {
        glRasterPos2i(0, 0);
        glDrawPixels(getWidth(), getHeight(), GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, depth);
    }

    /**
     * Clear the color buffer, drawing a black rectangle over the screen but
     * keeping the depth buffer, meaning that objects drawn behind those already
     * drawn will be hidden.
     */
    public static void clearColorBuffer() {
        glClear(GL_COLOR_BUFFER_BIT);
    }

    public static void color(Color color) {
        ShaderProgram current = ShaderProgram.getCurrent();
        if (current == null) {
            // glColor4f(color.red, color.green, color.blue, color.alpha);
        } else {
            // current.setAttribute("Color", color);
        }
    }

    public static void texCoord(Vector2 texCoord) {
        ShaderProgram current = ShaderProgram.getCurrent();
        if (current == null) {
            glTexCoord2f(texCoord.x, texCoord.y);
        } else {
            current.setAttribute("TexCoord", texCoord);
        }
    }

    public static void normal(Vector3 normal) {
        normal = normal.normalize();
        ShaderProgram current = ShaderProgram.getCurrent();
        if (current == null) {
            glNormal3f(normal.x, normal.y, normal.z);
        } else {
            current.setAttribute("Normal", normal);
        }
    }

    public static void vertex(Vertex vertex) {
        color(vertex.color);
        texCoord(vertex.texcoord);
        normal(vertex.normal);
        vertex(vertex.position);
    }

    public static void vertex(Vector3 vertex) {
        ShaderProgram current = ShaderProgram.getCurrent();
        if (current == null) {
            glVertex3f(vertex.x, vertex.y, vertex.z);
        } else {
            current.setAttribute("Vertex", vertex);
        }
    }

    public static void vertex(Vector2 vertex) {
        ShaderProgram current = ShaderProgram.getCurrent();
        if (current == null) {
            glVertex2f(vertex.x, vertex.y);
        } else {
            current.setAttribute("Vertex", vertex);
        }
    }

    public static void begin(GeometryType type) {
        glBegin(type.glEnum);
    }

    public static void end() {
        glEnd();
    }

    public static Color getPixelColor(int x, int y) {
        FloatBuffer buff = BufferUtils.createFloatBuffer(3);
        glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, buff);
        return new Color(buff.get(), buff.get(), buff.get());
    }

    public static float getPixelDepth(int x, int y) {
        FloatBuffer buff = BufferUtils.createFloatBuffer(1);
        glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, buff);
        return buff.get();
    }

    public static IntBuffer viewport() {
        IntBuffer buff = BufferUtils.createIntBuffer(16);
        glGetInteger(GL_VIEWPORT, buff);
        return buff;
    }

    /**
     * Draw an Entity at its position and angle, disregarding the current camera
     * position so that the object is drawn relative to the camera.
     *
     * @param ent
     *            the Entity to draw
     */
    public static void draw3DAtCamera(Entity ent) {
        viewMatrix.push();
        viewMatrix.set(Matrix4x4.identity());
        ent.render();
        viewMatrix.pop();
    }

    /**
     * Draw a 3D ViewModel at the given position and angles, disregarding the
     * current camera position so that the object is drawn relative to the
     * camera.
     *
     * @param mdl
     *            the ViewModel to draw
     */
    public static void draw3DAtCamera(ViewModel mdl) {
        viewMatrix.push();
        viewMatrix.set(Matrix4x4.identity());
        mdl.render();
        viewMatrix.pop();
    }

    /**
     * Add fog to the world, making objects further away appear less visible.
     * This does not increase the performance, but allows you to avoid drawing
     * objects that will not be visible to the user.
     *
     * @param density
     *            the density of the fog
     * @param color
     *            the color of the fog
     * @param mode
     *            the mode to use for the fog (1 fades slowly, 2 fades quickly)
     * @see #removeFog()
     */
    @Deprecated
    public static void makeFog(float density, Color color, int mode) {
        glEnable(GL_FOG);

        if (mode == 1) {
            glFogi(GL_FOG_MODE, GL_EXP);
        } else if (mode == 2) {
            glFogi(GL_FOG_MODE, GL_EXP2);
        }

        glFogf(GL_FOG_DENSITY, density);

        FloatBuffer fb = color.getBuffer();

        glFog(GL_FOG_COLOR, fb);
        glHint(GL_FOG_HINT, GL_DONT_CARE);
    }

    /**
     * Set the ambient light of the scene. Pixels will have their color values
     * multiplied by the values of the given Color before being drawn.
     *
     * @param color
     *            the light color
     */
    @Deprecated
    public static void setAmbientLight(Color color) {
        FloatBuffer light = color.getBuffer();

        glLightModel(GL_LIGHT_MODEL_AMBIENT, light);
    }

    public static void setLightingEnabled(boolean lighting) {
        if (lighting && !lightingEnabled) {
            glEnable(GL_LIGHTING);
            lightingEnabled = true;
        } else if (!lighting && lightingEnabled) {
            glDisable(GL_LIGHTING);
            lightingEnabled = false;
        }
    }

    public static boolean lightingEnabled() {
        return lightingEnabled;
    }

    public static void setWireframe(boolean wireframe) {
        if (wireframe && !wireframeOn) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            wireframeOn = true;
        } else if (!wireframe && wireframeOn) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            wireframeOn = false;
        }
    }

    public static boolean wireframe() {
        return wireframeOn;
    }

    public static void setTexturesEnabled(boolean textures) {
        if (textures && !texturesEnabled) {
            glEnable(GL_TEXTURE_2D);
            texturesEnabled = true;
        } else if (!textures && texturesEnabled) {
            glDisable(GL_TEXTURE_2D);
            texturesEnabled = false;
        }
    }

    public static boolean texturesEnabled() {
        return texturesEnabled;
    }

    /**
     * Remove any fog that has been created by makeFog.
     */
    @Deprecated
    public static void removeFog() {
        glDisable(GL_FOG);
    }

    /**
     * Set the background color. For the best results, you should make the
     * background the same color as the fog.
     *
     * @param color
     *            the color to set the background to
     */
    public static void setBackground(Color color) {
        glClearColor(color.red, color.green, color.blue, color.alpha);
    }

    public static void addSkybox(ViewModel model) {
        skyboxes.add(new Entity(model));
    }

    /**
     * Set the current camera viewpoint to the default camera, which is
     * controlled by the camera functions.
     *
     * @see #setCameraViewpoint(engine.core.CameraViewpoint)
     */
    public static void useDefaultCamera() {
        camera = DEFAULT_CAMERA;
    }

    /**
     * The set of supported render modes.
     */
    public static enum ProjectionMode {

        PERSPECTIVE, ORTHOGRAPHIC
    }

    public static boolean fboEnabled() {
        return GLContext.getCapabilities().GL_EXT_framebuffer_object;
    }

    public static int maxTextureUnits() {
        return glGetInteger(GL_MAX_TEXTURE_IMAGE_UNITS);
    }

    public static int maxLights() {
        return glGetInteger(GL_MAX_LIGHTS);
    }

    public static Entity getSelectedSimple(World world, int x, int y, Transform viewpoint, float box) {
        ViewModel hitbox = Resources.box(new Material(ShaderProgram.SELECT), box, box, box);
        clearRenderer(viewpoint);
        ShaderProgram.update();
        selectionBuffer.clear();
        world.forEach((entity) -> {
            if (selectionBuffer.size() < 255) {
                selectionBuffer.add(entity);
            }
        });

        int index = 1;
        for (Entity entity : selectionBuffer) {
            ShaderProgram.SELECT.setUniform("value", index);
            hitbox.transform.setPosition(entity.transform.global().position());
            hitbox.render();
            index++;
        }

        ByteBuffer buff = BufferUtils.createByteBuffer(1);
        glReadPixels(x, y, 1, 1, GL_RED, GL_UNSIGNED_BYTE, buff);
        int i = buff.get() - 1;

        if (i < 0 || i > selectionBuffer.size()) {
            return null;
        } else {
            return selectionBuffer.get(i);
        }
    }

    public static boolean isSelected(Entity entity, int x, int y, Transform viewpoint) {
        clearRenderer(viewpoint);
        ShaderProgram.update();

        ShaderProgram.SELECT.setUniform("value", 1);
        ShaderProgram.lock(ShaderProgram.SELECT);
        entity.render();
        ShaderProgram.unlock();

        ByteBuffer buff = BufferUtils.createByteBuffer(1);
        glReadPixels(x, y, 1, 1, GL_RED, GL_UNSIGNED_BYTE, buff);
        int i = buff.get();

        return i != 0;
    }

    public static Entity getSelected(World world, int x, int y) {
        return getSelected(world, x, y, camera);
    }

    public static Entity getSelected(World world, int x, int y, Transform viewpoint) {
        clearRenderer(viewpoint);
        ShaderProgram.update();
        selectionBuffer.clear();
        world.forEach((entity) -> {
            if (selectionBuffer.size() < 255 && entity.viewModel() != null) {
                selectionBuffer.add(entity);
            }
        });

        int index = 1;
        for (Entity entity : selectionBuffer) {
            ShaderProgram.SELECT.setUniform("value", index);
            ShaderProgram.lock(ShaderProgram.SELECT);
            entity.render();
            ShaderProgram.unlock();
            index++;
        }

        ByteBuffer buff = BufferUtils.createByteBuffer(1);
        glReadPixels(x, y, 1, 1, GL_RED, GL_UNSIGNED_BYTE, buff);
        int i = buff.get() - 1;

        if (i < 0 || i > 255) {
            return null;
        } else {
            return selectionBuffer.get(i);
        }
    }

    public static Hit raycast(World world, Transform viewpoint) {
        Vector3 forward = viewpoint.global().forward();
        Vector3 position = viewpoint.global().position();
        
        return raycast(world, position, forward);
    }
    
    public static Hit raycast(World world, Vector3 position, Vector3 forward) {

        float[] mag2 = { Float.POSITIVE_INFINITY };
        Hit[] hit = { null };

        world.forEach((entity) -> {
            if (entity.collider() != null) {
                Hit h = entity.collider().raycast(position, forward);
                if (h != null) {
                    float d = Vector3.distance2(position, h.position);
                    if (d < mag2[0]) {
                        mag2[0] = d;
                        hit[0] = h;
                    }
                }
            }
        });

        return hit[0];
    }

    public static Hit raycastSimple(World world, Transform viewpoint) {
        Vector3 forward = viewpoint.global().forward();
        Vector3 position = viewpoint.global().position();

        float[] mag2 = { Float.POSITIVE_INFINITY };
        Hit[] hit = { null };

        world.forEach((entity) -> {
            if (entity.collider() != null) {
                if (entity.collider().raycastSimple(position, forward)) {
                    float d = Vector3.distance2(position, entity.transform.position());
                    if (d < mag2[0]) {
                        mag2[0] = d;
                        hit[0] = new Hit(entity.transform.position(), entity);
                    }
                }
            }
        });

        return hit[0];
    }

    public static enum GeometryType {

        POINTS(GL_POINTS), LINES(GL_LINES), LINE_STRIP(GL_LINE_STRIP), LINE_LOOP(GL_LINE_LOOP), POLYGON(GL_POLYGON), QUADS(GL_QUADS), QUAD_STRIP(GL_QUAD_STRIP), TRIANGLES(GL_TRIANGLES), TRIANGLE_STRIP(GL_TRIANGLE_STRIP), TRIANGLE_FAN(GL_TRIANGLE_FAN);

        public final int glEnum;

        GeometryType(int glEnum) {
            this.glEnum = glEnum;
        }
    }
}

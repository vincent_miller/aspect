/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.core;

import aspect.core.Console.LogLevel;
import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.Display.*;
import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CCW;
import static org.lwjgl.opengl.GL11.GL_NICEST;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PERSPECTIVE_CORRECTION_HINT;
import static org.lwjgl.opengl.GL11.GL_RENDERER;
import static org.lwjgl.opengl.GL11.GL_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFrontFace;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL11.glHint;
import static org.lwjgl.opengl.GL11.glShadeModel;
import static org.lwjgl.opengl.GL20.GL_SHADING_LANGUAGE_VERSION;
import static aspect.core.AspectRenderer.*;
import static aspect.resources.Resources.createImage;
import static aspect.resources.Resources.getImage;
import static aspect.resources.Resources.rect;
import static aspect.resources.Resources.sprite;

import aspect.event.KeyEvent;
import aspect.event.KeyListener;
import aspect.event.MouseEvent;
import aspect.event.MouseListener;
import aspect.gui.GUI;
import aspect.input.Controller;
import aspect.render.DrawableTexture;
import aspect.render.Light;
import aspect.render.PointLight;
import aspect.render.Material;
import aspect.render.Texture;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.time.Time;
import aspect.util.Matrix4x4;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;
import aspect.world.ListWorld;
import aspect.world.World;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.PixelFormat;

/**
 * A utility class containing methods for creating and animating the game
 * window.
 *
 * @author MillerV
 */
public class AspectLauncher {

    /**
     * A preset World which is updated and rendered automatically. This field
     * can be changed if a different type of World is desired.
     */
    public static World mainWorld = new ListWorld();

    private static int renderFrames = 0;
    private static int updateFrames = 0;
    private static final Timer fpsTimer = new Timer();
    private static final LinkedBlockingDeque<KeyListener> keyListeners = new LinkedBlockingDeque<>();
    private static final LinkedBlockingDeque<MouseListener> mouseListeners = new LinkedBlockingDeque<>();

    private static float measuredFrameRate;
    private static boolean paused = false;
    private static boolean printFPS = false;
    private static Behavior control;

    public static void main(String[] args) {
        AspectLauncher.run(800, 600, false, 30, new AspectTestApplication());
    }

    /**
     * Run the animation in full screen, calling appropriate event methods in
     * the control Component.
     *
     * @param fps
     *            the desired frame rate (negative value means don't cap)
     * @param control
     *            the main event listener
     */
    public static void run(int fps, Behavior control) {
        AspectLauncher.run(0, 0, true, fps, control);
    }

    public static void run(Canvas canvas, final int fps, final Behavior control) {
        try {
            System.setProperty("org.lwjgl.librarypath", new File("native").getAbsolutePath());
            setParent(canvas);
            setResizable(true);
            canvas.setIgnoreRepaint(true);

            run(100, 100, false, fps, control);
        } catch (LWJGLException ex) {
            Logger.getLogger(AspectLauncher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Run the animation, calling appropriate event methods in the control
     * Component.
     *
     *
     * @param width
     *            the window width
     * @param height
     *            the window height
     * @param fullscreen
     *            whether the window should be full screen
     * @param fps
     *            the desired frame rate (negative value means don't cap)
     * @param control
     *            the main event listener
     * @see #printFPS(int)
     */
    public static void run(final int width, final int height, final boolean fullscreen, final int fps, final Behavior control) {

        final long nspf = 1_000_000_000l / 60;
        System.setProperty("org.lwjgl.librarypath", new File("native").getAbsolutePath());
        System.setProperty("net.java.games.input.librarypath", new File("native").getAbsolutePath());
        AspectLauncher.control = control;

        Canvas parent = getParent();
        
        if (fullscreen) {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            AspectRenderer.width = d.width;
            AspectRenderer.height = d.height;
        } else if (parent == null) {
            AspectRenderer.width = width;
            AspectRenderer.height = height;
        } else {
            AspectRenderer.width = parent.getWidth();
            AspectRenderer.height = parent.getHeight();
        }

        BufferedImage iconFlipped = getImage("Icon", AspectLauncher.class.getResourceAsStream("icon.png"));
        BufferedImage iconNormal = createImage(iconFlipped.getWidth(), iconFlipped.getHeight());
        iconNormal.createGraphics().drawImage(iconFlipped, iconFlipped.getWidth(), iconFlipped.getHeight(), -iconFlipped.getWidth(), -iconFlipped.getHeight(), null);

        new Thread() {
            @Override
            public void run() {
                JFrame window = null;
                try {
                    if (fullscreen) {
                        setFullscreen(true);
                    } else {
                        setDisplayMode(new DisplayMode(width, height));
                    }
                    Canvas c = new Canvas();
                    if (parent == null) {
                        setParent(c);
                        BufferedImage loading = getImage("Loading", AspectLauncher.class.getResourceAsStream("loading.png"));
                        window = new JFrame();
                        window.setUndecorated(true);
                        window.setIconImage(iconNormal);
                        window.setSize(loading.getWidth(), loading.getHeight());
                        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
                        window.setLocation((size.width - loading.getWidth()) / 2, (size.height - loading.getHeight()) / 2);
                        window.getContentPane().add(new JPanel() {
                            @Override
                            public void paintComponent(Graphics g) {
                                super.paintComponent(g);
                                g.drawImage(loading, 0, 0, null);
                            }
                        });
                        window.setVisible(true);
                        window.repaint();
                        window.add(c);
                    }
                    setTitle("Aspect");
                    setIcon(iconFlipped);
                    create(new PixelFormat(8, 8, 8));
                } catch (LWJGLException ex) {
                    Logger.getLogger(AspectLauncher.class.getName()).log(Level.SEVERE, null, ex);
                    System.exit(0);
                }
                printGLVersion();

                glEnable(GL_ALPHA_TEST);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glShadeModel(GL_SMOOTH);
                glFrontFace(GL_CCW);
                glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

                ShaderProgram.initialize();

                Time.tick();
                GUI.initialize();
                PauseMenu.initialize();
                Console.initialize();

                control.onAdd();

                if (parent == null) {
                    try {
                        setParent(parent);
                    } catch (LWJGLException e) {
                        Console.log(LogLevel.ERROR, e);
                    }
                    window.setVisible(false);
                    window.dispose();
                }

                while (!isCloseRequested()) {
                    Controller.updateAll();

                    while (Mouse.next()) {
                        int button = Mouse.getEventButton();
                        int x = Mouse.getEventX();
                        int y = Mouse.getEventY();
                        int dx = Mouse.getEventDX();
                        int dy = Mouse.getEventDY();
                        int w = Mouse.getEventDWheel();
                        boolean state = Mouse.getEventButtonState();
                        MouseEvent evt = new MouseEvent(button, x, y, dx, dy, w, state);
                        if (!paused) {
                            for (MouseListener c : mouseListeners) {
                                c.mouseEvent(evt);
                            }
                        }

                        GUI.mouseEvent(evt);
                    }

                    while (Keyboard.next()) {
                        char ch = Keyboard.getEventCharacter();
                        int key = Keyboard.getEventKey();
                        boolean state = Keyboard.getEventKeyState();
                        KeyEvent evt = new KeyEvent(key, ch, state);

                        if (evt.key == Keyboard.KEY_ESCAPE && evt.type && !Console.enabled()) {
                            setPaused(!paused);
                            continue;
                        }

                        if (evt.key == Keyboard.KEY_GRAVE && evt.type) {
                            Console.setEnabled(!Console.enabled());
                            continue;
                        }

                        if (!paused) {
                            for (KeyListener c : keyListeners) {
                                c.keyEvent(evt);
                            }
                        }
                        GUI.keyEvent(evt);
                    }

                    if (!paused) {
                        long ns = System.nanoTime();

                        if (fps <= 0) {
                            do {
                                Time.tick();
                                control.update();
                                mainWorld.update();

                                for (Entity skybox : skyboxes) {
                                    skybox.update();
                                }

                                updateFrames++;
                            } while (System.nanoTime() - ns < nspf / 1.1);
                        } else {
                            Time.tick();
                            measuredFrameRate = 1.0f / Time.deltaTime();
                            control.update();
                            mainWorld.update();

                            for (Entity skybox : skyboxes) {
                                skybox.update();
                            }

                            updateFrames++;
                        }
                    }

                    clearRenderer();
                    updateProjection(ProjectionMode.PERSPECTIVE);
                    ShaderProgram.update();

                    if (fbo != null) {
                        fbo.viewpoint = camera;
                        fbo.capture();
                    }

                    for (Entity skybox : skyboxes) {
                        skybox.transform.setPosition(camera.global().position());
                        skybox.transform.setOrthogonal(true);
                        skybox.render();
                        clearDepthBuffer();
                    }

                    mainWorld.render();
                    control.render();

                    if (fbo != null) {
                        fbo.stop();
                        fbo.render();
                    }

                    GUI.show();

                    renderFrames++;

                    update();

                    if (fps > 0) {
                        sync(fps);
                    }
                }

                control.onRemove();

                Display.destroy();
                fpsTimer.cancel();
                System.exit(0);
            }
        }.start();

        printFPS(1.0f);
    }

    public static void restart() {
        mainWorld.clear();
        Light.clear();
        keyListeners.clear();
        mouseListeners.clear();
        Controller.clearListeners();
        control.onAdd();
    }

    public static boolean paused() {
        return paused;
    }

    public static void setPaused(boolean paused) {
        AspectLauncher.paused = paused;
        GUI.setOverlayOnly(paused);
        PauseMenu.setEnabled(paused);
    }

    public static float measuredFrameRate() {
        return measuredFrameRate;
    }

    public static void setName(String name) {
        setTitle(name);
    }

    public static void setIcon(BufferedImage icon) {
        ByteBuffer[] buffers;

        String OS = System.getProperty("os.name").toUpperCase();
        if (OS.contains("WIN")) {
            buffers = new ByteBuffer[2];
            buffers[0] = loadInstance(icon, 16);
            buffers[1] = loadInstance(icon, 32);
        } else if (OS.contains("MAC")) {
            buffers = new ByteBuffer[1];
            buffers[0] = loadInstance(icon, 128);
        } else {
            buffers = new ByteBuffer[1];
            buffers[0] = loadInstance(icon, 32);
        }

        Display.setIcon(buffers);
    }

    private static ByteBuffer loadInstance(BufferedImage image, int size) {
        BufferedImage newImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB_PRE);
        newImage.createGraphics().drawImage(image.getScaledInstance(size, -size, BufferedImage.SCALE_SMOOTH), 0, 0, null);
        return Texture.convertImageData(newImage);
    }

    public static void addKeyListener(KeyListener c) {
        if (!keyListeners.contains(c)) {
            keyListeners.add(c);
        }
    }

    public static void addMouseListener(MouseListener c) {
        if (!mouseListeners.contains(c)) {
            mouseListeners.add(c);
        }
    }

    public static void setPrintFPS(boolean b) {
        printFPS = b;
    }

    /**
     * Print out the measured frame rate at the specified interval.
     *
     * @param interval
     *            the interval in milliseconds
     */
    private static void printFPS(final float interval) {
        int intervalMillis = (int) (interval * 1000);
        fpsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (printFPS) {
                    String s = "FPS: (Render: " + (renderFrames / interval) + ", Update: " + (updateFrames / interval) + ")";
                    Console.println(s);
                }
                renderFrames = 0;
                updateFrames = 0;
            }
        }, intervalMillis, intervalMillis);
    }
}

package aspect.core;

import static org.lwjgl.opengl.GL11.GL_RENDERER;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL20.GL_SHADING_LANGUAGE_VERSION;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import org.lwjgl.opengl.GL11;

import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.gui.GUI;
import aspect.gui.Rectangle;
import aspect.render.DrawableTexture;
import aspect.render.PointLight;
import aspect.render.Material;
import aspect.render.Texture;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.time.Time;
import aspect.util.Vector2;
import aspect.util.Vector3;

class AspectTestApplication extends Behavior {
    private DrawableTexture canvas;
    private Entity cube;

    @Override
    public void onAdd() {
        canvas = new DrawableTexture(1024, 1024);

        Graphics2D g = canvas.getGraphics();
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        g.setColor(Color.BLUE);
        g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
        g.drawString("Aspect is Running", 0, 20);
        g.setColor(Color.YELLOW);
        g.drawString("Shaders:", 0, 100);
        drawShaderString(ShaderProgram.PHONG_FRAGMENT, "Phong Fragment", g, 0, 130);
        drawShaderString(ShaderProgram.PHONG_VERTEX, "Phong Vertex", g, 0, 160);
        drawShaderString(ShaderProgram.TEXTURE, "Texture", g, 0, 190);
        drawShaderString(ShaderProgram.COLOR_UNIFORM, "Color", g, 0, 220);
        g.setColor(Color.YELLOW);
        g.drawString("OpenGL Version Info:", 0, 300);
        g.setColor(Color.BLACK);

        g.drawString("GL VENDOR: " + glGetString(GL_VENDOR), 0, 330);
        g.drawString("GL RENDERER: " + glGetString(GL_RENDERER), 0, 360);
        g.drawString("GL VERSION: " + glGetString(GL_VERSION), 0, 390);
        g.drawString("GLSL VERSION: " + glGetString(GL_SHADING_LANGUAGE_VERSION), 0, 420);
        canvas.updateTexture();

        cube = new Entity(Resources.getModel("models/a.obj", null));
        cube.transform.setPosition(new Vector3(1.0f, 1.0f, -4.0f));
        PointLight l = new PointLight();

        AspectRenderer.setLightingEnabled(true);
    }

    private void drawShaderString(ShaderProgram shader, String name, Graphics2D pen, int x, int y) {
        boolean loaded = shader != null && shader.programID != 0;
        if (loaded) {
            pen.setColor(Color.GREEN);
        } else {
            pen.setColor(Color.RED);
        }

        pen.drawString(name + ": " + (loaded ? "loaded" : "not loaded"), x, y);
    }

    @Override
    public void update() {
        cube.transform.yawLeft(Time.deltaTime());
    }

    @Override
    public void render() {
        canvas.renderModel(new Vector2(0, 0));
        cube.render();
    }
}
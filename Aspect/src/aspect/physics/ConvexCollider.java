/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics;

import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.render.Material;
import aspect.render.Texture;
import aspect.resources.Resources;
import aspect.resources.modeling.Vertex;
import aspect.util.BoundingBox;
import aspect.util.Matrix3x3;
import aspect.util.Matrix4x4;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author MillerV
 */
public class ConvexCollider extends Collider {

    private final Vector3[] vertices;
    private final Vector3[] normals;
    private final Vector3[] edges;
    public final int[][] faces;

    public final float radius;

    public final int numVertices;
    public final int numNormals;
    public final int numEdges;

    private final BoundingBox bounds;

    private static final float maxAv = 0.001f;

    public ConvexCollider(Vector3[] vertices, Vector3[] normals, Vector3[] edges, int[][] faces) {
        this.vertices = vertices;
        this.normals = normals;
        this.edges = edges;
        this.numVertices = vertices.length;
        this.numNormals = normals.length;
        this.numEdges = edges.length;
        this.faces = faces;

        float r = 0.0f;
        for (Vector3 vertex : vertices) {
            r = Math.max(r, vertex.mag());
        }

        this.radius = r;

        this.bounds = BoundingBox.cube(r);
    }

    public ConvexCollider copy() {
        return new ConvexCollider(vertices, normals, edges, faces);
    }

    public BoundingBox bounds() {
        bounds.setCenter(entity.transform.position());
        return bounds;
    }

    public Vector3[] getVertices() {
        Vector3[] tverts = new Vector3[numVertices];
        Matrix4x4 m = entity.transform.modelMatrix();

        for (int i = 0; i < numVertices; i++) {
            tverts[i] = m.transformPoint(vertices[i]);
        }

        return tverts;
    }

    public Vector3[] getNormals() {
        Vector3[] tnorms = new Vector3[numNormals];
        getNormals(tnorms, 0);
        return tnorms;
    }

    private int getNormals(Vector3[] v, int start) {
        Matrix4x4 m = entity.transform.modelMatrix().invert().transpose();

        for (int i = 0; i < numNormals; i++) {
            v[start + i] = m.transformVector(normals[i]);
        }

        return numNormals;
    }

    public Vector3[] getEdges() {
        Vector3[] tedges = new Vector3[numEdges];
        getEdges(tedges, 0);
        return tedges;
    }

    private int getEdges(Vector3[] v, int start) {
        Matrix4x4 m = entity.transform.modelMatrix();

        for (int i = 0; i < numEdges; i++) {
            v[start + i] = m.transformVector(edges[i]);
        }

        return numEdges;
    }

    public Collision colliding(Collider collider) {
        if (!(collider instanceof ConvexCollider)) {
            return collider.colliding(this);
        }
        
        ConvexCollider c = (ConvexCollider) collider;

        float maxRadius = radius + c.radius;
        if (Vector3.distance2(entity.transform.position(), c.entity.transform.position()) > maxRadius * maxRadius) {
            return null;
        }

        Vector3 displacement = Vector3.zero();
        Vector3 normal = Vector3.zero();

        Vector3[] verts = getVertices();
        Vector3[] cverts = c.getVertices();

        int pos = 0;
        Vector3[] axes = new Vector3[numNormals + c.numNormals + numEdges * c.numEdges];

        if (axes.length == 0) {
            return null;
        }
        // Vector3[] axes = {new Vector3(0, 1, 0)};

        pos += getNormals(axes, pos);
        pos += c.getNormals(axes, pos);

        Vector3[] edges = getEdges();
        Vector3[] cedges = c.getEdges();

        for (int i = 0; i < numEdges; i++) {
            for (int j = 0; j < c.numEdges; j++) {
                Vector3 cross = Vector3.cross(edges[i], cedges[j]);
                if (cross.mag2() != 0.0f) {
                    axes[pos++] = cross.normalize();
                } else {
                    axes[pos++] = null;
                }
            }
        }
        float dist = Float.POSITIVE_INFINITY;

        // System.out.println(Arrays.toString(axes));
        Vector3 min1v = null;
        Vector3 max1v = null;
        Vector3 min2v = null;
        Vector3 max2v = null;

        int i = 0;
        boolean side = false;
        int normalIndex = 0;
        for (Vector3 n : axes) {
            if (n == null) {
                i++;
                continue;
            }

            float min1 = Float.POSITIVE_INFINITY;
            float max1 = Float.NEGATIVE_INFINITY;
            float min2 = Float.POSITIVE_INFINITY;
            float max2 = Float.NEGATIVE_INFINITY;

            Vector3 min1vt = null;
            Vector3 max1vt = null;
            Vector3 min2vt = null;
            Vector3 max2vt = null;

            for (Vector3 v : verts) {
                float proj = Vector3.dot(v, n);
                if (proj < min1) {
                    min1 = proj;
                    min1vt = v;
                }
                if (proj > max1) {
                    max1 = proj;
                    max1vt = v;
                }
            }

            for (Vector3 v : cverts) {
                float proj = Vector3.dot(v, n);
                if (proj < min2) {
                    min2 = proj;
                    min2vt = v;
                }
                if (proj > max2) {
                    max2 = proj;
                    max2vt = v;
                }
            }

            if (min1 <= min2 && min2 <= max1 || min2 <= min1 && min1 <= max2) {

                if (max1 >= min2 && max1 - min2 < dist) {
                    dist = max1 - min2;
                    displacement = n.times(-dist);
                    normal = n.negate();
                    side = false;
                    normalIndex = i;
                    min1v = min1vt;
                    max1v = max1vt;
                    min2v = min2vt;
                    max2v = max2vt;
                }

                if (max2 >= min1 && max2 - min1 < dist) {
                    dist = max2 - min1;
                    displacement = n.times(dist);
                    normal = n;
                    side = true;
                    normalIndex = i;
                    min1v = min1vt;
                    max1v = max1vt;
                    min2v = min2vt;
                    max2v = max2vt;
                }

            } else {
                return null;
            }

            i++;
        }

        Vector3 ev1, ev2;

        if (side) {
            ev1 = min1v;
            ev2 = max2v;
        } else {
            ev1 = max1v;
            ev2 = min2v;
        }

        LinkedList<Vector3> points = new LinkedList<>();

        if (normalIndex < numNormals) {
            float proj = Vector3.dot(ev2, normal);
            for (Vector3 v : cverts) {
                float f = Math.abs(Vector3.dot(v, normal) - proj);
                if (f < maxAv) {
                    points.add(v);
                }
            }
        } else if (normalIndex < numNormals + c.numNormals) {

            float proj = Vector3.dot(ev1, normal);
            for (Vector3 v : verts) {
                float f = Math.abs(Vector3.dot(v, normal) - proj);
                if (f < maxAv) {
                    points.add(v);
                }
            }

        } else {

            int e = normalIndex - (numNormals + c.numNormals);

            Vector3 v1 = Vector3.cross(edges[e / c.numEdges], normal);
            Vector3 v2 = Vector3.cross(cedges[e % c.numEdges], normal);

            float off1 = Vector3.dot(v1, ev1);
            float off2 = Vector3.dot(v2, ev2);
            float off3 = Vector3.dot(normal, ev2);

            Vector3 v4 = new Vector3(off1, off2, off3);
            Matrix3x3 m = new Matrix3x3(v1, v2, normal);

            points.add(m.invert().transform(v4));
        }

        return new Collision(points, normal, displacement, c.entity);
    }

    public boolean raycastSimple(Vector3 origin, Vector3 ray) {
        Vector3 pos = entity.transform.position().minus(origin);

        if (Vector3.dot(pos, ray) < 0) {
            return false;
        }

        return Vector3.cross(pos.normalize(), ray).mag2() < radius * radius / pos.mag2();
    }

    public Hit raycast(Vector3 origin, Vector3 ray) {
        if (!raycastSimple(origin, ray)) {
            return null;
        }

        Vector3 nearest = null;
        float dist2 = Float.POSITIVE_INFINITY;

        Vector3[] vertices = getVertices();
        for (int[] face : faces) {
            Vector3 v1 = vertices[face[0]];
            Vector3 v2 = vertices[face[1]];
            Vector3 v3 = vertices[face[2]];
            Vector3 normal = Resources.normal(v1, v2, v3);

            if (Vector3.dot(normal, ray) == 0.0f) {
                continue;
            }

            Vector3 w = origin.minus(v1);

            float si = -Vector3.dot(normal, w) / Vector3.dot(normal, ray);
            Vector3 rv = ray.times(si);

            float mag2 = rv.mag2();
            if (si > 0 && mag2 < dist2) {
                Vector3 point = origin.plus(rv);

                boolean pass = true;

                for (int i = 0; i < face.length; i++) {
                    int j = (i + 1) % face.length;
                    v1 = vertices[face[i]];
                    v2 = vertices[face[j]];
                    Vector3 edge = v2.minus(v1);
                    Vector3 vec = point.minus(v1);
                    Vector3 cross = Vector3.cross(edge, normal);

                    if (Vector3.dot(vec, cross) > 0) {
                        pass = false;
                        break;
                    }
                }

                if (pass) {
                    dist2 = mag2;
                    nearest = point;
                }
            }
        }

        if (nearest != null) {
            return new Hit(nearest, entity);
        } else {
            return null;
        }
    }

    public static ConvexCollider rect(float w, float h) {
        Vector3[] vertices = new Vector3[4];
        Vector3[] normals = new Vector3[1];
        Vector3[] edges = new Vector3[2];

        w /= 2;
        h /= 2;

        vertices[0] = new Vector3(w, h);
        vertices[1] = new Vector3(w, -h);
        vertices[2] = new Vector3(-w, -h);
        vertices[3] = new Vector3(-w, h);

        normals[0] = Vector3.zAxis();

        edges[0] = Vector3.yAxis();
        edges[1] = Vector3.xAxis();

        int[][] faces = { { 0, 1, 2, 3 } };

        return new ConvexCollider(vertices, normals, edges, faces);
    }

    public static ConvexCollider box(float w, float h, float d) {
        Vector3[] vertices = new Vector3[8];
        Vector3[] normals = new Vector3[3];
        Vector3[] edges = normals;

        w /= 2;
        h /= 2;
        d /= 2;

        vertices[0] = new Vector3(w, h, d);
        vertices[1] = new Vector3(w, h, -d);
        vertices[2] = new Vector3(w, -h, -d);
        vertices[3] = new Vector3(w, -h, d);
        vertices[4] = new Vector3(-w, h, d);
        vertices[5] = new Vector3(-w, h, -d);
        vertices[6] = new Vector3(-w, -h, -d);
        vertices[7] = new Vector3(-w, -h, d);

        normals[0] = Vector3.xAxis();
        normals[1] = Vector3.yAxis();
        normals[2] = Vector3.zAxis();

        int[][] faces = { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 0, 1, 5, 4 }, { 2, 3, 7, 6 }, { 0, 3, 7, 4 }, { 1, 2, 6, 5 } };

        return new ConvexCollider(vertices, normals, edges, faces);
    }

    public static ConvexCollider point(Vector3 point) {
        return new ConvexCollider(new Vector3[] { point }, new Vector3[0], new Vector3[0], new int[0][0]);
    }

    public static ConvexCollider model(String file, Vector3 scale) {
        String obj = Resources.loadTextFile(file);

        ArrayList<Vector3> allVertices = new ArrayList<>(100);
        ArrayList<Vector3> allNormals = new ArrayList<>(100);
        ArrayList<int[]> allFaces = new ArrayList<>(20);

        for (String s : obj.trim().split("\n")) {
            s = s.trim();
            String[] parts = s.split(" ");
            if (s.startsWith("#") || s.isEmpty()) {
                continue;
            }
            switch (parts[0]) {
                case "v": {
                    float x = Float.parseFloat(parts[1]);
                    float y = Float.parseFloat(parts[2]);
                    float z = Float.parseFloat(parts[3]);
                    allVertices.add(new Vector3(x, y, z).componentTimes(scale));
                    break;
                }
                case "vn": {
                    float x = Float.parseFloat(parts[1]);
                    float y = Float.parseFloat(parts[2]);
                    float z = Float.parseFloat(parts[3]);
                    allNormals.add(new Vector3(x / scale.x, y / scale.y, z / scale.z).normalize());
                    break;
                }
                case "f": {
                    int[] face = new int[parts.length - 1];
                    for (int i = 0; i < parts.length - 1; i++) {
                        String[] info = parts[i + 1].split("/");
                        int vertex = Integer.parseInt(info[0]);
                        if (vertex < 0) {
                            vertex = allVertices.size() + vertex;
                        } else {
                            vertex--;
                        }

                        face[i] = vertex;
                    }
                    allFaces.add(face);
                    break;
                }
            }
        }

        Vector3[] empty = new Vector3[0];
        return new ConvexCollider(allVertices.toArray(empty), allNormals.toArray(empty), empty, allFaces.toArray(new int[0][0]));
    }

    @Override
    public float radius() {
        return radius;
    }
}

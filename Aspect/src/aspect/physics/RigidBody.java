/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics;

import aspect.core.AspectLauncher;
import aspect.entity.Entity;
import aspect.physics.dynamics.Force;
import aspect.render.Material;
import aspect.resources.Resources;
import aspect.time.Time;
import aspect.util.Debug;
import aspect.util.Matrix3x3;
import aspect.util.Vector3;
import aspect.world.World;

import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author MillerV
 */
public class RigidBody extends Motion {

    public float mass = 1f; // kg
    public Matrix3x3 inverseInertiaTensor; // kg * m ^ 2
    public float coefficientOfFriction = 0.5f;
    public float coefficientOfRestitution = 0.0f;

    private Vector3 accOld;
    public Vector3 angularMomentum;

    private Collection<Vector3> lastContact;
    private float lastImpulse;
    private Vector3 lastNormal;

    public boolean collisions = true;
    // private Vector3 rAccOld;

    public LinkedList<Force> forces = new LinkedList<>();

    public World world;

    public RigidBody() {
        this(AspectLauncher.mainWorld);
    }

    public RigidBody(World world) {
        super();
        this.world = world;
        this.inverseInertiaTensor = Matrix3x3.identity();

        accOld = Vector3.zero();
        angularMomentum = Vector3.zero();
    }

    public Matrix3x3 inverseInertiaTensor() {
        Matrix3x3 m = entity.transform.modelMatrix().getRotation();
        return m.times(inverseInertiaTensor.times(m.transpose()));
    }

    public void setMomentsOfInertia(Vector3 moi) {
        inverseInertiaTensor = Matrix3x3.zero();
        inverseInertiaTensor.m00 = 1.0f / moi.x;
        inverseInertiaTensor.m11 = 1.0f / moi.y;
        inverseInertiaTensor.m22 = 1.0f / moi.z;
    }

    public void setUniformMOI(float moi) {
        setMomentsOfInertia(new Vector3(moi));
    }

    public void impel(Vector3 impulse, Vector3 point) {
        impelLinear(impulse);
        impelAngular(Vector3.cross(point, impulse));
    }

    public void impelLinear(Vector3 impulse) {
        velocity = velocity.plus(Vector3.divide(impulse, mass));
    }

    public void impelAngular(Vector3 impulse) {
        angularMomentum = angularMomentum.plus(impulse);
    }

    public void addForce(Force force) {
        forces.add(force);
    }

    public Vector3 netForce() {
        Vector3 sigmaForce = Vector3.zero();
        for (Force force : forces) {
            sigmaForce = sigmaForce.plus(force.getForce(entity));
        }

        if (world != null) {
            for (Force force : world.forces()) {
                sigmaForce = sigmaForce.plus(force.getForce(entity));
            }
        }

        return sigmaForce;
    }

    @Override
    public void onCollision(Collision col) {
        if (!collisions) {
            return;
        }

        // ent.transform.position = ent.transform.position.plus(displacement);
        lastContact = col.points;

        boolean both = true;

        RigidBody rb2 = col.other.rigidBody();

        float moiA, moiB;

        if (rb2 == null) {
            rb2 = new RigidBody();
            rb2.freezePosition = true;
            rb2.freezeRotation = true;
            rb2.mass = Float.POSITIVE_INFINITY;
            both = false;
        }
        
        if (rb2.entity == null || rb2.freezePosition == true) {
            entity.transform.moveAbsolute(col.displacement);
        } else {
            float magt = (velocity.mag() * mass) + (rb2.velocity.mag() * rb2.mass);
            float f1;
            if (magt == 0.0f) {
                f1 = mass / (mass + rb2.mass);
            } else {
                f1 = velocity.mag() * mass / magt;
            }

            entity.transform.moveAbsolute(col.displacement.times(f1));
        }

        for (Vector3 contact : col.points) {
            Vector3 pos1 = contact.minus(entity.transform.position());
            Vector3 pos2 = contact.minus(col.other.transform.position());

            Vector3 va = velocity.plus(Vector3.cross(angularVelocity, pos1));
            Vector3 vb = rb2.velocity.plus(Vector3.cross(rb2.angularVelocity, pos2));

            Vector3 vr = va.minus(vb);

            // Normal impulse
            float num = -Vector3.dot(vr, col.normal) * (1 + coefficientOfRestitution * rb2.coefficientOfRestitution);
            float denom1 = 1.0f / mass + 1.0f / rb2.mass;

            Vector3 cross1 = Vector3.cross(inverseInertiaTensor().transform(Vector3.cross(pos1, col.normal)), pos1);
            Vector3 cross2 = Vector3.zero();
            if (rb2.entity != null) {
                cross2 = Vector3.cross(rb2.inverseInertiaTensor().transform(Vector3.cross(pos2, col.normal)), pos2);
            }

            Vector3 cross = cross1.plus(cross2);
            float denom2 = Vector3.dot(col.normal, cross);
            float denom = denom1 + denom2;
            float impulse = num / denom;

            Vector3 impulseN = col.normal.times(impulse / col.points.size());
            impel(impulseN, pos1);

            // Friction
            Vector3 tangent = Vector3.cross(Vector3.cross(vr, col.normal), col.normal).normalize();
            if (tangent.mag2() == 0.0f) {
                return;
            }

            num = -Vector3.dot(vr, tangent) * (coefficientOfFriction * rb2.coefficientOfFriction);
            cross1 = Vector3.cross(inverseInertiaTensor().transform(Vector3.cross(pos1, tangent)), pos1);
            cross2 = Vector3.zero();
            if (rb2.entity != null) {
                cross2 = Vector3.cross(inverseInertiaTensor().transform(Vector3.cross(pos2, tangent)), pos2);
            }

            cross = cross1.plus(cross2);
            denom2 = Vector3.dot(tangent, cross);
            denom = denom1 + denom2;
            float friction = num / denom;
            Vector3 frictionT = tangent.times(friction / col.points.size());
            impel(frictionT, pos1);
        }
    }

    @Override
    public void update() {
        Vector3 newAcc = Vector3.divide(netForce(), mass);
        velocity = velocity.plus(Vector3.divide(newAcc.plus(accOld), 2).times(Time.deltaTime()));
        accOld = newAcc;

        angularVelocity = inverseInertiaTensor.transform(angularMomentum);
        super.update();
    }

    public static Entity box(Material material, float width, float height, float depth, float density) {
        Entity entity;
        if (material != null) {
            try {
                entity = new Entity(Resources.box(material, width, height, depth));

            } catch (Throwable thr) {

                entity = new Entity();
            }
        } else {
            entity = new Entity();
        }

        entity.addBehavior(ConvexCollider.box(width, height, depth));
        entity.addBehavior(new RigidBody());

        float mass = width * height * depth * density;

        entity.rigidBody().mass = mass;

        float w2 = width * width;
        float h2 = height * height;
        float d2 = depth * depth;

        entity.rigidBody().inverseInertiaTensor.m00 = 12.0f / (mass * h2 * d2);
        entity.rigidBody().inverseInertiaTensor.m11 = 12.0f / (mass * w2 * d2);
        entity.rigidBody().inverseInertiaTensor.m22 = 12.0f / (mass * w2 * h2);

        return entity;
    }
}

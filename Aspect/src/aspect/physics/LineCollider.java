/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics;

import aspect.entity.Entity;
import aspect.util.Angles;
import aspect.util.Matrix4x4;
import aspect.util.Vector2;
import aspect.util.Vector3;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author vincent
 */
public class LineCollider {
    private Vector3 v1;
    private Vector3 v2;
    private Entity entity;
    
    public LineCollider(Entity entity, float width) {
        this.entity = entity;
        this.v1 = new Vector3(-width / 2, 0, 0);
        this.v2 = new Vector3(width / 2, 0, 0);
    }

    public boolean crossed(Vector2 l1, Vector2 l2) {
        Matrix4x4 m = entity.transform.modelMatrix();
        
        Vector2 v3 = m.transformPoint(v1).xz();
        Vector2 v4 = m.transformPoint(v2).xz();
        
        return Vector2.checkIntersection(l1, l2, v3, v4);
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.physics;

/**
 *
 * @author MillerV
 */
public class PhysicsConstants {
    /**
     * The universal gravitational constant
     */
    public static float G = 6.67384e-11f;
    /**
     * The electric permittivity of free space
     */
    public static float E0 = 8.85418782e-12f;
    /**
     * The magnetic permeability of free space
     */
    public static float U0 = 1.25663706e-6f;
}

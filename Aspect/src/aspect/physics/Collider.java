package aspect.physics;

import aspect.entity.behavior.Behavior;
import aspect.util.BoundingBox;
import aspect.util.Vector3;

public abstract class Collider extends Behavior {
    public abstract BoundingBox bounds();
    public abstract Collision colliding(Collider c);
    public abstract boolean raycastSimple(Vector3 origin, Vector3 ray);
    public abstract Hit raycast(Vector3 origin, Vector3 ray);
    public abstract float radius();
}

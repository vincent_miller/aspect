/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics;

import aspect.time.Time;
import aspect.util.Vector3;
import aspect.entity.behavior.Behavior;
import aspect.util.Matrix4x4;
import aspect.util.Trig;

import java.util.LinkedList;

/**
 *
 * @author MillerV
 */
public class Motion extends Behavior {

    private Vector3 velOld;
    private Vector3 rVelOld;

    public boolean freezePosition = false;
    public boolean freezeRotation = false;

    public Vector3 positionFreezeAxis = Vector3.zero();
    public Vector3 rotationFreezeAxis = Vector3.zero();

    public Vector3 velocity; // meters / second

    public Vector3 angularVelocity;

    public Motion() {
        this(Vector3.zero(), Vector3.zero());
    }

    public Motion(Vector3 vel) {
        this(vel, Vector3.zero());
    }

    public Motion(Vector3 vel, Vector3 rvel) {
        this.velocity = vel;
        this.angularVelocity = rvel;

        velOld = Vector3.zero();
        rVelOld = Vector3.zero();
    }

    @Override
    public void update() {
        if (freezePosition) {
            velocity = positionFreezeAxis.times(Vector3.dot(velocity, positionFreezeAxis));
        }

        if (velocity.mag2() > 1e6f) {
            velocity = velocity.normalize().times(1e3f);
        }

        Vector3 avgVel = Vector3.divide(velOld.plus(velocity), 2.0f);
        entity.transform.moveAbsolute(avgVel.times(Time.deltaTime()));
        velOld = velocity.copy();
        
        if (freezeRotation) {
            angularVelocity = rotationFreezeAxis.times(Vector3.dot(angularVelocity, rotationFreezeAxis));
        }

        if (angularVelocity.mag2() > Trig.FULL_CIRCLE * Trig.FULL_CIRCLE * 1e4f) {
            angularVelocity = angularVelocity.normalize().times(Trig.FULL_CIRCLE * 1e2f);
        }

        Vector3 avgRVel = Vector3.divide(rVelOld.plus(angularVelocity), 2.0f);

        if (avgRVel.mag2() > 0.0f) {
            entity.transform.rotate(avgRVel, Time.deltaTime());
        }

        rVelOld = angularVelocity.copy();

    }
}

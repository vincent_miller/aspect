package aspect.physics;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import aspect.util.BoundingBox;
import aspect.util.Vector3;

public class ConcaveCollider extends Collider {
    private LinkedList<ConvexCollider> colliders = new LinkedList<>();
    public final float radius;
    public final BoundingBox bounds;

    public ConcaveCollider(Collection<ConvexCollider> colliders) {
        float r = 0;
        for (ConvexCollider collider : colliders) {
            this.colliders.add(collider);
            r = Math.max(r, collider.radius());
        }
        radius = r;
        bounds = BoundingBox.cube(r);
    }

    public ConcaveCollider(ConvexCollider... colliders) {
        this(Arrays.asList(colliders));
    }

    @Override
    public BoundingBox bounds() {
        BoundingBox b = bounds.copy();
        b.setCenter(entity.transform.position());
        return b;
    }

    @Override
    public Collision colliding(Collider col) {
        if (col instanceof ConcaveCollider) {
            for (Collider collider : ((ConcaveCollider) col).colliders) {
                Collision collision = colliding(collider);
                if (collision != null) {
                    return collision;
                }
            }

            return null;
        } else if (col instanceof ConvexCollider) {
            for (Collider collider : colliders) {
                Collision collision = collider.colliding(col);
                if (collision != null) {
                    return collision;
                }
            }

            return null;
        } else {
            throw new IllegalArgumentException("Unrecognized Collider Type");
        }
    }

    @Override
    public boolean raycastSimple(Vector3 origin, Vector3 ray) {
        for (Collider collider : colliders) {
            if (collider.raycastSimple(origin, ray)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public Hit raycast(Vector3 origin, Vector3 ray) {
        Hit hit = null;
        float mag2 = Float.POSITIVE_INFINITY;
        
        for (Collider collider : colliders) {
            Hit h = collider.raycast(origin, ray);
            if (h != null) {
                float d = Vector3.distance2(origin, h.position);
                if (d < mag2) {
                    mag2 = d;
                    hit = h;
                }
            }
        }
        
        return hit;
    }

    @Override
    public float radius() {
        return radius;
    }
}

/*
 * Copyright (C) 2014 millerv
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics;

import aspect.entity.Entity;
import aspect.util.Vector3;
import java.util.Collection;

/**
 *
 * @author millerv
 */
public class Collision {
    public final Collection<Vector3> points;
    public final Vector3 normal;
    public final Vector3 displacement;
    public final Entity other;
    
    public Collision(Collection<Vector3> points, Vector3 normal, Vector3 displacement, Entity other) {
        this.points = points;
        this.normal = normal;
        this.displacement = displacement;
        this.other = other;
    }
}

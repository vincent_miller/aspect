/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.physics.dynamics;

import aspect.entity.Entity;
import aspect.util.Vector3;

/**
 *
 * @author MillerV
 */
public class DynamicPointForce extends PointForce {

    public Entity ent;

    public DynamicPointForce(Entity ent, float strength, IntensityModel model) {
        super(ent.transform.position(), strength, model);
        this.ent = ent;
    }

    @Override
    public Vector3 getForce(Entity ent2) {
        pos = ent.transform.position();

        if (ent == ent2) {
            return Vector3.zero();
        } else {
            return super.getForce(ent2);
        }
    }
}

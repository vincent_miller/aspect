/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.physics.dynamics;

import aspect.util.Vector3;
import aspect.entity.Entity;

/**
 *
 * @author MillerV
 */
public class UniformGravity implements Force {
    public Vector3 acc;
    
    public UniformGravity(Vector3 acc) {
        this.acc = acc;
    }
    
    public UniformGravity() {
        this.acc = new Vector3(0, -9.8f, 0);
    }
    
    public void setAcceleration(Vector3 acc) {
        this.acc = acc;
    }

    @Override
    public Vector3 getForce(Entity ent) {
        return acc.times(ent.rigidBody().mass);
    }
}

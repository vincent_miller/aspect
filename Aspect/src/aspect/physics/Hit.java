package aspect.physics;

import aspect.entity.Entity;
import aspect.util.Vector3;

public class Hit {
    public final Vector3 position;
    public final Entity entity;
    
    public Hit(Vector3 position, Entity entity) {
        this.position = position;
        this.entity = entity;
    }
}

package aspect.script;

import aspect.entity.Entity;

public class AssetLoader {
    public static Javascript loader = new Javascript();
    
    static {
        loader.evaluateWithImports(""
                + "import Resources;\n"
                + "import Shader;\n"
                + "import Vector3;\n"
                + "import Material;\n"
                + "import Texture;\n"
                + "import ConvexCollider;\n"
                + "import RigidBody;\n");
    }
    
    public static void load(String code) {
        loader.evaluate(code);
    }
    
    public static void applyCode(Entity entity, String code) {
        loader.set("entity", entity);
        loader.evaluate(code);
    }
}

package aspect.script;

import static aspect.resources.Resources.loadTextFile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.entity.behavior.Behavior;
import aspect.event.EntityEvent;
import aspect.event.KeyEvent;
import aspect.event.MouseEvent;
import aspect.physics.Collision;
import aspect.util.StringUtil;

public class ScriptBehavior extends Behavior {
    private final Javascript script;
    public final String scriptName;
    
    public ScriptBehavior(String file, Map<String, Object> args1, Map<String, String> args2) {
        script = new Javascript();
        script.evaluate("var entity; var component;");
        script.evaluate("function spawn(ent) { component.spawn(ent); }");
        
        String contents = loadTextFile(file);
        //System.out.println(contents);
        script.evaluateWithImports(contents);
        
        for (Map.Entry<String, Object> entry : args1.entrySet()) {
            script.set(entry.getKey(), entry.getValue());
        }
        
        for (Map.Entry<String, String> entry : args2.entrySet()) {
            script.evaluate(entry.getKey() + " = " + entry.getValue() + ";");
        }
        
        if (file.contains("/")) {
            scriptName = file.substring(file.lastIndexOf("/") + 1, file.indexOf("."));
        } else {
            scriptName = file.substring(0, file.indexOf("."));
        }
    }
    
    
    private void callIfDefined(String function, Object... args) {
        if (script.hasFunction(function)) {
            script.set("entity", entity);
            script.set("component", this);
            script.invoke(function, args);
        }
    }
    
    @Override
    public void onAdd() {
        callIfDefined("onAdd");
    }
    
    @Override
    public void update() {
        callIfDefined("update");
    }
    
    @Override
    public void render() {
        callIfDefined("render");
    }
    
    @Override
    public void onRemove() {
        callIfDefined("onRemove");
    }
    
    @Override
    public void onCollision(Collision col) {
        callIfDefined("onCollision", col);
    }
    
    @Override
    public void onEntityDestroy() {
        callIfDefined("onEntityDestroy");
    }
    
    @Override
    public void entityEvent(EntityEvent event) {
        callIfDefined("entityEvent", event);
    }
    
    @Override
    public void keyEvent(KeyEvent event) {
        callIfDefined("keyEvent", event);
    }
    
    @Override
    public void mouseEvent(MouseEvent event) {
        callIfDefined("mouseEvent", event);
    }
}

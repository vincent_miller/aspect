package aspect.script;

import java.io.Reader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import aspect.util.StringUtil;

public class Javascript {
    private static final ScriptEngineManager manager = new ScriptEngineManager();
    
    private final ScriptEngine engine;
    private final Invocable invoke;
    
    public Javascript() {
        engine = manager.getEngineByName("nashorn");
        invoke = (Invocable) engine;
    }
    
    public Javascript(String source) {
        this();
        evaluate(source);
    }
    
    private String getImports(String contents) {
        String[] lines = contents.split("\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].startsWith("import")) {
                String name = lines[i].substring(lines[i].indexOf(" ") + 1, lines[i].indexOf(";"));
                try {
                    Class.forName(name);
                    String shortName = name;
                    if (shortName.contains(".")) {
                        shortName = shortName.substring(shortName.lastIndexOf(".") + 1);
                    }
                    lines[i] = shortName + " = Java.type(\'" + name + "\');";
                } catch (ClassNotFoundException ex) {
                    for (Package pkg : Package.getPackages()) {
                        if (pkg.getName().startsWith("aspect") || pkg.getName().startsWith("java.lang")) {
                            try {
                                String fullName = pkg.getName() + "." + name;
                                Class.forName(fullName);
                                lines[i] = name + " = Java.type(\'" + fullName + "\');";
                            } catch (ClassNotFoundException ex1) {
                            }
                        }
                    }
                }
            }
        }
        
        return StringUtil.join("\n", lines);
    }
    
    public Object evaluate(String code) {
        try {
            return engine.eval(code);
        } catch (ScriptException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public Object evaluateWithImports(String code) {
        return evaluate(getImports(code));
    }
    
    public Object evaluate(Reader r) {
        try {
            return engine.eval(r);
        } catch (ScriptException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public Object invoke(String function, Object... args) {
        try {
            return invoke.invokeFunction(function, args);
        } catch (NoSuchMethodException | ScriptException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public boolean hasFunction(String name) {
        try {
            return (boolean) engine.eval("typeof " + name + " == \'function\'");
        } catch (ScriptException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public Object get(String variable) {
        return engine.get(variable);
        
    }
    
    public void set(String variable, Object value) {
        engine.put(variable, value);
    }
}

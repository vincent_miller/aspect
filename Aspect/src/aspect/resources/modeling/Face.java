package aspect.resources.modeling;

import static aspect.resources.Resources.copyVector2;
import static aspect.resources.Resources.copyVector3;

import java.util.ArrayList;
import java.util.LinkedList;

import aspect.resources.Resources;
import aspect.util.Vector3;

public class Face {
    public Vertex[] vertices;
    public Vector3 normal;

    public Face(Vertex... vertices) {
        this.vertices = vertices;
    }

    public void toObjString(StringBuilder str, int[] off) {
        for (Vertex vert : vertices) {
            vert.toObjString(str);
        }
        str.append("f");
        for (int i = 0; i < vertices.length; i++, off[0]++) {
            str.append(" " + String.join("/", String.valueOf(off[0])));
        }
    }
    
    public void calculateNormals() {
        normal = Resources.normal(vertices[0].position, vertices[1].position, vertices[2].position);
        for (Vertex vert : vertices) {
            vert.normal = normal;
        }
    }
    
    public void calculateTexcoords() {
        Vector3 cross1 = Vector3.cross(Vector3.yAxis(), normal);
        if (cross1.mag2() == 0) {
            for (Vertex vert : vertices) {
                vert.texcoord = vert.position.xz();
            }
        } else {
            Vector3 cross2 = Vector3.cross(normal, cross1);
            for (Vertex vert : vertices) {
                vert.texcoord = vert.position.projectOnPlane(cross1, cross2);
            }
        }
    }
    
    public void packData(float[] vertexBuffer, float[] normalBuffer, float[] texcoordBuffer, int[] offs) {
        ArrayList<Vertex> verts = new ArrayList<>(vertices.length);
        for (Vertex vert : vertices) {
            verts.add(vert);
        }
        LinkedList<Vertex> tris = EarTriangulator.triangulate(verts);
        
        for (Vertex vert : tris) {
            offs[0] += copyVector3(vert.position, vertexBuffer, offs[0], 1);
            offs[1] += copyVector3(vert.normal, normalBuffer, offs[1], 1);
            offs[2] += copyVector2(vert.texcoord, texcoordBuffer, offs[2], 1);
        }
    }
}

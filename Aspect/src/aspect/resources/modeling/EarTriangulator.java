package aspect.resources.modeling;

import java.util.ArrayList;
import java.util.LinkedList;

import aspect.util.Vector3;

public class EarTriangulator {
    private static final int CONVEX = 1;
    private static final int TANGENT = 0;
    private static final int CONCAVE = -1;
    
    public static LinkedList<Vertex> triangulate(ArrayList<Vertex> vertices) {
        LinkedList<Vertex> triangles = new LinkedList<>();
        while (vertices.size() > 3) {
            int tipIndex = findTip(vertices);
            cut(tipIndex, vertices, triangles);
        }
        
        if (vertices.size() == 3) {
            triangles.addAll(vertices);
        }
        
        return triangles;
    }
    
    private static int previous(int index, int length) {
        return (index == 0) ? (length - 1) : (index - 1);
    }
    
    private static int next(int index, int length) {
        return (index == length - 1) ? (0) : (index + 1);
    }
    
    private static int findTip(ArrayList<Vertex> vertices) {
        for (int i = 0; i < vertices.size(); i++) {
            if (isTip(i, vertices)) {
                return i;
            }
        }
        
        return 0;
    }
    
    private static void cut(int tip, ArrayList<Vertex> vertices, LinkedList<Vertex> triangles) {
        triangles.add(vertices.get(previous(tip, vertices.size())));
        triangles.add(vertices.get(tip));
        triangles.add(vertices.get(next(tip, vertices.size())));
        
        vertices.remove(tip);
    }
    
    private static boolean isTip(int index, ArrayList<Vertex> vertices) {
        if (vertexType(index, vertices) == CONCAVE) {
            return false;
        }
        
        int nextIndex = next(index, vertices.size());
        int prevIndex = previous(index, vertices.size());
        
        Vector3 next = vertices.get(nextIndex).position;
        Vector3 prev = vertices.get(prevIndex).position;
        Vector3 current = vertices.get(index).position;
        
        for (int i = next(nextIndex, vertices.size()); i != prevIndex; i = next(i, vertices.size())) {
            if (vertexType(i, vertices) != CONVEX) {
                Vector3 v = vertices.get(i).position;
                if (areaSign(next, prev, v) >= 0 && areaSign(prev, current, v) >= 0 && areaSign(current, next, v) >= 0) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    private static int vertexType(int index, ArrayList<Vertex> vertices) {
        Vector3 prev = vertices.get(previous(index, vertices.size())).position;
        Vector3 current = vertices.get(index).position;
        Vector3 next = vertices.get(next(index, vertices.size())).position;
        return areaSign(prev, current, next);
    }
    
    private static int areaSign(Vector3 prev, Vector3 current, Vector3 next) {
        return Vector3.angleSign(prev.minus(current), next.minus(current));
    }
}

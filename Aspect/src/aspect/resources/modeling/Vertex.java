/*
 * Copyright (C) 2014 millerv
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.resources.modeling;

import aspect.util.Color;
import aspect.util.Vector2;
import aspect.util.Vector3;

/**
 *
 * @author millerv
 */
public class Vertex {
    public Vector3 position;
    public Vector3 normal;
    public Vector2 texcoord;
    public Color color;
    public int texunit;
    
    public Vertex(Vector3 position) {
        this(position, Vector3.zAxis(), Vector2.zero(), 0, Color.WHITE);
    }

    public Vertex(Vector3 position, Vector3 normal, Vector2 texcoord, int texunit, Color color) {
        this.position = position;
        this.normal = normal;
        this.texcoord = texcoord;
        this.texunit = texunit;
        this.color = color;
    }

    public void toObjString(StringBuilder str) {
        str.append(position.toObjString("v"));
        str.append(normal.toObjString("vn"));
        str.append(texcoord.toObjString("vt"));
    }
}

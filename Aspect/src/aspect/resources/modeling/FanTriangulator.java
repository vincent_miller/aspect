package aspect.resources.modeling;

import java.util.ArrayList;
import java.util.LinkedList;

import aspect.util.Vector3;

public class FanTriangulator {
    public static LinkedList<Vector3> triangulate(ArrayList<Vector3> vertices) {
        LinkedList<Vector3> triangles = new LinkedList<>();
        
        for (int i = 1, j = 2; i < vertices.size() - 1; i++, j = i + 1) {
            triangles.add(vertices.get(0));
            triangles.add(vertices.get(i));
            triangles.add(vertices.get(j));
        }
        
        return triangles;
    }
}

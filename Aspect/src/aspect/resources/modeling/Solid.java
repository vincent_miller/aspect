package aspect.resources.modeling;

import static aspect.resources.Resources.copyColorRGBA;

import aspect.core.AspectRenderer.GeometryType;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.util.Color;

public class Solid {
    private Face[] faces;

    public Solid(Face... faces) {
        this.faces = faces;
    }

    public void toObjString(StringBuilder str, int[] off) {
        for (Face face : faces) {
            face.toObjString(str, off);
        }
    }
    
    public void calculateNormals() {
        for (Face face : faces) {
            face.calculateNormals();
        }
    }
    
    public void calculateTexcoords() {
        for (Face face : faces) {
            face.calculateTexcoords();
        }
    }
    
    public MeshRenderer buildMesh(Material mat) {
        int vertexCount = 0;
        
        for (Face face : faces) {
            vertexCount += (face.vertices.length - 2) * 3;
        }
        
        float[] vertices = new float[vertexCount * 3];
        float[] normals = new float[vertexCount * 3];
        float[] texcoords = new float[vertexCount * 2];
        int[] offs = new int[3];
        
        for (Face face : faces) {
            face.packData(vertices, normals, texcoords, offs);
        }
        
        return new MeshRenderer(new Mesh(GeometryType.TRIANGLES, vertices, normals, copyColorRGBA(Color.WHITE, vertexCount), texcoords), mat);
    }
}

package aspect.resources;

import java.util.HashMap;

import aspect.render.Material;
import aspect.render.Texture;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Transform;
import aspect.util.Vector3;

public class ModelImportSettings {
    public Transform transform;
    public Color specular;
    public boolean useDiffuseAsAmbient;
    public boolean importLines;
    public boolean calculateNormals;
    public ShaderProgram shader;
    
    private final HashMap<String, Material> override;
    
    public static final ModelImportSettings DEFAULT = new ModelImportSettings(new Transform(), Color.BLACK, true, false, false, ShaderProgram.PHONG_VERTEX);
    
    public ModelImportSettings(Transform transform, Color specular, boolean useDiffuseAsAmbient, boolean importLines, boolean calculateNormals, ShaderProgram shader) {
        this.transform = transform;
        this.specular = specular;
        this.useDiffuseAsAmbient = useDiffuseAsAmbient;
        this.importLines = importLines;
        this.calculateNormals = calculateNormals;
        this.shader = shader;
        this.override = new HashMap<>();
    }
    
    public ModelImportSettings() {
        this(DEFAULT.transform, DEFAULT.specular, DEFAULT.useDiffuseAsAmbient, DEFAULT.importLines, DEFAULT.calculateNormals, DEFAULT.shader);
        this.override.putAll(DEFAULT.override);
    }
    
    public ModelImportSettings(float scale) {
        this();
        transform.setScale(new Vector3(scale));
    }
    
    public boolean hasOverride(String name) {
        return override.containsKey(name);
    }
    
    public void addOverride(String name, Material replace) {
        override.put(name, replace);
    }
    
    public void addOverride(String name, Texture replace) {
        override.put(name, new Material(replace));
    }
    
    public void addOverride(String name, Color replace) {
        override.put(name, new Material(replace));
    }
    
    public void removeOverride(String name) {
        override.remove(name);
    }

    public Material getOverride(String name) {
        return override.get(name);
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.resources.ArcFile.ArcEntry;

/**
 * A class for reading and writing zip archives.
 *
 * @author MillerV
 */
public class ArcFile extends File implements Iterable<ArcEntry> {

    private static final byte[] buffer = new byte[1024 * 4096];

    private ZipFile zipFile;
    private boolean valid = false;

    private ArcFile(File file) {
        super(file.getPath());
    }

    public static ArcFile open(File file) {
        return new ArcFile(file);
    }

    public static ArcFile create(File file) {
        file.getParentFile().mkdirs();
        try {
            file.createNewFile();
        } catch (IOException e) {
            Console.log(LogLevel.ERROR, e);
        }
        return new ArcFile(file);
    }

    public ZipFile getZipFile() throws IOException {
        if (!valid || zipFile == null) {
            valid = true;
            if (zipFile != null) {
                zipFile.close();
            }
            zipFile = new ZipFile(this);
        }

        return zipFile;
    }

    public static synchronized void copy(InputStream in, OutputStream out) throws IOException {
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public InputStream getInputStream(String path) throws IOException {
        ZipFile zip = getZipFile();
        ZipEntry entry = zip.getEntry(path);
        if (entry == null) {
            throw new FileNotFoundException(path);
        }
        return zip.getInputStream(entry);
    }

    /*
     * public OutputStream getOutputStream(String path) throws IOException {
     * String dir = path; int i = dir.lastIndexOf("/"); if (i == -1) { dir = "";
     * } else { dir = dir.substring(0, i + 1); } final String d = dir;
     * 
     * File temp = new File(new File(path).getName()); return new
     * FileOutputStream(temp) {
     * 
     * @Override public void close() throws IOException { super.close();
     * addFile(temp, d); temp.delete(); } }; }
     */

    public ArcEntry getEntry(File file) throws IOException {
        return new ArcEntry(getZipFile().getEntry(file.getPath()));
    }

    @Override
    public Iterator<ArcEntry> iterator() {
        try {
            Enumeration<? extends ZipEntry> temp = null;
            try {
                temp = getZipFile().entries();
            } catch (ZipException e) {
                temp = Collections.emptyEnumeration();
            }
            final Enumeration<? extends ZipEntry> entries = temp;
            return new Iterator<ArcEntry>() {
                private ArcEntry last;

                @Override
                public boolean hasNext() {
                    return entries.hasMoreElements();
                }

                @Override
                public ArcEntry next() {
                    last = new ArcEntry(entries.nextElement());
                    return last;
                }

                @Override
                public void remove() {
                    try {
                        deleteFile(last.entry.getName());

                    } catch (IOException ex) {
                        Logger.getLogger(ArcFile.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
        } catch (IOException ex) {
            Logger.getLogger(ArcFile.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public void addFile(File toAdd, String path) throws IOException {
        if (zipFile != null) {
            zipFile.close();
        }

        if (path == null) {
            path = "";
        }
        File tmp = File.createTempFile(getName(), ".zip");
        tmp.delete();
        Files.move(Paths.get(getPath()), Paths.get(tmp.getPath()));

        try {
            ZipInputStream zin = new ZipInputStream(new FileInputStream(tmp));
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(this));

            InputStream in = new FileInputStream(toAdd);
            zout.putNextEntry(new ZipEntry(path + toAdd.getName()));
            copy(in, zout);
            zout.closeEntry();
            in.close();

            for (ZipEntry entry = zin.getNextEntry(); entry != null; entry = zin.getNextEntry()) {
                if (!entry.getName().equals(path + toAdd.getName())) {
                    zout.putNextEntry(new ZipEntry(entry.getName()));
                    copy(zin, zout);
                    zout.closeEntry();
                }
            }

            zout.close();
        } catch (IOException ex) {
            Files.move(Paths.get(tmp.getPath()), Paths.get(getPath()));
            throw ex;
        } finally {
            tmp.delete();
            valid = false;
        }

    }

    public void deleteFile(String path) throws IOException {
        if (zipFile != null) {
            zipFile.close();
        }

        File tmp = File.createTempFile(getName(), null);
        tmp.delete();
        Files.move(Paths.get(getPath()), Paths.get(tmp.getPath()));

        try {
            ZipInputStream zin = new ZipInputStream(new FileInputStream(tmp));
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(this));

            for (ZipEntry entry = zin.getNextEntry(); entry != null; entry = zin.getNextEntry()) {
                boolean keep = true;
                if (entry.getName().equals(path)) {
                    keep = false;
                } else {
                    String s = path;
                    if (!s.endsWith("/")) {
                        s += "/";
                    }

                    if (entry.getName().startsWith(s)) {
                        keep = false;
                    }
                }

                if (keep) {
                    zout.putNextEntry(new ZipEntry(entry.getName()));
                    copy(zin, zout);
                    zout.closeEntry();
                }
            }

            zout.close();
        } catch (IOException ex) {
            Files.move(Paths.get(tmp.getPath()), Paths.get(getPath()));
            throw ex;
        } finally {
            tmp.delete();
            valid = false;
        }
    }

    public void renameFile(String oldname, String newname) throws IOException {
        if (zipFile != null) {
            zipFile.close();
        }

        File tmp = File.createTempFile(getName(), null);
        tmp.delete();
        Files.move(Paths.get(getPath()), Paths.get(tmp.getPath()));

        try {
            ZipInputStream zin = new ZipInputStream(new FileInputStream(tmp));
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(this));

            String foldername = oldname;
            if (!foldername.endsWith("/")) {
                foldername += "/";
            }
            String newfoldername = newname;
            if (!newfoldername.endsWith("/")) {
                newfoldername += "/";
            }
            for (ZipEntry entry = zin.getNextEntry(); entry != null; entry = zin.getNextEntry()) {
                String name = entry.getName();
                if (name.equals(oldname)) {
                    name = newname;
                } else if (name.startsWith(foldername)) {
                    name = newfoldername + name.substring(foldername.length());
                }

                zout.putNextEntry(new ZipEntry(name));
                copy(zin, zout);
                zout.closeEntry();
            }

            zout.close();
        } catch (IOException ex) {
            Files.move(Paths.get(tmp.getPath()), Paths.get(getPath()));
            throw ex;
        } finally {
            tmp.delete();
            valid = false;
        }
    }

    public class ArcEntry {

        private final ZipEntry entry;

        public ArcEntry(ZipEntry entry) {
            this.entry = entry;
        }

        public ArcEntry(String name) {
            this.entry = new ZipEntry(name);
        }

        public ZipEntry getZipEntry() {
            return entry;
        }

        public String getName() {
            return entry.getName();
        }

        public long getSize() {
            return entry.getSize();
        }

        public InputStream getStream() throws IOException {
            return getZipFile().getInputStream(entry);
        }
    }
}

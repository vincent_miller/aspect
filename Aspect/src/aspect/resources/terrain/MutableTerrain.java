package aspect.resources.terrain;

import aspect.render.Material;
import aspect.util.Vector3;

public abstract class MutableTerrain extends Terrain {

    protected MutableTerrain(Vector3 scale, Material material, float texscale) {
        super(scale, material, texscale);
    }

    public void setHeight(int x, int z, float height) {
        setUnscaledHeight(x, z, height / scale.y);
    }
    
    public abstract void setUnscaledHeight(int x, int z, float height);
}

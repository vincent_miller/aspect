package aspect.resources.terrain;

import static aspect.resources.Resources.copyColorRGB;
import static aspect.resources.Resources.copyColorRGBA;
import static aspect.resources.Resources.copyVector2;
import static aspect.resources.Resources.copyVector3;
import static aspect.resources.Resources.normal;

import java.awt.image.BufferedImage;

import aspect.core.AspectRenderer.GeometryType;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.util.Color;
import aspect.util.Vector2;
import aspect.util.Vector3;

public abstract class Terrain extends ViewModel {
    protected MeshRenderer mesh;
    protected Vector3 scale;
    protected Material material;
    protected float texscale;
    protected boolean awareOfOutside = false;
    
    protected Terrain(Vector3 scale, Material material, float texscale) {
        this.scale = scale;
        this.material = material;
        this.texscale = texscale;
    }
    
    public abstract int getDataWidth();
    
    public abstract int getDataDepth();
    
    public abstract float getUnscaledHeight(int x, int z);

    public float getUnscaledHeightOutside(int x, int z) {
        throw new UnsupportedOperationException();
    }
    
    public float getHeight(int x, int z) {
        if (x >= 0 && x < getDataWidth() && z >= 0 && z < getDataDepth()) {
            return getUnscaledHeight(x, z) * scale.y;
        } else {
            return getUnscaledHeightOutside(x, z) * scale.y;
        }
    }
    
    public void rebuild() {
        this.mesh = createMesh();
    }
    
    protected MeshRenderer createMesh() {
        float[] vertices = new float[(getDataWidth() - 1) * (getDataDepth() - 1) * 6 * 3];
        float[] normals = new float[vertices.length];
        float[] texcoords = new float[vertices.length * 2 / 3];
        
        int index = 0, tindex = 0, nindex = 0;

        for (int i = 0; i < getDataWidth() - 1; i++) {
            for (int j = 0; j < getDataDepth() - 1; j++) {

                Vector3 v1 = getVertex(i, j);
                Vector3 v2 = getVertex(i, j + 1);
                Vector3 v3 = getVertex(i + 1, j + 1);
                Vector3 v4 = getVertex(i + 1, j);

                Vector2 t1 = Vector3.divide(v1, texscale).xz();
                Vector2 t2 = Vector3.divide(v2, texscale).xz();
                Vector2 t3 = Vector3.divide(v3, texscale).xz();
                Vector2 t4 = Vector3.divide(v4, texscale).xz();

                Vector3 n1 = getNormal(i, j);
                Vector3 n2 = getNormal(i, j + 1);
                Vector3 n3 = getNormal(i + 1, j + 1);
                Vector3 n4 = getNormal(i + 1, j);

                index += copyVector3(v1, vertices, index, 1);
                index += copyVector3(v2, vertices, index, 1);
                index += copyVector3(v3, vertices, index, 1);
                index += copyVector3(v3, vertices, index, 1);
                index += copyVector3(v4, vertices, index, 1);
                index += copyVector3(v1, vertices, index, 1);

                tindex += copyVector2(t1, texcoords, tindex, 1);
                tindex += copyVector2(t2, texcoords, tindex, 1);
                tindex += copyVector2(t3, texcoords, tindex, 1);
                tindex += copyVector2(t3, texcoords, tindex, 1);
                tindex += copyVector2(t4, texcoords, tindex, 1);
                tindex += copyVector2(t1, texcoords, tindex, 1);

                nindex += copyVector3(n1, normals, nindex, 1);
                nindex += copyVector3(n2, normals, nindex, 1);
                nindex += copyVector3(n3, normals, nindex, 1);
                nindex += copyVector3(n3, normals, nindex, 1);
                nindex += copyVector3(n4, normals, nindex, 1);
                nindex += copyVector3(n1, normals, nindex, 1);
            }
        }
        
        return new MeshRenderer(new Mesh(GeometryType.TRIANGLES, vertices, normals, copyColorRGBA(Color.WHITE, vertices.length / 3), texcoords), material);
    }
    
    private Vector3 get2Normals(int x1, int z1, int x2, int z2) {
        Vector3 v1 = getVertex(x1, z1);
        Vector3 v2 = getVertex(x1, z2);
        Vector3 v3 = getVertex(x2, z2);
        Vector3 v4 = getVertex(x2, z1);

        Vector3 normal1 = normal(v1, v2, v3);
        if (normal1.y < 0) {
            normal1 = normal1.negate();
        }

        Vector3 normal2 = normal(v3, v4, v1);
        if (normal2.y < 0) {
            normal2 = normal2.negate();
        }

        return Vector3.add(normal1, normal2);
    }

    public Vector3 getNormal(int x, int z) {
        Vector3 normal = Vector3.zero();
        int num = 0;

        if (x > 1 || awareOfOutside) {
            if (z > 1 || awareOfOutside) {
                normal = normal.plus(get2Normals(x, z, x - 1, z - 1));
                num += 2;
            }

            if (z < getDataDepth() - 1 || awareOfOutside) {
                normal = normal.plus(get2Normals(x, z, x - 1, z + 1));
                num += 2;
            }
        }

        if (x < getDataWidth() - 1 || awareOfOutside) {
            if (z > 1 || awareOfOutside) {
                normal = normal.plus(get2Normals(x, z, x + 1, z - 1));
                num += 2;
            }

            if (z < getDataDepth() - 1 || awareOfOutside) {
                normal = normal.plus(get2Normals(x, z, x + 1, z + 1));
                num += 2;
            }
        }

        return Vector3.divide(normal, num).normalize();
    }
    
    public Vector3 getVertex(int x, int z) {
        float h = getHeight(x, z);
        return new Vector3(x * scale.x, h, z * scale.z);
    }
    
    public Material getMaterial() {
        return material;
    }
    
    public void setMateral(Material material) {
        this.material = material;
    }
    
    public Vector3 getScale() {
        return scale;
    }
    
    public void setScale(Vector3 scale) {
        this.scale = scale;
    }
    
    public float getTexScale() {
        return texscale;
    }
    
    public void setTexScale(float texscale) {
        this.texscale = texscale;
    }
    
    public float getModelWidth() {
        return (getDataWidth() - 1) * scale.x;
    }
    
    public float getModelDepth() {
        return (getDataDepth() - 1) * scale.z;
    }

    public float interpolateHeight(float x, float z) {
        x /= scale.x;
        z /= scale.z;
        
        int i = (int) x;
        int j = (int) z;

        if (i < 0 || j < 0 || i >= getDataWidth() - 1 || j >= getDataWidth() - 1) {
            return 0;
        }

        float dx = x - i;
        float dz = z - j;

        Vector3 point1 = new Vector3(i, getUnscaledHeight(i , j), j);
        Vector3 point2 = new Vector3(i + 1, getUnscaledHeight(i + 1, j + 1), j + 1);
        Vector3 point3;

        if (dx > dz) {
            point3 = new Vector3(i + 1, getUnscaledHeight(i + 1, j), j);
        } else {
            point3 = new Vector3(i, getUnscaledHeight(i, j + 1), j + 1);
        }

        Vector3 normal = normal(point1, point2, point3);

        float a = normal.x;
        float b = normal.y;
        float c = normal.z;
        float d = -(a * point1.x + b * point1.y + c * point1.z);

        return -(a * x + c * z + d) * scale.y / b;
    }

    @Override
    protected void renderModel() {
        mesh.transform.setParent(transform);
        mesh.render();
    }
}

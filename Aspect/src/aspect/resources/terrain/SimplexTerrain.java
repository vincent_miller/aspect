package aspect.resources.terrain;

import static aspect.resources.Resources.pipe;

import java.util.LinkedList;

import aspect.entity.Entity;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Random;
import aspect.util.SimplexNoise;
import aspect.util.Trig;
import aspect.util.Vector2;
import aspect.util.Vector3;

public class SimplexTerrain extends BufferedTerrain {
    public Vector2 offset;
    private LinkedList<ViewModel> trees = new LinkedList<>();
    private LinkedList<Vector2> layers = new LinkedList<>();

    public SimplexTerrain(int width, int height, Vector2 offset, Vector3 scale, Material material, float texscale) {
        super(width, height, scale, material, texscale);
        this.offset = offset;
        awareOfOutside = true;
        
    }

    public void addLayer(float frequency, float scale) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                float f = getHeight(i, j);
                f += SimplexNoise.Generate((i + offset.x) * frequency, (j + offset.y) * frequency, 0) * scale;
                setHeight(i, j, f);
            }
        }
        
        layers.add(new Vector2(frequency, scale));
    }
    
    public void addTrees(float frequency, float density, ViewModel model) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                float treeChance = SimplexNoise.Generate((i + offset.x) * frequency, (j + offset.y) * frequency, 0) * 100;
                if (treeChance < density) {
                    if (Random.Chance(0.005f)) {
                        ViewModel tree = model.copy();
                        tree.transform.setParent(transform);
                        tree.transform.setPosition(getVertex(i, j));
                        tree.transform.setEulerAngles(new Angles(0.0f, Random.Float(Trig.FULL_CIRCLE), 0.0f));
                        trees.add(tree);
                    }
                }
            }
        }
    }
    
    @Override
    public void renderModel() {
        super.renderModel();
        for (ViewModel tree : trees) {
            tree.render();
        }
    }
    
    @Override
    public float getUnscaledHeightOutside(int x, int z) {
        float f = 0.0f;
        for (Vector2 layer : layers) {
            f += SimplexNoise.Generate((x + offset.x) * layer.x, (z + offset.y) * layer.x, 0) * layer.y;
        }
        return f;
    }
}

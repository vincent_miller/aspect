package aspect.resources.terrain;

import static aspect.core.AspectRenderer.modelMatrix;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.util.Vector2;
import aspect.util.Vector3;

public class ChunkySimplexTerrain extends Terrain {
    private HashMap<ChunkLocation, SimplexTerrain> terrain = new HashMap<>();
    private LinkedList<Vector2> layers = new LinkedList<>();
    private LinkedList<Vector2> trees = new LinkedList<>();
    private Vector2 offset;
    private int chunkSize;
    private ViewModel treeModel;

    public ChunkySimplexTerrain(int chunkSize, Vector2 offset, Vector3 scale, Material material, float texscale) {
        super(scale, material, texscale);
        this.offset = offset;
        this.chunkSize = chunkSize;
    }

    @Override
    public int getDataWidth() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getDataDepth() {
        throw new UnsupportedOperationException();
    }

    @Override
    public float getUnscaledHeight(int x, int z) {
        SimplexTerrain terrain = getChunk(x, z);
        ChunkLocation location = getChunkLocation(x, z);
        
        return terrain.getUnscaledHeight(x - location.x * chunkSize, z - location.z * chunkSize);
    }
    
    @Override
    public float interpolateHeight(float x, float z) {
        SimplexTerrain terrain = getChunk((int)x, (int)z);
        ChunkLocation location = getChunkLocation((int)x, (int)z);
        
        return terrain.interpolateHeight(x - location.x * chunkSize, z - location.z * chunkSize);
    }
    
    private ChunkLocation getChunkLocation(int x, int z) {
        return new ChunkLocation(Math.floorDiv(x, chunkSize), Math.floorDiv(z, chunkSize));
    }
    
    private SimplexTerrain getChunk(int x, int z) {
        ChunkLocation location = getChunkLocation(x, z);
        if (terrain.containsKey(location)) {
            return terrain.get(location);
        } else {
            return createChunk(location);
        }
    }
    
    public void createChunk(int x, int z) {
        ChunkLocation location = getChunkLocation(x, z);
        if (!terrain.containsKey(location)) {
            createChunk(location);
        }
    }
    
    public void deleteOutsideRange(int x, int z, int range) {
        LinkedList<ChunkLocation> marked = new LinkedList<>();
        for (ChunkLocation location : terrain.keySet()) {
            Vector2 loc = location.toWorld();
            if (loc.x + chunkSize < x - range || loc.x > x + range || loc.y + chunkSize < z - range || loc.y > z + range) {
                marked.add(location);
            }
        }
        
        for (ChunkLocation location : marked) {
            terrain.remove(location).mesh.destroy();
        }
    }
    
    public void deleteChunk(int x, int z) {
        SimplexTerrain chunk = terrain.remove(getChunkLocation(x, z));
        if (chunk != null) {
            chunk.mesh.destroy();
        }
    }
    
    private SimplexTerrain createChunk(ChunkLocation location) {
        SimplexTerrain newChunk = new SimplexTerrain(chunkSize, chunkSize, offset.plus(location.toWorld()), scale, material, texscale);
        newChunk.transform.setPosition(location.toWorld().asXZ());
        newChunk.transform.setParent(transform);
        for (Vector2 layer : layers) {
            newChunk.addLayer(layer.x, layer.y);
        }
        for (Vector2 tree : trees) {
            newChunk.addTrees(tree.x, tree.y, treeModel);
        }
        newChunk.rebuild();
        terrain.put(location, newChunk);
        return newChunk;
    }

    @Override
    public ViewModel copy() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void rebuild() {
        for (SimplexTerrain chunk : terrain.values()) {
            chunk.rebuild();
        }
    }
    
    @Override
    public void renderModel() {
        for (SimplexTerrain chunk : terrain.values()) {
            chunk.render();
        }
    }

    private class ChunkLocation {
        int x;
        int z;
        
        private ChunkLocation(int x, int z) {
            this.x = x;
            this.z = z;
        }
        
        private Vector2 toWorld() {
            return new Vector2(x * chunkSize, z * chunkSize);
        }
        
        @Override
        public int hashCode() {
            int hash = 5;
            hash = 79 * hash + x;
            hash = 79 * hash + z;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            return hashCode() == obj.hashCode();
        }
    }

    public void addLayer(float frequency, float scale) {
        for (SimplexTerrain chunk : terrain.values()) {
            chunk.addLayer(frequency, scale);
        }
        layers.add(new Vector2(frequency, scale));
    }
    
    public void addTrees(float frequency, float density, ViewModel model) {
        treeModel = model;
        for (SimplexTerrain chunk : terrain.values()) {
            chunk.addTrees(frequency, density, model);
        }
        trees.add(new Vector2(frequency, density));
    }
}

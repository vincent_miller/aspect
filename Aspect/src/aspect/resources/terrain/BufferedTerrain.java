package aspect.resources.terrain;

import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.util.Vector3;

public class BufferedTerrain extends MutableTerrain {
    protected final float[] data;
    protected final int width;
    protected final int height;
    
    public BufferedTerrain(int width, int height, Vector3 scale, Material material, float texscale) {
        super(scale, material, texscale);
        this.data = new float[(width + 1) * (height + 1)];
        this.width = width + 1;
        this.height = height + 1;
    }
    
    public BufferedTerrain(float[] data, int width, int height, Vector3 scale, Material material, float texscale) {
        this(data, width, height, scale, material, texscale, true);
    }
    
    public BufferedTerrain(float[] data, int width, int height, Vector3 scale, Material material, float texscale, boolean build) {
        super(scale, material, texscale);
        this.data = data;
        this.width = width + 1;
        this.height = height + 1;
        if (build) {
            rebuild();
        }
    }
    
    public BufferedTerrain(float[][] data, Vector3 scale, Material material, float texscale) {
        this(data, scale, material, texscale, true);
    }
    
    public BufferedTerrain(float[][] data, Vector3 scale, Material material, float texscale, boolean build) {
        super(scale, material, texscale);
        this.width = data.length;
        this.height = data[0].length;
        this.data = new float[width * height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.data[getIndex(i, j)] = data[i][j];
            }
        }
        if (build) {
            rebuild();
        }
    }
    
    public void addUnscaled(Terrain terrain, int x, int z) {
        int width2 = terrain.getDataWidth();
        int height2 = terrain.getDataDepth();
        
        for (int i = 0; i < width2 && i + x < width; i++) {
            for (int j = 0; j < height2 && j + z < height; j++) {
                this.data[getIndex(i + x, j + z)] += terrain.getUnscaledHeight(i, j);
            }
        }
    }
    
    public void add(float[][] data, int x, int z) {
        int width2 = data.length;
        int height2 = data[0].length;
        
        for (int i = 0; i < width2 && i + x < width; i++) {
            for (int j = 0; j < height2 && j + z < height; j++) {
                this.data[getIndex(i + x, j + z)] += data[i][j];
            }
        }
    }
    
    public void add(float[] data, int width, int height, int x, int z) {
        for (int i = 0; i < width && i + x < this.width; i++) {
            for (int j = 0; j < height && j + z < this.height; j++) {
                this.data[getIndex(i + x, j + z)] += data[getIndex(i, j)];
            }
        }
    }
    
    private int getIndex(int x, int z) {
        return z * width + x;
    }
    
    @Override
    public int getDataWidth() {
        return width;
    }

    @Override
    public int getDataDepth() {
        return height;
    }

    @Override
    public float getUnscaledHeight(int x, int z) {
        return data[getIndex(x, z)];
    }
    
    @Override
    public void setUnscaledHeight(int x, int z, float height) {
        data[getIndex(x, z)] = height;
    }

    @Override
    public ViewModel copy() {
        float[] f = new float[data.length];
        System.arraycopy(data, 0, f, 0, data.length);
        return new BufferedTerrain(data, width, height, scale, material, texscale);
    }

}

package aspect.resources.terrain;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.resources.Resources;
import aspect.util.Vector3;

public class HeightmapTerrain extends MutableTerrain {
    protected BufferedImage heightmap;
    protected Graphics2D pen;
    
    public HeightmapTerrain(BufferedImage heightmap, Vector3 scale, Material material, float texscale) {
        super(scale, material, texscale);
        this.heightmap = heightmap;
        this.pen = heightmap.createGraphics();
        pen.setColor(Color.WHITE);
        rebuild();
    }
    
    public Graphics2D getPen() {
        return pen;
    }

    @Override
    public int getDataWidth() {
        return heightmap.getWidth();
    }

    @Override
    public int getDataDepth() {
        return heightmap.getHeight();
    }

    @Override
    public float getUnscaledHeight(int x, int z) {
        return new Color(heightmap.getRGB(x, z)).getRed() / 255f;
    }

    @Override
    public ViewModel copy() {
        BufferedImage img = Resources.createImage(getDataWidth(), getDataDepth());
        Graphics2D g = img.createGraphics();
        g.drawImage(heightmap, 0, 0, null);
        g.dispose();
        return new HeightmapTerrain(img, scale, material.copy(), texscale);
    }

    @Override
    public void setUnscaledHeight(int x, int z, float height) {
        Color c = new Color(height, height, height);
        heightmap.setRGB(x, z, c.getRGB());
    }
}

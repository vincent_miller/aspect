/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.resources;

import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.audio.Music;
import aspect.audio.Sound2D;
import aspect.audio.AudioClip;
import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.render.AbstractMesh;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Logger;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

import javax.imageio.ImageIO;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import aspect.util.Vector2;
import aspect.util.Vector3;
import aspect.render.MultiModel;
import aspect.render.MultiTexturedMaterial;
import aspect.render.MultiTexturedMesh;
import aspect.render.Texture;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.resources.ArcFile.ArcEntry;
import aspect.resources.modeling.EarTriangulator;
import aspect.resources.modeling.Vertex;
import aspect.script.AssetLoader;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Matrix3x3;
import aspect.util.Matrix4x4;
import aspect.util.Trig;

import static aspect.core.AspectRenderer.GeometryType.POLYGON;
import static aspect.core.AspectRenderer.GeometryType.QUADS;
import static aspect.core.AspectRenderer.GeometryType.TRIANGLES;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import org.lwjgl.LWJGLException;
import org.lwjgl.util.WaveData;

/**
 * A utility class for loading, storing, and retrieving assets such as audio,
 * textures, and models.
 *
 * @author MillerV
 */
public class Resources {

    public static final HashMap<String, Sound2D> loadedSound2D = new HashMap<>();
    public static final HashMap<String, AudioClip> loadedSound3D = new HashMap<>();
    public static final HashMap<String, Music> loadedMusic = new HashMap<>();
    public static final HashMap<String, Texture> loadedTextures = new HashMap<>();
    public static final HashMap<String, Material> loadedMaterials = new HashMap<>();
    public static final HashMap<String, BufferedImage> loadedImages = new HashMap<>();
    public static final HashMap<String, ViewModel> loadedModels = new HashMap<>();
    public static final HashMap<String, Shader> loadedShaders = new HashMap<>();

    public static ArcFile archive = null;
    public static long audioMaxSize = (long) 1e6;

    public static final Sound2D UNLOADED_AUDIO2D;
    public static final Music UNLOADED_MUSIC;
    public static final BufferedImage UNLOADED_IMAGE;
    public static final Shader UNLOADED_SHADER;
    public static final ViewModel UNLOADED_MODEL = new ViewModel() {
        @Override
        protected void renderModel() {

        }

        @Override
        public ViewModel copy() {
            return this;
        }
    };

    public static final int VERTEX_DATA_QUAD = 4 * 3;
    public static final int TEXCOORD_DATA_QUAD = 4 * 2;
    public static final int VERTEX_DATA_BOX = VERTEX_DATA_QUAD * 6;
    public static final int TEXCOORD_DATA_BOX = TEXCOORD_DATA_QUAD * 6;
    public static final int VERTEX_DATA_TRIANGLE = 3 * 3;
    public static final int TEXCOORD_DATA_TRIANGLE = 3 * 2;

    static {

        BufferedImage ui;
        try {
            ui = ImageIO.read(Resources.class.getResource("materials/unloaded.jpg"));
        } catch (IOException ex) {
            ui = null;
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, "Could not load default texture: ", ex);
        }
        UNLOADED_AUDIO2D = new Sound2D();
        UNLOADED_MUSIC = new Music();
        UNLOADED_IMAGE = ui;
        UNLOADED_SHADER = new Shader(0);
    }

    public static InputStream getStream(String file) throws IOException {
        if (archive == null) {
            return new FileInputStream(file);
        } else {
            return archive.getInputStream(file);
        }
    }

    public static Sound2D getSound2D(String source) {
        return getSound2D(source, source);
    }

    public static Sound2D getSound2D(String name, String source) {
        if (loadedSound2D.containsKey(name)) {
            return loadedSound2D.get(name);
        }

        try {
            return getSound2D(name, getStream(source));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return UNLOADED_AUDIO2D;
        }
    }

    public static Sound2D getSound2D(String name, InputStream source) {
        if (loadedSound2D.containsKey(name)) {
            return loadedSound2D.get(name);
        }

        try {
            Sound2D s = new Sound2D(source);
            loadedSound2D.put(name, s);
            return s;
        } catch (IOException | UnsupportedAudioFileException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return UNLOADED_AUDIO2D;
        }
    }

    public static AudioClip getSound3D(String source) {
        return getSound3D(source, source);
    }

    public static AudioClip getSound3D(String name, String source) {
        if (loadedSound3D.containsKey(name)) {
            return loadedSound3D.get(name);
        }

        try {
            return getSound3D(name, getStream(source));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return null;
        }
    }

    public static AudioClip getSound3D(String name, InputStream source) {
        if (loadedSound3D.containsKey(name)) {
            return loadedSound3D.get(name);
        }

        try {
            WaveData data = WaveData.create(new BufferedInputStream(source));
            AudioClip clip = new AudioClip(data);
            loadedSound3D.put(name, clip);
            return clip;
        } catch (LWJGLException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return null;
        }
    }

    public static Music getMusic(String name) {
        return getMusic(name, name);
    }

    public static Music getMusic(String name, String file) {
        if (loadedMusic.containsKey(name)) {
            return loadedMusic.get(name);
        }

        try {
            return getMusic(name, getStream(file));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return UNLOADED_MUSIC;
        }
    }

    public static Music getMusic(String name, InputStream source) {
        if (loadedMusic.containsKey(name)) {
            return loadedMusic.get(name);
        }

        try {
            Music mus = new Music(source);
            loadedMusic.put(name, mus);
            return mus;
        } catch (IOException | UnsupportedAudioFileException ex) {
            Console.log(LogLevel.ERROR, "Sound could not be loaded:", ex);
            return UNLOADED_MUSIC;
        }
    }

    public static Texture getTexture(String file) {
        return getTexture(file, file);
    }

    public static Texture getTexture(String name, String file) {
        if (loadedTextures.containsKey(name)) {
            return loadedTextures.get(name);
        }

        try {
            return getTexture(name, getStream(file));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Texture could not be loaded:", ex);
            return Texture.create(UNLOADED_IMAGE);
        }

    }

    public static Texture getTexture(String name, InputStream source) {
        if (loadedTextures.containsKey(name)) {
            return loadedTextures.get(name);
        }

        try {
            Texture t = Texture.create(source);
            loadedTextures.put(name, t);
            return t;
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Texture could not be loaded:", ex);
            return Texture.create(UNLOADED_IMAGE);
        }
    }

    public static Shader getShader(String file, Shader.Type type) {
        return getShader(file, file, type);
    }

    public static Shader getShader(String name, String file, Shader.Type type) {
        if (loadedShaders.containsKey(name)) {
            return loadedShaders.get(name);
        }

        try {
            return getShader(name, getStream(file), type);
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Shader could not be loaded: " + ex);
            return null;
        }
    }

    public static Shader getShader(String name, InputStream stream, Shader.Type type) {
        if (loadedShaders.containsKey(name)) {
            return loadedShaders.get(name);
        }

        Shader shader = new Shader(loadTextFile(stream), name, type);
        loadedShaders.put(name, shader);
        return shader;

    }

    public static Material getMaterial(String source) {
        return getMaterial(source, source);
    }

    // When loading a Material from an InputStream, make sure to load the Shader
    // and Texture first.
    public static Material getMaterial(String name, InputStream source) {
        if (loadedMaterials.containsKey(name)) {
            return loadedMaterials.get(name);
        }
        String s = loadTextFile(source);
        Material m = new Material();
        for (String line : s.trim().split("\n")) {
            line = line.trim();
            String[] args = line.split(" ");
            if (line.startsWith("#") || line.isEmpty()) {
                continue;
            }
            switch (args[0]) {
                case "texture:": {
                    m.texture = getTexture(args[1]);
                    break;
                }

                case "ambient:": {
                    m.ambient = new Color(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]));
                    break;
                }

                case "diffuse:": {
                    m.diffuse = new Color(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]));
                    break;
                }

                case "specular:": {
                    m.specular = new Color(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]));
                    break;
                }

                case "emissive:": {
                    m.emissive = new Color(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]));
                }

                case "shininess:": {
                    m.shininess = Float.parseFloat(args[1]);
                    break;
                }

                case "shader:": {
                    if (args[1].equals("prebuilt")) {
                        m.shader = ShaderProgram.loadPrebuilt(args[2]);
                    } else {
                        Shader vert = getShader(args[1], Shader.Type.VERTEX);
                        Shader frag = getShader(args[2], Shader.Type.FRAGMENT);

                        ShaderProgram shader = new ShaderProgram(vert, frag);

                        m.shader = shader;
                    }
                    break;
                }

                case "cull:": {
                    m.cull = Boolean.parseBoolean(args[1]);
                    break;
                }

                default: {
                    Console.log(LogLevel.ERROR, "Could not parse line of AMP file: " + line);
                }
            }
        }
        loadedMaterials.put(name, m);
        return m;
    }

    public static Material getMaterial(String name, String source) {
        if (loadedMaterials.containsKey(name)) {
            return loadedMaterials.get(name);
        }

        try {
            return getMaterial(name, getStream(source));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Material could not be loaded: ", ex);
            return new Material(Texture.create(UNLOADED_IMAGE));
        }
    }

    public static BufferedImage getImage(String file) {
        return getImage(file, file);
    }

    public static BufferedImage getImage(String name, String file) {
        if (loadedImages.containsKey(name)) {
            return loadedImages.get(name);
        }

        try {
            return getImage(name, getStream(file));
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Image could not be loaded: ", ex);
            return UNLOADED_IMAGE;
        }
    }

    public static BufferedImage getImage(String name, InputStream stream) {
        if (loadedImages.containsKey(name)) {
            return loadedImages.get(name);
        }

        try {
            BufferedImage img = ImageIO.read(stream);
            loadedImages.put(name, img);
            return img;
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Image could not be loaded: ", ex);
            return UNLOADED_IMAGE;
        }
    }

    public static BufferedImage createImage(int width, int height) {
        GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();

        return gc.createCompatibleImage(width, height, java.awt.Color.BITMASK);
    }

    public static String loadTextFile(String file) {
        try {
            return loadTextFile(getStream(file));
        } catch (IOException ex) {
            Logger.getLogger(Resources.class.getName()).log(Level.SEVERE, null, ex);
            return new String();
        }
    }

    public static String loadTextFile(InputStream stream) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder contents = new StringBuilder();

            String s;

            while ((s = reader.readLine()) != null) {
                contents.append(s).append('\n');
            }

            reader.close();
            return contents.toString();
        } catch (IOException ex) {
            Logger.getLogger(Resources.class.getName()).log(Level.SEVERE, null, ex);
            return new String();
        }
    }

    public static ViewModel getModel(String file, ModelImportSettings settings) {
        return getModel(file, file, settings);
    }

    public static ViewModel getModel(String name, String file, ModelImportSettings settings) {
        if (loadedModels.containsKey(name)) {
            return loadedModels.get(name).copy();
        }

        try {
            return getModel(name, getStream(file), settings);
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Model could not be loaded: ", ex);
            return UNLOADED_MODEL;
        }
    }

    public static ViewModel getModel(String name, InputStream stream, ModelImportSettings settings) {
        if (loadedModels.containsKey(name)) {
            return loadedModels.get(name).copy();
        }

        try {
            ViewModel mdl = parseObj(loadTextFile(stream), settings);
            loadedModels.put(name, mdl);
            return mdl;
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Model could not be loaded: ", ex);
            return UNLOADED_MODEL;
        }
    }

    public static void loadAssets(String file) {
        try {
            String contents = loadTextFile(file);
            JSONParser parser = new JSONParser();

            JSONObject obj = (JSONObject) parser.parse(contents);
            JSONArray arr = (JSONArray) obj.get("Assets");

            for (Object a : arr) {
                JSONObject asset = (JSONObject) a;
                if (asset.containsKey("type")) {
                    String type = (String) asset.get("type");
                    String source = (String) asset.get("file");
                    String name = asset.containsKey("name") ? (String) asset.get("name") : source;
                    String call = "Resources.get" + type + "(\"" + name + "\", \"" + source + "\"";

                    if (type.equals("Model")) {
                        call += ", " + asset.get("scale");
                    } else if (type.equals("Shader")) {
                        call += ", " + asset.get("shaderType");
                    }

                    call += ");";
                    System.out.println(call);
                    AssetLoader.load(call);
                }
            }

        } catch (ParseException | ClassCastException ex) {
            Console.log(LogLevel.ERROR, "Asset file could not be loaded: ", ex);
        }
    }

    public static int vertexDataPipe(int lod) {
        return lod * 4 * 3;
    }

    public static int texCoordDataPipe(int lod) {
        return lod * 4 * 2;
    }

    public static int vertexDataSphere(int lod) {
        return lod * (lod / 2) * 4 * 3;
    }

    public static int texCoordDataSphere(int lod) {
        return lod * (lod / 2) * 4 * 2;
    }

    public static int vertexDataPolygon(int npoints) {
        return npoints * 3;
    }

    public static int texCoordDataPolygon(int npoints) {
        return npoints * 2;
    }

    public static float[] polygonTexCoords(float hRepeat, float vRepeat, Vector3... points){
    float[] f = new float[texCoordDataPolygon(points.length)];
    polygonTexCoords(hRepeat, vRepeat, points, f, 0);
    return f;
}

public static void polygonTexCoords(float hRepeat, float vRepeat, Vector3[] points, float[] dest, int start) {
        float minx = Float.POSITIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;
        float minz = Float.POSITIVE_INFINITY;
        float maxx = Float.NEGATIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float maxz = Float.NEGATIVE_INFINITY;
        for (Vector3 point : points) {
            float x = point.x;
            float y = point.y;
            float z = point.z;
            minx = Math.min(minx, x);
            miny = Math.min(miny, y);
            minz = Math.min(minz, z);
            maxx = Math.max(maxx, x);
            maxy = Math.max(maxy, y);
            maxz = Math.max(maxz, z);
        }
        float w = new Vector2(maxx - minx, maxz - minz).mag();
        for (int i = 0; i < points.length; i++) {
            float x = (new Vector2(points[i].x - minx, points[i].z - minz).mag() / w) * hRepeat;
            float y = ((points[i].y - miny) / (maxy - miny)) * vRepeat;
            dest[i * 2 + start + 0] = x;
            dest[i * 2 + start + 1] = y;
        }
    }

    public static MeshRenderer parseObj(String obj, ModelImportSettings settings) throws IOException {
        if (settings == null) {
            settings = ModelImportSettings.DEFAULT;
        }

        Matrix4x4 mat = settings.transform.globalModelMatrix();
        Matrix3x3 nmat = Matrix4x4.normalMatrix(mat);

        ArrayList<Vector3> allVertices = new ArrayList<>(100);
        ArrayList<Vector2> allTexCoords = new ArrayList<>(100);
        ArrayList<Vector3> allNormals = new ArrayList<>(100);

        LinkedList<Texture> allTextures = null;

        LinkedList<Vertex> triangles = new LinkedList<>();

        Material currentMaterial = null;
        int line = 1;

        for (String s : obj.trim().split("\n")) {
            s = s.trim();
            String[] parts = s.split(" ");
            if (parts.length > 1 && parts[1].isEmpty()) {
                for (int i = 1; i < parts.length - 1; i++) {
                    parts[i] = parts[i + 1];
                }
            }
            if (s.startsWith("#") || s.isEmpty()) {
                line++;
                continue;
            }
            switch (parts[0]) {
                case "mtllib": {

                    allTextures = parseMtl(loadTextFile("models/" + parts[1]), settings);
                    break;
                }
                case "usemtl": {
                    if (settings.hasOverride(parts[1])) {
                        currentMaterial = settings.getOverride(parts[1]);
                    } else {
                        currentMaterial = getMaterial(parts[1]);
                    }
                    break;
                }
                case "v": {
                    float x = Float.parseFloat(parts[1]);
                    float y = Float.parseFloat(parts[2]);
                    float z = Float.parseFloat(parts[3]);
                    allVertices.add(mat.transformPoint(new Vector3(x, y, z)));
                    break;
                }
                case "vt": {
                    float x = Float.parseFloat(parts[1]);
                    float y = Float.parseFloat(parts[2]);
                    allTexCoords.add(new Vector2(x, y));
                    break;
                }
                case "vn": {
                    if (!settings.calculateNormals) {
                        float x = Float.parseFloat(parts[1]);
                        float y = Float.parseFloat(parts[2]);
                        float z = Float.parseFloat(parts[3]);
                        allNormals.add(nmat.transform(new Vector3(x, y, z)));
                    }
                    break;
                }
                case "f": {
                    ArrayList<Vertex> face = new ArrayList<>(parts.length - 1);

                    for (int i = 1; i < parts.length; i++) {
                        String[] info = parts[i].split("/");
                        int vertex = Integer.parseInt(info[0]) - 1;
                        int texCoord = info[1].isEmpty() ? -1 : Integer.parseInt(info[1]) - 1;
                        int normal = info[2].isEmpty() || settings.calculateNormals ? -1 : Integer.parseInt(info[2]) - 1;

                        Vector3 v = allVertices.get(vertex);
                        Vector3 n = allNormals.get(normal);
                        Vector2 t = texCoord == -1 ? Vector2.zero() : allTexCoords.get(texCoord);

                        int ti = currentMaterial.texture == null ? -1 : allTextures.indexOf(currentMaterial.texture);
                        face.add(new Vertex(v, n, t, ti, currentMaterial.diffuse));
                    }

                    triangles.addAll(EarTriangulator.triangulate(face));
                    break;
                }
            }

            line++;
        }

        float[] vertices = new float[triangles.size() * 3];
        float[] texcoords = new float[triangles.size() * 2];
        float[] normals = new float[triangles.size() * 3];
        float[] colors = new float[triangles.size() * 4];
        int[] texunits = new int[triangles.size()];

        int vindex = 0;
        int tcindex = 0;
        int nindex = 0;
        int cindex = 0;
        int tuindex = 0;

        for (Vertex v : triangles) {
            vindex += copyVector3(v.position, vertices, vindex, 1);
            tcindex += copyVector2(v.texcoord, texcoords, tcindex, 1);
            nindex += copyVector3(v.normal, normals, nindex, 1);
            cindex += copyColorRGBA(v.color, colors, cindex, 1);
            texunits[tuindex++] = v.texunit;
        }

        if (allTextures.size() > 0) {
            MultiTexturedMaterial m = new MultiTexturedMaterial(allTextures.toArray(new Texture[0]));
            m.specular = settings.specular;
            return new MeshRenderer(new MultiTexturedMesh(TRIANGLES, vertices, normals, colors, texcoords, texunits), m);
        } else {
            Material m = new Material(settings.shader);
            m.specular = settings.specular;
            return new MeshRenderer(new Mesh(TRIANGLES, vertices, normals, colors, texcoords), m);
        }
    }

    public static LinkedList<Texture> parseMtl(String mtl, ModelImportSettings settings) throws IOException {
        Material current = null;
        LinkedList<Texture> textures = new LinkedList<>();

        for (String s : mtl.trim().split("\n")) {
            s = s.trim();
            String[] parts = s.split(" ");
            switch (parts[0]) {
                case "newmtl": {
                    current = new Material();
                    loadedMaterials.put(parts[1], current);
                    Material override = settings.getOverride(parts[1]);
                    if (override != null) {
                        if (override.texture != null) {
                            textures.add(override.texture);
                        }

                        current = null;
                    } else {
                        current = new Material();
                        loadedMaterials.put(parts[1], current);
                    }
                    break;
                }
                case "Ka": {
                    if (current != null && !settings.useDiffuseAsAmbient) {
                        float red = Float.parseFloat(parts[1]);
                        float green = Float.parseFloat(parts[2]);
                        float blue = Float.parseFloat(parts[3]);
                        current.ambient = new Color(red, green, blue);
                    }
                    break;
                }
                case "Kd": {
                    if (current != null) {
                        float red = Float.parseFloat(parts[1]);
                        float green = Float.parseFloat(parts[2]);
                        float blue = Float.parseFloat(parts[3]);
                        current.diffuse = new Color(red, green, blue);
                        if (settings.useDiffuseAsAmbient) {
                            current.ambient = new Color(red, green, blue);
                        }
                    }
                    break;
                }
                case "map_Kd": {
                    if (current != null) {
                        String path = "models/" + parts[1];
                        current.texture = getTexture(path, getStream(path));
                        textures.add(current.texture);
                    }
                    break;
                }
            }
        }

        return textures;
    }

    public static ViewModel rect(Material texture, float width, float height) {
        return rect(texture, 1, 1, width, height);
    }

    public static ViewModel rect(String material, float width, float height) {
        return rect(getMaterial(material), width, height);
    }

    public static MeshRenderer rect(Material texture, float hRepeat, float vRepeat, float width, float height) {
        float[] vertices = rectVertices(width, height);
        float[] texCoords = rectTexCoords(hRepeat, vRepeat);
        return new MeshRenderer(new Mesh(QUADS, vertices, copyVector3(Vector3.zAxis(), 4), copyColorRGBA(Color.WHITE, 4), texCoords), texture);
    }

    public static ViewModel rect(String material, float hRepeat, float vRepeat, float width, float height) {
        return rect(getMaterial(material), hRepeat, vRepeat, width, height);
    }

    public static ViewModel rect(Color color, float width, float height) {
        float[] vertices = rectVertices(width, height);
        float[] colors = copyColorRGBA(color, 4);
        float[] texCoords = rectTexCoords(1.0f, 1.0f);
        return new MeshRenderer(new Mesh(QUADS, vertices, copyVector3(Vector3.zAxis(), 4), colors, texCoords), new Material());
    }

    public static MeshRenderer plane(Color color, float width, float height, int sub) {
        int verts = 4 * (1 + sub) * (1 + sub);
        float[] vertices = new float[verts * 3];
        float[] texcoords = new float[verts * 2];

        int vpos = 0;
        int tpos = 0;

        for (int i = 0; i < sub; i++) {
            float tx1 = (i + 0.0f) / sub;
            float tx2 = (i + 1.0f) / sub;

            float vx1 = (tx1 - 0.5f) * width;
            float vx2 = (tx2 - 0.5f) * width;

            for (int j = 0; j < sub; j++) {
                float ty1 = (j + 0.0f) / sub;
                float ty2 = (j + 1.0f) / sub;

                float vy1 = (ty1 - 0.5f) * width;
                float vy2 = (ty2 - 0.5f) * width;

                vpos += copyVector3(new Vector3(vx1, vy1), vertices, vpos, 1);
                vpos += copyVector3(new Vector3(vx2, vy1), vertices, vpos, 1);
                vpos += copyVector3(new Vector3(vx2, vy2), vertices, vpos, 1);
                vpos += copyVector3(new Vector3(vx1, vy2), vertices, vpos, 1);

                tpos += copyVector2(new Vector2(tx1, ty1), texcoords, tpos, 1);
                tpos += copyVector2(new Vector2(tx2, ty1), texcoords, tpos, 1);
                tpos += copyVector2(new Vector2(tx2, ty2), texcoords, tpos, 1);
                tpos += copyVector2(new Vector2(tx1, ty2), texcoords, tpos, 1);
            }
        }

        float[] colors = copyColorRGBA(color, verts);
        return new MeshRenderer(new Mesh(QUADS, vertices, copyVector3(Vector3.zAxis(), verts), colors, texcoords), new Material());
        //return new Mesh(QUADS, new Material(), new AbstractMesh.Data("Vertex", 3, vertices), new AbstractMesh.Data("Normal", 3, copyVector3(Vector3.zAxis(), verts)), new AbstractMesh.Data("Color", 4, colors), new AbstractMesh.Data("TexCoord", 2, texcoords));
    }

    public static ViewModel sprite(Texture image, float xscl, float yscl) {
        return rect(new Material(image, ShaderProgram.TEXTURE), image.getWidth() * xscl, image.getHeight() * yscl);
    }

    public static ViewModel sprite(String image, float xscl, float yscl) {
        return sprite(getTexture(image), xscl, yscl);
    }

    public static ViewModel ellipse(Material texture, float width, float height, int npoints) {
        float[] vertices = ellipseVertices(width, height, npoints);
        float[] texCoords = ellipseTexCoords(npoints);
        return new MeshRenderer(new Mesh(POLYGON, vertices, copyVector3(Vector3.zAxis(), npoints), copyColorRGBA(Color.WHITE, npoints), texCoords), texture);
    }

    public static ViewModel ellipse(String texture, float width, float height, int npoints) {
        return ellipse(getMaterial(texture), width, height, npoints);
    }

    public static ViewModel ellipse(Color color, float width, float height, int npoints) {
        float[] vertices = ellipseVertices(width, height, npoints);
        float[] colors = copyColorRGBA(color, npoints);
        return new MeshRenderer(new Mesh(POLYGON, vertices, copyVector3(Vector3.zAxis(), npoints), colors, new float[0]), new Material());
    }

    public static float[] pipeVertices(float radius, float length, int lod) {
        float[] f = new float[vertexDataPipe(lod)];
        pipeVertices(radius, length, lod, f, 0);
        return f;
    }

    public static int pipeVertices(float radius, float length, int lod, float[] dest, int start) {
        length /= 2;
        float angleStep = (Trig.FULL_CIRCLE / lod);

        int index = start;
        for (int i = 0; i < lod; i++) {
            float angle = i * angleStep;

            float x1 = Trig.cos(angle) * radius;
            float z1 = Trig.sin(angle) * radius;

            float x2 = Trig.cos(angle + angleStep) * radius;
            float z2 = Trig.sin(angle + angleStep) * radius;

            Vector3 vert1 = new Vector3(x1, -length, z1);
            Vector3 vert2 = new Vector3(x1, length, z1);
            Vector3 vert3 = new Vector3(x2, length, z2);
            Vector3 vert4 = new Vector3(x2, -length, z2);

            index += copyVector3(vert1, dest, index, 1);
            index += copyVector3(vert2, dest, index, 1);
            index += copyVector3(vert3, dest, index, 1);
            index += copyVector3(vert4, dest, index, 1);
        }

        return vertexDataPipe(lod);
    }

    public static float[] pipeTexCoords(float hRepeat, float vRepeat, int lod) {
        float[] f = new float[texCoordDataPipe(lod)];
        pipeTexCoords(hRepeat, vRepeat, lod, f, 0);
        return f;
    }

    public static int pipeTexCoords(float hRepeat, float vRepeat, int lod, float[] dest, int start) {
        float texStep = (1.0f / lod);

        int index = start;
        for (int i = 0; i < lod; i++) {
            float tex = i * texStep;

            Vector2 tex1 = new Vector2(tex * hRepeat, vRepeat);
            Vector2 tex2 = new Vector2(tex * hRepeat, 0);
            Vector2 tex3 = new Vector2((tex + texStep) * hRepeat, 0);
            Vector2 tex4 = new Vector2((tex + texStep) * hRepeat, vRepeat);

            index += copyVector2(tex1, dest, index, 1);
            index += copyVector2(tex2, dest, index, 1);
            index += copyVector2(tex3, dest, index, 1);
            index += copyVector2(tex4, dest, index, 1);
        }

        return texCoordDataPipe(lod);
    }

    public static float[] pipeNormals(int lod) {
        float[] f = new float[vertexDataPipe(lod)];
        pipeNormals(lod, f, 0);
        return f;
    }

    public static int pipeNormals(int lod, float[] dest, int start) {
        return pipeVertices(1.0f, 0.0f, lod, dest, start);
    }

    public static ViewModel pipe(Material sides, float hRepeat, float vRepeat, float radius, float length, int lod) {
        return new MeshRenderer(new Mesh(QUADS, pipeVertices(radius, length, lod), pipeNormals(lod), copyColorRGBA(Color.WHITE, vertexDataPipe(lod) / 3), pipeTexCoords(hRepeat, vRepeat, lod)), sides);
    }

    public static ViewModel pipe(String material, float hRepeat, float vRepeat, float radius, float length, int lod) {
        return pipe(getMaterial(material), hRepeat, vRepeat, radius, length, lod);
    }

    public static int sphereVertices(float radius, int lod, float[] dest, int start) {
        float step = Trig.FULL_CIRCLE / lod;

        int vlod = lod / 2;

        int index = start;
        for (int i = 0; i < lod; i++) {
            float lon = i * step;
            for (int j = 0; j < vlod; j++) {
                float lat = j * step - Trig.QUARTER_CIRCLE;
                Vector3 vert1 = Vector3.fromAngles(new Angles(lat, lon, 0.0f), radius);
                Vector3 vert2 = Vector3.fromAngles(new Angles(lat, lon + step, 0.0f), radius);
                Vector3 vert3 = Vector3.fromAngles(new Angles(lat + step, lon + step, 0.0f), radius);
                Vector3 vert4 = Vector3.fromAngles(new Angles(lat + step, lon, 0.0f), radius);

                index += copyVector3(vert1, dest, index, 1);
                index += copyVector3(vert2, dest, index, 1);
                index += copyVector3(vert3, dest, index, 1);
                index += copyVector3(vert4, dest, index, 1);
            }
        }

        return vertexDataSphere(lod);
    }

    public static float[] sphereVertices(float radius, int lod) {
        float[] f = new float[vertexDataSphere(lod)];
        sphereVertices(radius, lod, f, 0);
        return f;
    }

    public static int sphereTexCoords(int lod, float[] dest, int start) {
        float texstep = 1.0f / lod;

        int vlod = lod / 2;

        int index = start;
        for (int i = lod; i > 0; i--) {
            float u = i * texstep;
            for (int j = 0; j < vlod; j++) {
                float v = j * texstep * 2;
                Vector2 tex1 = new Vector2(u + texstep, v);
                Vector2 tex2 = new Vector2(u, v);
                Vector2 tex3 = new Vector2(u, v + texstep * 2);
                Vector2 tex4 = new Vector2(u + texstep, v + texstep * 2);

                index += copyVector2(tex1, dest, index, 1);
                index += copyVector2(tex2, dest, index, 1);
                index += copyVector2(tex3, dest, index, 1);
                index += copyVector2(tex4, dest, index, 1);
            }
        }

        return texCoordDataSphere(lod);
    }

    public static float[] sphereTexCoords(int lod) {
        float[] f = new float[texCoordDataSphere(lod)];
        sphereTexCoords(lod, f, 0);
        return f;
    }

    public static int sphereNormals(int lod, float[] dest, int start) {
        return sphereVertices(1.0f, lod, dest, start);
    }

    public static float[] sphereNormals(int lod) {
        return sphereVertices(1.0f, lod);
    }

    public static ViewModel sphere(Material mtl, float radius, int lod) {
        return new MeshRenderer(new Mesh(QUADS, sphereVertices(radius, lod), sphereNormals(lod), copyColorRGBA(Color.WHITE, vertexDataSphere(lod) / 3), sphereTexCoords(lod)), mtl);
    }

    public static ViewModel sphere(String mtl, float radius, int lod) {
        return sphere(getMaterial(mtl), radius, lod);
    }

    public static float[] rectVertices(float width, float height) {
        width /= 2;
        height /= 2;
        return new float[] { width, -height, 0, width, height, 0, -width, height, 0, -width, -height, 0 };
    }

    public static int rectVertices(float width, float height, float[] dest, int start) {
        System.arraycopy(rectVertices(width, height), 0, dest, start, 12);

        return VERTEX_DATA_QUAD;
    }

    public static float[] ellipseTexCoords(int lod) {
        float[] f = new float[texCoordDataPolygon(lod)];
        ellipseTexCoords(lod, f, 0);
        return f;
    }

    public static int ellipseTexCoords(int lod, float[] dest, int start) {

        float step = (float) (2 * Math.PI / lod);
        for (int i = 0; i < lod; i++) {
            dest[i * 2 + start + 0] = -0.49f * Trig.cos(i * step) + 0.5f;
            dest[i * 2 + start + 1] = 0.49f * Trig.sin(i * step) + 0.5f;
        }

        return texCoordDataPolygon(lod);
    }

    public static float[] ellipseVertices(float width, float height, int lod) {
        float[] f = new float[vertexDataPolygon(lod)];
        ellipseVertices(width, height, lod, f, 0);
        return f;
    }

    public static int ellipseVertices(float width, float height, int lod, float[] dest, int start) {
        width /= 2;
        height /= 2;

        float step = Trig.FULL_CIRCLE / lod;
        for (int i = 0; i < lod; i++) {
            dest[i * 3 + start + 0] = width * Trig.cos(i * step);
            dest[i * 3 + start + 1] = height * Trig.sin(i * step);
            dest[i * 3 + start + 2] = 0.0f;
        }

        return vertexDataPolygon(lod);
    }

    public static float[] rectTexCoords(float hRepeat, float vRepeat) {
        float[] f = new float[TEXCOORD_DATA_QUAD];
        rectTexCoords(hRepeat, vRepeat, f, 0);
        return f;
    }

    public static int rectTexCoords(float hRepeat, float vRepeat, float[] dest, int start) {
        dest[start + 0] = 0;
        dest[start + 1] = 0;
        dest[start + 2] = 0;
        dest[start + 3] = vRepeat;
        dest[start + 4] = hRepeat;
        dest[start + 5] = vRepeat;
        dest[start + 6] = hRepeat;
        dest[start + 7] = 0;

        return TEXCOORD_DATA_BOX;
    }

    public static ViewModel box(Material texture, float width, float height, float depth) {
        return box(texture, 1, 1, 1, width, height, depth);
    }

    public static ViewModel box(String material, float width, float height, float depth) {
        return box(getMaterial(material), width, height, depth);
    }

    public static ViewModel box(Material texture, float xRepeat, float yRepeat, float zRepeat, float width, float height, float depth) {
        float[] vertices = boxVertices(width, height, depth);
        float[] texCoords = boxTexCoords(xRepeat, yRepeat, zRepeat);
        return new MeshRenderer(new Mesh(QUADS, vertices, boxNormals(), copyColorRGBA(Color.WHITE, 24), texCoords), texture);
    }

    public static ViewModel box(String material, float xRepeat, float yRepeat, float zRepeat, float width, float height, float depth) {
        return box(getMaterial(material), xRepeat, yRepeat, zRepeat, width, height, depth);
    }

    public static ViewModel box(Color color, float width, float height, float depth) {
        float[] vertices = boxVertices(width, height, depth);
        float[] colors = copyColorRGBA(color, VERTEX_DATA_BOX);
        return new MeshRenderer(new Mesh(QUADS, vertices, boxNormals(), colors, copyVector2(Vector2.zero(), 4 * 6)), new Material());
    }

    public static float[] boxVertices(float width, float height, float depth) {
        width /= 2;
        height /= 2;
        depth /= 2;
        return new float[] {
                // front (-z)
                -width, height, depth, -width, -height, depth, width, -height, depth, width, height, depth,
                // left (-x)
                -width, height, -depth, -width, -height, -depth, -width, -height, depth, -width, height, depth,
                // back (+z)
                width, height, -depth, width, -height, -depth, -width, -height, -depth, -width, height, -depth,
                // right (+x)
                width, height, depth, width, -height, depth, width, -height, -depth, width, height, -depth,
                // top (+y)
                width, height, -depth, -width, height, -depth, -width, height, depth, width, height, depth,
                // bottom (-y)
                width, -height, depth, -width, -height, depth, -width, -height, -depth, width, -height, -depth };
    }

    public static int boxVertices(float width, float height, float depth, float[] dest, int start) {
        System.arraycopy(boxVertices(width, height, depth), 0, dest, start, 6 * 4 * 3);

        return VERTEX_DATA_BOX;
    }

    public static float[] boxNormals() {
        float[] f = new float[VERTEX_DATA_BOX];
        boxNormals(f, 0);
        return f;
    }

    public static int boxNormals(float[] dest, int start) {
        copyVector3(Vector3.zAxis(), dest, start + 0 * 3, 4);
        copyVector3(Vector3.xAxis().negate(), dest, start + 4 * 3, 4);
        copyVector3(Vector3.zAxis().negate(), dest, start + 8 * 3, 4);
        copyVector3(Vector3.xAxis(), dest, start + 12 * 3, 4);
        copyVector3(Vector3.yAxis(), dest, start + 16 * 3, 4);
        copyVector3(Vector3.yAxis().negate(), dest, start + 20 * 3, 4);

        return VERTEX_DATA_BOX;
    }

    public static float[] polygonVertices(Vector3... points){
    float[] f = new float[vertexDataPolygon(points.length)];
    polygonVertices(points, f, 0);
    return f;
}

public static int polygonVertices(Vector3[] points, float[] dest, int start) {
        for (int i = 0; i < points.length; i++) {
            copyVector3(points[i], dest, start + i * 3, 1);
        }

        return vertexDataPolygon(points.length);
    }

    public static float[] boxTexCoords(float xRepeat, float yRepeat, float zRepeat) {
        float[] f = new float[TEXCOORD_DATA_BOX];
        boxTexCoords(xRepeat, yRepeat, zRepeat, f, 0);
        return f;
    }

    public static int boxTexCoords(float xRepeat, float yRepeat, float zRepeat, float[] dest, int start) {
        float[] hRepeats = { xRepeat, zRepeat, xRepeat, zRepeat, zRepeat, zRepeat };
        float[] vRepeats = { yRepeat, yRepeat, yRepeat, yRepeat, xRepeat, xRepeat };
        for (int i = 0; i < 6; i++) {
            dest[i * 8 + start + 0] = hRepeats[i];
            dest[i * 8 + start + 1] = 0;
            dest[i * 8 + start + 2] = hRepeats[i];
            dest[i * 8 + start + 3] = vRepeats[i];
            dest[i * 8 + start + 4] = 0;
            dest[i * 8 + start + 5] = vRepeats[i];
            dest[i * 8 + start + 6] = 0;
            dest[i * 8 + start + 7] = 0;
        }

        return TEXCOORD_DATA_BOX;
    }

    public static int copyVector2(Vector2 vector, float[] dest, int start, int num) {
        for (int i = 0; i < num; i++) {
            dest[i * 2 + start + 0] = vector.x;
            dest[i * 2 + start + 1] = vector.y;
        }

        return num * 2;
    }

    public static float[] copyVector2(Vector2 vector, int num) {
        float[] f = new float[num * 2];
        copyVector2(vector, f, 0, num);
        return f;
    }

    public static int copyVector3(Vector3 vector, float[] dest, int start, int num) {
        for (int i = 0; i < num; i++) {
            dest[i * 3 + start + 0] = vector.x;
            dest[i * 3 + start + 1] = vector.y;
            dest[i * 3 + start + 2] = vector.z;
        }

        return num * 3;
    }

    public static float[] copyVector3(Vector3 vector, int num) {
        float[] f = new float[num * 3];
        copyVector3(vector, f, 0, num);
        return f;
    }

    public static int copyColorRGB(Color color, float[] dest, int start, int num) {
        for (int i = 0; i < num; i++) {
            dest[i * 3 + start + 0] = color.red;
            dest[i * 3 + start + 1] = color.green;
            dest[i * 3 + start + 2] = color.blue;
        }

        return num * 3;
    }

    public static float[] copyColorRGB(Color color, int num) {
        float[] f = new float[num * 3];
        copyColorRGB(color, f, 0, num);
        return f;
    }

    public static int copyColorRGBA(Color color, float[] dest, int start, int num) {
        for (int i = 0; i < num; i++) {
            dest[i * 4 + start + 0] = color.red;
            dest[i * 4 + start + 1] = color.green;
            dest[i * 4 + start + 2] = color.blue;
            dest[i * 4 + start + 3] = color.alpha;
        }

        return num * 4;
    }

    public static float[] copyColorRGBA(Color color, int num) {
        float[] f = new float[num * 4];
        copyColorRGBA(color, f, 0, num);
        return f;
    }

    public static void transformVectors(float[] vectors, Matrix4x4 matrix, int start, int num) {
        for (int i = start; i < start + num; i += 3) {
            Vector3 vec = new Vector3(vectors[i], vectors[i + 1], vectors[i + 2]);
            Vector3 transformed = matrix.transformVector(vec);
            copyVector3(transformed, vectors, i, 1);
        }
    }

    public static void transformPoints(float[] vectors, Matrix4x4 matrix, int start, int num) {
        for (int i = start; i < start + num; i += 3) {
            Vector3 vec = new Vector3(vectors[i], vectors[i + 1], vectors[i + 2]);
            Vector3 transformed = matrix.transformPoint(vec);
            copyVector3(transformed, vectors, i, 1);
        }
    }

    public static Vector3 normal(Vector3 v1, Vector3 v2, Vector3 v3) {
        Vector3 u = Vector3.subtract(v2, v1);
        Vector3 v = Vector3.subtract(v3, v1);
        Vector3 n = Vector3.cross(u, v).normalize();

        return n;
    }

    public static float[] faceNormals(Vector3... vertices){
    float[] normals = new float[vertexDataPolygon(vertices.length)];
    faceNormals(vertices, normals, 0, vertices.length);
    return normals;
}

public static int faceNormals(Vector3[] vertices, float[] dest, int index, int num) {
        return copyVector3(normal(vertices[0], vertices[1], vertices[2]), dest, index, num);
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.entity.behavior;

import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.Trig;
import aspect.util.Vector3;
import static org.lwjgl.input.Keyboard.*;

import org.lwjgl.input.Mouse;

import static org.lwjgl.input.Mouse.*;

import org.lwjgl.opengl.Display;

public class PlayerControl extends Behavior {

    public float maxWalkSpeed = 4;
    public float maxStrafeSpeed = maxWalkSpeed / 1.2f;
    public float mouseSensitivity = 0.01f;
    public float walkAcceleration = maxWalkSpeed / 0.125f;

    private float speedForward;
    private float speedRight;
    
    public boolean grounded = true;
    
    public PlayerControl() {
        Mouse.setGrabbed(true);
    }

    public void move() {
        
        if (isKeyDown(KEY_W)) {
            if (speedForward <= maxWalkSpeed) {
                speedForward += walkAcceleration * Time.deltaTime();
            }
        } else if (speedForward > 0) {
            if (speedForward > walkAcceleration * Time.deltaTime()) {
                speedForward -= walkAcceleration * Time.deltaTime();
            } else {
                speedForward = 0;
            }
        }

        if (isKeyDown(KEY_S)) {
            if (speedForward >= -maxWalkSpeed) {
                speedForward -= walkAcceleration * Time.deltaTime();
            }
        } else if (speedForward < 0) {
            if (speedForward < -walkAcceleration * Time.deltaTime()) {
                speedForward += walkAcceleration * Time.deltaTime();
            } else {
                speedForward = 0;
            }
        }

        if (isKeyDown(KEY_A)) {
            if (speedRight >= -maxStrafeSpeed) {
                speedRight -= walkAcceleration * Time.deltaTime();
            }
        } else if (speedRight < 0) {
            if (speedRight < -walkAcceleration * Time.deltaTime()) {
                speedRight += walkAcceleration * Time.deltaTime();
            } else {
                speedRight = 0;
            }
        }

        if (isKeyDown(KEY_D)) {
            if (speedRight <= maxStrafeSpeed) {
                speedRight += walkAcceleration * Time.deltaTime();
            }
        } else if (speedRight > 0) {
            if (speedRight > walkAcceleration * Time.deltaTime()) {
                speedRight -= walkAcceleration * Time.deltaTime();
            } else {
                speedRight = 0;
            }
        }

        Vector3 forward = entity.transform.forward().xz().normalize().times(Time.deltaTime() * speedForward).asXZ();
        Vector3 sideways = entity.transform.right().xz().normalize().times(Time.deltaTime() * speedRight).asXZ();

        entity.transform.setPosition(entity.transform.position().plus(sideways));
        entity.transform.setPosition(entity.transform.position().plus(forward));
    }

    public void look() {
        
        Angles eulerAngles = entity.transform.eulerAngles();

        eulerAngles.pitch += getMouseDY() * mouseSensitivity;
        eulerAngles.yaw += getMouseDX() * mouseSensitivity;
        
        
        eulerAngles.pitch %= Trig.FULL_CIRCLE;
        eulerAngles.pitch += Trig.FULL_CIRCLE;
        eulerAngles.pitch %= Trig.FULL_CIRCLE;

        
        if (eulerAngles.pitch > Trig.QUARTER_CIRCLE && eulerAngles.pitch < Trig.HALF_CIRCLE) {
            eulerAngles.pitch = Trig.QUARTER_CIRCLE - 0.01f;
        }
        if (eulerAngles.pitch >= Trig.HALF_CIRCLE && eulerAngles.pitch < Trig.HALF_CIRCLE + Trig.QUARTER_CIRCLE) {
            eulerAngles.pitch = Trig.HALF_CIRCLE + Trig.QUARTER_CIRCLE + 0.01f;
        }
        
        entity.transform.setEulerAngles(eulerAngles);
        
        centerMouse();
    }
    
    public void stop() {
        speedForward = 0;
        speedRight = 0;
    }
    
    public static int getMouseDX() {
        return getX() - getCenterX();
    }
    
    public static int getMouseDY() {
        return getY() - getCenterY();
    }
    
    public static int getCenterX() {
        return Display.getWidth() / 2;
    }
    
    public static int getCenterY() {
        return Display.getHeight() / 2;
    }
    
    public static void centerMouse() {
        setCursorPosition(getCenterX(), getCenterY());
    }
    
    @Override
    public void update() {
        move();
        look();
    }
}

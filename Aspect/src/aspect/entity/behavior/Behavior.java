/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.entity.behavior;

import static aspect.core.AspectLauncher.mainWorld;

import aspect.entity.Entity;
import aspect.event.EntityEvent;
import aspect.event.KeyEvent;
import aspect.event.KeyListener;
import aspect.event.MouseEvent;
import aspect.event.MouseListener;
import aspect.physics.Collision;

/**
 * This class can be used to define custom behaviors for Entities. A single
 * Behavior instance can be used by multiple Entities, as the entity field will 
 * be set before the update and render functions are called. These functions
 * will be called individually for each Entity.
 * 
 * @author MillerV
 */
public class Behavior implements KeyListener, MouseListener {

    protected Entity entity;
    public boolean enabled = true;
    private int id;
    
    public void spawn(Entity ent) {
        if (entity == null || entity.container == null) {
            mainWorld.add(ent);
        } else {
            entity.container.add(ent);
        }
    }
    
    public final void initialize(Entity ent, int id) {
        if (this.entity != null) {
            throw new IllegalStateException("This Behavior of type " + getClass().getSimpleName() + " is already added to the Entity " + entity.getName());
        }
        
        this.entity = ent;
        this.id = id;
    }
    
    public int getID() {
        return id;
    }

    public Entity spawn(String prefab) {
        Entity ent = new Entity(prefab);
        spawn(ent);
        return ent;
    }

    /**
     * Called when this Behavior is added to a new Entity. The entity field will
     * be set to the new Entity.
     */
    public void onAdd() {

    }

    /**
     * Called during the update period of a frame. Use this method to update
     * the Entity's information. The entity field will be set to the updating
     * Entity.
     */
    public void update() {

    }

    /**
     * Called during the render period of a frame. Use this method to give the
     * Behavior visuals. The entity field will be set to the rendering Entity.
     */
    public void render() {

    }

    /**
     * Called when this Component is removed from an owner Entity. Override this
     * method to add custom behavior.
     */
    public void onRemove() {

    }
    
    public void onCollision(Collision collision) {
        
    }
    
    /**
     * Called when an owner entity is removed from the World. The entity field 
     * will be set to the destroyed Entity.
     */
    public void onEntityDestroy() {
        
    }

    /**
     * Called by the owner Entity when an EntityEvent is fired. Override this
     * method to add custom behavior.
     *
     * @param event the EntityEvent
     */
    public void entityEvent(EntityEvent event) {

    }

    /**
     * Called when a key is pressed or released, but only if this Behavior is
     * registered as a key listener in AspectLauncher. Override this method to 
     * add custom behavior.
     *
     * @param event the KeyEvent
     */
    @Override
    public void keyEvent(KeyEvent event) {

    }

    /**
     * Called when a mouse button is pressed or released, the scroll wheel is
     * turned, or the mouse is moved, but only if this Behavior is registered as 
     * a key listener in AspectLauncher. Override this method to add custom 
     * behavior.
     *
     * @param event the MouseEvent
     */
    @Override
    public void mouseEvent(MouseEvent event) {

    }
}

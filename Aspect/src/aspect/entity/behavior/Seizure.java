/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.entity.behavior;

import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.util.Color;
import java.util.Random;


/**
 *
 * @author MillerV
 */
public class Seizure extends Behavior {
    private Random random;
    
    public Seizure() {
        this.random = new Random();
    }
    
    @Override
    public void update() {
        entity.getBehavior(MeshRenderer.class).material.diffuse = Color.random(false);
    }
}

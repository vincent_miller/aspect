/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.entity;

import aspect.core.AspectLauncher;
import aspect.core.AspectRenderer;
import aspect.entity.behavior.PlayerControl;
import aspect.util.Vector3;

/**
 *
 * @author MillerV
 */
public class Player extends Entity {
    public PlayerControl control;
    public void setWalkSpeed(float speed) {
        control.maxWalkSpeed = speed;
        control.maxStrafeSpeed = speed / 1.2f;
    }
    
    public static void create(Vector3 position) {
        AspectLauncher.mainWorld.add(new Player(position));
    }
    
    public Player(Vector3 pos) {
        transform.setPosition(pos);
        control = new PlayerControl();
        addBehavior(control);
        AspectRenderer.camera = transform;
        /*
        try {
            addBehavior(new AudioListener());
        } catch (LWJGLException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
}

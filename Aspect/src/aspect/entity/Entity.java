/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.entity;

import static aspect.core.AspectLauncher.addKeyListener;
import static aspect.core.AspectLauncher.addMouseListener;

import aspect.event.EntityEvent;
import aspect.event.KeyEvent;
import aspect.event.KeyListener;
import aspect.event.Listener;
import aspect.event.MouseEvent;
import aspect.event.MouseListener;
import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.entity.behavior.Behavior;
import aspect.render.ViewModel;
import aspect.resources.Resources;
import aspect.script.AssetLoader;
import aspect.script.ScriptBehavior;

import static aspect.resources.Resources.loadTextFile;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.physics.Motion;
import aspect.physics.RigidBody;
import aspect.util.Transform;
import aspect.world.World;

/**
 * An object in the 3D or 2D world. Update and render behavior is typically
 * defined by Components rather than by overriding methods in the Entity itself.
 *
 * @author MillerV
 */
public class Entity implements KeyListener, MouseListener {
    private static int currentID = 0;

    /**
     * The child Entities. Their update() and draw() methods will be called when
     * this Entity's update() and draw() methods are called, and they will be
     * drawn relative to the position of this Entity. Entities also function as
     * CameraViewpoints.
     */
    public final LinkedList<Entity> children;

    private final HashMap<Integer, Behavior> behaviors;

    /**
     * The colliders used in this Entity's collision detection. It is not
     * recommended that you add to this list yourself.
     *
     * @see aspect.entity.component.CmpntCollider
     */

    public final Transform transform;

    /**
     * The container that holds this Entity.
     */
    public World container;

    /**
     * The name of this Entity, which is returned by it's toString() method.
     */
    private String name;
    
    public final int id;
    
    public final JSONObject json;

    /**
     * Whether this Entity is solid. Setting this to false does not prevent
     * collision detection, but could be used to change collision handling
     * behavior.
     */
    public boolean solid = true;

    private ViewModel viewModel;
    private RigidBody rigidBody;
    private ConvexCollider collider;

    /**
     * Create a new Entity.
     */
    public Entity() {
        this(currentID++);
    }
    
    public Entity(int id) {
        this.json = new JSONObject();
        this.children = new LinkedList<>();
        this.behaviors = new HashMap<>();
        this.transform = new Transform();
        this.setName(getClass().getSimpleName());
        this.id = id;
        addKeyListener(this);
        addMouseListener(this);
    }

    public Entity(Behavior viewModel) {
        this();
        addBehavior(viewModel);
    }
    
    public Entity(String file) {
        this();
        loadPrefab(file);
        json.clear();
        json.put("prefab", file);
    }
    
    public Entity(JSONObject data) {
        this();
        load(data);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        json.put("name", name);
    }
    
    public final void onUpdate(Listener listener) {
        addBehavior(new Behavior() {
            @Override
            public void update() {
                listener.event();
            }
        });
    }
    
    public void onRender(Listener listener) {
        addBehavior(new Behavior() {
            @Override
            public void render() {
                listener.event();
            }
        });
    }


    /**
     * Add a Component to this Entity.
     *
     * @param cmpnt
     *            the Component to add
     */
    public final void addBehavior(Behavior cmpnt) {
        addBehavior(cmpnt, behaviors.size());
    }
    
    private final void addBehavior(Behavior cmpnt, int index) {
        cmpnt.initialize(this, index);
        behaviors.put(index, cmpnt);
        cmpnt.onAdd();

        if (viewModel == null && cmpnt instanceof ViewModel) {
            viewModel = (ViewModel) cmpnt;
        } else if (rigidBody == null && cmpnt instanceof RigidBody) {
            rigidBody = (RigidBody) cmpnt;
        } else if (collider == null && cmpnt instanceof ConvexCollider) {
            collider = (ConvexCollider) cmpnt;
        }
    }
    
    public final void addBehavior(String scriptFile) {
        addBehavior(scriptFile, Collections.emptyMap());
    }
    
    public final void addBehavior(String scriptFile, Map<String, String> args) {
        if (scriptFile.endsWith(".js")) {
            addBehavior(new ScriptBehavior(scriptFile, Collections.emptyMap(), args));
        } else {
            addBehavior(new ScriptBehavior("scripts/" + scriptFile + ".js", Collections.emptyMap(), args));
        }
    }

    /**
     * Remove the specified Behavior.
     *
     * @param b
     *            the Behavior to remove
     */
    public void removeBehavior(Behavior b) {
        b.onRemove();
        behaviors.remove(b.getID());

        if (viewModel != null && b instanceof ViewModel) {
            viewModel = null;
        } else if (rigidBody != null && b instanceof RigidBody) {
            rigidBody = null;
        } else if (collider != null && b instanceof ConvexCollider) {
            collider = null;
        }
    }

    /**
     * Remove a Behavior of the specified class, or whose class is a subclass of
     * the specified class.
     *
     * @param c
     *            the Class of the Behavior to remove
     */
    public void removeBehavior(Class<?> c) {
        if (!Behavior.class.isAssignableFrom(c)) {
            return;
        }

        Behavior toRemove = null;

        for (Behavior cmpnt : behaviors.values()) {
            if (c.isInstance(cmpnt)) {
                toRemove = cmpnt;
                break;
            }
        }

        if (toRemove != null) {
            removeBehavior(toRemove);
        }
    }

    /**
     * Get a Behavior of the specified class.
     *
     * @param <C>
     *            the type that will be returned
     * @param c
     *            the Class to search for
     * @return the Behavior or null if that Behavior was not found
     */
    @SuppressWarnings("unchecked")
    public <C extends Behavior> C getBehavior(Class<C> c) {
        for (Behavior cmpnt : behaviors.values()) {
            if (c.isInstance(cmpnt)) {
                return (C) cmpnt;
            }
        }

        return null;
    }
    
    public ScriptBehavior getBehavior(String name) {
        for (Behavior cmpnt : behaviors.values()) {
            if (cmpnt instanceof ScriptBehavior) {
                ScriptBehavior sb = (ScriptBehavior) cmpnt;
                if (sb.scriptName.equals(name)) {
                    return sb;
                }
            }
        }

        return null;
    }
    
    public Collection<Behavior> getBehaviors() {
        return behaviors.values();
    }

    public ViewModel viewModel() {
        return viewModel;
    }

    public ConvexCollider collider() {
        return collider;
    }

    public Motion motion() {
        return getBehavior(Motion.class);
    }

    public RigidBody rigidBody() {
        return rigidBody;
    }

    public void onRemove() {
        for (Behavior behavior : behaviors.values()) {
            behavior.onRemove();
        }
    }

    /**
     * Update all components and children of this Entity.
     */
    public void update() {
        for (Behavior b : behaviors.values()) {
            if (b.enabled) {
                b.update();
            }
        }

        for (Entity child : children) {
            child.update();
        }
    }

    public void onCollision(Collision col) {
        for (Behavior behavior : behaviors.values()) {
            behavior.onCollision(col);
        }
    }

    /**
     * Add the specified Entity as a child of this Entity.
     *
     * @param child
     *            the Entity to add as a child
     */
    public void addChild(Entity child) {
        children.add(child);

        if (child.transform.parent() == null) {
            child.transform.setParent(transform);
        }
    }

    /**
     * Render all components and children of this Entity.
     */
    public void render() {

        for (Behavior b : behaviors.values()) {
            if (b.enabled) {
                b.render();
            }
        }

        for (Entity child : children) {
            child.render();
        }
    }

    /**
     * Remove this Entity from it's container, if the container is not null.
     */
    public void destroy() {
        if (container != null) {
            container.remove(this);
        }

        for (Behavior behavior : behaviors.values()) {
            behavior.onEntityDestroy();
        }
    }

    /**
     * Fire an EntityEvent with the given name and data, which will be passed to
     * all components of this Entity.
     *
     * @param name
     *            the event name
     * @param args
     *            the event data
     */
    public void fireEvent(String name, Object... args) {
        fireEvent(new EntityEvent(Behavior.class, name, args));
    }

    /**
     * Fire an EntityEvent, which will be passed to all Components of this
     * Entity that are instances of the event's target class.
     *
     * @param event
     *            the EntityEvent to fire
     */
    public void fireEvent(EntityEvent event) {
        for (Behavior cmpnt : behaviors.values()) {
            event.fire(cmpnt);
        }
    }

    @Override
    public String toString() {
        if (getName() != null) {
            return getName();
        } else {
            return super.toString();
        }
    }

    @Override
    public void mouseEvent(MouseEvent event) {
        for (Behavior b : behaviors.values()) {
            b.mouseEvent(event);
        }
    }

    @Override
    public void keyEvent(KeyEvent event) {
        for (Behavior b : behaviors.values()) {
            b.keyEvent(event);
        }
    }

    private void load(JSONObject data) {
        String code = new String();
        if (data.containsKey("transform")) {
            JSONObject transform = (JSONObject) data.get("transform");
            if (transform.containsKey("position")) {
                code += "entity.transform.setPosition(" + transform.get("position") + ");\n";
            }
    
            if (transform.containsKey("scale")) {
                code += "entity.transform.setScale(" + transform.get("scale") + ");\n";
            }
    
            if (transform.containsKey("angles")) {
                code += "entity.transform.setEulerAngles(" + transform.get("angles") + ");\n";
            }
        }
    
        if (data.containsKey("prefab")) {
            loadPrefab((String) data.get("prefab"));
        }
        
        if (data.containsKey("name")) {
            setName(data.get("name").toString());
        }
    
        if (data.containsKey("components")) {
            JSONArray components = (JSONArray) data.get("components");
            for (Object o : components) {
                JSONObject behavior = (JSONObject) o;
                if (behavior.containsKey("script")) {
                    Map<String, String> params = new HashMap<>();
                    for (Object o2 : behavior.entrySet()) {
                        Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) o2;
                        if (!entry.getKey().equals("script")) {
                            params.put(entry.getKey().toString(), entry.getValue().toString());
                        }
                    }
                    addBehavior(behavior.get("script").toString(), params);
                } else {
                    code += "entity.addBehavior(" + behavior.get("code") + ");\n";
                }
            }
        }
    
        AssetLoader.applyCode(this, code);
    
        if (data.containsKey("children")) {
            JSONArray children = (JSONArray) data.get("children");
            for (Object o : children) {
                JSONObject child = (JSONObject) o;
                Entity childEntity = new Entity();
                childEntity.load(child);
                addChild(childEntity);
            }
        }
    
    }

    private void loadPrefab(String file) {
        if (!file.endsWith(".json")) {
            file = "prefabs/" + file + ".json";
        }
        
        try {
            String text = Resources.loadTextFile(file);
            JSONParser parser = new JSONParser();
    
            JSONObject obj = (JSONObject) parser.parse(text);
            load(obj);
    
        } catch (ParseException | ClassCastException ex) {
            Console.log(LogLevel.ERROR, "Prefab could not be loaded: ", ex);
        }
    }
}

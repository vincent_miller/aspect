package aspect.net;

import java.io.IOException;
import java.net.Socket;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

public class AspectClient {
    private static int id = -1;
    private static RemoteHost server;
    
    public static void run(String host) {
        run(host, AspectServer.DEFAULT_PORT);
    }
    
    public static void run(String host, int port) {
        try {
            server = new RemoteHost(new Socket(host, port));
            id = server.in.read();
        } catch (IOException e) {
            Console.log(LogLevel.ERROR, e);
        }
    }
    
    public static int id() {
        return id;
    }
    
    public static boolean connected() {
        return id != -1;
    }
}

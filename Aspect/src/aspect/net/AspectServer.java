package aspect.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

public class AspectServer {
    public static final int DEFAULT_PORT = 12345;

    private static ServerSocket socket;
    private static boolean running;
    private static HashMap<Integer, RemoteHost> clients;
    private static int currentID;

    public static void run() {
        run(DEFAULT_PORT);
    }

    public static void run(int port) {
        try {
            socket = new ServerSocket(port);
            clients = new HashMap<>();
            currentID = 0;
            running = true;

            new Thread(() -> {
                while (running) {
                    try {
                        RemoteHost client = new RemoteHost(socket.accept());
                        clients.put(currentID, client);
                        client.out.write(currentID++);
                    } catch (Exception e) {
                        Console.log(LogLevel.ERROR, e);
                    }
                }
            }).start();
        } catch (IOException e) {
            Console.log(LogLevel.ERROR, e);
        }
    }
}

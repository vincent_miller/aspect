package aspect.net;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

public class SendThread extends Thread {
    private boolean running = true;
    private final BlockingQueue<byte[]> outgoing;
    private final OutputStream out;
    
    public SendThread(BlockingQueue<byte[]> outgoing, OutputStream out) {
        this.outgoing = outgoing;
        this.out = out;
    }
    
    @Override
    public void run() {
        while (running) {
            try {
                byte[] b = outgoing.take();
                out.write(b);
            } catch (InterruptedException | IOException e) {
                Console.log(LogLevel.ERROR, e);
            }
        }
    }
    
    public void cancel() {
        running = false;
    }
    
    public static OutputStream createConcurrentOutput(OutputStream out) {
        BlockingQueue<byte[]> outgoing = new LinkedBlockingQueue<>();
        SendThread thread = new SendThread(outgoing, out);
        BlockingQueueOutputStream bqos = new BlockingQueueOutputStream(outgoing) {
            @Override
            public void close() throws IOException {
                thread.cancel();
                super.close();
            }
        };
        thread.start();
        return bqos;
    }
}

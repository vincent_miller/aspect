package aspect.net;

import java.io.IOException;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

public class RecieveThread extends Thread {
    private final RemoteHost client;
    private final MessageListener listener;
    private boolean running;
    
    public RecieveThread(RemoteHost client, MessageListener listener) {
        this.client = client;
        this.listener = listener;
    }
    
    @Override
    public void run() {
        running = true;
        
        while (running) {
            try {
                int type = client.in.read();
                listener.message(client, type);
            } catch (IOException e) {
                Console.log(LogLevel.ERROR, e);
            }
        }
    }
    
    public void cancel() {
        running = false;
    }
}

package aspect.net.entity;

import java.io.IOException;

import aspect.entity.behavior.Behavior;
import aspect.net.RemoteHost;
import aspect.util.Transform;

public class NetworkBinding extends Behavior {
    private Transform last;

    public static final int ID_POSITION = 0;
    public static final int ID_ORIENTATION = 1;
    public static final int ID_SCALE = 2;
    public static final int ID_DESTROY = 3;
    public static final int ID_END = 4;

    @Override
    public void onAdd() {
        last = entity.transform.copy();
    }

    public void read(RemoteHost host) throws IOException {
        int id;

        while ((id = host.in.read()) != ID_END) {
            switch (id) {
                case ID_POSITION: {
                    entity.transform.setPosition(host.in.readVector3());
                }

                case ID_ORIENTATION: {
                    entity.transform.setRotation(host.in.readVector3(), host.in.readVector3(), host.in.readVector3());
                }
                
                case ID_DESTROY: {
                    entity.destroy();
                }
                
                case ID_SCALE: {
                    entity.transform.setScale(host.in.readVector3());
                }
            }
        }
    }

    public void write(RemoteHost host) throws IOException {
        if (!entity.transform.position().equals(last.position())) {
            host.out.write(ID_POSITION);
            host.out.writeVector3(entity.transform.position());
        }

        if (!(entity.transform.right().equals(last.right()) && entity.transform.up().equals(last.up()) && entity.transform.forward().equals(last.forward()))) {
            host.out.write(ID_ORIENTATION);
            host.out.writeVector3(entity.transform.right());
            host.out.writeVector3(entity.transform.up());
            host.out.writeVector3(entity.transform.forward());
        }

        if (!entity.transform.scale().equals(last.scale())) {
            host.out.write(ID_SCALE);
            host.out.writeVector3(entity.transform.scale());
        }
        
        if (entity.container == null) {
            host.out.write(ID_DESTROY);
        }

        host.out.write(ID_END);
        host.out.flush();
        last = entity.transform.copy();
    }
}

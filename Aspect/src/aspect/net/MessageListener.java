package aspect.net;

public interface MessageListener {
    public void message(RemoteHost remote, int type);
}

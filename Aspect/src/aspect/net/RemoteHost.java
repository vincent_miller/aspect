package aspect.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.util.AspectInputStream;
import aspect.util.AspectOutputStream;

public class RemoteHost {
    public final InetAddress address;
    public final AspectInputStream in;
    public final AspectOutputStream out;
    public final Socket socket;
    
    public RemoteHost(Socket socket) throws IOException {
        this.address = socket.getInetAddress();
        this.socket = socket;
        this.in = new AspectInputStream(socket.getInputStream());
        this.out = new AspectOutputStream(SendThread.createConcurrentOutput(socket.getOutputStream()));
    }
    
    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            Console.log(LogLevel.ERROR, e);
        }
    }
}

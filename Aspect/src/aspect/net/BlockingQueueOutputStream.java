package aspect.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueOutputStream extends OutputStream {
    
    private BlockingQueue<byte[]> queue;
    private ByteArrayOutputStream stream;
    
    public BlockingQueueOutputStream(BlockingQueue<byte[]> queue) {
        this.queue = queue;
        this.stream = new ByteArrayOutputStream();
    }
    
    @Override
    public void flush() throws IOException {
        try {
            queue.put(stream.toByteArray());
            stream.reset();
            super.flush();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void write(int b) throws IOException {
        stream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        stream.write(b);
    }
    
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        stream.write(b);
    }
}

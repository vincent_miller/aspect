/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.world;

import static aspect.core.AspectRenderer.GeometryType.*;

import aspect.util.Vector3;
import aspect.entity.Entity;
import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.physics.dynamics.Force;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Debug;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

/**
 * A World that splits all Entities into eight sections based on their position.
 * As soon as a section reaches it's capacity, the section divides further. This
 * allows O(log N) insertion, O(log N) removal using World.remove(Entity), O(1)
 * removal using Entity.removeSelf(), and O(N) update and draw. Iterating
 * through all Entities is O(N), but is slower than iterating through a
 * ListWorld. Currently out of service.
 *
 * @author MillerV
 */
public class Octree implements World {

    private final OctreeSection root;

    private final int maxObjects;
    private final int maxLevels;
    private final LinkedList<Entity> toRemove;
    private final LinkedList<Entity> toAdd;
    private final LinkedList<Force> forces;

    private int checks = 0;
    private int count = 0;

    public Octree(BoundingBox bounds, int maxObjects, int maxLevels) {
        this.maxObjects = maxObjects;
        this.maxLevels = maxLevels;
        this.root = new OctreeSection(0, bounds);
        this.toRemove = new LinkedList<>();
        this.toAdd = new LinkedList<>();
        this.forces = new LinkedList<>();
    }

    @Override
    public boolean add(Entity ent) {
        if (root.getBounds(ent).inside(root.bounds)) {
            toAdd.add(ent);
            count++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Entity ent) {
        toRemove.add(ent);
        count--;
        return true;
    }

    @Override
    public void clear() {
        root.clear();
        count = 0;
    }

    @Override
    public void update() {
        checks = 0;
        LinkedList<Entity> outside = root.update();
        for (Entity entity : outside) {
            System.out.println("Entity: " + entity + " left the world.");
            entity.container = null;
            entity.onRemove();
            count--;
        }
        //System.out.println("Octree Checks: " + checks);
    }

    @Override
    public void render() {
        root.render();
    }

    @Override
    public Collection<Force> forces() {
        return forces;
    }

    @Override
    public void addForce(Force force) {
        forces.add(force);
    }

    @Override
    public void removeForce(Force force) {
        forces.remove(force);
    }

    private class OctreeSection {

        private static final int PX_PY_PZ = 0;
        private static final int PX_PY_NZ = 1;
        private static final int PX_NY_PZ = 2;
        private static final int PX_NY_NZ = 3;
        private static final int NX_PY_PZ = 4;
        private static final int NX_PY_NZ = 5;
        private static final int NX_NY_PZ = 6;
        private static final int NX_NY_NZ = 7;

        private final OctreeSection[] children;
        private final LinkedList<Entity> entities;

        private final BoundingBox bounds;

        private final int level;
        private Color color = Color.random();

        private OctreeSection(int level, BoundingBox bounds) {
            children = new OctreeSection[8];
            entities = new LinkedList<>();

            this.level = level;
            this.bounds = bounds;
        }
        
        private void forEach(Consumer<Entity> action) {
            entities.forEach(action);
            if (children[0] != null) {
                for (OctreeSection section : children) {
                    section.forEach(action);
                }
            }
        }

        private void clear() {
            Iterator<Entity> it = entities.iterator();
            while (it.hasNext()) {
                Entity entity = it.next();
                entity.onRemove();
                entity.container = null;
                it.remove();
            }

            for (int i = 0; i < 8; i++) {
                if (children[i] != null) {
                    children[i].clear();
                    children[i] = null;
                }
            }
        }

        private void divide() {
            BoundingBox[] octants = bounds.getOctants();

            for (int i = 0; i < 8; i++) {
                children[i] = new OctreeSection(level + 1, octants[i]);
            }

            ListIterator<Entity> it = entities.listIterator();

            while (it.hasNext()) {
                Entity entity = it.next();
                OctreeSection section = getCorrectSection(entity);
                if (section != this) {
                    it.remove();
                    section.add(entity);
                }
            }
        }

        private boolean add(Entity entity) {
            if (!getBounds(entity).inside(bounds)) {
                return false;
            } else {
                if (children[0] == null) {
                    if (entities.size() < maxObjects - 1 || level >= maxLevels) {
                        entities.add(entity);
                    } else {
                        divide();
                        place(entity);
                    }
                } else {
                    place(entity);
                }

                return true;
            }
        }

        private void place(Entity entity) {
            OctreeSection section = getCorrectSection(entity);
            if (section == this) {
                entities.add(entity);
            } else {
                section.add(entity);
            }
        }

        private void render() {
            if (entities.size() > 0) {
                GL11.glLineWidth(3);
            } else {
                GL11.glLineWidth(1);
            }
            Debug.showBounds(bounds, color);

            if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
                ShaderProgram.COLOR_UNIFORM.setUniform("color", color);
                ShaderProgram.lock(ShaderProgram.COLOR_UNIFORM);
            }
            for (Entity entity : entities) {
                entity.render();
            }
            ShaderProgram.unlock();

            if (children[0] != null) {
                for (OctreeSection child : children) {
                    child.render();
                }
            }
        }

        private LinkedList<Entity> update() {
            Iterator<Entity> it = toAdd.iterator();
            while (it.hasNext()) {
                Entity entity = it.next();
                if (getBounds(entity).inside(bounds)) {
                    getCorrectSection(entity).add(entity);
                    it.remove();
                }
            }

            LinkedList<Entity> pushed = new LinkedList<>();
            if (children[0] != null) {
                for (OctreeSection child : children) {
                    pushed.addAll(child.update());
                }
            }

            LinkedList<Entity> toPush = new LinkedList<>();
            it = entities.iterator();

            while (it.hasNext()) {
                Entity entity = it.next();
                entity.update();

                if (toRemove.contains(entity)) {
                    toRemove.remove(entity);
                    it.remove();
                    entity.container = null;
                    entity.onRemove();
                } else if (!getBounds(entity).inside(bounds)) {
                    toPush.add(entity);
                    it.remove();
                } else {
                    OctreeSection section = getCorrectSection(entity);
                    if (section != this) {
                        section.add(entity);
                        it.remove();
                    }
                }
            }

            for (Entity entity : pushed) {
                if (getBounds(entity).inside(bounds)) {
                    add(entity);
                } else {
                    toPush.add(entity);
                }
            }

            for (Entity entity : entities) {
                ConvexCollider col = entity.collider();

                if (col == null) {
                    continue;
                }

                boolean searching = false;
                for (Entity entity1 : entities) {
                    if (searching) {
                        if (entity1.collider() == null) {
                            continue;
                        }
                        checks++;
                        Collision c = col.colliding(entity1.collider());
                        if (c != null) {
                            entity.onCollision(c);
                            entity1.onCollision(new Collision(c.points, c.normal.negate(), c.displacement.negate(), entity));
                        }
                    }

                    if (entity1 == entity) {
                        searching = true;
                    }
                }
                if (children[0] != null) {
                    for (OctreeSection child : children) {
                        child.checkCollisions(entity);
                    }
                }
            }

            return toPush;
        }

        private void checkCollisions(Entity entity) {
            ConvexCollider col = entity.collider();
            BoundingBox b = col.bounds();

            if (!b.overlapping(bounds)) {
                return;
            }

            for (Entity entity1 : entities) {
                if (entity1.collider() == null) {
                    continue;
                }
                checks++;
                Collision c = col.colliding(entity1.collider());
                if (c != null) {
                    entity.onCollision(c);
                    entity1.onCollision(new Collision(c.points, c.normal.negate(), c.displacement.negate(), entity));
                }
            }

            if (children[0] != null) {
                for (OctreeSection child : children) {
                    child.checkCollisions(entity);
                }
            }
        }

        private BoundingBox getBounds(Entity ent) {
            BoundingBox entBounds;

            ConvexCollider c = ent.collider();
            if (c != null) {
                entBounds = c.bounds();
            } else {
                entBounds = new BoundingBox(ent.transform.position(), ent.transform.position());
            }

            return entBounds;
        }

        private OctreeSection getCorrectSection(Entity ent) {
            BoundingBox entBounds = getBounds(ent);

            Vector3 center = bounds.center();

            boolean px = entBounds.min.x > center.x;
            boolean nx = entBounds.max.x < center.x;
            boolean py = entBounds.min.y > center.y;
            boolean ny = entBounds.max.y < center.y;
            boolean pz = entBounds.min.z > center.z;
            boolean nz = entBounds.max.z < center.z;

            if (!((px || nx) && (py || ny) && (pz || nz)) || children[0] == null) {
                return this;
            }

            if (px) {
                if (py) {
                    if (pz) {
                        return children[PX_PY_PZ];
                    } else {
                        return children[PX_PY_NZ];
                    }
                } else {
                    if (pz) {
                        return children[PX_NY_PZ];
                    } else {
                        return children[PX_NY_NZ];
                    }
                }
            } else {
                if (py) {
                    if (pz) {
                        return children[NX_PY_PZ];
                    } else {
                        return children[NX_PY_NZ];
                    }
                } else {
                    if (pz) {
                        return children[NX_NY_PZ];
                    } else {
                        return children[NX_NY_NZ];
                    }
                }
            }
        }
    }

    @Override
    public void forEach(Consumer<Entity> action) {
        root.forEach(action);
    }

    @Override
    public int count() {
        return count;
    }

    @Override
    public <T extends Entity> T find(Class<T> c) {
        return null;
    }

    @Override
    public <T extends Entity> T find(Class<T> c, Predicate<? super T> filter) {
        return null;
    }

    @Override
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position, Predicate<? super T> filter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key, Predicate<? super T> filter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Entity> Collection<T> findAll(Class<T> c) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Entity> Collection<T> findAll(Class<T> c, Predicate<? super T> filter) {
        // TODO Auto-generated method stub
        return null;
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.world;

import aspect.entity.Entity;
import aspect.physics.ConvexCollider;
import aspect.physics.dynamics.Force;
import aspect.util.Vector3;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

/**
 * A collection of Entities that can all be updated or drawn at once.
 * 
 * @author MillerV
 */
public interface World {
    
    /**
     * Add an Entity to this World.
     * 
     * @param ent the Entity to add
     * @return whether the Entity could be added
     */
    public boolean add(Entity ent);
    
    /**
     * Remove an Entity from this World.
     * 
     * @param ent the Entity to remove
     * @return whether the Entity was removed.
     */
    public boolean remove(Entity ent);
    
    /**
     * Remove all entities from this world.
     */
    public void clear();

    /**
     * Update all entities contained within this World. This must also check for
     * collisions.
     */
    public void update();

    /**
     * Draw all entities contained within this World.
     */
    public void render();
    
    public int count();
    
    /**
     * Get a Collection of Forces that act on all RigidBodies in this World.
     * @return 
     */
    public Collection<Force> forces();
    
    /**
     * Add a force that acts on all RigidBodies in this World.
     * 
     * @param force the Force to add
     */
    public void addForce(Force force);
    
    /**
     * Remove a force from this World so that it no longer acts on all
     * RigidBodies.
     * 
     * @param force the Force to remove
     */
    public void removeForce(Force force);
    
    public void forEach(Consumer<Entity> action);
    
    public <T extends Entity> T find(Class<T> c);
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position);
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key);
    public <T extends Entity> Collection<T> findAll(Class<T> c);
    public <T extends Entity> T find(Class<T> c, Predicate<? super T> filter);
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position, Predicate<? super T> filter);
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key, Predicate<? super T> filter);
    public <T extends Entity> Collection<T> findAll(Class<T> c, Predicate<? super T> filter);
}

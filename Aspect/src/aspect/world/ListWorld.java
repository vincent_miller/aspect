/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.world;

import aspect.entity.Entity;
import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.physics.dynamics.Force;
import aspect.util.Vector3;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

/**
 * A basic world backed by a LinkedList of Entities. Provides guaranteed O(N)
 * removal, update, and draw, and O(1) insertion. Safe for use between multiple
 * threads.
 *
 * @author vincent
 */
public class ListWorld implements World {

    protected LinkedList<Entity> toAdd = new LinkedList<>();
    protected LinkedList<Entity> toRemove = new LinkedList<>();
    protected ConcurrentLinkedDeque<Entity> entities = new ConcurrentLinkedDeque<>();
    protected LinkedList<Force> forces = new LinkedList<>();

    private boolean iterating = false;

    private synchronized boolean iterating() {
        return iterating;
    }

    private synchronized void setIterating(boolean iterating) {
        this.iterating = iterating;
    }

    @Override
    public void update() {

        entities.addAll(toAdd);
        toAdd.clear();
        entities.removeAll(toRemove);
        toRemove.clear();

        setIterating(true);

        for (Entity ent : entities) {
            ent.update();
        }

        for (Entity entity : entities) {
            if (entity.collider() == null) {
                continue;
            }

            boolean checking = false;

            for (Entity entity1 : entities) {
                if (checking && (entity.rigidBody() != null || entity1.rigidBody() != null)) {
                    if (entity1.collider() == null) {
                        continue;
                    }
                    Collision c = entity.collider().colliding(entity1.collider());
                    if (c != null) {
                        entity.onCollision(c);
                        entity1.onCollision(new Collision(c.points, c.normal.negate(), c.displacement.negate(), entity));
                    }
                }

                if (entity1 == entity) {
                    checking = true;
                }
            }
        }

        setIterating(false);
    }

    @Override
    public void render() {

        setIterating(true);
        entities.addAll(toAdd);
        toAdd.clear();
        entities.removeAll(toRemove);
        toRemove.clear();
        for (Entity ent : entities) {
            ent.render();
        }
        setIterating(false);
    }

    @Override
    public boolean add(Entity ent) {
        if (ent != null) {
            ent.container = this;
            if (iterating()) {
                return toAdd.add(ent);
            } else {
                return entities.add(ent);
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Entity ent) {
        if (ent != null) {
            ent.onRemove();
            ent.container = null;
            if (iterating()) {
                return toRemove.add(ent);
            } else {
                return entities.remove(ent);
            }
        } else {
            return false;
        }
    }

    @Override
    public void clear() {
        if (iterating()) {
            toRemove.addAll(entities);
        } else {
            for (Entity entity : entities) {
                entity.onRemove();
                entity.container = null;
            }
            entities.clear();
        }
        toAdd.clear();
    }

    @Override
    public int count() {
        return entities.size();
    }

    public Entity[] toArray() {
        return entities.toArray(new Entity[count()]);
    }

    @Override
    public Collection<Force> forces() {
        return forces;
    }

    @Override
    public void addForce(Force force) {
        forces.add(force);
    }

    @Override
    public void removeForce(Force force) {
        forces.remove(force);
    }

    @Override
    public void forEach(Consumer<Entity> action) {
        boolean it = iterating();

        setIterating(true);
        entities.forEach(action);
        setIterating(it);
    }

    @Override
    public <T extends Entity> T find(Class<T> c) {
        return find(c, (f) -> true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Entity> T find(Class<T> c, Predicate<? super T> filter) {
        for (Entity entity : entities) {
            if (c.isAssignableFrom(entity.getClass()) && filter.test((T) entity)) {
                return (T) entity;
            }
        }

        return null;
    }

    @Override
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position) {
        return findNearest(c, position, (f) -> true);
    }

    @Override
    public <T extends Entity> T findNearest(Class<T> c, Vector3 position, Predicate<? super T> filter) {
        return findBest(c, (entity) -> -Vector3.distance2(position, entity.transform.position()), filter);
    }

    @Override
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key) {
        return findBest(c, key, (entity) -> true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Entity> T findBest(Class<T> c, ToDoubleFunction<? super T> key, Predicate<? super T> filter) {
        T best = null;
        double bestval = Float.NEGATIVE_INFINITY;
        for (Entity entity : entities) {
            if (c.isAssignableFrom(entity.getClass()) && filter.test((T) entity)) {
                double val = key.applyAsDouble((T) entity);
                if (val > bestval) {
                    best = (T) entity;
                    bestval = val;
                }
            }
        }

        return best;
    }

    @Override
    public <T extends Entity> Collection<T> findAll(Class<T> c) {
        return findAll(c, (e) -> true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Entity> Collection<T> findAll(Class<T> c, Predicate<? super T> filter) {
        LinkedList<T> results = new LinkedList<>();

        for (Entity entity : entities) {
            if (c.isAssignableFrom(entity.getClass()) && filter.test((T) entity)) {
                results.add((T) entity);
            }
        }

        return results;
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.event;

import org.lwjgl.opengl.Display;

/**
 *
 * @author MillerV
 */
public class MouseEvent {
    /**
     * The button that was pressed or released (if there is one). Buttons start
     * at 0 (the left button).
     */
    public final int button;
    /**
     * The x coordinate of the mouse at the time of the event.
     */
    public final int x;
    /**
     * The y coordinate of the mouse at the time of the event.
     */
    public final int y;
    
    public final int screenx;
    
    public final int screeny;
    /**
     * The change in y since the last frame.
     */
    public final int dx;
    /**
     * The change in x since the last frame.
     */
    public final int dy;
    /**
     * The mouse wheel rotation (if applicable).
     */
    public final int wheel;
    /**
     * The type of event, true if pressed or false if released.
     */
    public final boolean type;
    
    /**
     * The type for pressed events (true).
     */
    public static final boolean PRESSED = true;
    /**
     * The type for released events (false).
     */
    public static final boolean RELEASED = false;
    
    /**
     * Create a new MouseEvent.
     * 
     * @param button the button
     * @param x the x coordinate
     * @param y the y coordinate
     * @param dx the delta x
     * @param dy the delta y
     * @param wheel the wheel rotation
     * @param type the event type
     */
    public MouseEvent(int button, int x, int y, int dx, int dy, int wheel, boolean type) {
        this.button = button;
        this.x = x;
        this.y = y;
        this.screenx = Display.getX() + x;
        this.screeny = Display.getY() + y;
        this.dx = dx;
        this.dy = dy;
        this.wheel = wheel;
        this.type = type;
    }
    
    @Override
    public String toString() {
        return "Mouse Event: button = " + button + ", x = " + x + ", y = " + y + ", wheel = " + wheel + ", type = " + type;
    }
}

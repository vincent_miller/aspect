package aspect.event;

public interface AxisListener {
    public void axisEvent(AxisEvent evt);
}

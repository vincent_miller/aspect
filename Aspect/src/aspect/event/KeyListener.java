package aspect.event;


public interface KeyListener {
	public void keyEvent(KeyEvent event);
}

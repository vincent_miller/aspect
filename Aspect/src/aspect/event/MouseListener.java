package aspect.event;


public interface MouseListener {
	public void mouseEvent(MouseEvent event);
}

package aspect.event;

public class AxisEvent {
    public final int axis;
    public final float value;
    
    public AxisEvent(int axis, float value) {
        this.axis = axis;
        this.value = value;
    }
}

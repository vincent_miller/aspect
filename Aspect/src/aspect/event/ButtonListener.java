package aspect.event;

public interface ButtonListener {
    public void buttonEvent(ButtonEvent evt);
}

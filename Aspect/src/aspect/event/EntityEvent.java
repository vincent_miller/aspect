/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.event;

import aspect.entity.behavior.Behavior;

/**
 *
 * @author MillerV
 */
public class EntityEvent {
    /**
     * The event name, specified in the constructor.
     */
    public final String name;
    /**
     * The event data, specified in the constructor.
     */
    public final Object[] args;
    
    private final Class<? extends Behavior> targetType;

    /**
     * Create a new EntityEvent with the specified target type, name, and data.
     * 
     * @param targetType the target type
     * @param name the name
     * @param args the data
     */
    public EntityEvent(Class<? extends Behavior> targetType, String name, Object... args) {
        this.name = name;
        this.targetType = targetType;
        this.args = args;
    }

    /**
     * Fire this EntityEvent on the specified Component if the Component's type
     * is the target type (or a subclass).
     * 
     * @param cmpnt the Component to fire the even on
     */
    public void fire(Behavior cmpnt) {
        if (targetType.isInstance(cmpnt)) {
            cmpnt.entityEvent(this);
        }
    }
}

package aspect.event;

public class ButtonEvent {
    public final int button;
    public final boolean type;
    
    public ButtonEvent(int button, boolean type) {
        this.button = button;
        this.type = type;
    }
}

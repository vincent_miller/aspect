/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.event;

/**
 *
 * @author MillerV
 */
public class KeyEvent {
    /**
     * The key code. These codes can be found in the class Keyboard.
     * @see org.lwjgl.input.Keyboard
     */
    public final int key;
    /**
     * The character represented by the key, if there is one.
     */
    public final char character;
    /**
     * The type of event, true if pressed or false if released.
     */
    public final boolean type;
    
    /**
     * The type for pressed events (true).
     */
    public static final boolean PRESSED = true;
    /**
     * The type for released events (false).
     */
    public static final boolean RELEASED = false;
    
    /**
     * Create a new KeyEvent
     * 
     * @param code the key code
     * @param character the key character
     * @param type the event type
     */
    public KeyEvent(int code, char character, boolean type) {
        this.key = code;
        this.character = character;
        this.type = type;
    }
    
    @Override
    public String toString() {
        return "Key Event: key = " + character + ", code = " + key + ", type = " + type;
    }
}

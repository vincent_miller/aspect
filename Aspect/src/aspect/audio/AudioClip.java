/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.audio;

import java.io.InputStream;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import static org.lwjgl.openal.AL10.*;
import org.lwjgl.util.WaveData;

/**
 *
 * @author MillerV
 */
public class AudioClip {
    public final int id;
    
    public AudioClip(WaveData data) throws LWJGLException {
        IntBuffer ib = BufferUtils.createIntBuffer(1);
        alGenBuffers(ib);
        id = ib.get(0);
        
        int error = alGetError();
        if (error != AL_NO_ERROR) {
            throw new LWJGLException("Could not load audio: OpenAL error " + error);
        }
        
        alBufferData(id, data.format, data.data, data.samplerate);
        data.dispose();
    }
}

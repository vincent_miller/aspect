/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.audio;

import aspect.time.Time;
import aspect.util.Angles;
import static org.lwjgl.openal.AL10.*;
import aspect.entity.behavior.Behavior;
import aspect.physics.Motion;
import aspect.util.Matrix4x4;
import aspect.util.Transform;
import aspect.util.Vector3;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;

/**
 *
 * @author MillerV
 */
public class AudioListener extends Behavior {

    private static boolean initialized = false;

    public AudioListener() throws LWJGLException {
        if (!initialized) {
            AL.create();
            alGetError();
            initialized = true;
        } else {
            throw new LWJGLException("AudioListener already exists.");
        }
    }

    @Override
    public void onAdd() {
        update();
    }

    @Override
    public void update() {
        if (!entity.transform.moved()) {
            return;
        }
        
        Transform t = entity.transform.global();

        FloatBuffer pos = t.position().getBuffer();
        FloatBuffer vel = BufferUtils.createFloatBuffer(3);
        FloatBuffer ori = BufferUtils.createFloatBuffer(6);
        Motion motion = entity.getBehavior(Motion.class);

        if (motion != null) {
            motion.velocity.store(vel);
        } else {
            Vector3.zero().store(vel);
        }
        
        vel.flip();

        t.forward().store(ori);
        t.up().store(ori);
        
        ori.flip();

        alListener(AL_ORIENTATION, ori);
        alListener(AL_POSITION, pos);
        alListener(AL_VELOCITY, vel);
    }
}

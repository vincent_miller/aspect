/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.audio;

import aspect.entity.behavior.Behavior;
import aspect.event.EntityEvent;
import aspect.physics.Motion;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import static org.lwjgl.openal.AL10.*;

/**
 *
 * @author MillerV
 */
public class AudioSource extends Behavior {
    private final int id;
    private AudioClip clip;
    
    public AudioSource(AudioClip clip) throws LWJGLException {
        IntBuffer buff = BufferUtils.createIntBuffer(1);
        alGenSources(buff);
        id = buff.get(0);
        
        int error = alGetError();
        if (error != AL_NO_ERROR) {
            throw new LWJGLException("Could not load audio: OpenAL error " + error);
        }
        
        alSourcei(id, AL_BUFFER, clip.id);
        alSourcef(id, AL_PITCH, 1.0f);
        alSourcef(id, AL_GAIN, 1.0f);
    }
    
    @Override
    public void onAdd() {
        update();
    }
    
    @Override
    public void update() {
        if (!entity.transform.moved()) {
            return;
        }
        
        FloatBuffer pos = entity.transform.global().position().getBuffer();
        FloatBuffer vel = BufferUtils.createFloatBuffer(3);
        Motion motion = entity.getBehavior(Motion.class);
        
        if (motion != null) {
            motion.velocity.store(vel);
            vel.flip();
        }
        
        alSource(id, AL_POSITION, pos);
        alSource(id, AL_VELOCITY, vel);
    }
    
    public void play() {
        alSourcePlay(id);
    }
    
    public void pause() {
        alSourcePause(id);
    }
    
    public void stop() {
        alSourceStop(id);
    }
    
    @Override
    public void entityEvent(EntityEvent evt) {
        if (evt.name.equals("play") && (int)evt.args[0] == clip.id) {
            play();
        } else if (evt.name.equals("pause") && (int)evt.args[0] == clip.id) {
            pause();
        } else if (evt.name.equals("stop") && (int)evt.args[0] == clip.id) {
            stop();
        }
    }
}

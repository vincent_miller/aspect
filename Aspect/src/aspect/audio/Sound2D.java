/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.audio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A short sound effect. Large files should not be used, as the entire file must
 * be loaded into memory. To play large files, use a {@link  Music}. Compressed
 * files, such as mp3s, are not supported.
 * 
 * @author MillerV
 */
public class Sound2D {

    private Clip clip;
    private FloatControl gain;
    private BooleanControl mute;

    /**
     * The reference sound intensity for setting volume, in w/m^2.
     */
    public static final double REFERENCE_INTENSITY = 1;

    /**
     * Load sound data from an InputStream.
     * 
     * @param in the InputStream
     * @throws UnsupportedAudioFileException if the file format is not supported
     * @throws IOException if the data could not be read
     */
    public Sound2D(InputStream in) throws UnsupportedAudioFileException, IOException {
        this(AudioSystem.getAudioInputStream(new BufferedInputStream(in)));
    }

    /**
     * Load sound data from a URL (this must be a local URL, it will
     * not directly read data from a web site.
     * 
     * @param url the URL to read
     * @throws UnsupportedAudioFileException if the file format is not supported
     * @throws IOException if the data could not be read
     */
    public Sound2D(URL url) throws UnsupportedAudioFileException, IOException {
        this(AudioSystem.getAudioInputStream(url));
    }

    /**
     * Load sound data from an AudioInputStream.
     * 
     * @param stream the AudioInputStream
     * @throws IOException if the data could not be read
     */
    public Sound2D(AudioInputStream stream) throws IOException {
        try {
            clip = AudioSystem.getClip();
            clip.open(stream);
            gain = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            mute = (BooleanControl) clip.getControl(BooleanControl.Type.MUTE);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Sound2D.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Load sound data from a File.
     * 
     * @param file the file to read
     * @throws FileNotFoundException if the file does not exist
     * @throws UnsupportedAudioFileException if the file format is not supported
     * @throws IOException if the data could not be read
     */
    public Sound2D(File file) throws FileNotFoundException, UnsupportedAudioFileException, IOException {
        this(AudioSystem.getAudioInputStream(file));
        
    }

    /**
     * Load sound data from a file at the given location, relative to the run
     * directory.
     * 
     * @param filename the file name
     * @throws FileNotFoundException if the file does not exist
     * @throws UnsupportedAudioFileException if the file format is not supported
     * @throws IOException if the data could not be read
     */
    public Sound2D(String filename) throws FileNotFoundException, UnsupportedAudioFileException, IOException {
        this(new File(filename));
    }

    /**
     * Create an empty sound effect. Method calls will have no effect.
     */
    public Sound2D() {
    }

    /**
     * Play the sound clip from the current position.
     */
    public void play() {
        if (clip != null) {
            clip.start();
        }
    }

    /**
     * Play the sound clip from a given position.
     * 
     * @param position the position to play from, in milliseconds
     */
    public void playFrom(int position) {
        if (clip != null) {
            clip.setMicrosecondPosition(position * 1000);
            clip.start();
        }
    }

    /**
     * Play the sound clip from the beginning.
     */
    public void playFromBeginning() {
        if (clip != null) {
            playFrom(0);
        }
    }

    /**
     * Pause the sound clip, keeping the current position.
     */
    public void pause() {
        if (clip != null) {
            clip.stop();
        }
    }

    /**
     * Stop the sound clip, discarding the current position.
     */
    public void stop() {
        if (clip != null) {
            clip.setMicrosecondPosition(0);
            clip.stop();
        }
    }

    /**
     * Play the sound clip the specified number of times.
     * 
     * @param i the number of loops
     */
    public void loop(int i) {
        if (clip != null) {
            clip.loop(i);
        }
    }

    /**
     * Infinitely loop the sound clip.
     */
    public void loop() {
        if (clip != null) {
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        }
    }

    /**
     * Get the length of the sound clip.
     * 
     * @return the length in milliseconds.
     */
    public float getLength() {
        if (clip != null) {
            return (float)(clip.getMicrosecondLength() / 1e6f);
        } else {
            return 0;
        }
    }

    /**
     * Set the volume of the sound clip.
     * 
     * @param vol the new volume, in decibels.
     */
    public void setVolume(float vol) {
        if (clip != null) {
            if (vol > getMaxVolume()) {
                vol = getMaxVolume();
            }
            if (vol < getMinVolume()) {
                vol = getMinVolume();
            }
            gain.setValue(vol);
        }
    }

    /**
     * Set the volume of the sound clip, which is the base ten log of the
     * intensity.
     * 
     * @param intensity the new intensity, in w/m^2
     */
    public void setVolumeByIntensity(float intensity) {
        if (clip != null) {
            float volume = (float) (10 * Math.log10(intensity / REFERENCE_INTENSITY));
            setVolume(volume);
        }
    }
    
    /**
     * Set the volume based on distance. The new volume is the base ten log
     * of the reference intensity over distance^2.
     * 
     * @param distance the distance in meters
     */
    public void setVolumeByDistance(float distance) {
        setVolumeByIntensity(1 / (float)Math.pow(distance, 2));
    }

    /**
     * Get the minimum volume that the sound clip can be set to.
     * 
     * @return the minimum volume in decibels
     */
    public float getMinVolume() {
        if (clip != null) {
            return gain.getMinimum();
        } else {
            return 0;
        }
    }

    /**
     * Get the maximum volume that this sound clip can be set to.
     * 
     * @return the maximum volume in decibels
     */
    public float getMaxVolume() {
        if (clip != null) {
            return gain.getMaximum();
        } else {
            return 0;
        }
    }

    /**
     * Get the current volume that this sound clip is set to.
     * 
     * @return the current volume in decibels
     */
    public float getVolume() {
        if (clip != null) {
            return gain.getValue();
        } else {
            return 0;
        }
    }

    /**
     * Mute of unmute this sound clip. This does not stop playback.
     *
     * @param muted whether to mute or unmute the sound clip
     */
    public void setMuted(boolean muted) {
        if (clip != null) {
            mute.setValue(muted);
        }
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.audio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A large sound file that may be too big to load into memory. The file is
 * loaded using a {@link SourceDataLine} to read bytes as they are needed.
 * Compressed file types such as mp3 files are not supported.
 * 
 * @author MillerV
 */
public class Music {

    private SourceDataLine line;
    private final AudioInputStream audioStream;
    private final DataLine.Info info;
    private final AudioFormat format;
    private final byte[] buffer = new byte[4096];
    private boolean playing = false;
    private boolean stop = true;
    private boolean pause = false;

    /**
     * Create a new Music with the file at the given location, relative to the
     * run directory.
     * 
     * @param filename the file name
     * @throws UnsupportedAudioFileException
     * @throws java.io.FileNotFoundException
     */
    public Music(String filename) throws IOException, UnsupportedAudioFileException {
        this(new File(filename));
    }

    /**
     * Create a new Music with the given file.
     * 
     * @param file the source file
     * @throws UnsupportedAudioFileException
     * @throws java.io.FileNotFoundException
     */
    public Music(File file) throws IOException, UnsupportedAudioFileException {
        this(new FileInputStream(file));
    }

    public Music(InputStream stream) throws IOException, UnsupportedAudioFileException {
        audioStream = AudioSystem.getAudioInputStream(new BufferedInputStream(stream));
        audioStream.mark(Integer.MAX_VALUE);
        format = audioStream.getFormat();
        info = new DataLine.Info(SourceDataLine.class, format);
    }

    /**
     * Create a new empty Music. Nothing will happen when play() is called.
     */
    public Music() {
        this.audioStream = null;
        this.format = null;
        this.info = null;
    }

    /**
     * Play the sound file, either from the beginning if stop was called, or
     * from the current location if pause was called.
     * 
     * @param repeat whether the file should be looped.
     */
    public void play(final boolean repeat) {
        if (playing || audioStream == null) {
            return;
        }

        dispatchPlayThread(repeat);
    }

    private void dispatchPlayThread(boolean repeat) {
        new Thread(() -> {
            try {
                playing = true;
                pause = false;
                stop = false;

                line = (SourceDataLine) AudioSystem.getLine(info);
                line.open(format);
                line.start();

                do {
                    int read;
                    while ((read = audioStream.read(buffer)) != -1 && !(pause || stop)) {
                        line.write(buffer, 0, read);
                    }

                    if (!pause) {
                        audioStream.reset();
                    }
                } while (repeat && !(pause || stop));

                line.drain();
                line.close();
            } catch (IOException | LineUnavailableException ex) {
                Logger.getLogger(Music.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                playing = false;
            }
        }).start();
    }

    /**
     * Stop the sound file. The current position is not saved.
     */
    public void stop() {
        stop = true;
    }

    /**
     * Pause the sound file. The current position is saved.
     */
    public void pause() {
        pause = true;
    }
}

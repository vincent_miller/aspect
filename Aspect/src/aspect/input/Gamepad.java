package aspect.input;

import net.java.games.input.Component;
import net.java.games.input.Component.Identifier;

public class Gamepad extends Joystick {
    protected static Gamepad[] gamepads;
    
    protected int rxAxis = -1;
    protected int ryAxis = -1;
    protected int rzAxis = -1;

    protected Gamepad(net.java.games.input.Controller controller) {
        super(controller);
        
        for (int i = 0; i < getAxisCount(); i++) {
            if (axes[i].getIdentifier() == Identifier.Axis.RX) {
                rxAxis = i;
            } else if (axes[i].getIdentifier() == Identifier.Axis.RY) {
                ryAxis = i;
            } else if (axes[i].getIdentifier() == Identifier.Axis.RZ) {
                rzAxis = i;
            }
        }
    }
    
    public static int count() {
        if (!Controller.initialized()) {
            Controller.initialize();
        }
        
        return gamepads.length;
    }
    
    public static Gamepad get(int index) {
        if (!Controller.initialized()) {
            Controller.initialize();
        }
        
        return gamepads[index];
    }
    
    @Override
    public float getAxis(int index) {
        return axes[index].getPollData();
    }

    public float getRX() {
        return getAxis(rxAxis);
    }

    public float getRY() {
        return getAxis(rzAxis);
    }

    public float getRZ() {
        return getAxis(rzAxis);
    }
}

package aspect.input;

import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Input {
    public static float mouseVelocitySensitivity = 0.25f;
    public static float mousePositionSensitivity = 0.003f;

    public static final boolean MOUSE_POSITION = true;
    public static final boolean MOUSE_VELOCITY = false;

    public static boolean mouseMode = MOUSE_POSITION;

    public static float getAxis(String name, boolean clamp) {
        if (clamp) {
            return getAxis(name, 1);
        } else {
            return getAxis(name, Float.POSITIVE_INFINITY);
        }
    }

    public static float getAxis(String name, float clamp) {
        
        Joystick.count();

        name = name.toLowerCase();
        float value = 0.0f;

        if (name.equals("x")) {
            for (Joystick joystick : Joystick.joysticks) {
                value += joystick.getX();
            }

            for (Gamepad gamepad : Gamepad.gamepads) {
                value += gamepad.getX();
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
                value--;
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                value++;
            }
        } else if (name.equals("y")) {
            for (Joystick joystick : Joystick.joysticks) {
                value += joystick.getZ();
            }

            for (Gamepad gamepad : Gamepad.gamepads) {
                value += gamepad.getY();
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                value++;
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
                value--;
            }
        } else if (name.equals("rx")) {
            for (Joystick joystick : Joystick.joysticks) {
                value += joystick.getZRotation();
            }

            for (Gamepad gamepad : Gamepad.gamepads) {
                value += gamepad.getRX();
            }

            value += (mouseMode ? (Mouse.getX() - getCanvasWidth() / 2) * mousePositionSensitivity : Mouse.getDX() * mouseVelocitySensitivity);
        } else if (name.equals("ry")) {
            for (Joystick joystick : Joystick.joysticks) {
                value += joystick.getY();
            }

            for (Gamepad gamepad : Gamepad.gamepads) {
                value += gamepad.getRY();
            }

            value += (mouseMode ? (Mouse.getY() - getCanvasHeight() / 2) * mousePositionSensitivity : Mouse.getDY() * mouseVelocitySensitivity);
        }

        value = Math.max(value, -clamp);
        value = Math.min(value, clamp);

        return value;
    }
}

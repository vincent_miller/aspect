package aspect.input;

import net.java.games.input.Component;
import net.java.games.input.Component.Identifier;


public class Joystick extends Controller {
    protected static Joystick[] joysticks;
    
    protected int xAxis = -1;
    protected int yAxis = -1;
    protected int zAxis = -1;
    protected int zRotation = -1;
    protected int pov = -1;
    
    protected static final Identifier.Axis[] negate = { Identifier.Axis.Z };

    protected Joystick(net.java.games.input.Controller controller) {
        super(controller);
        
        for (int i = 0; i < getAxisCount(); i++) {
            if (axes[i].getIdentifier() == Identifier.Axis.X) {
                xAxis = i;
            } else if (axes[i].getIdentifier() == Identifier.Axis.Y) {
                yAxis = i;
            } else if (axes[i].getIdentifier() == Identifier.Axis.Z) {
                zAxis = i;
            } else if (axes[i].getIdentifier() == Identifier.Axis.POV) {
                pov = i;
            } else if (axes[i].getName().equalsIgnoreCase("Z Rotation")) {
                zRotation = i;
            }
        }
    }
    
    protected boolean negate(int axis) {
        for (Identifier.Axis a : negate) {
            if (a == axes[axis].getIdentifier()) {
                return true;
            }
        }
        
        return false;
    }
    
    public static int count() {
        if (!Controller.initialized()) {
            Controller.initialize();
        }
        
        return joysticks.length;
    }
    
    public static Joystick get(int index) {
        if (!Controller.initialized()) {
            Controller.initialize();
        }
        
        return joysticks[index];
    }
    
    @Override
    public float getAxis(int index) {
        if (negate(index)) {
            return -axes[index].getPollData();
        } else {
            return axes[index].getPollData();
        }
    }

    public float getX() {
        return getAxis(xAxis);
    }

    public float getY() {
        return getAxis(yAxis);
    }

    public float getZ() {
        return getAxis(zAxis);
    }
    
    public float getZRotation() {
        return getAxis(zRotation);
    }
    
    public float getPOV() {
        return getAxis(pov);
    }
}

package aspect.input;

import java.util.HashMap;
import java.util.LinkedList;

import net.java.games.input.Component;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;
import net.java.games.input.Component.Identifier;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.event.AxisEvent;
import aspect.event.AxisListener;
import aspect.event.ButtonEvent;
import aspect.event.ButtonListener;

public class Controller {
    protected net.java.games.input.Controller base;
    protected Component[] buttons;
    protected Component[] axes;

    private HashMap<String, Integer> namedAxes = new HashMap<>();
    private HashMap<String, Integer> namedButtons = new HashMap<>();

    private LinkedList<AxisListener> axisListeners = new LinkedList<>();
    private LinkedList<ButtonListener> buttonListeners = new LinkedList<>();

    private static Controller[] controllers;
    private static boolean initialized = false;

    public static boolean initialized() {
        return initialized;
    }

    public static void initialize() {
        net.java.games.input.Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
        int numJoysticks = 0;
        int numGamepads = 0;

        for (net.java.games.input.Controller controller : controllers) {
            net.java.games.input.Controller.Type type = controller.getType();
            if (type == net.java.games.input.Controller.Type.GAMEPAD) {
                numGamepads++;
            } else if (type == net.java.games.input.Controller.Type.STICK) {
                numJoysticks++;
            }
        }

        Joystick.joysticks = new Joystick[numJoysticks];
        Gamepad.gamepads = new Gamepad[numGamepads];
        Controller.controllers = new Controller[numJoysticks];

        int joystickIndex = 0;
        int gamepadIndex = 0;
        int controllerIndex = 0;

        for (net.java.games.input.Controller controller : controllers) {
            net.java.games.input.Controller.Type type = controller.getType();
            Controller c = null;

            if (type == net.java.games.input.Controller.Type.GAMEPAD) {
                Gamepad g = new Gamepad(controller);
                Gamepad.gamepads[gamepadIndex++] = g;
                c = g;
            } else if (type == net.java.games.input.Controller.Type.STICK) {
                Joystick j = new Joystick(controller);
                Joystick.joysticks[joystickIndex++] = j;
                c = j;
            }

            if (c != null) {
                Controller.controllers[controllerIndex++] = c;
            }
        }

        initialized = true;
    }

    public static void updateAll() {
        if (initialized()) {
            for (Controller controller : controllers) {
                controller.update();
            }
        }
    }

    private void update() {
        base.poll();

        Event evt = new Event();
        EventQueue queue = base.getEventQueue();

        if (buttonListeners.size() > 0 || axisListeners.size() > 0) {
            while (queue.getNextEvent(evt)) {
                Component cmpnt = evt.getComponent();

                if (cmpnt.getIdentifier() instanceof Identifier.Button) {
                    int index = indexOf(buttons, cmpnt);
                    if (index != -1) {
                        boolean state = evt.getValue() != 0;
                        for (ButtonListener listener : buttonListeners) {
                            listener.buttonEvent(new ButtonEvent(index, state));
                        }
                    }
                } else if (cmpnt.getIdentifier() instanceof Identifier.Axis) {
                    int index = indexOf(axes, cmpnt);
                    if (index != -1) {
                        float value = evt.getValue();
                        for (AxisListener listener : axisListeners) {
                            listener.axisEvent(new AxisEvent(index, value));
                        }
                    }
                }
            }
        }
    }

    public static void clearListeners() {
        if (initialized()) {
            for (Controller controller : controllers) {
                controller.axisListeners.clear();
                controller.buttonListeners.clear();
            }
        }
    }

    public void addAxisListener(AxisListener listener) {
        axisListeners.add(listener);
    }

    public void addButtonListener(ButtonListener listener) {
        buttonListeners.add(listener);
    }

    private static int indexOf(Component[] components, Component c) {
        for (int i = 0; i < components.length; i++) {
            if (components[i] == c) {
                return i;
            }
        }

        return -1;
    }

    public static int count() {
        if (!Controller.initialized()) {
            Controller.initialize();
        }

        return controllers.length;
    }

    public static Controller get(int index) {
        if (!Controller.initialized()) {
            Controller.initialize();
        }

        return controllers[index];
    }

    protected Controller(net.java.games.input.Controller controller) {
        this.base = controller;

        LinkedList<Component> axes = new LinkedList<>();
        LinkedList<Component> buttons = new LinkedList<>();

        for (Component component : controller.getComponents()) {
            if (component.getIdentifier() instanceof Identifier.Axis) {
                axes.add(component);
            } else if (component.getIdentifier() instanceof Identifier.Button) {
                System.out.println(component.getIdentifier());
                buttons.add(component);
            }
        }

        this.buttons = buttons.toArray(new Component[0]);
        this.axes = axes.toArray(new Component[0]);
    }

    public String getName() {
        return base.getName();
    }

    public String getAxisName(int axis) {
        String name = axes[axis].getName();
        namedAxes.put(name.toLowerCase(), axis);
        return name;
    }

    public String getButtonName(int button) {
        String name = buttons[button].getName();
        namedButtons.put(name.toLowerCase(), button);
        return name;
    }

    public int getAxisIndex(String name) {
        name = name.toLowerCase();
        if (namedAxes.containsKey(name)) {
            return namedAxes.get(name);
        } else {
            for (int i = 0; i < getAxisCount(); i++) {
                String n = getAxisName(i).toLowerCase();
                if (n.equals(name)) {
                    namedAxes.put(name, i);
                    return i;
                } else if (n.equals(name + " axis")) {
                    namedAxes.put(name + " axis", i);
                    return i;
                }
            }

            return -1;
        }
    }

    public int getButtonIndex(String name) {
        name = name.toLowerCase();
        if (namedButtons.containsKey(name)) {
            return namedButtons.get(name);
        } else {
            for (int i = 0; i < getButtonCount(); i++) {
                if (getButtonName(i).equalsIgnoreCase(name)) {
                    namedButtons.put(name, i);
                    return i;
                }
            }

            return -1;
        }
    }

    public int getAxisCount() {
        return axes.length;
    }

    public int getButtonCount() {
        return buttons.length;
    }

    public float getAxis(int axis) {
        String name = getAxisName(axis);
        return axes[axis].getPollData();
    }

    public float getAxis(String name) {
        int axis = getAxisIndex(name);
        if (axis != -1) {
            return getAxis(axis);
        } else {
            return 0.0f;
        }
    }

    public boolean getButton(int button) {
        return buttons[button].getPollData() == 1.0f;
    }

    public boolean getButton(String name) {
        int button = getButtonIndex(name);
        if (button != -1) {
            return getButton(button);
        } else {
            Console.log(LogLevel.ERROR, "Bad Axis: " + name);
            return false;
        }
    }
}

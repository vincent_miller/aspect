/*
 * Copyright (C) 2015 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

/**
 *
 * @author MillerV
 */
public class BoundingBox2D {

    public Vector2 min;
    public Vector2 max;

    public BoundingBox2D(Vector2 min, Vector2 max) {
        this.min = min;
        this.max = max;
        
        revalidate();
    }
    
    public static BoundingBox2D square(float dim) {
        return new BoundingBox2D(new Vector2(dim / -2.0f), new Vector2(dim / 2.0f));
    }
    
    public BoundingBox2D copy() {
        return new BoundingBox2D(min.copy(), max.copy());
    }
    
    public BoundingBox2D plus(Vector2 v) {
        return new BoundingBox2D(min.plus(v), max.plus(v));
    }
    
    public void set(BoundingBox2D bounds) {
        min = bounds.min;
        max = bounds.max;
    }

    public BoundingBox2D[] getQuadrants() {
        BoundingBox2D[] quadrants = new BoundingBox2D[4];

        Vector2 center = center();

        quadrants[0] = new BoundingBox2D(center.copy(), max.copy());
        quadrants[2] = new BoundingBox2D(new Vector2(center.x, min.y), new Vector2(max.x, center.y));
        quadrants[4] = new BoundingBox2D(new Vector2(min.x, center.y), new Vector2(center.x, max.y));
        quadrants[3] = new BoundingBox2D(min.copy(), center.copy());
        
        return quadrants;
    }

    public Vector2 center() {
        return Vector2.average(min, max);
    }

    public void setCenter(Vector2 center) {
        Vector2 diff = center.minus(center());

        min = min.plus(diff);
        max = max.plus(diff);
    }
    
    public void revalidate() {
        if (min.x > max.x) {
            float t = min.x;
            min.x = max.x;
            max.x = t;
        }
        
        if (min.y > max.y) {
            float t = min.y;
            min.y = max.y;
            max.y = t;
        }
    }

    public Vector2 dimensions() {
        return max.minus(min);
    }

    public boolean overlapping(BoundingBox2D b) {
        return (max.x > b.min.x || b.max.x > min.x) && (max.y > b.min.y || b.max.y > min.y);
    }

    public boolean inside(BoundingBox2D b) {
        return max.x < b.max.x && min.x > b.min.x && max.y < b.max.y && min.y > b.min.y;
    }
    
    public boolean contains(Vector2 p) {
        return p.x >= min.x && p.x <= max.x && p.y >= min.y && p.y <= max.y;
    }
    
    @Override
    public String toString() {
        return "BoundingBox (Center: " + center() + ", Dimensions: " + dimensions();
    }
}

package aspect.util;

/**
 * Static alternative to Java's built-in Random class. Provides more utilities for setting the bounds of resulting random numbers. 
 * Method names are capitalized to avoid naming conflicts with the underlying native types.
 * @author vincent
 *
 */
public class Random {
    public static final java.util.Random BASE = new java.util.Random();
    
    public static void setSeed(long seed) {
        BASE.setSeed(seed);
    }
    
    public static int Integer() {
        return BASE.nextInt();
    }
    
    public static int Integer(int bound) {
        return BASE.nextInt(bound);
    }
    
    public static int Integer(int min, int max) {
        return min + BASE.nextInt(max - min);
    }
    
    public static boolean Boolean() {
        return BASE.nextBoolean();
    }
    
    public static boolean Chance(float chance) {
        return BASE.nextFloat() < chance;
    }
    
    public static float Float() {
        return BASE.nextFloat();
    }
    
    public static float Float(float bound) {
        return BASE.nextFloat() * bound;
    }
    
    public static float Float(float min, float max) {
        return min + BASE.nextFloat() * (max - min);
    }
    
    public static Vector3 Vector3(float length) {
        return new Vector3(Float(), Float(), Float()).normalize().times(length);
    }
    
    public static Vector3 Vector3(BoundingBox bounds) {
        Vector3 dims = bounds.dimensions();
        return new Vector3(dims.x * Float(), dims.y * Float(), dims.z * Float()).plus(bounds.min);
    }
}

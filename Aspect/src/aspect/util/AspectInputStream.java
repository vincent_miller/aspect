package aspect.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class AspectInputStream extends DataInputStream {

    public AspectInputStream(InputStream in) {
        super(in);
    }
    
    public FloatBuffer readFloats(int num) throws IOException {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(num);
        readFloats(buffer);
        buffer.clear();
        return buffer;
    }

    public void readFloats(FloatBuffer buffer) throws IOException {
        for (int i = buffer.position(); i < buffer.limit(); i++) {
            buffer.put(readFloat());
        }
    }

    public Vector3 readVector3() throws IOException {
        return new Vector3(readFloats(3));
    }
    
    public Vector2 readVector2() throws IOException {
        return new Vector2(readFloats(2));
    }
    
    public Color readColor4() throws IOException {
        return new Color(readFloats(4), true);
    }
    
    public Color readColor3() throws IOException {
        return new Color(readFloats(3), false);
    }
    
    public Matrix4x4 readMatrix4x4() throws IOException {
        return new Matrix4x4(readFloats(16));
    }
    
    public Matrix3x3 readMatrix3x3() throws IOException {
        return new Matrix3x3(readFloats(9));
    }
    
    public Transform readTransform() throws IOException {
        Transform t = new Transform();
        t.set(readVector3(), readVector3(), readVector3(), readVector3(), readVector3());
        t.setOrthogonal(readBoolean());
        return t;
    }
    
    public Complex readComplex() throws IOException {
        return new Complex(readFloat(), readFloat());
    }
    
    public BoundingBox readBoundingBox() throws IOException {
        return new BoundingBox(readVector3(), readVector3());
    }
    
    public Angles readAngles() throws IOException {
        return new Angles(readFloat(), readFloat(), readFloat());
    }
}

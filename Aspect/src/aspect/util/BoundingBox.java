/*
 * Copyright (C) 2015 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

/**
 *
 * @author MillerV
 */
public class BoundingBox {

    public Vector3 min;
    public Vector3 max;

    public BoundingBox(Vector3 min, Vector3 max) {
        this.min = min;
        this.max = max;
        
        revalidate();
    }
    
    public static BoundingBox cube(float dim) {
        return new BoundingBox(new Vector3(dim / -2.0f), new Vector3(dim / 2.0f));
    }
    
    public BoundingBox copy() {
        return new BoundingBox(min.copy(), max.copy());
    }

    public BoundingBox[] getOctants() {
        BoundingBox[] octants = new BoundingBox[8];

        Vector3 center = center();

        octants[0] = new BoundingBox(center.copy(), max.copy());
        octants[1] = new BoundingBox(new Vector3(center.x, center.y, min.z), new Vector3(max.x, max.y, center.z));
        octants[2] = new BoundingBox(new Vector3(center.x, min.y, center.z), new Vector3(max.x, center.y, max.z));
        octants[3] = new BoundingBox(new Vector3(center.x, min.y, min.z), new Vector3(max.x, center.y, center.z));
        octants[4] = new BoundingBox(new Vector3(min.x, center.y, center.z), new Vector3(center.x, max.y, max.z));
        octants[5] = new BoundingBox(new Vector3(min.x, center.y, min.z), new Vector3(center.x, max.y, center.z));
        octants[6] = new BoundingBox(new Vector3(min.x, min.y, center.z), new Vector3(center.x, center.y, max.z));
        octants[7] = new BoundingBox(min.copy(), center.copy());
        
        return octants;
    }

    public Vector3 center() {
        return Vector3.average(min, max);
    }

    public void setCenter(Vector3 center) {
        Vector3 diff = center.minus(center());

        min = min.plus(diff);
        max = max.plus(diff);
    }
    
    public void revalidate() {
        if (min.x > max.x) {
            float t = min.x;
            min.x = max.x;
            max.x = t;
        }
        
        if (min.y > max.y) {
            float t = min.y;
            min.y = max.y;
            max.y = t;
        }
        
        if (min.z > max.z) {
            float t = min.z;
            min.z = max.z;
            max.z = t;
        }
    }

    public Vector3 dimensions() {
        return max.minus(min);
    }

    public boolean overlapping(BoundingBox b) {
        return (max.x > b.min.x || b.max.x > min.x) && (max.y > b.min.y || b.max.y > min.y) && (max.z > b.min.z || b.max.z > min.z);
    }

    public boolean inside(BoundingBox b) {
        return max.x < b.max.x && min.x > b.min.x && max.y < b.max.y && min.y > b.min.y && max.z < b.max.z && min.z > b.min.z;
    }
    
    @Override
    public String toString() {
        return "BoundingBox (Center: " + center() + ", Dimensions: " + dimensions();
    }
}

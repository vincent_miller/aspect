/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package aspect.util;

/**
 * This class is used for convenient access to trig functions in java.lang.Math,
 * using floats instead of doubles.
 * 
 * @author MillerV
 */
public class Trig {
    public static final float PI = (float)Math.PI;
    
    public static final float FULL_CIRCLE = 2 * PI;
    public static final float HALF_CIRCLE = PI;
    public static final float QUARTER_CIRCLE = PI / 2.0f;
    public static final float EIGHTH_CIRCLE = PI / 4.0f;
    
    public static float sin(float angle) {
        return (float)Math.sin(angle);
    }
    
    public static float cos(float angle) {
        return (float)Math.cos(angle);
    }
    
    public static float tan(float angle) {
        return (float)Math.tan(angle);
    }
    
    public static float asin(float sin) {
        return (float)Math.asin(sin);
    }
    
    public static float acos(float cos) {
        return (float)Math.acos(cos);
    }
    
    public static float atan(float tan) {
        return (float)Math.atan(tan);
    }
    
    public static float atan2(float y, float x) {
        return (float)Math.atan2(y, x);
    }
    
    public static float toRadians(float degrees) {
        return (float)Math.toRadians(degrees);
    }
    
    public static float toDegrees(float radians) {
        return (float)Math.toDegrees(radians);
    }
}

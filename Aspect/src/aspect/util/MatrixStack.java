package aspect.util;

import java.util.Stack;

public class MatrixStack {
    private Stack<Matrix4x4> stack = new Stack<>();
    private Matrix4x4 current = Matrix4x4.identity();

    public void push() {
        stack.push(new Matrix4x4(current));
    }

    public Matrix4x4 pop() {
        Matrix4x4 temp = current;
        if (!stack.isEmpty()) {
            current = stack.pop();
        } else {
            System.err.println("No matrices left to pop");
            current = Matrix4x4.identity();
        }
        return temp;
    }

    public void loadIdentity() {
        if (stack.isEmpty()) {
            current = Matrix4x4.identity();
        } else {
            current = new Matrix4x4(stack.peek());
        }
    }

    public void set(Matrix4x4 mat) {
        current = mat;
    }

    public void clear() {
        stack.clear();
        current = Matrix4x4.identity();
    }

    public Matrix4x4 current() {
        return current;
    }

    public Vector3 transformVector(Vector3 vec) {
        return current.transformVector(vec);
    }

    public Vector3 transformPoint(Vector3 point) {
        return current.transformPoint(point);
    }

    public void multiply(Matrix4x4 mat) {
        current = current.times(mat);
    }

    public void add(Matrix4x4 mat) {
        current = current.plus(mat);
    }

    public void subtract(Matrix4x4 mat) {
        current = current.minus(mat);
    }

    public void translate(Vector3 translation) {
        current = current.translate(translation);
    }

    public void scale(Vector3 scale) {
        current = current.scale(scale);
    }

    public void rotate(Vector3 axis, float angle) {
        current = current.rotate(axis, angle);
    }

    public void rotateX(float angle) {
        current = current.rotateX(angle);
    }

    public void rotateY(float angle) {
        current = current.rotateY(angle);
    }

    public void rotateZ(float angle) {
        current = current.rotateZ(angle);
    }
}

package aspect.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.FloatBuffer;

public class AspectOutputStream extends DataOutputStream {

    public AspectOutputStream(OutputStream out) {
        super(out);
    }
    
    public void writeFloats(FloatBuffer buffer) throws IOException {
        for (int i = buffer.position(); i < buffer.limit(); i++) {
            writeFloat(buffer.get());
        }
    }

    public void writeVector3(Vector3 vector) throws IOException {
        writeFloats(vector.getBuffer());
    }
    
    public void writeVector2(Vector2 vector) throws IOException {
        writeFloats(vector.getBuffer());
    }
    
    public void writeColor4(Color color) throws IOException {
        writeFloats(color.getBuffer(true));
    }
    
    public void writeColor3(Color color) throws IOException {
        writeFloats(color.getBuffer(false));
    }
    
    public void writeMatrix4x4(Matrix4x4 matrix) throws IOException {
        writeFloats(matrix.getBuffer());
    }
    
    public void writeMatrix3x3(Matrix3x3 matrix) throws IOException {
        writeFloats(matrix.getBuffer());
    }
    
    public void writeTransform(Transform transform) throws IOException {
        writeFloats(transform.position().getBuffer());
        writeFloats(transform.right().getBuffer());
        writeFloats(transform.up().getBuffer());
        writeFloats(transform.forward().getBuffer());
        writeFloats(transform.scale().getBuffer());
        writeBoolean(transform.orthogonal());
    }
    
    public void writeComplex(Complex complex) throws IOException {
        writeFloat(complex.real);
        writeFloat(complex.imaginary);
    }
    
    public void writeBoundingBox(BoundingBox bounds) throws IOException {
        writeVector3(bounds.min);
        writeVector3(bounds.max);
    }
    
    public void writeAngles(Angles angles) throws IOException {
        writeFloat(angles.pitch);
        writeFloat(angles.yaw);
        writeFloat(angles.roll);
    }
}

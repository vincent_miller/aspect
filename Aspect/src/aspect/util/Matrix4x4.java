/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

/**
 *
 * @author MillerV
 */
public class Matrix4x4 extends Matrix4f {

    private Matrix4x4() {
    }

    public Matrix4x4(FloatBuffer buff) {
        load(buff);
    }
    
    public Matrix4x4(Matrix4f source) {
        load(source);
    }

    public Matrix3x3 getRotation() {
        Matrix3x3 m = Matrix3x3.identity();
        m.m00 = m00;
        m.m01 = m01;
        m.m02 = m02;

        m.m10 = m10;
        m.m11 = m11;
        m.m12 = m12;

        m.m20 = m20;
        m.m21 = m21;
        m.m22 = m22;

        return m;
    }

    public FloatBuffer getBuffer() {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        store(buffer);
        buffer.flip();
        return buffer;
    }

    public static Matrix4x4 identity() {
        Matrix4x4 result = new Matrix4x4();
        result.setIdentity();
        return result;
    }

    public static Matrix4x4 zero() {
        Matrix4x4 result = new Matrix4x4();
        result.setZero();
        return result;
    }
    
    public static Matrix4x4 viewMatrix(Vector3 right, Vector3 up, Vector3 forward, Vector3 translation) {
        Matrix4x4 matrix = identity();
        right = right.times(1.0f / right.mag2());
        up = up.times(1.0f / up.mag2());
        forward = forward.times(-1.0f / forward.mag2());
        
        matrix.m00 = right.x;
        matrix.m10 = right.y;
        matrix.m20 = right.z;

        matrix.m01 = up.x;
        matrix.m11 = up.y;
        matrix.m21 = up.z;

        matrix.m02 = forward.x;
        matrix.m12 = forward.y;
        matrix.m22 = forward.z;
        
        return matrix.translate(translation.negate());
    }
    
    public static Matrix4x4 modelMatrix(Vector3 right, Vector3 up, Vector3 forward, Vector3 translation) {
        Matrix4x4 matrix = identity();
        forward = forward.negate();
        
        matrix.m00 = right.x;
        matrix.m01 = right.y;
        matrix.m02 = right.z;
        
        matrix.m10 = up.x;
        matrix.m11 = up.y;
        matrix.m12 = up.z;
        
        matrix.m20 = forward.x;
        matrix.m21 = forward.y;
        matrix.m22 = forward.z;
        
        matrix.m30 = translation.x;
        matrix.m31 = translation.y;
        matrix.m32 = translation.z;
        
        return matrix;
    }

    public static Matrix4x4 TRS(Vector3 translation, Angles rotation, Vector3 scale) {
        Matrix4x4 m = identity();
        m = m.translate(translation);
        m = m.rotateY(rotation.yaw);
        m = m.rotateX(rotation.pitch);
        m = m.rotateZ(rotation.roll);
        m = m.scale(scale);
        return m;
    }

    public static Matrix4x4 ITRS(Vector3 translation, Angles rotation, Vector3 scale) {
        Matrix4x4 m = identity();
        m = m.scale(Vector3.divide(1, scale));
        m = m.rotateZ(-rotation.roll);
        m = m.rotateX(-rotation.pitch);
        m = m.rotateY(-rotation.yaw);
        m = m.translate(translation.negate());
        return m;
    }

    public static Matrix4x4 add(Matrix4x4 lhs, Matrix4x4 rhs) {
        Matrix4x4 result = new Matrix4x4();
        add(lhs, rhs, result);
        return result;
    }

    public static Matrix4x4 sub(Matrix4x4 lhs, Matrix4x4 rhs) {
        Matrix4x4 result = new Matrix4x4();
        sub(lhs, rhs, result);
        return result;
    }

    public static Matrix4x4 mul(Matrix4x4 lhs, Matrix4x4 rhs) {
        //long time = System.nanoTime();
        Matrix4x4 result = new Matrix4x4();
        //System.out.println("Matrix multiplication took " + (System.nanoTime() - time) + " nanoseconds.");
        mul(lhs, rhs, result);
        return result;
    }

    public static Vector3 transformPoint(Matrix4x4 mat, Vector3 vec) {
        Vector4f vec4 = new Vector4f(vec.x, vec.y, vec.z, 1.0f);
        Vector4f result = new Vector4f();
        transform(mat, vec4, result);
        return new Vector3(result.x, result.y, result.z);
    }
    
    public static Vector4f transformPointFull(Matrix4x4 mat, Vector3 vec) {
        Vector4f vec4 = new Vector4f(vec.x, vec.y, vec.z, 1.0f);
        Vector4f result = new Vector4f();
        transform(mat, vec4, result);
        return result;
    }

    public static Vector3 transformVector(Matrix4x4 mat, Vector3 vec) {
        Vector4f vec4 = new Vector4f(vec.x, vec.y, vec.z, 0.0f);
        Vector4f result = new Vector4f();
        transform(mat, vec4, result);
        return new Vector3(result.x, result.y, result.z);
    }

    public Matrix4x4 plus(Matrix4x4 rhs) {
        return add(this, rhs);
    }

    public Matrix4x4 minus(Matrix4x4 rhs) {
        return sub(this, rhs);
    }

    public Matrix4x4 times(Matrix4x4 rhs) {
        return mul(this, rhs);
    }

    public static Matrix4x4 translate(Matrix4x4 mat, Vector3 trans) {
        Matrix4x4 result = new Matrix4x4();
        load(mat, result);

        translate(trans, result, result);
        return result;
    }

    public static Matrix4x4 scale(Matrix4x4 mat, Vector3 scale) {
        Matrix4x4 result = new Matrix4x4();
        load(mat, result);

        scale(scale, result, result);
        return result;
    }

    public static Matrix4x4 rotate(Matrix4x4 mat, Vector3 axis, float angle) {
        Matrix4x4 result = new Matrix4x4();
        load(mat, result);

        rotate(angle, axis, result, result);
        return result;
    }

    public static Matrix4x4 rotateX(Matrix4x4 mat, float angle) {
        return rotate(mat, Vector3.xAxis(), angle);
    }

    public static Matrix4x4 rotateY(Matrix4x4 mat, float angle) {
        return rotate(mat, Vector3.yAxis(), angle);
    }

    public static Matrix4x4 rotateZ(Matrix4x4 mat, float angle) {
        return rotate(mat, Vector3.zAxis(), angle);
    }

    public Matrix4x4 translate(Vector3 trans) {
        return translate(this, trans);
    }

    public Matrix4x4 scale(Vector3 scale) {
        return scale(this, scale);
    }

    public Matrix4x4 rotate(Vector3 axis, float angle) {
        return rotate(this, axis, angle);
    }

    public Matrix4x4 rotateX(float angle) {
        return rotateX(this, angle);
    }

    public Matrix4x4 rotateY(float angle) {
        return rotateY(this, angle);
    }

    public Matrix4x4 rotateZ(float angle) {
        return rotateZ(this, angle);
    }

    @Override
    public Matrix4x4 invert() {
        Matrix4x4 result = new Matrix4x4();
        invert(this, result);
        return result;
    }

    @Override
    public Matrix4x4 transpose() {
        Matrix4x4 result = new Matrix4x4();
        transpose(this, result);
        return result;
    }

    @Override
    public Matrix4x4 negate() {
        Matrix4x4 result = new Matrix4x4();
        negate(this, result);
        return result;
    }

    public static Matrix3x3 normalMatrix(Matrix4x4 modelview) {
        return modelview.invert().transpose().getRotation();
    }

    public static Matrix4x4 perspective(float vfov, float ratio, float near, float far) {
        Matrix4x4 mat = Matrix4x4.identity();

        float uh = 1.0f / Trig.tan(vfov / 2.0f);
        float uw = uh / ratio;
        float depth = far - near;

        mat.m00 = uw;
        mat.m11 = uh;
        mat.m22 = -(far + near) / depth;
        mat.m33 = 0;

        mat.m32 = -2.0f * far * near / depth;
        mat.m23 = -1;

        return mat;
    }

    public static Matrix4x4 orthographic(float left, float right, float bottom, float top) {
        Matrix4x4 mat = Matrix4x4.identity();

        float near = -1.0f;
        float far = 1.0f;

        mat.m00 = 2.0f / (right - left);
        mat.m11 = 2.0f / (top - bottom);
        mat.m22 = -2 / (far - near);
        mat.m33 = 1;

        mat.m30 = -(right + left) / (right - left);
        mat.m31 = -(top + bottom) / (top - bottom);
        mat.m32 = -(far + near) / (far - near);

        return mat;
    }

    public Vector3 transformPoint(Vector3 point) {
        return transformPoint(this, point);
    }
    
    public Vector4f transformPointFull(Vector3 point) {
        return transformPointFull(this, point);
    }

    public Vector3 transformVector(Vector3 vector) {
        return transformVector(this, vector);
    }
}

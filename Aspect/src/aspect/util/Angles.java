/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import aspect.util.Vector3;

/**
 * A set of three angles (pitch, yaw, roll). Fields are mutable. If an 
 * unchanging copy is needed, use the copy() method.
 * 
 * @author MillerV
 */
public class Angles {
    public float pitch;
    public float yaw;
    public float roll;
    
    /**
     * Construct a new Angles with the given pitch, yaw, and roll.
     * 
     * @param pitch the pitch, in degrees
     * @param yaw the yaw, in degrees
     * @param roll the roll, in degrees
     */
    public Angles(float pitch, float yaw, float roll) {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }
    
    /**
     * Copy the values of pitch, yaw, and roll into a new Angles object.
     * 
     * @return a copy of this set of Angles
     */
    public Angles copy() {
        return new Angles(pitch, yaw, roll);
    }

    public Angles plus(Angles a) {
        return add(this, a);
    }

    public Angles minus(Angles a) {
        return subtract(this, a);
    }
    
    public Angles negate() {
        return negate(this);
    }

    public Angles times(float f) {
        return multiply(this, f);
    }
    
    public static Angles zero() {
        return new Angles(0, 0, 0);
    }
    
    public static Angles add(Angles a1, Angles a2) {
        return new Angles(a1.pitch + a2.pitch, a1.yaw + a2.yaw, a1.roll + a2.roll);
    }
    
    public static Angles multiply(Angles a, float scale) {
        return new Angles(a.pitch * scale, a.yaw * scale, a.roll * scale);
    }
    
    public static Angles subtract(Angles s, Angles d) {
        return new Angles(s.pitch - d.pitch, s.yaw - d.yaw, s.roll - d.roll);
    }
    
    public static Angles fromVector(Vector3 v) {
        return Vector3.dir(v);
    }
    
    public static Vector3 toVector(Angles a, float length) {
        return Vector3.fromAngles(a, length);
    }
    
    public Vector3 toVector(float length) {
        return toVector(this, length);
    }
    
    public static Angles negate(Angles a) {
        return new Angles((a.pitch + 180) % 360, (a.yaw + 180) % 360, (a.roll + 180) % 360);
    } 
    
    public static Angles normalize(Angles a) {
        return new Angles(((a.pitch % 360) + 360) % 360, ((a.yaw % 360) + 360) % 360, ((a.roll % 360) + 360) % 360);
    }
    
    @Override
    public String toString() {
        return "{" + pitch + ", " + yaw + ", " + roll + "}";
    }
    
    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Angles) {
            Angles a = (Angles)o;
            return a.pitch == this.pitch && a.yaw == this.yaw && a.roll == this.roll;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Float.floatToIntBits(this.pitch);
        hash = 71 * hash + Float.floatToIntBits(this.yaw);
        hash = 71 * hash + Float.floatToIntBits(this.roll);
        return hash;
    }
}

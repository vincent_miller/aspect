/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author MillerV
 */
public class Matrix3x3 extends Matrix3f {

    private Matrix3x3() {
    }

    public Matrix3x3(FloatBuffer buff) {
        load(buff);
    }

    public Matrix3x3(Vector3 v1, Vector3 v2, Vector3 v3) {
        m00 = v1.x;
        m10 = v1.y;
        m20 = v1.z;

        m01 = v2.x;
        m11 = v2.y;
        m21 = v2.z;

        m02 = v3.x;
        m12 = v3.y;
        m22 = v3.z;
    }

    public FloatBuffer getBuffer() {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        store(buffer);
        buffer.flip();
        return buffer;
    }

    public static Matrix3x3 identity() {
        Matrix3x3 result = new Matrix3x3();
        result.setIdentity();
        return result;
    }

    public static Matrix3x3 zero() {
        Matrix3x3 result = new Matrix3x3();
        result.setZero();
        return result;
    }

    public static Matrix3x3 add(Matrix3x3 lhs, Matrix3x3 rhs) {
        Matrix3x3 result = new Matrix3x3();
        add(lhs, rhs, result);
        return result;
    }

    public static Matrix3x3 sub(Matrix3x3 lhs, Matrix3x3 rhs) {
        Matrix3x3 result = new Matrix3x3();
        sub(lhs, rhs, result);
        return result;
    }

    public static Matrix3x3 mul(Matrix3x3 lhs, Matrix3x3 rhs) {
        Matrix3x3 result = new Matrix3x3();
        mul(lhs, rhs, result);
        return result;
    }

    public static Vector3 transform(Matrix3x3 mat, Vector3 vec) {
        Vector3 result = new Vector3(0.0f);
        transform(mat, vec, result);
        return result;
    }

    public Matrix3x3 plus(Matrix3x3 rhs) {
        return add(this, rhs);
    }

    public Matrix3x3 minus(Matrix3x3 rhs) {
        return sub(this, rhs);
    }

    public Matrix3x3 times(Matrix3x3 rhs) {
        return mul(this, rhs);
    }

    @Override
    public Matrix3x3 invert() {
        Matrix3x3 result = new Matrix3x3();
        invert(this, result);
        return result;
    }

    @Override
    public Matrix3x3 transpose() {
        Matrix3x3 result = new Matrix3x3();
        transpose(this, result);
        return result;
    }

    @Override
    public Matrix3x3 negate() {
        Matrix3x3 result = new Matrix3x3();
        negate(this, result);
        return result;
    }

    public Vector3 transform(Vector3 vec) {
        return transform(this, vec);
    }
}

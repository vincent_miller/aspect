package aspect.util;

public class StringUtil {
    public static String join(String delimeter, Object... parts) {
        return joinImpl(delimeter, parts);
    }
    
    public static <T> String join(String delimeter, String... parts) {
        return joinImpl(delimeter, parts);
    }
    
    private static <T> String joinImpl(String delimeter, T[] parts) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            sb.append(parts[i]);
            if (i < parts.length - 1) {
                sb.append(delimeter);
            }
        }
        
        return sb.toString();
    }
    
    public static String path(Object... path) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < path.length; i++) {
            String s = path[i].toString();
            if (i == path.length - 1) {
                if (s.endsWith("/")) {
                    sb.append(s.substring(0, s.length() - 2));
                } else {
                    sb.append(s);
                }
            } else {
                if (s.endsWith("/")) {
                    sb.append(s);
                } else {
                    sb.append(s).append("/");
                }
            }
        }
        
        return sb.toString();
    }
    
    public static String format(String format, Object... params) {
        for (int i = 0; i < params.length; i++) {
            format = format.replace("{" + i + "}", params[i].toString());
        }
        
        return format;
    }
    
    public static int getInteger(String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            throw new FormatException(string, "integer");
        }
    }
    
    public static float getFloat(String string) {
        try {
            return Float.parseFloat(string);
        } catch (NumberFormatException ex) {
            throw new FormatException(string, "float");
        }
    }
    
    public static boolean getBoolean(String string) {
        string = string.toLowerCase();
        if (string.equals("true") || string.equals("yes") || string.equals("1") || string.equals("on") || string.equals("high")) {
            return true;
        } else if (string.equals("false") || string.equals("no") || string.equals("0") || string.equals("off") || string.equals("low")) {
            return false;
        } else {
            throw new FormatException(string, "boolean");
        }
    }
    
    public static class FormatException extends RuntimeException {
        public FormatException(String input, String type) {
            super(format("Input string \"{0}\" was not formatted correctly as a {1}."));
        }
    }
}

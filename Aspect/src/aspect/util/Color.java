/*
 * Copyright (C) 2014 millerv
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import java.nio.FloatBuffer;
import java.util.Random;
import org.lwjgl.BufferUtils;

/**
 *
 * @author millerv
 */
public class Color {

    public static final Color RED = new Color(1, 0, 0);
    public static final Color GREEN = new Color(0, 1, 0);
    public static final Color BLUE = new Color(0, 0, 1);
    public static final Color YELLOW = new Color(1, 1, 0);
    public static final Color CYAN = new Color(0, 1, 1);
    public static final Color MAGENTA = new Color(1, 0, 1);
    public static final Color ORANGE = new Color(1, 0.5f, 0);
    public static final Color WHITE = new Color(1, 1, 1);
    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color LIGHT_GRAY = new Color(0.8f, 0.8f, 0.8f);
    public static final Color GRAY = new Color(0.5f, 0.5f, 0.5f);
    public static final Color DARK_GRAY = new Color(0.2f, 0.2f, 0.2f);

    public final float red;
    public final float green;
    public final float blue;
    public final float alpha;

    private static final Random random = new Random();
    
    public Color(float gray) {
        this(gray, gray, gray, 1.0f);
    }

    public Color(float red, float green, float blue) {
        this(red, green, blue, 1.0f);
    }

    public Color(float red, float green, float blue, float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public Color(FloatBuffer buffer, boolean a) {
        red = buffer.get();
        green = buffer.get();
        blue = buffer.get();
        if (a) {
            alpha = buffer.get();
        } else {
            alpha = 1.0f;
        }
    }

    public static Color random(boolean randomAlpha) {
        if (randomAlpha) {
            return new Color(random.nextFloat(), random.nextFloat(), random.nextFloat(), random.nextFloat());
        } else {
            return new Color(random.nextFloat(), random.nextFloat(), random.nextFloat());
        }
    }

    public static Color random() {
        return random(false);
    }

    public java.awt.Color toAWT() {
        return new java.awt.Color(red, green, blue, alpha);
    }

    public FloatBuffer getBuffer(boolean a) {
        FloatBuffer buffer;
        
        if (a) {
            buffer = BufferUtils.createFloatBuffer(4);
        } else {
            buffer = BufferUtils.createFloatBuffer(3);
        }
        
        store(buffer, a);
        buffer.clear();
        return buffer;
    }

    public FloatBuffer getBuffer() {
        return getBuffer(true);
    }

    public void store(FloatBuffer buffer, boolean a) {
        buffer.put(red);
        buffer.put(green);
        buffer.put(blue);

        if (a) {
            buffer.put(alpha);
        }
    }

    @Override
    public String toString() {
        String s = red + ", " + green + ", " + blue;
        if (alpha != 1.0f) {
            s += ", " + alpha;
        }
        return s;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Color)) {
            return false;
        }

        Color c = (Color) obj;

        return Math.abs(c.red - red) < 0.01f && Math.abs(c.green - green) < 0.01f && Math.abs(c.blue - blue) < 0.01f && Math.abs(c.alpha - alpha) < 0.01f;
    }

    @Override
    public int hashCode() {
        // return toAWT().hashCode();
        return 0;
    }
}

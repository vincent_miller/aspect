/*
 * Copyright (C) 2014 millerv
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

/**
 *
 * @author millerv
 */
public class Complex {

    public float real;
    public float imaginary;

    public Complex(float real, float imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }
    
    public Complex copy() {
        return new Complex(real, imaginary);
    }

    @Override
    public String toString() {
        if (imaginary == 0) {
            return real + "";
        } else if (real == 0) {
            return imaginary + "i";
        } else if (imaginary < 0) {
            return real + " - " + (-imaginary) + "i";
        } else {
            return real + " + " + imaginary + "i";
        }
    }

    public Complex plus(Complex b) {
        Complex a = this;
        float r = a.real + b.real;
        float i = a.imaginary + b.imaginary;
        return new Complex(r, i);
    }

    public Complex minus(Complex b) {
        Complex a = this;
        float r = a.real - b.real;
        float i = a.imaginary - b.imaginary;
        return new Complex(r, i);
    }

    public Complex times(Complex b) {
        Complex a = this;
        float r = a.real * b.real - a.imaginary * b.imaginary;
        float i = a.real * b.imaginary + a.imaginary * b.real;
        return new Complex(r, i);
    }

    public Complex times(float f) {
        return new Complex(f * real, f * imaginary);
    }

    public Complex conjugate() {
        return new Complex(real, -imaginary);
    }

}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author MillerV
 */
public class Transform {

    private Vector3 position;
    private Vector3 right;
    private Vector3 up;
    private Vector3 forward;
    private Vector3 scale;

    private boolean orthogonal = true;
    private Transform parent = null;
    private LinkedList<Transform> children = new LinkedList<>();

    private Matrix4x4 storedLocalModelMatrix = null;
    private Matrix4x4 storedGlobalModelMatrix = null;
    private Matrix4x4 storedLocalViewMatrix = null;
    private Matrix4x4 storedGlobalViewMatrix = null;
    private Transform storedGlobal = null;
    private boolean moved = true;

    public Transform() {
        reset();
    }

    private void invalidate() {
        storedLocalModelMatrix = null;
        storedLocalViewMatrix = null;

        invalidateGlobal();
    }

    private void invalidateGlobal() {
        moved = true;
        storedGlobalModelMatrix = null;
        storedGlobalViewMatrix = null;
        storedGlobal = null;

        for (Transform child : children) {
            child.invalidateGlobal();
        }
    }

    public void reset() {
        resetAngles();
        this.position = Vector3.zero();
        this.scale = Vector3.one();
        invalidate();
    }

    public void resetAngles() {
        this.right = Vector3.xAxis();
        this.up = Vector3.yAxis();
        this.forward = Vector3.zAxis().negate();
        invalidate();
    }

    public void set(Vector3 position, Vector3 right, Vector3 up, Vector3 forward, Vector3 scale) {
        setPosition(position);
        setRotation(right, up, forward);
        setScale(scale);
        invalidate();
    }

    public void set(Transform t) {
        position = t.position;
        right = t.right;
        up = t.up;
        forward = t.forward;
        scale = t.scale;
        orthogonal = t.orthogonal;
        invalidate();
    }

    public void setRotation(Vector3 right, Vector3 up, Vector3 forward) {
        if (orthogonal) {
            this.forward = forward.normalize();
            this.up = Vector3.cross(Vector3.cross(this.forward, up), this.forward).normalize();
            this.right = Vector3.cross(this.forward, this.up);
        } else {
            this.right = right.copy();
            this.up = up.copy();
            this.forward = forward.copy();
        }
        invalidate();
    }

    public void setRotation(Transform t) {
        if (t.orthogonal || !orthogonal) {
            this.right = t.right;
            this.up = t.up;
            this.forward = t.forward;
        } else {
            setRotation(t.right, t.up, t.forward);
        }
        invalidate();
    }

    public Transform copy() {
        Transform t = new Transform();
        t.set(this);
        t.setParent(parent);
        return t;
    }

    public Vector3 position() {
        return position.copy();
    }

    public void setPosition(Vector3 v) {
        position = v.copy();
        invalidate();
    }

    public Vector3 right() {
        return right.normalize();
    }

    public void setRight(Vector3 v) {
        if (orthogonal) {
            rotate(right, v);
        } else {
            right = v.copy();
        }
        invalidate();
    }

    public Vector3 up() {
        return up.normalize();
    }

    public void setUp(Vector3 v) {
        if (orthogonal) {
            rotate(up, v);
        } else {
            up = v.copy();
        }
        invalidate();
    }

    public Vector3 forward() {
        return forward.normalize();
    }

    public void setForward(Vector3 v) {
        if (orthogonal) {
            rotate(forward, v);
        } else {
            forward = v.copy();
        }
        invalidate();
    }

    public Vector3 scale() {
        return scale.copy();
    }

    public void setScale(Vector3 v) {
        scale = v.copy();
        invalidate();
    }

    public boolean orthogonal() {
        return orthogonal;
    }

    public void setOrthogonal(boolean b) {
        orthogonal = b;

        if (orthogonal) {
            forward = forward.normalize();
            right = Vector3.cross(forward, up).normalize();
            up = Vector3.cross(right, forward);
            invalidate();
        }
    }

    public Transform parent() {
        return parent;
    }

    public void setParent(Transform t) {
        if (parent == t) {
            return;
        }

        if (parent != null) {
            parent.children.remove(this);
        }

        if (t != null) {
            t.children.add(this);
        }

        parent = t;
        invalidateGlobal();
    }

    public void setParentStayPut(Transform t) {
        if (parent == t) {
            return;
        }

        if (parent != null) {
            Matrix4x4 o = parent.modelMatrix();
            set(multiply(o, this));
        }

        setParent(t);
        if (parent != null) {
            Matrix4x4 n = parent.viewMatrix();
            set(multiply(n, this));
        }

        invalidate();
    }

    public Transform global() {
        if (parent != null) {
            if (storedGlobal == null) {
                storedGlobal = multiply(parent.globalModelMatrix(), this);
            }
            return storedGlobal;
        } else {
            return this;
        }
    }

    public Matrix4x4 globalModelMatrix() {
        if (parent != null) {
            if (storedGlobalModelMatrix == null) {
                storedGlobalModelMatrix = parent.globalModelMatrix().times(modelMatrix());
            }
            return storedGlobalModelMatrix;
        } else {
            return modelMatrix();
        }
    }

    public Matrix4x4 modelMatrix() {
        if (storedLocalModelMatrix == null) {
            setOrthogonal(orthogonal);
            storedLocalModelMatrix = Matrix4x4.modelMatrix(right.times(scale.x), up.times(scale.y), forward.times(scale.z), position);
        }

        return storedLocalModelMatrix;
    }

    public Matrix4x4 globalViewMatrix() {
        if (parent != null) {
            if (storedGlobalViewMatrix == null) {
                storedGlobalViewMatrix = viewMatrix().times(parent.globalViewMatrix());
            }
            return storedGlobalViewMatrix;
        } else {
            return viewMatrix();
        }
    }

    public Matrix4x4 viewMatrix() {
        if (storedLocalViewMatrix == null) {
            storedLocalViewMatrix = Matrix4x4.viewMatrix(right.times(scale.x), up.times(scale.y), forward.times(scale.z), position);
        }
        return storedLocalViewMatrix;
    }

    public boolean moved() {
        boolean m = moved;
        moved = false;
        return m;
    }
    
    public boolean moved(Transform t) {
        return t.position.equals(position) && t.right.equals(right) && t.up.equals(up) && t.forward.equals(forward) && t.scale.equals(scale);
    }

    /*
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Transform) {
            Transform t = (Transform) obj;
            return t.position.equals(position) && t.right.equals(right) && t.up.equals(up) && t.forward.equals(forward) && t.scale.equals(scale);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(position);
        hash = 11 * hash + Objects.hashCode(right);
        hash = 11 * hash + Objects.hashCode(up);
        hash = 11 * hash + Objects.hashCode(forward);
        return hash;
    }*/

    public void setEulerAngles(Angles angles) {
        resetAngles();
        yawLeft(-angles.yaw);
        pitchUp(angles.pitch);
        rollRight(angles.roll);

        invalidate();
    }

    public Angles eulerAngles() {
        Angles angles = forward.dir();
        Vector3 upAxis = Vector3.cross(Vector3.cross(forward, Vector3.yAxis()), forward);
        if (upAxis.mag2() > 0) {
            angles.roll = Vector3.angleSigned(upAxis, upAxis);
        } else {
            angles.roll = 0.0f;
        }

        return angles;
    }

    public boolean rotate(Vector3 from, Vector3 to, float maxAngle) {
        boolean b = true;
        float angle = Vector3.angle(from, to);
        
        if (angle > maxAngle) {
            angle = maxAngle;
            b = false;
        }
        
        Vector3 cross = Vector3.cross(from, to);

        if (cross.mag2() == 0.0f) {
            if (Vector3.dot(from, to) < 0) {
                cross = Vector3.cross(from, Vector3.yAxis());
            } else {
                return true;
            }
        }

        cross = cross.normalize();

        Matrix4x4 m = Matrix4x4.identity().rotate(cross, angle);

        right = m.transformVector(right);
        up = m.transformVector(up);
        forward = m.transformVector(forward);

        invalidate();
        
        return b;
    }

    public void rotate(Vector3 from, Vector3 to) {
        rotate(from, to, Float.POSITIVE_INFINITY);
    }

    public void rotate(Vector3 axis, float angle) {
        Matrix4x4 m = Matrix4x4.identity().rotate(axis, angle);
        forward = m.transformVector(forward);
        up = m.transformVector(up);
        right = m.transformVector(right);

        invalidate();
    }
    
    public void orbit(Vector3 origin, Vector3 axis, float angle) {
        Matrix4x4 m = Matrix4x4.identity().translate(origin).rotate(axis, angle).translate(origin.negate());
        forward = m.transformVector(forward);
        up = m.transformVector(up);
        right = m.transformVector(right);
        position = m.transformPoint(position);

        invalidate();
    }

    public void orbit(Vector3 axis, float angle) {
        Matrix4x4 m = Matrix4x4.identity().rotate(axis, angle);
        forward = m.transformVector(forward);
        up = m.transformVector(up);
        right = m.transformVector(right);
        position = m.transformPoint(position);

        invalidate();
    }

    public void pitchUpAbsolute(float angle) {
        rotate(Vector3.xAxis(), angle);
    }

    public void pitchUp(float angle) {
        rotate(right, angle);
    }

    public void yawLeftAbsolute(float angle) {
        rotate(Vector3.yAxis(), angle);
    }

    public void yawLeft(float angle) {
        rotate(up, angle);
    }

    public void rollRightAbsolute(float angle) {
        rotate(Vector3.zAxis(), -angle);
    }

    public void rollRight(float angle) {
        rotate(forward, angle);
    }

    public void moveRightAbsolute(float amount) {
        position.x += amount;

        invalidate();
    }

    public void moveRight(float amount) {
        position = position.plus(right().times(amount));

        invalidate();
    }

    public void moveUpAbsolute(float amount) {
        position.y += amount;

        invalidate();
    }

    public void moveUp(float amount) {
        position = position.plus(up().times(amount));

        invalidate();
    }

    public void moveForwardAbsolute(float amount) {
        position.z -= amount;

        invalidate();
    }

    public void moveForward(float amount) {
        position = position.plus(forward().times(amount));

        invalidate();
    }

    public void moveAbsolute(Vector3 amount) {
        position = position.plus(amount);

        invalidate();
    }

    public void move(Vector3 amount) {
        position = position.plus(right.times(amount.x));
        position = position.plus(up.times(amount.y));
        position = position.minus(forward.times(amount.z));

        invalidate();
    }

    public static Transform multiply(Matrix4x4 m, Transform t) {
        Transform result = new Transform();

        result.position = m.transformPoint(t.position);
        result.right = m.transformVector(t.right);
        result.up = m.transformVector(t.up);
        result.forward = m.transformVector(t.forward);
        result.scale = t.scale;
        result.orthogonal = false;

        return result;
    }
}

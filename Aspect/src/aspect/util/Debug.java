/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.util;

import static aspect.core.AspectRenderer.*;
import static aspect.core.AspectRenderer.GeometryType.*;
import aspect.core.AspectRenderer;
import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.render.shader.ShaderProgram;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author MillerV
 */
public class Debug {
    public static void showPoint(Vector3 position, float length) {
        boolean b = lightingEnabled();
        setLightingEnabled(false);
        modelMatrix.push();
        modelMatrix.set(Matrix4x4.identity());
        modelMatrix.translate(position);
        ShaderProgram.COLOR.updateMatrices();
        ShaderProgram.COLOR.bind();
        begin(LINES);
        color(Color.BLUE);
        vertex(new Vector3(-length, 0, 0));
        vertex(new Vector3(length, 0, 0));
        color(Color.RED);
        vertex(new Vector3(0, -length, 0));
        vertex(new Vector3(0, length, 0));
        color(Color.GREEN);
        vertex(new Vector3(0, 0, -length));
        vertex(new Vector3(0, 0, length));
        end();
        modelMatrix.pop();
        setLightingEnabled(b);
    }
    
    public static void showVector(Vector3 tail, Vector3 vector) {
        boolean b = glIsEnabled(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        begin(LINES);
        color(Color.BLUE);
        vertex(tail);
        vertex(tail.plus(vector));
        end();
        if (b) {
            glEnable(GL_LIGHTING);
        }
    }
    
    public static void showBounds(BoundingBox b, Color c) {
        ShaderProgram.lock(ShaderProgram.COLOR_UNIFORM);
        
        boolean l = lightingEnabled();
        setLightingEnabled(false);
        
        ShaderProgram.COLOR_UNIFORM.setUniform("color", c);
        
        begin(LINE_LOOP);
        glVertex3f(b.min.x, b.min.y, b.min.z);
        glVertex3f(b.max.x, b.min.y, b.min.z);
        glVertex3f(b.max.x, b.min.y, b.max.z);
        glVertex3f(b.min.x, b.min.y, b.max.z);
        end();
        
        begin(LINE_LOOP);
        glVertex3f(b.min.x, b.max.y, b.min.z);
        glVertex3f(b.max.x, b.max.y, b.min.z);
        glVertex3f(b.max.x, b.max.y, b.max.z);
        glVertex3f(b.min.x, b.max.y, b.max.z);
        end();
        
        begin(LINES);
        glVertex3f(b.min.x, b.min.y, b.min.z);
        glVertex3f(b.min.x, b.max.y, b.min.z);
        
        glVertex3f(b.max.x, b.min.y, b.min.z);
        glVertex3f(b.max.x, b.max.y, b.min.z);
        
        glVertex3f(b.max.x, b.min.y, b.max.z);
        glVertex3f(b.max.x, b.max.y, b.max.z);
        
        glVertex3f(b.min.x, b.min.y, b.max.z);
        glVertex3f(b.min.x, b.max.y, b.max.z);
        end();
        
        setLightingEnabled(l);
        
        ShaderProgram.unlock();
    }
    
    public static void showAxis(Vector3 point, Vector3 axis) {
        showVector(point.minus(axis.times(1e3f)), axis.times(2e3f));
    }
    
    public static void showTransform(Transform t) {
        boolean b = glIsEnabled(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        modelMatrix.push();
        modelMatrix.multiply(t.globalModelMatrix());
        begin(LINES);
        color(Color.BLUE);
        vertex(new Vector3(0, 0, 0));
        vertex(new Vector3(1, 0, 0));
        color(Color.RED);
        vertex(new Vector3(0, 0, 0));
        vertex(new Vector3(0, 1, 0));
        color(Color.GREEN);
        vertex(new Vector3(0, 0, 0));
        vertex(new Vector3(0, 0, 1));
        end();
        modelMatrix.push();
        if (b) {
            glEnable(GL_LIGHTING);
        }
    }
    
    public static void printOnRender(Entity entity, final String message) {
        entity.addBehavior(new Behavior() {
            @Override
            public void render() {
                System.out.println(message);
            }
        });
    }
    
    public static void printOnUpdate(Entity entity, final String message) {
        entity.addBehavior(new Behavior() {
            @Override
            public void update() {
                System.out.println(message);
            }
        });
    }
}

package aspect.gui;

import static aspect.resources.Resources.rect;

import aspect.render.Material;
import aspect.render.Texture;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Vector2;

public class Rectangle extends Shape {
    private Texture texture;

    public Rectangle(Vector2 position, Vector2 size) {
        super(position, size);
    }
    
    public Rectangle(Vector2 size) {
        super(size);
    }

    public Rectangle() {
    }

    public Rectangle(Vector2 position, Texture texture) {
        this(position, texture, new Vector2(texture.width, texture.height));
    }
    
    public Rectangle(Texture texture) {
        this(texture, new Vector2(texture.width, texture.height));
    }

    public Rectangle(Vector2 position, Texture texture, Vector2 size) {
        setPosition(position);
        setSize(size);
        setTexture(texture);
    }
    
    public Rectangle(Texture texture, Vector2 size) {
        setSize(size);
        setTexture(texture);
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
        resized();
    }

    @Override
    protected void resized() {
        if (texture == null) {
            model = rect(new Material(ShaderProgram.COLOR_UNIFORM), width(), height());
        } else {
            model = rect(new Material(texture, ShaderProgram.TEXTURE), width(), height());
        }
        
        model.transform.setPosition(size().times(0.5f).asXY());
    }
}

package aspect.gui;

import static aspect.time.Time.frameTime;

import aspect.time.AbilityTimer;
import aspect.util.Vector2;

public class FPSMonitor extends Counter {
    private AbilityTimer update = new AbilityTimer(0.0f, 0.25f);
    private int frames = 0;
    private float lastTest;

    public FPSMonitor(Vector2 position, boolean drawBackground) {
        super(position, "FPS: ", drawBackground);
    }
    
    public FPSMonitor(boolean drawBackground) {
        super("FPS: ", drawBackground);
    }

    @Override
    public void render() {
        frames++;
        
        if (update.use()) {
            setValue((int)Math.round(frames / (frameTime() - lastTest)));
            lastTest = frameTime();
            frames = 0;
        }
        
        super.render();
    }
}

package aspect.gui;

import aspect.util.Color;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;

public class Healthbar extends Rectangle {
    private float health = 1.0f;
    
    public Healthbar(Vector2 size) {
        super(size);
    }
    
    public void setHealth(float health) {
        this.health = health;
    }
    
    @Override
    public void render() {
        super.render();
        Color c = new Color(1.0f - health, health, 0.0f);
        Transform t = model.transform.copy();
        model.transform.setScale(new Vector3(health, 1.0f));
        model.transform.setPosition(new Vector3(width() * health / 2, height() / 2));
        GUI.drawOutlinedShape(model, c);
        model.transform.set(t);
    }
}

package aspect.gui;

import aspect.util.Vector2;

public class RectangleBorder extends Rectangle {

    public RectangleBorder(Vector2 position, Vector2 size) {
        super(position, size);
    }

    public RectangleBorder(Vector2 size) {
        super(size);
    }

    public RectangleBorder() {
    }

    @Override
    public void render() {
        if (color == null) {
            GUI.drawOutline(model);
        } else {
            GUI.drawOutline(model, color);
        }
    }
}

package aspect.gui;

import aspect.event.Listener;
import aspect.util.Vector2;

public class Counter extends Label implements Listener {
    private int value;
    private String title;
    
    public Counter(Vector2 position, String title, int initial, boolean drawBackground) {
        super(position, title + initial, drawBackground);
        value = initial;
        this.title = title;
    }
    
    public Counter(Vector2 position, String title, boolean drawBackground) {
        this(position, title, 0, drawBackground);
    }
    
    public Counter(String title, int initial, boolean drawBackground) {
        super(title + initial, drawBackground);
        value = initial;
        this.title = title;
    }
    
    public Counter(String title, boolean drawBackground) {
        this(title, 0, drawBackground);
    }
    
    public void setValue(int value) {
        this.value = value;
        setText(title + value);
        fitToText();
    }
    
    public int getValue() {
        return value;
    }
    
    public void increment(int inc) {
        setValue(value + inc);
    }
    
    public void increment() {
        setValue(value + 1);
    }
    
    public void setTitle(String title) {
        this.title = title;
        setText(title + value);
        fitToText();
    }

    @Override
    public void event() {
        increment();
    }
}

package aspect.gui;

import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Consumer;

import aspect.util.Color;
import aspect.util.Vector2;

public class TextLog extends Rectangle implements Consumer<String> {
    private ConcurrentLinkedDeque<TextLogItem> text = new ConcurrentLinkedDeque<>();
    private int textSize = 24;
    private boolean drawBackground = true;
    private float scrollPosition = 0;
    private int lines = 0;
    private Color textColor;

    public TextLog(Vector2 position, Vector2 size) {
        super(position, size);
    }

    public TextLog(Vector2 size) {
        super(size);
    }

    public void append(String str) {
        append(str, GUI.outlineColor);
    }

    public void append(String str, Color color) {
        str = str.replaceAll("\t", "    ");
        String[] parts = str.split("\n");
        str = "";

        for (int i = parts.length - 1; i >= 0; i--) {
            str += parts[i];
            str += "\n";
        }
        text.addFirst(new TextLogItem(str, color));
    }

    public void appendln(String str) {
        append(str + "\n");
    }

    public void appendln(String str, Color color) {
        append(str + "\n", color);
    }

    public void appendln() {
        append("\n");
    }

    public void clear() {
        text.clear();
    }

    public void setBackgroundEnabled(boolean enabled) {
        drawBackground = enabled;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public void setTextColor(Color color) {
        this.textColor = color;
    }

    public void setScrollPosition(float position) {
        scrollPosition = position;
    }

    @Override
    public void render() {
        if (drawBackground) {
            super.render();
        }

        if (text != null) {
            GUI.font.setSize(textSize);
            float height = GUI.font.getSize("", textSize).y;
            int i = 0;
            for (TextLogItem item : text) {
                int s = 0;
                int e;
                while ((e = item.string.indexOf('\n', s + 1)) != -1) {
                    Vector2 position = new Vector2(10, 10 + i * height - scrollPosition * Math.max((lines + 1) * GUI.font.getHeight(textSize) - height(), 0));
                    if (textColor == null) {
                        GUI.drawText(item.string.substring(s, e), position, item.color);
                    } else {
                        GUI.drawText(item.string.substring(s, e), position, textColor);
                    }
                    i++;
                    s = e;
                }
            }
            lines = i;
        }
    }

    @Override
    public void accept(String t) {
        appendln(t);
    }

    private class TextLogItem {
        private String string;
        private Color color;

        private TextLogItem(String string, Color color) {
            this.string = string;
            this.color = color;
        }
    }
}

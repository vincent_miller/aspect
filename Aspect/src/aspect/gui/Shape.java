package aspect.gui;

import aspect.render.ViewModel;
import aspect.util.Color;
import aspect.util.Vector2;

public abstract class Shape extends Component {
    protected ViewModel model;
    protected Color color = null;

    public Shape(Vector2 position, Vector2 size) {
        super(position, size);
    }
    
    public Shape(Vector2 size) {
        super(size);
    }

    public Shape() {
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    protected abstract void resized();

    @Override
    protected void render() {
        if (color == null) {
            GUI.drawOutlinedShape(model, false);
        } else {
            GUI.drawOutlinedShape(model, color);
        }

    }
}

package aspect.gui;

import static aspect.resources.Resources.rect;

import java.util.LinkedList;

import aspect.event.Listener;
import aspect.event.MouseEvent;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Vector2;

public class CheckBox extends Rectangle {
    private LinkedList<Listener> listeners = new LinkedList<>();
    private boolean checked = false;
    private final ViewModel checkmark;
    
    public CheckBox(Vector2 position) {
        this();
        setPosition(position);
    }
    
    public CheckBox() {
        super(new Vector2(20, 20));
        
        checkmark = rect(new Material(ShaderProgram.COLOR_UNIFORM), 15, 15);
        checkmark.transform.setPosition(size().times(0.5f).asXY());
        setResizeable(false);
    }
    
    public boolean checked() {
        return checked;
    }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    public void addListener(Listener l) {
        listeners.add(l);
    }
    
    @Override
    public void render() {
        super.render();
        
        if (checked) {
            ShaderProgram.COLOR_UNIFORM.setUniform("color", GUI.outlineColor);
            checkmark.render();
        }
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        if (evt.button == 0) {
            checked = !checked;
            for (Listener listener : listeners) {
                listener.event();
            }
        }
    }
}

package aspect.gui;

import aspect.util.Vector2;

public class FlowLayout extends Layout {
    public static final boolean HORIZONTAL = true;
    public static final boolean VERTICAL = false;

    private float pos;
    private float max;
    private float hborder;
    private float vborder;
    private boolean type;
    private boolean centered;

    public FlowLayout(float hborder, float vborder, boolean type, boolean centered) {
        this.hborder = hborder;
        this.vborder = vborder;
        this.type = type;
        this.centered = centered;
    }

    @Override
    public void attach(Container container) {
        super.attach(container);
        reset();
    }

    @Override
    protected void assign(Component component) {
        if (type) {
            max = Math.max(max, component.height());

            if (container.height() < max + vborder * 2) {
                container.setHeight(max + vborder * 2);
                if (centered) {
                    for (Component c : container.components) {
                        if (c != component) {
                            c.setY((container.height() - c.height()) / 2);
                        }
                    }
                }
            }
            
            float y = centered ? (container.height() - component.height()) / 2 : container.height() - vborder - component.height();

            component.setPosition(new Vector2(pos, y));
            pos += component.width() + hborder;

            if (container.width() < pos) {
                container.setWidth(pos);
            }
        } else {
            max = Math.max(max, component.width());

            if (container.width() < max + hborder * 2) {
                container.setWidth(max + hborder * 2);
                if (centered) {
                    for (Component c : container.components) {
                        if (c != component) {
                            c.setX((container.width() - c.width()) / 2);
                        }
                    }
                }
            }
            
            float x = centered ? (container.width() - component.width()) / 2 : hborder;

            component.setPosition(new Vector2(x, pos - component.height()));
            pos -= component.height() + vborder;

            if (pos < 0) {
                float f = -pos;
                container.setHeight(container.height() - pos);
                pos = 0;
                for (Component c : container.components) {
                    c.setY(c.y() + f);
                }
            }
        }
    }

    @Override
    protected void reset() {
        max = 0;
        if (type) {
            pos = hborder;
        } else {
            pos = container.height() - vborder;
        }
    }
}

package aspect.gui;

public abstract class Layout {
    protected Container container;
    
    protected void attach(Container container) {
        this.container = container;
    }
    
    protected void assign(Component component, int hint) {
        assign(component);
    }
    
    protected abstract void assign(Component component);
    
    protected abstract void reset();
}

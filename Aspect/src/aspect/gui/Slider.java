package aspect.gui;

import static aspect.resources.Resources.rect;

import java.util.LinkedList;
import java.util.function.Consumer;

import aspect.event.MouseEvent;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Vector2;
import aspect.util.Vector3;

public class Slider extends Component {
    public static final boolean HORIZONTAL = true;
    public static final boolean VERTICAL = false;
    
    private boolean type;
    private ViewModel track;
    private ViewModel slider;
    private float position = 0.5f;
    private LinkedList<Consumer<Float>> listeners = new LinkedList<>();
    
    public Slider(Vector2 position, boolean type, float length) {
        this(type, length);
        setPosition(position);
    }
    
    public Slider(boolean type, float length) {
        this.type = type;
        if (type) {
            setSize(new Vector2(length + 5, 20.0f));
            slider = rect(new Material(ShaderProgram.COLOR_UNIFORM), 10.0f, 20.0f);
        } else {
            setSize(new Vector2(20.0f, length + 5));
            slider = rect(new Material(ShaderProgram.COLOR_UNIFORM), 20.0f, 10.0f);
        }
        setValue(0.5f);
        setResizeable(false);
    }
    
    @Override
    protected void mouseDragged(MouseEvent evt) {
        if (type) {
            position += evt.dx / (width() - 10);
        } else {
            position += evt.dy / (height() - 10);
        }
        
        setValue(Math.min(value(), 1.0f));
        setValue(Math.max(value(), 0.0f));
        
        for (Consumer<Float> consumer : listeners) {
            consumer.accept(position);
        }
    }
    
    public void addListener(Consumer<Float> listener) {
        listeners.add(listener);
    }

    public void removeListener(Consumer<Float> listener) {
        listeners.remove(listener);
    }
    
    @Override
    protected void resized() {
        if (type) {
            track = rect(new Material(ShaderProgram.COLOR_UNIFORM), width() - 10, 5.0f);
        } else {
            track = rect(new Material(ShaderProgram.COLOR_UNIFORM), 5.0f, height() - 10);
        }
        track.transform.setPosition(size().times(0.5f).asXY());
    }
    
    @Override
    protected void render() {
        ShaderProgram.COLOR_UNIFORM.setUniform("color", GUI.outlineColor);
        track.render();
        Color c = new Color(GUI.mainColor.red, GUI.mainColor.green, GUI.mainColor.blue, 1.0f);
        ShaderProgram.COLOR_UNIFORM.setUniform("color", c);
        GUI.drawOutlinedShape(slider, c);
    }

    public float value() {
        return position;
    }

    public void setValue(float position) {
        this.position = position;
        if (type) {
            slider.transform.setPosition(new Vector3(5 + value() * (width() - 10), height() / 2));
        } else {
            slider.transform.setPosition(new Vector3(width() / 2, 5 + value() * (height() - 10)));
        }
    }
}

package aspect.gui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Consumer;

import aspect.event.KeyEvent;
import aspect.event.Listener;
import aspect.event.MouseEvent;
import aspect.time.Time;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Trig;
import aspect.util.Vector2;
import aspect.util.Vector3;

import static aspect.resources.Resources.rect;
import static org.lwjgl.input.Keyboard.*;

public class TextField extends Rectangle {
    private String text = "";
    private int textSize = 24;

    private boolean clearOnSend = false;
    private ViewModel cursor;

    private LinkedList<Consumer<String>> listeners = new LinkedList<>();
    private LinkedList<Listener> actionListeners = new LinkedList<>();

    private ArrayList<String> entries = new ArrayList<>(20);
    private int currentHistory = -1;
    private int cursorIndex = 0;

    public TextField(Vector2 position, float width) {
        this(width);
        setPosition(position);
    }
    
    public TextField(float width) {
        float height = GUI.font.getHeight(textSize) + 10;
        setSize(new Vector2(width, height));
        cursor = rect(new Material(ShaderProgram.COLOR_UNIFORM), 3, height - 5);
    }

    public void setClearOnSend(boolean b) {
        clearOnSend = b;
    }

    public void addListener(Consumer<String> listener) {
        listeners.add(listener);
    }

    public void removeListener(Consumer<String> listener) {
        listeners.remove(listener);
    }

    public void addListener(Listener listener) {
        actionListeners.add(listener);
    }

    public void removeListener(Listener listener) {
        actionListeners.remove(listener);
    }

    @Override
    public void keyPressed(KeyEvent evt) {
        if (evt.key == KEY_BACK) {
            if (text.length() > 0) {
                if (cursorIndex > 0 && cursorIndex < text.length()) {
                    text = text.substring(0, cursorIndex - 1) + text.substring(cursorIndex, text.length());
                    cursorIndex--;
                } else if (cursorIndex == text.length()) {
                    text = text.substring(0, text.length() - 1);
                    cursorIndex--;
                }
            }
        } else if (evt.key == KEY_RETURN) {
            if (!text.isEmpty()) {
                for (Consumer<String> consumer : listeners) {
                    consumer.accept(text);
                }
                for (Listener listener : actionListeners) {
                    listener.event();
                }
                entries.add(text);
                if (clearOnSend) {
                    text = "";
                    currentHistory = -1;
                    cursorIndex = 0;
                }
            }
        } else if (evt.key == KEY_TAB) {
        } else if (evt.key == KEY_UP) {
            if (currentHistory < entries.size() - 1) {
                currentHistory++;
            }
            if (currentHistory < entries.size()) {
                text = entries.get(entries.size() - currentHistory - 1);
                cursorIndex = text.length();
            }
        } else if (evt.key == KEY_DOWN) {
            if (currentHistory >= 0) {
                currentHistory--;
            }
            if (currentHistory >= 0) {
                text = entries.get(entries.size() - currentHistory - 1);
                cursorIndex = text.length();
            } else {
                text = "";
            }
        } else if (evt.key == KEY_LEFT) {
            if (cursorIndex > 0) {
                cursorIndex--;
            }
        } else if (evt.key == KEY_RIGHT) {
            if (cursorIndex < text.length()) {
                cursorIndex++;
            }
        } else if (evt.character != 0) {
            if (cursorIndex > 0 && cursorIndex < text.length()) {
                text = text.substring(0, cursorIndex) + evt.character + text.substring(cursorIndex, text.length());
            } else if (cursorIndex == text.length()) {
                text += evt.character;
            } else {
                text = evt.character + text;
            }
            
            cursorIndex++;
        }
    }
    
    @Override
    public void mousePressed(MouseEvent evt) {
        float x = evt.x - 10;
        for (int i = 0; i < text.length(); i++) {
            float last = GUI.font.getWidth(text.substring(0, i), textSize);
            float next = GUI.font.getWidth(text.substring(0, i + 1), textSize);
            
            if ((last <= x && next >= x) || (x > next && i == text.length() - 1)) {
                cursorIndex = i + 1;
                break;
            } else if (x < last && i == 0) {
                cursorIndex = 0;
                break;
            }
        }
    }

    @Override
    public void render() {
        setColor(selected() ? GUI.highlightColor : GUI.mainColor);
        super.render();

        GUI.font.setSize(textSize);
        Vector2 position = new Vector2(5);
        GUI.drawText(text, position);

        if (selected() && Trig.sin(Time.currentTime() * 5) > 0) {
            ShaderProgram.COLOR_UNIFORM.setUniform("color", GUI.outlineColor);
            cursor.transform.setPosition(new Vector3(GUI.font.getWidth(text.substring(0, cursorIndex), textSize) + 10, height() / 2));
            cursor.render();
        }
    }
}

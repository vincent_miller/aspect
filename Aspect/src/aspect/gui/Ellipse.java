package aspect.gui;

import static aspect.resources.Resources.ellipse;
import static aspect.resources.Resources.rect;

import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Vector2;

public class Ellipse extends Shape {
    public Ellipse(Vector2 position, Vector2 size) {
        super(position, size);
    }
    
    public Ellipse(Vector2 size) {
        super(size);
    }

    public Ellipse() {
    }

    @Override
    protected void resized() {
        model = ellipse(new Material(ShaderProgram.COLOR_UNIFORM), width(), height(), 20);
        model.transform.setPosition(size().times(0.5f).asXY());
    }
}

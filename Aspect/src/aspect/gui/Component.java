package aspect.gui;

import static aspect.core.AspectRenderer.modelMatrix;
import static org.lwjgl.opengl.GL11.GL_SCISSOR_TEST;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glIsEnabled;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glScissor;
import static org.lwjgl.opengl.GL11.glTranslatef;

import aspect.event.KeyEvent;
import aspect.event.MouseEvent;
import aspect.util.BoundingBox2D;
import aspect.util.Vector2;
import aspect.util.Vector3;

import org.lwjgl.input.Mouse;

public class Component {
    private boolean enabled = true;
    private boolean selected = false;
    private boolean resizeable = true;
    private final BoundingBox2D bounds = new BoundingBox2D(Vector2.zero(), Vector2.zero());
    protected Container parent;

    public Component(Vector2 position, Vector2 size) {
        setPosition(position);
        setSize(size);
    }

    public Component() {
    }

    public Component(Vector2 size) {
        setSize(size);
    }

    public final BoundingBox2D bounds() {
        return bounds.copy();
    }

    public final BoundingBox2D globalBounds() {
        Vector2 p = globalPosition();
        return new BoundingBox2D(p, p.plus(size()));
    }

    public final void setBounds(BoundingBox2D bounds) {
        if (resizeable) {
            this.bounds.set(bounds);
            moved();
            resized();
        }
    }

    public final boolean enabled() {
        return enabled;
    }

    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;

        if (!enabled) {
            setSelected(false);
        }
    }

    public final boolean selected() {
        return selected;
    }

    public final void setSelected(boolean selected) {
        if (this.selected != selected) {
            this.selected = selected;
            if (selected) {
                onSelected();
            } else {
                onDeselected();
            }
        }
    }

    public final boolean resizeable() {
        return resizeable;
    }

    public final void setResizeable(boolean resizeable) {
        this.resizeable = resizeable;
    }

    public final Vector2 position() {
        return bounds.min.copy();
    }

    public final Vector2 globalPosition() {
        if (parent != null) {
            return parent.globalPosition().plus(position());
        } else {
            return position();
        }
    }

    public final float x() {
        return position().x;
    }

    public final float y() {
        return position().y;
    }

    public final Vector2 size() {
        return bounds.dimensions();
    }

    public final float width() {
        return size().x;
    }

    public final float height() {
        return size().y;
    }

    public void setPosition(Vector2 position) {
        Vector2 size = size();
        bounds.min = position.copy();
        bounds.max = position.plus(size);
        moved();
    }

    public void setX(float x) {
        float width = width();
        bounds.min.x = x;
        bounds.max.x = x + width;
        moved();
    }

    public void setY(float y) {
        float height = height();
        bounds.min.y = y;
        bounds.max.y = y + height;
        moved();
    }

    public void setSize(Vector2 size) {
        if (resizeable) {
            bounds.max = bounds.min.plus(size);
            resized();
        }
    }

    public final void setWidth(float width) {
        if (resizeable) {
            bounds.max.x = bounds.min.x + width;
            resized();
        }
    }

    public final void setHeight(float height) {
        if (resizeable) {
            bounds.max.y = bounds.min.y + height;
            resized();
        }
    }

    public final void renderComponent() {
        if (enabled) {
            modelMatrix.push();
            modelMatrix.translate(position().asXY());
            glPushMatrix();
            glTranslatef(x(), y(), 0);
            Vector3 pos = modelMatrix.current().transformPoint(Vector3.zero());
            glScissor((int) pos.x - 2, (int) pos.y - 2, (int) width() + 4, (int) height() + 4);
            boolean s = glIsEnabled(GL_SCISSOR_TEST);
            glEnable(GL_SCISSOR_TEST);
            render();
            if (!s) {
                glDisable(GL_SCISSOR_TEST);
            }
            modelMatrix.pop();
            glPopMatrix();
        }
    }

    public final boolean mouseInside() {
        return globalBounds().contains(new Vector2(Mouse.getX(), Mouse.getY()));
    }
    
    public static void makeUniformSize(Component... components) {
        float maxwidth = 0;
        float maxheight = 0;
        
        for (Component component : components) {
            maxwidth = Math.max(maxwidth, component.width());
            maxheight = Math.max(maxheight, component.height());
        }
        
        for (Component component : components) {
            component.setSize(new Vector2(maxwidth, maxheight));
        }
    }

    protected void render() {
    }

    protected void moved() {
    }

    protected void resized() {
    }

    protected void mousePressed(MouseEvent evt) {
    }

    protected void mouseReleased(MouseEvent evt) {
    }

    protected void mouseDragged(MouseEvent evt) {
    }

    protected void mouseMoved(MouseEvent evt) {
    }

    protected void mouseWheelMoved(MouseEvent evt) {
    }

    protected void keyPressed(KeyEvent evt) {
    }

    protected void keyReleased(KeyEvent evt) {
    }

    protected void onSelected() {
    }

    protected void onDeselected() {
    }
}

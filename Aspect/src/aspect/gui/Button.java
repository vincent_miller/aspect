package aspect.gui;

import java.util.LinkedList;

import aspect.event.Listener;
import aspect.event.MouseEvent;
import aspect.util.Vector2;

import org.lwjgl.input.Mouse;

public class Button extends Label {
    private LinkedList<Listener> listeners = new LinkedList<>();
    
    public Button(Vector2 position, Vector2 size) {
        super(position, size);
    }
    
    public Button(Vector2 position, String text) {
        super(position, text, true);
    }
    
    public Button(Vector2 size) {
        super(size);
    }
    
    public Button(String text) {
        super(text, true);
    }
    
    public void addListener(Listener l) {
        listeners.add(l);
    }
    
    @Override
    public void render() {
        setColor(mouseInside() ? GUI.highlightColor : GUI.mainColor);
        
        super.render();
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        if (evt.button == 0) {
            for (Listener listener : listeners) {
                listener.event();
            }
        }
    }
}

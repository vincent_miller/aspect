package aspect.gui;

import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import java.util.HashMap;

import org.newdawn.slick.TrueTypeFont;

import aspect.util.Color;
import aspect.util.Vector2;

import static org.lwjgl.util.glu.GLU.*;

public class Font {
    public static final int BOLD = java.awt.Font.BOLD;
    public static final int ITALIC = java.awt.Font.ITALIC;
    public static final int BOLD_ITALIC = BOLD | ITALIC;
    public static final int PLAIN = 0;

    private final java.awt.Font base;
    private final HashMap<Integer, TrueTypeFont> ttf = new HashMap<>();
    private int currentSize;

    public Font(String name, int style, int size) {
        base = new java.awt.Font(name, style, size);
        ttf.put(size, new TrueTypeFont(base, true));
        currentSize = size;
    }
    
    public TrueTypeFont get(int size) {
        TrueTypeFont current = ttf.get(size);
        if (current == null) {
            current = new TrueTypeFont(new java.awt.Font(base.getFontName(), base.getStyle(), size), true);
            ttf.put(currentSize, current);
        }
        return current;
    }
    
    public TrueTypeFont getCurrent() {
        return get(currentSize);
    }
    
    public void setSize(int size) {
        currentSize = size;
    }
    
    public float getWidth(String str, int size) {
        TrueTypeFont current = get(size);
        return current.getWidth(str);
    }
    
    public float getHeight(int size) {
        TrueTypeFont current = get(size);
        return current.getHeight();
    }

    public Vector2 getSize(String str, int size) {
        TrueTypeFont current = get(size);
        return new Vector2(current.getWidth(str), current.getHeight(str));
    }
}

package aspect.gui;

import static aspect.resources.Resources.ellipse;
import static aspect.resources.Resources.rect;

import java.util.LinkedList;

import aspect.event.KeyEvent;
import aspect.event.Listener;
import aspect.event.MouseEvent;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Vector2;

public class RadioButton extends Ellipse {
    private LinkedList<Listener> listeners = new LinkedList<>();
    private boolean checked = false;
    private final ViewModel checkmark;
    private RadioButton[] group;

    public static void group(RadioButton... buttons) {
        for (RadioButton button : buttons) {
            button.group = buttons;
        }
    }

    public RadioButton(Vector2 position) {
        this();
        setPosition(position);
    }
    
    public RadioButton() {
        super(new Vector2(20, 20));

        checkmark = ellipse(new Material(ShaderProgram.COLOR_UNIFORM), 15, 15, 20);
        checkmark.transform.setPosition(size().times(0.5f).asXY());
        setResizeable(false);
    }

    public boolean checked() {
        return checked;
    }

    public void check() {
        if (group != null) {
            for (RadioButton button : group) {
                button.checked = false;
            }
        }
        checked = true;
    }

    public void addListener(Listener l) {
        listeners.add(l);
    }

    @Override
    public void render() {
        super.render();

        if (checked) {
            ShaderProgram.COLOR_UNIFORM.setUniform("color", GUI.outlineColor);
            checkmark.render();
        }
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        if (evt.button == 0) {
            check();
            for (Listener listener : listeners) {
                listener.event();
            }
        }
    }
}

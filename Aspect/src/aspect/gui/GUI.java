package aspect.gui;

import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.core.AspectRenderer.modelMatrix;
import static aspect.core.AspectRenderer.projectionMatrix;
import static aspect.core.AspectRenderer.setWireframe;
import static aspect.core.AspectRenderer.viewMatrix;
import static aspect.core.AspectRenderer.wireframe;
import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawBuffer;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetInteger;
import static org.lwjgl.opengl.GL11.glIsEnabled;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.GL_CURRENT_PROGRAM;
import static org.lwjgl.util.glu.GLU.gluOrtho2D;

import java.util.LinkedList;

import org.newdawn.slick.TrueTypeFont;

import aspect.event.KeyEvent;
import aspect.event.KeyListener;
import aspect.event.MouseEvent;
import aspect.event.MouseListener;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Vector2;
import aspect.util.Vector3;

import org.lwjgl.input.Mouse;

public class GUI {
    private static Container mainLayer;
    private static Container overlayLayer;
    private static boolean enabled = true;
    
    private static FPSMonitor fpsMonitor;

    private static LinkedList<KeyListener> keyListeners = new LinkedList<>();
    private static LinkedList<MouseListener> mouseListeners = new LinkedList<>();

    public static Font font;
    public static Color backColor = new Color(0.7f, 0.7f, 0.9f, 0.5f);
    public static Color mainColor = new Color(0.5f, 0.5f, 0.7f, 0.5f);
    public static Color highlightColor = new Color(0.3f, 0.3f, 0.5f, 0.5f);
    public static Color outlineColor = Color.BLACK;

    public static void initialize() {
        font = new Font("Ariel", Font.PLAIN, 36);
        mainLayer = new Container(Vector2.zero(), new Vector2(getCanvasWidth(), getCanvasHeight()));
        overlayLayer = new Container(Vector2.zero(), new Vector2(getCanvasWidth(), getCanvasHeight()));
        mainLayer.setBackgroundEnabled(false);
        overlayLayer.setBackgroundEnabled(false);
        fpsMonitor = new FPSMonitor(new Vector2(getCanvasWidth() - 100, getCanvasHeight() - 50), false);
        fpsMonitor.setTextColor(Color.RED);
        fpsMonitor.setEnabled(false);
        mainLayer.add(fpsMonitor);
    }

    public static void show() {
        if (enabled) {
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluOrtho2D(0, getCanvasWidth(), 0, getCanvasHeight());
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            boolean w = wireframe();
            setWireframe(false);
            glDisable(GL_LIGHTING);
            projectionMatrix.push();
            projectionMatrix.set(Matrix4x4.orthographic(0, getCanvasWidth(), 0, getCanvasHeight()));
            viewMatrix.push();
            viewMatrix.set(Matrix4x4.identity());
            boolean d = glIsEnabled(GL_DEPTH_TEST);
            glDisable(GL_DEPTH_TEST);
            mainLayer.renderComponent();
            overlayLayer.renderComponent();
            projectionMatrix.pop();
            viewMatrix.pop();
            if (d) {
                glEnable(GL_DEPTH_TEST);
            }
            setWireframe(w);
        }
    }

    public static void addKeyListener(KeyListener c) {
        if (!keyListeners.contains(c)) {
            keyListeners.add(c);
        }
    }

    public static void addMouseListener(MouseListener c) {
        if (!mouseListeners.contains(c)) {
            mouseListeners.add(c);
        }
    }
    
    public static void setDrawFPS(boolean b) {
        fpsMonitor.setEnabled(b);
    }

    public static boolean enabled() {
        return enabled;
    }

    public static void setEnabled(boolean enabled) {
        GUI.enabled = enabled;
    }

    public static void setOverlayOnly(boolean b) {
        mainLayer.setEnabled(!b);
    }

    public static void add(Component component) {
        mainLayer.add(component);
    }

    public static void remove(Component component) {
        mainLayer.remove(component);
    }

    public static void addOverlay(Component component) {
        overlayLayer.add(component);
    }

    public static void removeOverlay(Component component) {
        overlayLayer.remove(component);
    }

    public static void mouseEvent(MouseEvent evt) {
        if (enabled) {
            if (evt.wheel != 0) {
                mainLayer.mouseWheelMoved(evt);
                overlayLayer.mouseWheelMoved(evt);
            } else if (evt.dx == 0 && evt.dy == 0) {
                if (evt.type) {
                    mainLayer.mousePressed(evt);
                    overlayLayer.mousePressed(evt);
                } else {
                    mainLayer.mouseReleased(evt);
                    overlayLayer.mouseReleased(evt);
                }
            } else {
                if (Mouse.isButtonDown(0)) {
                    mainLayer.mouseDragged(evt);
                    overlayLayer.mouseDragged(evt);
                } else {
                    mainLayer.mouseMoved(evt);
                    overlayLayer.mouseMoved(evt);
                }
            }

            for (MouseListener listener : mouseListeners) {
                listener.mouseEvent(evt);
            }
        }
    }

    public static void keyEvent(KeyEvent evt) {
        if (enabled) {
            mainLayer.setSelected(true);
            overlayLayer.setSelected(true);
            if (evt.type) {
                mainLayer.keyPressed(evt);
                overlayLayer.keyPressed(evt);
            } else {
                mainLayer.keyReleased(evt);
                overlayLayer.keyReleased(evt);
            }
            
            for (KeyListener listener : keyListeners) {
                listener.keyEvent(evt);
            }
        }
    }

    public static void drawText(String str, Vector2 pos, int size, Color color) {
        font.setSize(size);
        drawText(str, pos, font, color);
    }

    public static void drawText(String str, Vector2 pos, int size) {
        font.setSize(size);
        drawText(str, pos, font, outlineColor);
    }

    public static void drawText(String str, Vector2 pos, Color color) {
        drawText(str, pos, font, color);
    }

    public static void drawText(String str, Vector2 pos) {
        drawText(str, pos, font, outlineColor);
    }

    public static void drawText(String str, Vector2 pos, Font font, Color color) {
        ShaderProgram.unbind();
        TrueTypeFont current = font.getCurrent();

        glPushMatrix();
        glTranslatef(pos.x, pos.y + current.getHeight(), 0);
        glScalef(1, -1, 1);
        pos = modelMatrix.current().transformPoint(pos.asXY()).xy();
        current.drawString(0, 0, str, new org.newdawn.slick.Color(color.red, color.green, color.blue, color.alpha));
        glPopMatrix();
    }

    public static void drawOutlinedShape(ViewModel model, boolean highlight) {
        drawOutlinedShape(model, highlight ? highlightColor : mainColor, outlineColor);
    }

    public static void drawOutlinedShape(ViewModel model, Color backColor) {
        drawOutlinedShape(model, backColor, outlineColor);
    }

    public static void drawOutlinedShape(ViewModel model, Color backColor, Color outlineColor) {
        ShaderProgram.COLOR_UNIFORM.setUniform("color", backColor);
        model.render();
        drawOutline(model, outlineColor);
    }

    public static void drawOutline(ViewModel model) {
        drawOutline(model, outlineColor);
    }
    
    public static void drawOutline(ViewModel model, Color outlineColor) {
        ShaderProgram.COLOR_UNIFORM.setUniform("color", outlineColor);
        boolean w = wireframe();
        setWireframe(true);
        model.render();
        setWireframe(w);
    }
}

package aspect.gui;

import java.util.LinkedList;

import aspect.event.KeyEvent;
import aspect.event.MouseEvent;
import aspect.util.BoundingBox2D;
import aspect.util.Vector2;

public class Container extends Rectangle {
    protected LinkedList<Component> components = new LinkedList<>();
    private Layout layout = new FreeLayout();
    private boolean drawBackground = true;

    public Container(Vector2 position, Vector2 size) {
        super(position, size);
    }

    public Container(Vector2 size) {
        super(size);
    }
    
    public Container(Layout layout) {
        setLayout(layout);
    }

    public void setBackgroundEnabled(boolean enabled) {
        drawBackground = enabled;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
        layout.attach(this);
        for (Component component : components) {
            layout.assign(component);
        }
    }

    public void add(Component component) {
        components.add(component);
        component.parent = this;
        layout.assign(component);
    }

    public void remove(Component component) {
        components.remove(component);
        component.parent = null;
        layout.reset();
        for (Component c : components) {
            layout.assign(c);
        }
    }

    @Override
    protected void render() {
        if (drawBackground) {
            super.render();
        }
        components.forEach((c) -> c.renderComponent());
    }

    private MouseEvent processMouseEvent(MouseEvent evt, Component c) {
        
        BoundingBox2D b = c.bounds();
        if (c.enabled() && b.contains(new Vector2(evt.x, evt.y))) {
            Vector2 v = c.position();
            return new MouseEvent(evt.button, (int) (evt.x - v.x), (int) (evt.y - v.y), evt.dx, evt.dy, evt.wheel, evt.type);
        } else {
            return null;
        }
    }

    @Override
    protected void mousePressed(MouseEvent evt) {
        if (enabled()) {
            components.forEach((c) -> {
                MouseEvent local = processMouseEvent(evt, c);
                if (local != null) {
                    c.setSelected(true);
                    c.mousePressed(local);
                } else {
                    c.setSelected(false);
                }
            });
        }
    }

    @Override
    protected void mouseReleased(MouseEvent evt) {
        if (enabled()) {
            components.forEach((c) -> {
                MouseEvent local = processMouseEvent(evt, c);
                if (local != null) {
                    c.mouseReleased(local);
                }
            });
        }
    }

    @Override
    protected void mouseDragged(MouseEvent evt) {
        if (enabled()) {
            components.forEach((c) -> {
                MouseEvent local = processMouseEvent(evt, c);
                if (local != null) {
                    c.mouseDragged(local);
                }
            });
        }
    }

    @Override
    protected void mouseMoved(MouseEvent evt) {
        if (enabled()) {
            components.forEach((c) -> {
                MouseEvent local = processMouseEvent(evt, c);
                if (local != null) {
                    c.mouseMoved(local);
                }
            });
        }
    }

    @Override
    protected void mouseWheelMoved(MouseEvent evt) {
        if (enabled()) {
            components.forEach((c) -> {
                MouseEvent local = processMouseEvent(evt, c);
                if (local != null) {
                    c.mouseWheelMoved(local);
                }
            });
        }
    }

    @Override
    protected void keyPressed(KeyEvent evt) {
        if (enabled() && selected()) {
            components.forEach((c) -> {
                if (c.selected() && c.enabled()) {
                    c.keyPressed(evt);
                }
            });
        }
    }

    @Override
    protected void keyReleased(KeyEvent evt) {
        if (enabled() && selected()) {
            components.forEach((c) -> {
                if (c.selected() && c.enabled()) {
                    c.keyReleased(evt);
                }
            });
        }
    }

    @Override
    protected void onDeselected() {
        components.forEach((c) -> c.setSelected(false));
    }
}

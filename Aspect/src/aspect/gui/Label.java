package aspect.gui;

import aspect.util.Color;
import aspect.util.Vector2;

public class Label extends Rectangle {
    private String text;
    private int textSize = 24;
    private Color textColor;
    private boolean drawBackground = true;

    public Label(Vector2 position, String text, boolean drawBackground) {
        this(text, drawBackground);
        setPosition(position);
    }

    public Label(String text, boolean drawBackground) {
        setTextSize(textSize);
        setText(text);
        setBackgroundEnabled(drawBackground);
        fitToText();
    }

    public Label(Vector2 position, Vector2 size) {
        super(position, size);
    }

    public Label(Vector2 size) {
        super(size);
    }

    public void setBackgroundEnabled(boolean enabled) {
        drawBackground = enabled;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String text() {
        return text == null ? "" : text;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public void setTextColor(Color c) {
        textColor = c;
    }

    public Color textColor() {
        return textColor;
    }

    public void fitToText() {
        setSize(GUI.font.getSize(text, textSize).plus(new Vector2(10)));
    }

    @Override
    public void render() {
        if (drawBackground) {
            super.render();
        }

        if (text != null) {
            GUI.font.setSize(textSize);
            Vector2 textDims = GUI.font.getSize(text, textSize);
            Vector2 position = size().minus(textDims).times(0.5f);
            if (textColor == null) {
                GUI.drawText(text, position);
            } else {
                GUI.drawText(text, position, textColor);
            }
        }
    }
}

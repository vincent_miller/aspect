/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import aspect.core.AspectRenderer.GeometryType;
import aspect.core.Console.LogLevel;
import aspect.core.Console;
import static aspect.core.AspectRenderer.*;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glVertexAttribIPointer;

/**
 * A 3D ViewModel that stores vertices, normals, colors, and texture coordinates
 * within the graphics card so they can be rendered very quickly. The Resources
 * class provides many utility methods for quickly building 3D models.
 *
 * @author MillerV
 * @see aspect.resources.Resources
 */
public class AbstractMesh extends ViewModel {

    public final HashMap<String, Integer> bufferIDs;
    private final HashMap<String, Integer> indices;
    private final HashMap<String, Integer> keys;
    private final HashMap<String, Integer> types;

    public final GeometryType geomType;

    public Material material;

    public final int numVertices;
    
    
    public static class Data {
        private String key;
        private Buffer values;
        private int perVertex;
        
        public Data(String key, int perVertex, float[] values) {
            this(key, perVertex, createBuffer(values));
        }
        
        public Data(String key, int perVertex, int[] values) {
            this(key, perVertex, createBuffer(values));
        }
        
        public Data(String key, int perVertex, FloatBuffer values) {
            this.key = key;
            this.values = values;
            this.perVertex = perVertex;
        }
        
        public Data(String key, int perVertex, IntBuffer values) {
            this.key = key;
            this.values = values;
            this.perVertex = perVertex;
        }
    }

    public AbstractMesh(GeometryType type, Material material, Data... data) {
        geomType = type;
        keys = new HashMap<>();
        bufferIDs = new HashMap<>();
        indices = new HashMap<>();
        types = new HashMap<>();
        
        if (data.length == 0) {
            throw new IllegalArgumentException("Must supply at least one data array.");
        }
        
        numVertices = data[0].values.limit() / data[0].values.capacity();
        
        for (Data datum : data) {
            keys.put(datum.key, datum.perVertex);
            int id = glGenBuffers();
            bufferIDs.put(datum.key, id);
            glBindBuffer(GL_ARRAY_BUFFER, id);
            if (datum.values instanceof FloatBuffer) {
                glBufferData(GL_ARRAY_BUFFER, (FloatBuffer)datum.values, GL_STATIC_DRAW);
                types.put(datum.key, GL_FLOAT);
            } else {
                glBufferData(GL_ARRAY_BUFFER, (IntBuffer)datum.values, GL_STATIC_DRAW);
                types.put(datum.key, GL_INT);
            }
            
            indices.put(datum.key, material.shader.getAttributeAddress(datum.key));
        }

        this.material = material;
    }
    
    private void updateIndices() {
        for (String s : indices.keySet()) {
            indices.put(s, material.shader.getAttributeAddress(s));
        }
    }

    public AbstractMesh(GeometryType type, Material material, int numVertices, HashMap<String, Integer> keys, HashMap<String, Integer> bufferIDs, HashMap<String, Integer> indices, HashMap<String, Integer> types) {
        this.material = material;
        this.numVertices = numVertices;
        this.geomType = type;
        this.keys = keys;
        this.bufferIDs = bufferIDs;
        this.indices = indices;
        this.types = types;
    }
    /**
     * Creates another Mesh object which points to same VBO but can have an
     * independent material and transform.
     *
     * @return a shallow copy of this Mesh
     */
    @Override
    public AbstractMesh copy() {
        AbstractMesh m = new AbstractMesh(geomType, material, numVertices, keys, bufferIDs, indices, types);
        m.transform.set(transform);
        return m;
    }

    public void destroy() {
        for (int i : bufferIDs.values()) {
            glDeleteBuffers(i);
        }
    }

    public static FloatBuffer createBuffer(float[] floats) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(floats.length);
        buffer.put(floats);
        buffer.flip();
        return buffer;
    }

    public static IntBuffer createBuffer(int[] ints) {
        IntBuffer buffer = BufferUtils.createIntBuffer(ints.length);
        buffer.put(ints);
        buffer.flip();
        return buffer;
    }

    @Override
    protected void renderModel() {
        modelMatrix.push();
        modelMatrix.multiply(transform.globalModelMatrix());

        enableBuffers();
        
        if (material != null) {
            material.bind();
        }
        
        updateIndices();

        bindBuffers();
        glDrawArrays(geomType.glEnum, 0, numVertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        disableBuffers();

        if (material != null) {
            material.unbind();
        }
        
        modelMatrix.pop();
    }

    protected void enableBuffers() {
        for (String s : indices.keySet()) {
            glEnableVertexAttribArray(indices.get(s));
        }
    }

    protected void bindBuffers() {
        for (String s : keys.keySet()) {
            glBindBuffer(GL_ARRAY_BUFFER, bufferIDs.get(s));
            int t = types.get(s);
            if (t == GL_FLOAT) {
                glVertexAttribPointer(indices.get(s), keys.get(s), GL_FLOAT, false, 0, 0);
            } else {
                glVertexAttribIPointer(indices.get(s), keys.get(s), GL_INT, 0, 0);
            }
        }
    }

    protected void disableBuffers() {
        for (String s : indices.keySet()) {
            glDisableVertexAttribArray(indices.get(s));
        }
    }
}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import aspect.core.AspectRenderer;
import static aspect.core.AspectRenderer.*;
import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.render.shader.ShaderProgram;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Vector3;

import java.nio.FloatBuffer;
import java.util.LinkedList;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.util.vector.Matrix4f;


/**
 *
 * @author MillerV
 */
public class PointLight extends Entity {
    public final Light light;
    
    public PointLight(Vector3 position) {
        this();
        transform.setPosition(position);
    }
    
    public PointLight(Color color) {
        this.light = new Light(color);
        addBehavior(light);
    }
    
    public PointLight(Color color, Vector3 position) {
        this(color);
        transform.setPosition(position);
    }
    
    public PointLight() {
        this(Color.WHITE);
    }
}

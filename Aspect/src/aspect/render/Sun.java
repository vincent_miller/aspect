package aspect.render;

import static aspect.core.AspectLauncher.mainWorld;
import static aspect.core.AspectRenderer.*;
import static aspect.resources.Resources.rect;

import org.lwjgl.opengl.GL11;

import aspect.core.Console;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Vector2;
import aspect.util.Vector3;

public class Sun extends ViewModel {
    private ViewModel back;
    
    private ShaderProgram sun;
    private ShaderProgram flare;
    
    public Sun() {
        this(ShaderProgram.SUN, ShaderProgram.SUN_FLARE);
    }
    
    public Sun(Vector3 position) {
        this();
        transform.setPosition(position);
    }
    
    public Sun(ShaderProgram sun, ShaderProgram flare) {
        this.sun = sun;
        this.flare = flare;
        back = rect(new Material(sun), getCanvasWidth(), getCanvasHeight());
        back.transform.setPosition(new Vector3(getCanvasWidth() / 2, getCanvasHeight() / 2));
        back.transform.setScale(new Vector3(-1, 1));
        
        postProcess(flare);
    }
    
    public Sun(Vector3 position, ShaderProgram sun, ShaderProgram flare) {
        this(sun, flare);
        transform.setPosition(position);
    }

    @Override
    protected void renderModel() {
        pushIdentityMatrices();
        updateProjection(ProjectionMode.ORTHOGRAPHIC);
        back.render();
        popMatrices();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }
    
    @Override
    public void update () {
        boolean showflare = false;
        Vector2 pnt = projectVectorNorm(transform.position());
        clearRenderer();
        ShaderProgram.lock(ShaderProgram.COLOR);
        //mainWorld.render();
        ShaderProgram.unlock();
        float depth = (pnt == null) ? 0 : getPixelDepth((int) (pnt.x * getCanvasWidth()), (int) (pnt.y * getCanvasHeight()));
        float t = getPixelDepth(getCanvasWidth() / 2, getCanvasHeight() / 2);
        t = 0.1f + (1000 - 0.1f) * t;
        t = (float) Math.pow(t, 256);
        //Console.println(t);
        boolean visible = (depth == 1.0f);
        //boolean visible = pnt != null;
        if (visible) {
            float f = pnt.mag2() / 20;
            f = Math.min(f, 0.2f);
            Light.ambient = new Color(f);
        } else {
            Light.ambient = new Color(0.1f);
        }

        if (pnt != null) {
            flare.setUniform("sun", pnt);
            sun.setUniform("sun", pnt);

            showflare = visible;
        }

        flare.setUniform("show", showflare);
        sun.setUniform("show", pnt != null);
    }

    @Override
    public ViewModel copy() {
        return new Sun(sun, flare);
    }

}

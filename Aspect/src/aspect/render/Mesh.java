/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import aspect.core.AspectRenderer.GeometryType;
import aspect.core.Console.LogLevel;
import aspect.core.Console;
import static aspect.core.AspectRenderer.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

/**
 * A 3D ViewModel that stores vertices, normals, colors, and texture coordinates
 * within the graphics card so they can be rendered very quickly. The Resources
 * class provides many utility methods for quickly building 3D models.
 *
 * @author MillerV
 * @see aspect.resources.Resources
 */
public class Mesh {

    public final int vertexID;
    public final int normalID;
    public final int colorID;
    public final int texcoordID;

    private int vertexIndex;
    private int normalIndex;
    private int colorIndex;
    private int texcoordIndex;

    public final GeometryType geomType;

    public final int numVertices;

    private Mesh(GeometryType type, FloatBuffer vertices, FloatBuffer normals, FloatBuffer colors, FloatBuffer texcoords) {
        geomType = type;
        numVertices = vertices.capacity() / 3;

        vertexID = glGenBuffers();
        normalID = glGenBuffers();
        colorID = glGenBuffers();
        texcoordID = glGenBuffers();

        glBindBuffer(GL_ARRAY_BUFFER, vertexID);
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, normalID);
        glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, colorID);
        glBufferData(GL_ARRAY_BUFFER, colors, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, texcoordID);
        glBufferData(GL_ARRAY_BUFFER, texcoords, GL_STATIC_DRAW);
    }

    public Mesh(GeometryType type, int numVertices, int vertexID, int normalID, int colorID, int texcoordID) {
        this.vertexID = vertexID;
        this.normalID = normalID;
        this.colorID = colorID;
        this.texcoordID = texcoordID;
        this.numVertices = numVertices;
        this.geomType = type;
    }

    public Mesh(GeometryType type, float[] vertices, float[] normals, float[] colors, float[] texcoords) {
        this(type, createBuffer(vertices), createBuffer(normals), createBuffer(colors), createBuffer(texcoords));
    }

    protected void updateIndices(Material material) {
        if (material != null && material.shader != null) {
            vertexIndex = material.shader.getAttributeAddress("Vertex");
            normalIndex = material.shader.getAttributeAddress("Normal");
            colorIndex = material.shader.getAttributeAddress("Color");
            texcoordIndex = material.shader.getAttributeAddress("TexCoord");
        } else {
            vertexIndex = -1;
            normalIndex = -1;
            colorIndex = -1;
            texcoordIndex = -1;
        }
    }

    /**
     * Creates another Mesh object which points to same VBO but can have an
     * independent material and transform.
     *
     * @return a shallow copy of this Mesh
     */
    public Mesh copy() {
        Mesh m = new Mesh(geomType, numVertices, vertexID, normalID, colorID, texcoordID);
        return m;
    }

    public void destroy() {
        glDeleteBuffers(vertexID);
        glDeleteBuffers(normalID);
        glDeleteBuffers(texcoordID);
    }

    public static FloatBuffer createBuffer(float[] floats) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(floats.length);
        buffer.put(floats);
        buffer.flip();
        return buffer;
    }

    protected void render(Material material) {
        if (material != null) {
            material.bind();
        }
        
        updateIndices(material);
        enableBuffers();

        bindBuffers();
        glDrawArrays(geomType.glEnum, 0, numVertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        disableBuffers();

        if (material != null) {
            material.unbind();
        }
        
    }

    protected void enableBuffers() {
        if (vertexIndex != -1) {
            glEnableVertexAttribArray(vertexIndex);
        } else {
            glEnableClientState(GL_VERTEX_ARRAY);
        }

        if (normalIndex != -1) {
            glEnableVertexAttribArray(normalIndex);
        } else {
            glEnableClientState(GL_NORMAL_ARRAY);
        }

        if (colorIndex != -1) {
            glEnableVertexAttribArray(colorIndex);
        } else {
            glEnableClientState(GL_COLOR_ARRAY);
        }

        if (texcoordIndex != -1) {
            glEnableVertexAttribArray(texcoordIndex);
        } else {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        }
    }

    protected void bindBuffers() {
        glBindBuffer(GL_ARRAY_BUFFER, vertexID);
        if (vertexIndex != -1) {
            glVertexAttribPointer(vertexIndex, 3, GL_FLOAT, false, 0, 0);
        } else {
            glVertexPointer(3, GL_FLOAT, 0, 0);
        }

        glBindBuffer(GL_ARRAY_BUFFER, normalID);
        if (normalIndex != -1) {
            glVertexAttribPointer(normalIndex, 3, GL_FLOAT, false, 0, 0);
        } else {
            glNormalPointer(GL_FLOAT, 0, 0);
        }

        glBindBuffer(GL_ARRAY_BUFFER, colorID);
        if (colorIndex != -1) {
            glVertexAttribPointer(colorIndex, 4, GL_FLOAT, false, 0, 0);
        } else {
            glColorPointer(4, GL_FLOAT, 0, 0);
        }

        glBindBuffer(GL_ARRAY_BUFFER, texcoordID);
        if (texcoordIndex != -1) {
            glVertexAttribPointer(texcoordIndex, 2, GL_FLOAT, false, 0, 0);
        } else {
            glTexCoordPointer(2, GL_FLOAT, 0, 0);
        }

    }

    protected void disableBuffers() {
        if (vertexIndex != -1) {
            glDisableVertexAttribArray(vertexIndex);
        } else {
            glDisableClientState(GL_VERTEX_ARRAY);
        }

        if (normalIndex != -1) {
            glDisableVertexAttribArray(normalIndex);
        } else {
            glDisableClientState(GL_NORMAL_ARRAY);
        }

        if (colorIndex != -1) {
            glDisableVertexAttribArray(colorIndex);
        } else {
            glDisableClientState(GL_COLOR_ARRAY);
        }

        if (texcoordIndex != -1) {
            glDisableVertexAttribArray(texcoordIndex);
        } else {
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        }
    }
}

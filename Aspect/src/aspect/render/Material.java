/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import static aspect.core.AspectRenderer.*;
import aspect.render.shader.ShaderProgram;
import aspect.time.Time;
import aspect.util.Color;
import aspect.util.Matrix4x4;

import java.awt.image.BufferedImage;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

/**
 * The material that is applied to a face. This includes texture, color, texture
 * properties, and all other relevant information.
 *
 * @author MillerV
 */
public class Material {

    public Texture texture = null;
    public ShaderProgram shader = defaultShader;
    public boolean cull = false;
    public Color ambient = Color.WHITE;
    public Color diffuse = Color.WHITE;
    public Color specular = Color.BLACK;
    public Color emissive = Color.BLACK;
    public float shininess = 1.0f;

    public static ShaderProgram defaultShader = ShaderProgram.PHONG_VERTEX;

    public Material() {
    }

    public Material(ShaderProgram shader) {
        this.shader = shader;
    }

    public Material(Texture texture) {
        this.texture = texture;
    }

    public Material(Texture texture, ShaderProgram shader) {
        this.texture = texture;
        this.shader = shader;
    }

    public Material(Color color) {
        this.diffuse = color;
        this.ambient = color;
    }

    public Material(Color color, ShaderProgram shader) {
        this.diffuse = color;
        this.ambient = color;
        this.shader = shader;
    }

    public Material copy() {
        Material m = new Material();
        m.texture = texture;
        m.shader = shader;
        m.cull = cull;
        m.ambient = ambient;
        m.diffuse = diffuse;
        m.specular = specular;
        m.emissive = emissive;
        m.shininess = shininess;
        return m;
    }

    /**
     * Set the filters for this texture (in other words, tells OpenGL what to do
     * when the texture must be scaled up or down.
     *
     * @param minFilter
     *            the filter for scaling down
     * @param magFilter
     *            the filter for scaling up
     */
    public void setTextureFilter(Filter minFilter, Filter magFilter) {
        if (texture != null) {
            texture.setFilter(minFilter.glMode, magFilter.glMode);
        }
    }

    /**
     * Set the texture repeat behavior for the Texture (in other words, tells
     * OpenGL whether to wrap around if it gets a texture coordinate greater
     * than 1.0 or less than 0.0).
     *
     * @param repeat
     *            whether the texture should repeat
     */
    public void setTextureRepeat(boolean repeat) {
        if (texture != null) {
            texture.setRepeat(repeat);
        }
    }

    /**
     * Get the texture used by this Material.
     *
     * @return the texture
     */
    public Texture getTexture() {
        return texture;
    }

    /**
     * Set this Material as the active material for drawing geometry. It is not
     * usually necessary to call this method, as it is called by a Mesh.
     */
    public void bind() {
        color(ambient);

        if (shader != null) {
            shader.bind();

            ShaderProgram currentShader = ShaderProgram.getCurrent();

            currentShader.updateMaterial(this);

            currentShader.updateMatrices();
        }

        if (texture != null) {
            glActiveTexture(GL_TEXTURE0);
            texture.bind();
        }

        setCulling(cull);
    }

    /**
     * Unbind this Material so that geometry will not be draw with a texture
     * (until bind() is called on another Material).
     */
    public void unbind() {
        color(Color.WHITE);

        if (shader != null) {
            ShaderProgram.unbind();
        }

        if (texture != null) {
            texture.unbind();
        }
    }

    /**
     * The set of filters that can be used for texture scaling.
     */
    public enum Filter {

        /**
         * Linear filtering finds the average of nearby pixels and creates a
         * smooth effect.
         */
        LINEAR(GL_LINEAR),
        /**
         * Nearest filtering finds the nearest pixel on the texture and uses
         * that exact color, creating a pixelated effect.
         */
        NEAREST(GL_NEAREST),
        /**
         * Mipmap filtering uses pre-scaled versions of the texture to create an
         * even smoother effect than Linear filtering. Mipmap filtering can only
         * be used as a minFilter.
         */
        MIPMAP(GL_LINEAR_MIPMAP_LINEAR);

        private final int glMode;

        private Filter(int glMode) {
            this.glMode = glMode;
        }
    }
}

package aspect.render;

import static aspect.core.AspectRenderer.modelMatrix;

public class MeshRenderer extends ViewModel {
    public Mesh mesh;
    public Material material;

    public MeshRenderer(Mesh mesh, Material material) {
        this.mesh = mesh;
        this.material = material;
    }
    
    public MeshRenderer() {
        
    }

    @Override
    protected void renderModel() {
        modelMatrix.push();
        modelMatrix.multiply(transform.globalModelMatrix());
        mesh.render(material);
        modelMatrix.pop();
    }

    @Override
    public ViewModel copy() {
        MeshRenderer cpy = new MeshRenderer(mesh, material);
        cpy.transform.set(transform);
        return cpy;
    }

    public void destroy() {
        mesh.destroy();
    }

}

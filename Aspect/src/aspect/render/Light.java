package aspect.render;

import static aspect.core.AspectRenderer.camera;
import static aspect.core.AspectRenderer.lightingEnabled;

import java.util.LinkedList;

import aspect.entity.behavior.Behavior;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Vector3;

public class Light extends Behavior {

    public Color color = Color.WHITE;

    public float constantAttenuation = 1.0f;
    public float linearAttenuation = 0.0f;
    public float quadraticAttenuation = 0.0f;

    public boolean directional = false;
    public boolean enabled = true;

    private static final LinkedList<Light> lights = new LinkedList<>();
    public static Color ambient = Color.BLACK;

    public static void clear() {
        lights.clear();
    }

    @Override
    public void onRemove() {
        lights.remove(this);
        super.onRemove();
    }

    public Light() {
        lights.add(this);
    }
    
    public Light(Color color) {
        this();
        this.color = color;
    }

    public static void drawLights(LinkedList<ShaderProgram> shaders) {
        if (shaders.isEmpty()) {
            return;
        }

        int index = 0;
        for (Light light : lights) {
            if (index < 8) {
                if (light.enabled) {
                    light.drawLight(index, shaders);
                    index++;
                }
            } else {
                break;
            }
        }

        int numLights = index;
        
        for (int i = index; i < 8; i++) {
            disableLight(i, shaders);
        }

        for (ShaderProgram shader : shaders) {
            shader.setUniform("LightModel.enabled", lightingEnabled());
            shader.setUniform("LightModel.count", numLights);
            shader.setUniform("LightModel.ambient", ambient);
        }
    }
    
    private static void disableLight(int index, LinkedList<ShaderProgram> shaders) {
        for (ShaderProgram shader : shaders) {
            shader.setUniform("Lights[" + index + "].enabled", false);
        }
    }

    private void drawLight(int index, LinkedList<ShaderProgram> shaders) {
        Matrix4x4 view = camera.globalViewMatrix();
        Vector3 position = directional ? view.transformVector(entity.transform.global().position()) : view.transformPoint(entity.transform.global().position());
        
        for (ShaderProgram shader : shaders) {
            shader.setUniform("Lights[" + index + "].position", position);
            shader.setUniform("Lights[" + index + "].color", color);
            shader.setUniform("Lights[" + index + "].directional", directional);
            shader.setUniform("Lights[" + index + "].enabled", true);
            if (!directional) {
                shader.setUniform("Lights[" + index + "].constantAttenuation", constantAttenuation);
                shader.setUniform("Lights[" + index + "].linearAttenuation", linearAttenuation);
                shader.setUniform("Lights[" + index + "].quadraticAttenuation", quadraticAttenuation);
            }
        }
    }

}

/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Hashtable;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.*;

/**
 * A reference to an OpenGL texture object. Also has static utility methods for
 * loading image data into OpenGL.
 *
 * @author MillerV
 */
public class Texture {

    public final int target;
    public final int id;

    public int width;
    public int height;

    public BufferedImage source;

    public static boolean USE_POWER_OF_2 = false;

    private static int savedTexture = 0;

    private static final ColorModel glAlphaColorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 8, 8, 8, 8 }, true, true, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);

    private static final ColorModel glColorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 8, 8, 8, 0 }, false, false, ComponentColorModel.OPAQUE, DataBuffer.TYPE_BYTE);

    /**
     * Create a new Texture wrapping the specified OpenGL texture object. This
     * does not load any data into OpenGL, it only wraps existing data.
     *
     * @param target
     *            the OpenGL target
     * @param textureID
     *            the ID of the texture
     */
    public Texture(int target, int textureID) {
        this.target = target;
        this.id = textureID;
    }

    public void bind() {
        savedTexture = glGetInteger(GL_TEXTURE_BINDING_2D);
        glBindTexture(target, id);
    }

    public void unbind() {
        glBindTexture(target, savedTexture);
    }

    public int getID() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setFilter(int minFilter, int magFilter) {
        bind();
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
        unbind();
    }

    public void setRepeat(boolean repeat) {
        bind();
        if (repeat) {
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
        } else {
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP);
        }
        unbind();
    }

    /**
     * Create a new Texture, with GL_TEXTURE_2D as the target, GL_RGBA as the
     * dstPixelFormat, GL_LINEAR_MIPMAP_LINEAR as the minFilter, and GL_LINEAR
     * as the magFilter.
     *
     * @param img
     *            the image data
     * @return a new Texture object wrapping the created OpenGL texture object
     */
    public static Texture create(BufferedImage img) {
        return create(img, GL_TEXTURE_2D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    }

    public static Texture create(InputStream in) throws IOException {
        return create(ImageIO.read(in));
    }

    /**
     * Create a new Texture
     *
     * @param bufferedImage
     *            the image data
     * @param target
     *            the OpenGL target
     * @param dstPixelFormat
     *            the destination pixel format
     * @param minFilter
     *            the minification filter
     * @param magFilter
     *            the magnification filter
     * @return a new Texture object wrapping the created OpenGL texture object
     */
    public static Texture create(BufferedImage bufferedImage, int target, int minFilter, int magFilter) {

        int textureID = glGenTextures();
        Texture texture = new Texture(target, textureID);

        texture.updateTexture(bufferedImage);

        if (target == GL_TEXTURE_2D) {
            texture.setFilter(minFilter, magFilter);
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }

        return texture;
    }

    public void updateTexture(BufferedImage image) {
        bind();

        glEnable(target);

        int srcPixelFormat;

        if (image.getColorModel().hasAlpha()) {
            srcPixelFormat = GL_RGBA;
        } else {
            srcPixelFormat = GL_RGB;
        }

        ByteBuffer textureBuffer = convertImageData(image);

        int po2 = get2Fold(image.getWidth());

        int _width = USE_POWER_OF_2 ? po2 : image.getWidth();
        int _height = USE_POWER_OF_2 ? po2 : image.getHeight();

        glTexImage2D(target, 0, GL_RGBA, _width, _height, 0, srcPixelFormat, GL_UNSIGNED_BYTE, textureBuffer);

        glGenerateMipmap(target);

        width = image.getWidth();
        height = image.getHeight();
        source = image;
        
        unbind();
    }

    private static int get2Fold(int fold) {
        int ret = 2;
        while (ret < fold) {
            ret *= 2;
        }
        return ret;
    }

    public static ByteBuffer convertImageData(BufferedImage bufferedImage) {
        ByteBuffer imageBuffer;
        WritableRaster raster;
        BufferedImage texImage;

        int texWidth;
        int texHeight;

        if (USE_POWER_OF_2) {
            texWidth = 2;
            texHeight = 2;

            while (texWidth < bufferedImage.getWidth()) {
                texWidth *= 2;
            }
            while (texHeight < bufferedImage.getHeight()) {
                texHeight *= 2;
            }
        } else {
            texWidth = bufferedImage.getWidth();
            texHeight = bufferedImage.getHeight();
        }

        if (bufferedImage.getColorModel().hasAlpha()) {
            raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, texWidth, texHeight, 4, null);
            texImage = new BufferedImage(glAlphaColorModel, raster, false, new Hashtable());
        } else {
            raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, texWidth, texHeight, 3, null);
            texImage = new BufferedImage(glColorModel, raster, false, new Hashtable());
        }

        AffineTransform t = AffineTransform.getScaleInstance(-1, -1);
        t.translate(-bufferedImage.getWidth(), -bufferedImage.getHeight());
        AffineTransformOp op = new AffineTransformOp(t, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        bufferedImage = op.filter(bufferedImage, null);
        Graphics g = texImage.getGraphics();
        if (USE_POWER_OF_2) {
            g.drawImage(bufferedImage.getScaledInstance(texWidth, texHeight, BufferedImage.SCALE_SMOOTH), 0, 0, null);
        } else {
            g.drawImage(bufferedImage, 0, 0, null);
        }

        byte[] data = ((DataBufferByte) texImage.getRaster().getDataBuffer()).getData();

        imageBuffer = ByteBuffer.allocateDirect(data.length);
        imageBuffer.order(ByteOrder.nativeOrder());
        imageBuffer.put(data, 0, data.length);
        imageBuffer.flip();

        return imageBuffer;
    }
}

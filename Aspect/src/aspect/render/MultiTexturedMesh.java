package aspect.render;

import aspect.core.AspectRenderer.GeometryType;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL31;

import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL11.*;

public class MultiTexturedMesh extends Mesh {
    public final int texunitID;
    
    private int texunitIndex;
    
    public MultiTexturedMesh(GeometryType type, float[] vertices, float[] normals, float[] colors, float[] texcoords, int[] texunits) {
        super(type, vertices, normals, colors, texcoords);
        
        texunitID = glGenBuffers();
        
        
        init(texunits);
    }
    
    @Override
    public void updateIndices(Material material) {
        super.updateIndices(material);
        texunitIndex = material.shader.getAttributeAddress("textureUnit");
    }
    
    public MultiTexturedMesh(GeometryType type, int numVertices, int vertexID, int normalID, int colorID, int texcoordID, int texunitID) {
        super(type, numVertices, vertexID, normalID, colorID, texcoordID);
        this.texunitID = texunitID;
        
        texunitID = glGenBuffers();
    }
    
    private final void init(int[] texunits) {
        IntBuffer buffer = BufferUtils.createIntBuffer(texunits.length);
        buffer.put(texunits);
        buffer.clear();
        
        glBindBuffer(GL_ARRAY_BUFFER, texunitID);
        glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
    }
    
    @Override
    public Mesh copy() {
        MultiTexturedMesh m = new MultiTexturedMesh(geomType, numVertices, vertexID, normalID, colorID, texcoordID, texunitID);
        return m;
    }
    
    @Override
    protected void enableBuffers() {
        super.enableBuffers();
        glEnableVertexAttribArray(texunitIndex);
    }
    
    @Override
    protected void bindBuffers() {
        super.bindBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, texunitID);
        glVertexAttribIPointer(texunitIndex, 1, GL_INT, 0, 0);
    }
    
    @Override
    protected void disableBuffers() {
        super.disableBuffers();
        glDisableVertexAttribArray(texunitIndex);
    }

}

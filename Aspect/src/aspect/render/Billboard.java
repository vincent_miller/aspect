/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import static aspect.core.AspectRenderer.camera;
import aspect.util.Transform;

/**
 *
 * @author MillerV
 */
public class Billboard extends ViewModel {
    private final ViewModel model;
    private final Transform local;

    public Billboard(ViewModel model) {
        this.model = model;
        this.local = new Transform();
    }
    
    @Override
    public void renderModel() {
        model.transform.setParent(local);
        local.setRotation(camera.global());
        local.setPosition(transform.global().position());
        model.renderModel();
    }

    @Override
    public ViewModel copy() {
        return new Billboard(model);
    }
}

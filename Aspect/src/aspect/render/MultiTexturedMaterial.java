package aspect.render;

import static aspect.core.AspectRenderer.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;

public class MultiTexturedMaterial extends Material {
    public final Texture[] textures;

    public MultiTexturedMaterial(ShaderProgram shader, Texture... textures) {
        int units = maxTextureUnits();

        if (textures.length > units) {
            System.err.println("Can have at most " + units + " textures at once.");
            this.textures = new Texture[units];
            System.arraycopy(textures, 0, this.textures, 0, units);
        } else {
            this.textures = textures;
        }

        this.shader = shader;
    }
    
    public MultiTexturedMaterial(Texture... textures) {
        int units = maxTextureUnits();

        if (textures.length > units) {
            System.err.println("Can have at most " + units + " textures at once.");
            this.textures = new Texture[units];
            System.arraycopy(textures, 0, this.textures, 0, units);
        } else {
            this.textures = textures;
        }

        Shader.clearTempParameters();
        Shader.setTempParameter("numTextures", textures.length);
        shader = ShaderProgram.loadPrebuilt("phong-multitex");
    }

    @Override
    public void bind() {
        super.bind();

        ShaderProgram currentShader = ShaderProgram.getCurrent();

        currentShader.setUniform("useTexture", texturesEnabled());

        for (int i = 0; i < textures.length; i++) {
            glActiveTexture(GL_TEXTURE0 + i);
            textures[i].bind();
            currentShader.setUniform("textures[" + i + "]", i);
        }

    }

    @Override
    public void unbind() {
        super.unbind();

        for (int i = 0; i < textures.length; i++) {
            glActiveTexture(GL_TEXTURE0 + i);
            textures[i].unbind();
        }
    }
}

/*
 * Copyright (C) 2014 vincent
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import aspect.entity.behavior.Behavior;
import aspect.util.Angles;
import aspect.util.Transform;
import aspect.util.Vector3;

/**
 *
 * @author vincent
 */
public abstract class ViewModel extends Behavior {

    public final Transform transform = new Transform();

    @Override
    public final void render() {
        if (entity != null) {
            transform.setParent(entity.transform);
        }
        
        renderModel();
    }

    protected abstract void renderModel();

    public abstract ViewModel copy();
}

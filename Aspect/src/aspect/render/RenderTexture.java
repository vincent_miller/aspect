/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import static aspect.core.AspectRenderer.*;

import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;

import static aspect.resources.Resources.*;

import aspect.time.Time;
import aspect.util.Matrix4x4;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT24;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT0;
import static org.lwjgl.opengl.GL30.GL_DEPTH_ATTACHMENT;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;
import static org.lwjgl.opengl.GL30.glGenFramebuffers;
import static org.lwjgl.opengl.GL32.glFramebufferTexture;

import org.lwjgl.util.glu.GLU;

/**
 *
 * @author MillerV
 */
public class RenderTexture extends Texture {

    public final int fboID;

    private final int depthID;
    public final MeshRenderer drawModel;

    public Transform viewpoint;

    private ShaderProgram postProcess;
    private int savedTexture;

    public RenderTexture(int width, int height, Transform viewpoint) {
        super(GL_TEXTURE_2D, glGenTextures());

        if (!fboEnabled()) {
            System.err.println("FBO is not supported on your version of OpenGL");
            fboID = 0;
            depthID = 0;
            drawModel = null;
            return;
        }

        fboID = glGenFramebuffers();
        depthID = glGenTextures();
        glBindFramebuffer(GL_FRAMEBUFFER, fboID);

        bind();
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (java.nio.ByteBuffer) null);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, id, 0);
        
        glBindTexture(GL_TEXTURE_2D, depthID);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (java.nio.ByteBuffer) null);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthID, 0);
        
        bind();

        this.width = width;
        this.height = height;

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        drawModel = (MeshRenderer) rect(new Material(this, ShaderProgram.TEXTURE), -getCanvasWidth(), getCanvasHeight());

        setRepeat(false);
        setFilter(GL11.GL_NEAREST, GL11.GL_NEAREST);

        this.viewpoint = viewpoint;
    }

    public void postProcess(ShaderProgram shader) {
        if (fboID == 0) {
            return;
        }
        postProcess = shader;
        drawModel.material.shader = postProcess;

        postProcess.setUniform("width", (float) width);
        postProcess.setUniform("height", (float) height);
    }

    public void update() {
        if (fboID == 0) {
            return;
        }

        postProcess.setUniform("time", Time.frameTime());
    }

    public void capture() {
        if (fboID == 0) {
            return;
        }

        glViewport(0, 0, width, height);

        savedTexture = glGetInteger(GL_TEXTURE_BINDING_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, fboID);

        clearRenderer(viewpoint);
    }
    
    public Texture getDepthTexture() {
        return new Texture(GL_TEXTURE_2D, depthID);
    }

    public void stop() {
        if (fboID == 0) {
            return;
        }
        
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, depthID);
        
        postProcess.setUniform("depth", 1);
        glActiveTexture(GL_TEXTURE0);

        glViewport(0, 0, getCanvasWidth(), getCanvasHeight());

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        

        glBindTexture(GL_TEXTURE_2D, savedTexture);

        clearRenderer();
    }

    public void render() {
        if (fboID == 0) {
            return;
        }

        projectionMatrix.push();
        viewMatrix.push();
        modelMatrix.push();

        updateProjection(ProjectionMode.ORTHOGRAPHIC);
        viewMatrix.set(Matrix4x4.identity());
        modelMatrix.set(Matrix4x4.identity());

        drawModel.transform.setPosition(new Vector3(getCanvasWidth() / 2, getCanvasHeight() / 2));
        drawModel.render();

        projectionMatrix.pop();
        viewMatrix.pop();
        modelMatrix.pop();
        glEnable(GL_DEPTH_TEST);
    }
}

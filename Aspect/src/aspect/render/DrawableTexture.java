/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render;

import static aspect.core.AspectRenderer.*;
import static aspect.core.AspectRenderer.ProjectionMode.ORTHOGRAPHIC;
import static aspect.core.AspectRenderer.ProjectionMode.PERSPECTIVE;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.glWindowPos2i;

/**
 * A 2D canvas on which graphics can be drawn using java 2D. This is not the
 * most efficient way to draw 2D graphics in OpenGL; it is faster to actually
 * build a model for each Entity. The models can then use textures linked to a
 * Canvas2D, and only updated when their sprite changes.
 *
 * @author millerv
 */
public class DrawableTexture extends Texture {

    private final BufferedImage image;
    protected final Graphics2D pen;
    private ViewModel model;
    private Material material;

    /**
     * Create a new Canvas2D.
     *
     * @param width the image width
     * @param height the image height
     */
    public DrawableTexture(int width, int height) {
        super(GL_TEXTURE_2D, glGenTextures());
        image = Resources.createImage(width, height);
        pen = image.createGraphics();

        updateTexture();
    }

    /**
     * Get the width of this Canvas2D.
     *
     * @return the width of the image
     */
    @Override
    public int getWidth() {
        return image.getWidth();
    }

    /**
     * Get the height of this Canvas2D.
     *
     * @return the width of the image
     */
    @Override
    public int getHeight() {
        return image.getHeight();
    }

    /**
     * Get the Graphics2D context used to draw onto this Canvas2D.
     *
     * @return the Graphics2D for drawing on this Canvas2D
     */
    public Graphics2D getGraphics() {
        return pen;
    }

    /**
     * Fill a rectangle with the specified color at the coordinates (0, 0) and
     * covering the entire Canvas2D.
     *
     * @param background the background color
     */
    public void clear(Color background) {
        pen.setColor(background);
        pen.fillRect(0, 0, getWidth(), getHeight());
    }

    public void clearAlpha() {
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                image.setRGB(i, j, 0);
            }
        }
    }

    /**
     * Get a ViewModel that will allow this canvas to be drawn on the screen.
     * Make sure to call AspectRenderer.setRenderMode(RenderMode.A2D);
     *
     * @return a model that can be used to draw this canvas.
     */
    public ViewModel getModel() {
        if (model == null) {
            material = new Material(this);
            material.shader = ShaderProgram.TEXTURE;
            model = Resources.rect(material, getWidth(), getHeight());
        }

        return model;
    }

    public void updateTexture() {
        updateTexture(image);
        setFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    public void renderModel(Vector2 position) {
        updateProjection(ORTHOGRAPHIC);
        getModel();
        model.transform.setPosition(new Vector3(position.x + getWidth() / 2, getCanvasHeight() - getHeight() / 2 - position.y));
        model.render();
        updateProjection(PERSPECTIVE);
    }
    
    public void renderPixels(Vector2 position) {
        ByteBuffer data = convertImageData(image);
        glPixelZoom(-1.0f, 1.0f);
        glWindowPos2i((int)position.x + getWidth(), getCanvasHeight() - getHeight() - (int)position.y);
        glDrawPixels(getWidth(), getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, data);
    }
}

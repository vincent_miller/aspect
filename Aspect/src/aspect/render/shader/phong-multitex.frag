#version 130
#include <lighting.ase>

uniform bool useTexture;
uniform sampler2D textures[{numTextures}];

in vec3 normal;
in vec3 ambient;
in vec4 color;
in vec4 position;

in float diffuse[{maxLights}];
in float texUnit;

in vec2 texcoord;

int round(float f) {
	return int(f + 0.5);
}

void main() {
	vec4 texColor = color;

    if (useTexture && texUnit >= 0) {
        texColor *= texture2D(textures[round(texUnit)], texcoord);
    }
    
    if (texColor.a == 0) {
        discard;
    }
    	
	
	vec3 lightColor = vec3(1.0);
	
	if (LightModel.enabled) {
	    lightColor = ambient;
		lightColor += lightTotalDiffuseFloat(lightApplyAttenuationVectorFloat(position, diffuse));
		if (!useTexture) {
			lightColor = min(lightColor, 1.0);
		}
		//lightColor += lightTotalSpecularFloat(lightApplyAttenuationVectorFloat(position, lightSpecularFloat(position, normal)));
	}

    gl_FragColor = texColor * vec4(lightColor, 1.0);
}
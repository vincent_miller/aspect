#version 130
#include <matrices.ase>
#include <vertex.ase>

out vec2 texcoord;

void main() {
    gl_Position = Matrices.projection * Matrices.modelview * Vertex;
    texcoord = TexCoord;
}

#version 130
#include <matrices.ase>
#include <vertex.ase>

void main() {
    gl_Position = Matrices.projection * Matrices.modelview * Vertex;
}

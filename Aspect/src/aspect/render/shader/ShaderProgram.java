/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render.shader;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.render.PointLight;
import aspect.render.Light;
import aspect.render.Material;
import aspect.time.Time;
import aspect.util.Matrix3x3;
import aspect.util.Vector3;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Vector2;

import java.util.HashMap;
import java.util.LinkedList;

import org.lwjgl.opengl.OpenGLException;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static aspect.core.AspectRenderer.*;

/**
 *
 * @author MillerV
 */
public class ShaderProgram {

    public static ShaderProgram PHONG_VERTEX;
    public static ShaderProgram PHONG_FRAGMENT;
    public static ShaderProgram COLOR_UNIFORM;
    public static ShaderProgram TEXTURE;
    public static ShaderProgram SELECT;
    public static ShaderProgram COLOR;
    public static ShaderProgram SUN;
    public static ShaderProgram SUN_FLARE;

    private static ShaderProgram current = null;
    private static boolean locked = false;
    private boolean updateMaterial = false;
    private boolean updateMatrices = false;

    private static final LinkedList<ShaderProgram> updateLights = new LinkedList<>();
    private static final LinkedList<ShaderProgram> updateTime = new LinkedList<>();

    private HashMap<String, Integer> variables = new HashMap<>();
    
    private static HashMap<Integer, ShaderProgram> lookup = new HashMap<>();

    public static boolean locked() {
        return locked;
    }

    public final int programID;

    public ShaderProgram(int programID) {
        this.programID = programID;
        lookup.put(programID, this);
    }

    public ShaderProgram(Shader... shaders) {
        for (Shader shader : shaders) {
            if (shader.id == -1) {
                Console.log(LogLevel.ERROR, "ShaderProgram could not be created (one or more shaders were not loaded properly).");
                programID = 0;
                return;
            }
        }

        int id;

        try {
            id = glCreateProgram();
        } catch (Exception ex) {
            programID = 0;
            return;
        }

        for (Shader shader : shaders) {
            glAttachShader(id, shader.id);
        }

        glLinkProgram(id);
        if (glGetProgrami(id, GL_LINK_STATUS) == GL_FALSE) {
            String info = getLogInfo(id);
            glDeleteProgram(id);
            Console.log(LogLevel.ERROR, "Error linking program:\n" + info);
            programID = 0;
            return;
        }

        glValidateProgram(id);
        if (glGetProgrami(id, GL_VALIDATE_STATUS) == GL_FALSE) {
            String info = getLogInfo(id);
            glDeleteProgram(id);
            Console.log(LogLevel.ERROR, "Error validating program:\n" + info);
            programID = 0;
            return;
        }

        programID = id;

        boolean lighting = false;
        boolean time = false;
        for (Shader shader : shaders) {
            if (shader.libraries.contains("lighting.ase") && !lighting) {
                updateLights.add(this);
                lighting = true;
            }

            if (shader.libraries.contains("time.ase") && !time) {
                updateTime.add(this);
                time = true;
            }

            if (shader.libraries.contains("matrices.ase") && !updateMatrices) {
                updateMatrices = true;
            }

            if (shader.libraries.contains("material.ase")) {
                updateMaterial = true;
            }
        }
        lookup.put(programID, this);
    }
    
    public static ShaderProgram getCurrent() {
        return current;
    }
    
    public static ShaderProgram get(int programID) {
        return lookup.get(programID);
    }

    public void destroy() {
        glDeleteProgram(programID);
        updateTime.remove(this);
        updateLights.remove(this);
    }

    public void bind() {
        if (!locked) {
            glUseProgram(programID);
            current = this;
        }
    }

    public static void unbind() {
        if (!locked) {
            glUseProgram(0);
            current = null;
        }
    }

    public static void lock(ShaderProgram shader) {
        if (!locked) {
            if (shader != null) {
                shader.bind();
                shader.updateMaterial(null);
                shader.updateMatrices();
                locked = true;
            } else {
                unbind();
                locked = true;
            }
        }
    }

    public static void unlock() {
        locked = false;
        unbind();
    }

    public void setUniform(String name, Vector3 v) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform3f(getUniformAddress(name), v.x, v.y, v.z);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, Vector3 v) {
        glVertexAttrib3f(getAttributeAddress(name), v.x, v.y, v.z);
    }

    public void setUniform(String name, Vector2 v) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform2f(getUniformAddress(name), v.x, v.y);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, Vector2 v) {
        glVertexAttrib2f(getAttributeAddress(name), v.x, v.y);
    }

    public void setUniform(String name, Color c) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform4f(getUniformAddress(name), c.red, c.green, c.blue, c.alpha);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, Color c) {
        glVertexAttrib4f(getAttributeAddress(name), c.red, c.green, c.blue, c.alpha);
    }

    public void setUniform(String name, Matrix4x4 m) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniformMatrix4(getUniformAddress(name), false, m.getBuffer());
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setUniform(String name, Matrix3x3 m) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniformMatrix3(getUniformAddress(name), false, m.getBuffer());
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setUniform(String name, int i) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform1i(getUniformAddress(name), i);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, int i) {
        if (((short) i) != i) {
            throw new RuntimeException("Integer must be in range " + Short.MIN_VALUE + ", " + Short.MAX_VALUE);
        }
        glVertexAttrib1s(getAttributeAddress(name), (short) i);
    }

    public void setUniform(String name, float f) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform1f(getUniformAddress(name), f);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, float f) {
        glVertexAttrib1f(getAttributeAddress(name), f);
    }

    public void setUniform(String name, boolean b) {
        int id = glGetInteger(GL_CURRENT_PROGRAM);
        if (id != programID) {
            glUseProgram(programID);
        }
        glUniform1i(getUniformAddress(name), b ? 1 : 0);
        if (id != programID) {
            glUseProgram(id);
        }
    }

    public void setAttribute(String name, boolean b) {
        glVertexAttrib1s(getAttributeAddress(name), (short) (b ? 1 : 0));
    }

    public int getUniformAddress(String name) {
        if (variables.containsKey(name)) {
            return variables.get(name);
        } else {
            int loc = glGetUniformLocation(programID, name);
            if (loc != -1) {
                variables.put(name, loc);
            }

            return loc;
        }
    }

    public int getAttributeAddress(String name) {
        if (variables.containsKey(name)) {
            return variables.get(name);
        } else {
            int loc = glGetAttribLocation(programID, name);
            if (loc != -1) {
                variables.put(name, loc);
            }

            return loc;
        }
    }

    public static void initialize() {
        Shader.initialize();
        
        PHONG_VERTEX = new ShaderProgram(Shader.V_PHONG_VERTEX, Shader.F_PHONG_VERTEX);
        PHONG_FRAGMENT = new ShaderProgram(Shader.V_PHONG_FRAGMENT, Shader.F_PHONG_FRAGMENT);
        COLOR_UNIFORM = new ShaderProgram(Shader.V_COLOR_UNIFORM, Shader.F_COLOR_UNIFORM);
        TEXTURE = new ShaderProgram(Shader.V_TEXTURE, Shader.F_TEXTURE);
        SELECT = new ShaderProgram(Shader.V_SELECT, Shader.F_SELECT);
        COLOR = new ShaderProgram(Shader.V_COLOR, Shader.F_COLOR);
        SUN = new ShaderProgram(Shader.V_TEXTURE, Shader.F_SUN);
        SUN_FLARE = new ShaderProgram(Shader.V_TEXTURE, Shader.F_SUN_FLARE);
    }

    public static void update() {
        Light.drawLights(updateLights);

        for (ShaderProgram shader : updateTime) {
            shader.updateTime();
        }
    }
    
    public void updateTime() {
        setUniform("Time.elapsed", Time.frameTime());
        setUniform("Time.delta", Time.deltaTime());
    }

    public void updateMatrices() {
        if (updateMatrices) {
            Matrix4x4 modelViewMatrix = viewMatrix.current().times(modelMatrix.current());
            setUniform("Matrices.model", modelMatrix.current());
            setUniform("Matrices.view", viewMatrix.current());
            setUniform("Matrices.modelview", modelViewMatrix);
            setUniform("Matrices.normal", Matrix4x4.normalMatrix(modelViewMatrix));
            setUniform("Matrices.projection", projectionMatrix.current());
        }
    }

    public void updateMaterial(Material m) {
        if (m != null) {
            setUniform("useTexture", m.texture != null && texturesEnabled());
            if (updateMaterial) {
                setUniform("Material.ambient", m.ambient);
                setUniform("Material.diffuse", m.diffuse);
                setUniform("Material.specular", m.specular);
                setUniform("Material.emissive", m.emissive);
                setUniform("Material.shininess", m.shininess);
            }
        } else {
            setUniform("useTexture", false);
            if (updateMaterial) {
                setUniform("Material.ambient", Color.WHITE);
                setUniform("Material.diffuse", Color.WHITE);
                setUniform("Material.specular", Color.BLACK);
                setUniform("Material.emissive", Color.BLACK);
                setUniform("Material.shininess", 0.0f);
            }
        }
    }

    public static String getLogInfo(int obj) {
        return glGetProgramInfoLog(obj, glGetProgrami(obj, GL_INFO_LOG_LENGTH));
    }

    public static ShaderProgram loadPrebuilt(String name) {
        Shader vert = Shader.loadPrebuilt(name + ".vert", Shader.Type.VERTEX);
        Shader frag = Shader.loadPrebuilt(name + ".frag", Shader.Type.FRAGMENT);

        return new ShaderProgram(vert, frag);
    }
}

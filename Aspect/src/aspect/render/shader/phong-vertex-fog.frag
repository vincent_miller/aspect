#version 130

uniform bool useTexture;
uniform sampler2D tex;

in vec2 texcoord;
in vec3 lightIntensity;
in vec4 color;
in vec4 camera;
in float worldy;
in float camy;

void main() {
    vec4 texColor = color;

    if (useTexture) {
        texColor *= texture2D(tex, texcoord);
    }
    
    if (texColor.a == 0.0) {
        discard;
    }
    
    vec3 light = texColor.rgb * lightIntensity;
    
    float d = dot(camera, camera);
    float be = max((20 / worldy) * 0.004, 0);
    float bi = max((20 / worldy) * 0.001, 0);
    
    float ext = exp(-d * be);
    float insc = exp(-d * bi);
    //float f = (40 - d) / (40 - 5);
    float f = 1.0 / exp(d * 0.01);
    
    f = min(max(f, 0), 1);
    
    light = (1.0 - insc) * vec3(1.0) + ext * light;
	
    gl_FragColor = vec4(light, texColor.a);
}
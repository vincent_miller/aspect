/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.render.shader;

import static aspect.core.AspectRenderer.getAspectRatio;
import static aspect.core.AspectRenderer.getCanvasHeight;
import static aspect.core.AspectRenderer.getCanvasWidth;
import static aspect.resources.Resources.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import aspect.core.Console;
import aspect.core.Console.LogLevel;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL40.*;
import static org.lwjgl.opengl.GL43.*;

/**
 *
 * @author MillerV
 */
public class Shader {

    private static final String includeOpen = "#include <";
    private static final String includeClose = ">\n";
    private static final String loopOpen = "#loop";
    private static final String loopClose = "#endloop";
    
    public static Shader V_COLOR_UNIFORM;
    public static Shader F_COLOR_UNIFORM;
    public static Shader V_COLOR;
    public static Shader F_COLOR;
    public static Shader V_PHONG_FRAGMENT;
    public static Shader F_PHONG_FRAGMENT;
    public static Shader V_PHONG_VERTEX;
    public static Shader F_PHONG_VERTEX;
    public static Shader V_SELECT;
    public static Shader F_SELECT;
    public static Shader F_SUN_FLARE;
    public static Shader F_SUN;
    public static Shader V_TEXTURE;
    public static Shader F_TEXTURE;

    public final int id;

    public final HashSet<String> libraries = new HashSet<>();

    private static final HashMap<String, String> parameters = new HashMap<>();
    private static HashMap<String, String> parametersTemp = new HashMap<>();

    public final String name;

    private int linesAdded = 0;

    public Shader(int id) {
        this.id = id;
        this.name = "unnamed";
    }

    public Shader(String file, Shader.Type type) {
        this.name = file;
        int shader = 0;
        try {
            shader = create(loadTextFile(new FileInputStream(file)), type);
        } catch (IOException ex) {
            Console.log(LogLevel.ERROR, "Error loading shader: ", ex);
        }
        id = shader;
    }

    public Shader(String program, String name, Shader.Type type) {
        String n;
        if (name == null) {
            n = type.name();
        } else {
            n = name;
        }
        this.name = n;
        id = create(program, type);
    }

    public static enum Type {
        VERTEX(GL_VERTEX_SHADER),
        TESSELATION_CTRL(GL_TESS_CONTROL_SHADER),
        TESSELATION_EVAL(GL_TESS_EVALUATION_SHADER),
        GEOMETRY(GL_GEOMETRY_SHADER),
        FRAGMENT(GL_FRAGMENT_SHADER),
        COMPUTE(GL_COMPUTE_SHADER);

        private final int glType;

        private Type(int glType) {
            this.glType = glType;
        }
    }

    private String addImports(String program) {
        while (true) {
            int index = program.indexOf(includeOpen);
            if (index != -1) {
                index += includeOpen.length();
                int index2 = program.indexOf(includeClose, index);
                if (index2 == -1) {
                    throw new RuntimeException("Syntax error in #include");
                }
                String libname = program.substring(index, index2);
                String filename = libname;
                if (filename.indexOf(".") == -1) {
                    filename += ".ase";
                }
                if (!libraries.add(filename)) {
                    continue;
                }
                try {
                    String library = loadTextFileFromPackage(filename);
                    linesAdded += numOccurances(library, '\n') - 1;
                    program = program.replace(includeOpen + libname + includeClose, library);
                } catch (IOException | URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            } else {
                break;
            }
        }

        return program;
    }
    
    private String addLoops(String program) {
        int lastIndex = 0;
        for (int it = 0; true; it++) {
            int index1 = program.indexOf(loopOpen, lastIndex);
            lastIndex = index1;
            if (index1 != -1) {
                int index2 = program.indexOf(" ", index1);
                int index3 = program.indexOf(" ", index2 + 1);
                int index4 = program.indexOf(" ", index3 + 1);
                int index5 = program.indexOf("\n", index4 + 1);
                int indexEnd = program.indexOf(loopClose, index5);
                
                
                if (indexEnd == -1) {
                    throw new RuntimeException("Syntax error in #loop");
                }
                
                String var = program.substring(index2 + 1, index3);
                int begin = Integer.parseInt(program.substring(index3 + 1, index4));
                int end = Integer.parseInt(program.substring(index4 + 1, index5));
                String body = program.substring(index5, indexEnd);
                
                
                StringBuilder loop = new StringBuilder();
                
                for (int i = begin; i < end; i++) {
                    loop = loop.append(body.replaceAll("\\{" + var + "\\}", String.valueOf(i)));
                }
                
                program = program.replace(program.substring(index1, indexEnd) + loopClose, loop);
                
            } else {
                break;
            }
        }

        return program;
    }

    public static Shader loadPrebuilt(String name, Shader.Type type) {
        try {
            Shader s = new Shader(loadTextFileFromPackage(name), name, type);
            return s;
        } catch (IOException | URISyntaxException ex) {
            Console.log(LogLevel.ERROR, "Error loading shader: ", ex);
            return new Shader(0);
        }
    }

    public static void setParameter(String name, Object value) {
        parameters.put(name, value.toString());
    }

    public static void clearTempParameters() {
        parametersTemp = new HashMap<>();
    }

    public static void setTempParameter(String name, Object value) {
        parametersTemp.put(name, value.toString());
    }

    private static int numOccurances(String string, char search) {
        int n = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == search) {
                n++;
            }
        }
        return n;
    }

    private static String replaceLineNumbers(String log, int linesAdded) {
        /*StringBuilder builder = new StringBuilder();

        int i = 0;
        for (int j = log.indexOf("\n"); j < log.length() && j > 0; i = j, j = log.indexOf("\n", j + 1)) {
            int i1 = log.indexOf(":", i);
            int i2 = log.indexOf(":", i1 + 1);
            int i3 = log.indexOf(":", i2 + 1);

            if (i3 > j || i3 < i) {
                builder.append(log.substring(i, j));
            } else {
                builder.append(log.substring(i, i2 + 1));
                String numString = log.substring(i2 + 1, i3);
                int num = Integer.parseInt(numString) - linesAdded;
                builder.append(String.valueOf(num));
                builder.append(log.substring(i3, j));
            }
        }

        return builder.toString();*/
        return log;
    }
    
    public static void initialize() {
        setParameter("version", getGLSLVersionString());
        setParameter("maxLights", 8);
        setParameter("aspectRatio", getAspectRatio());
        setParameter("screenWidth", getCanvasWidth());
        setParameter("screenHeight", getCanvasHeight());
        
        V_COLOR_UNIFORM = loadPrebuilt("color-uniform.vert", Type.VERTEX);
        F_COLOR_UNIFORM = loadPrebuilt("color-uniform.frag", Type.FRAGMENT);
        V_COLOR = loadPrebuilt("color.vert", Type.VERTEX);
        F_COLOR = loadPrebuilt("color.frag", Type.FRAGMENT);
        V_PHONG_FRAGMENT = loadPrebuilt("phong-fragment.vert", Type.VERTEX);
        F_PHONG_FRAGMENT  = loadPrebuilt("phong-fragment.frag", Type.FRAGMENT);
        V_PHONG_VERTEX = loadPrebuilt("phong-vertex.vert", Type.VERTEX);
        F_PHONG_VERTEX  = loadPrebuilt("phong-vertex.frag", Type.FRAGMENT);
        V_SELECT = loadPrebuilt("select.vert", Type.VERTEX);
        F_SELECT = loadPrebuilt("select.frag", Type.FRAGMENT);
        F_SUN_FLARE = loadPrebuilt("sun-flare.frag", Type.FRAGMENT);
        F_SUN = loadPrebuilt("sun.frag", Type.FRAGMENT);
        V_TEXTURE = loadPrebuilt("texture.vert", Type.VERTEX);
        F_TEXTURE = loadPrebuilt("texture.frag", Type.FRAGMENT);
    }

    private int create(String program, Type type) {
        program = addImports(program);

        for (Map.Entry<String, String> entry : parametersTemp.entrySet()) {
            program = program.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
            linesAdded += numOccurances(entry.getValue(), '\n');
        }

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            program = program.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
            linesAdded += numOccurances(entry.getValue(), '\n');
        }
        
        program = addLoops(program);

        int shader = 0;

        shader = glCreateShader(type.glType);

        glShaderSource(shader, program);

        glCompileShader(shader);

        if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE) {
            String info = replaceLineNumbers(getLogInfo(shader), linesAdded);
            glDeleteShader(shader);
            Console.log(LogLevel.ERROR, "Error creating shader (" + name + "):\n" + info);
            return -1;
        } else {
            return shader;
        }
    }

    public static String getGLSLVersionString() {
        String version = glGetString(GL_SHADING_LANGUAGE_VERSION);
        if (version.indexOf(" ") != -1) {
            version = version.substring(0, version.indexOf(" ")).replaceFirst("\\.", "");
        } else {
            version = version.replaceFirst("\\.", "");
        }
        return version;
    }

    public static String getLogInfo(int obj) {
        return glGetShaderInfoLog(obj, glGetShaderi(obj, GL_INFO_LOG_LENGTH));
    }

    private static String loadTextFileFromPackage(String filename) throws IOException, URISyntaxException {
        InputStream stream = Shader.class.getResourceAsStream(filename);

        return loadTextFile(stream);
    }
}

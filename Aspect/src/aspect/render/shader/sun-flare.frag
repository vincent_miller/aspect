#version 130
#include <shapes>
#include <time>
#include <screen>

uniform sampler2D tex;
uniform vec2 sun;
uniform bool show;

in vec2 texcoord;

void main() {
	vec2 tc = texcoord;
	
	vec4 texcolor = texture(tex, tc);
	
	tc *= NormSize;
	vec2 s = sun * NormSize;

	vec2 vec = NormCenter - s;

	float l = length(vec);
	if (show) {
		if (l < 1) {
			for (int i = 2; i < 5; i++) {
				vec2 point = vec * i * 0.5 + s;
				float r = 0.05 + abs(i - 2) * 0.04;
				float d = distance(tc, point);

				float mul = min(pow(0.2 / l, 4), 0.5);

				vec3 color1 = vec3(0.5, 0.5, 0.2);
				vec3 color2 = vec3(0.6, 0.3, 0.2);
				vec3 color3 = vec3(0.6, 0.2, 0.0);

				if (i == 2) {
					texcolor += vec4(color1 * fillGlow(circlef(point, r, tc)) * mul, 1);
					texcolor += vec4(color1 * fillGlow(circlef(point, 0.8 * r, tc)) * mul, 1);
				} else if (i == 3) {
					texcolor += vec4(color2 * fillGlow(circlef(point, r, tc)) * mul, 1);
				} else if (i == 4) {
					texcolor += vec4(color3 * fillGlow(hexagonf(point, r, tc)) * mul, 1);
				}
			}
		}
	}

	gl_FragColor = texcolor;
}
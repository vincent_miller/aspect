#version 140
#include <vertex.ase>
#include <lighting.ase>
#include <matrices.ase>

out vec3 lightIntensity;
out vec2 texcoord;
out vec4 color;
out vec4 camera;
out float worldy;
out float camy;

void main() {
    vec4 world = Matrices.model * Vertex;
    worldy = world.y;
    camera = Matrices.view * world;
    camy = (Matrices.view * vec4(0, 0, 0, 1)).y;
    gl_Position = Matrices.projection * camera;
    vec3 normal = normalize(Matrices.normal * Normal);
    texcoord = TexCoord;
    color = Color;

	if (LightModel.enabled) {
	    lightIntensity = lightAmbient();
	    lightIntensity += lightTotalVector(lightApplyAttenuationVectorVector(camera, lightDiffuseVector(camera, normal)));
	    //lightIntensity += lightTotalVector(lightApplyAttenuationVectorVector(camera, lightSpecularVector(camera, normal)));
    } else {
    	lightIntensity = vec3(1.0);
    }
}
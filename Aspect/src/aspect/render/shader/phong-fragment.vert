#version 130
#include <lighting.ase>
#include <matrices.ase>
#include <vertex.ase>

uniform bool useTexture;
uniform sampler2D tex;

out vec3 normal;
out vec4 color;

out float diffuse[{maxLights}];
out float dist[{maxLights}];

out vec3 position;
out vec2 texcoord;

void main() {
    vec4 pos = Matrices.modelview * Vertex;
    gl_Position = Matrices.projection * pos;
    position = pos.xyz;
    normal = normalize(Matrices.normal * Normal);
    texcoord = TexCoord;
    color = Color;
	
	if (LightModel.enabled) {
	    dist = lightDistanceFloat(pos);
	    diffuse = lightDiffuseFloat(pos, normal);
    }
}
#version 130
#include <matrices.ase>
#include <vertex.ase>

out vec4 color;

void main() {
    gl_Position = Matrices.projection * Matrices.modelview * Vertex;
    
    color = Color;
}

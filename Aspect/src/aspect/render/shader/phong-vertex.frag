#version 130

uniform bool useTexture;
uniform sampler2D tex;

in vec2 texcoord;
in vec3 lightIntensity;
in vec4 color;

void main() {
    vec4 texColor = color;

    if (useTexture) {
        texColor *= texture2D(tex, texcoord);
    }
    
    if (texColor.a == 0.0) {
        discard;
    }
	
    gl_FragColor = texColor * vec4(lightIntensity, 1.0);
}
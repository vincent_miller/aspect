#version 130
#include <lighting.ase>
#include <matrices.ase>
#include <vertex.ase>

in int textureUnit;

out vec3 normal;
out vec3 ambient;
out vec4 color;
out vec4 position;

out float diffuse[{maxLights}];
out float texUnit;

out vec2 texcoord;

void main() { 
    position = Matrices.modelview * Vertex;
    gl_Position = Matrices.projection * position;
    normal = normalize(Matrices.normal * Normal);
    texcoord = TexCoord;
    color = Color;
    texUnit = float(textureUnit);

    ambient = lightAmbient();
	
	if (LightModel.enabled) {
	    diffuse = lightDiffuseFloat(position, normal);
    }
}
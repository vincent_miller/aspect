#version 130
#include <screen>

uniform vec2 sun;
uniform bool show;

in vec2 texcoord;

void main() {
	vec2 tc = texcoord * NormSize;
	//tc.x /= {aspectRatio};
	
	vec4 texcolor = vec4(0, 0, 0, 0);
	
	vec2 s = sun * NormSize;
	
	vec2 vec = NormCenter - s;
	
	float l = length(vec);
	if (show) {
		
		float d = distance(tc, s);
		float f = (0.15 / d) / max(pow(l, 0.5), 0.25);
		texcolor += vec4(1, 0.2 * f, 0.2 * f, 0.2 * f);
		f = min(0.003 / (abs(d - 0.02 / max(l, 0.1))), 0.5);
		texcolor += vec4(0.6 * f, 0.2 * f, 0.2 * f, 0.6 * f);
	}
	
	gl_FragColor = texcolor;
}
#version 130

uniform int value;

void main() {
	float color = value / 255.0;
    gl_FragColor = vec4(color, color, color, 1.0);
}
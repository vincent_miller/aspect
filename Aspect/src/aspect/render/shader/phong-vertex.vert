#version 140
#include <vertex.ase>
#include <lighting.ase>
#include <matrices.ase>

out vec3 lightIntensity;
out vec2 texcoord;
out vec4 color;

void main() {
    vec4 position = Matrices.modelview * Vertex;
    gl_Position = Matrices.projection * position;
    vec3 normal = normalize(Matrices.normal * Normal);
    texcoord = TexCoord;
    color = Color;

	if (LightModel.enabled) {
	    lightIntensity = lightAmbient();
	    lightIntensity += lightTotalVector(lightApplyAttenuationVectorVector(position, lightDiffuseVector(position, normal)));
	    //lightIntensity += lightTotalVector(lightApplyAttenuationVectorVector(position, lightSpecularVector(position, normal)));
    } else {
    	lightIntensity = vec3(1.0);
    }
}
#version 130
#include <lighting.ase>

uniform bool useTexture;
uniform sampler2D tex;

in vec3 normal;
in vec4 color;

in float diffuse[{maxLights}];
in float dist[{maxLights}];

in vec3 position;
in vec2 texcoord;

void main() {
    vec4 texColor = color;

    if (useTexture) {
        texColor *= texture(tex, texcoord);
    }
    
    if (texColor.a == 0.0) {
        discard;
    }
	
	vec3 lightColor = vec3(1.0);
	
	vec4 pos = vec4(position, 1.0);
	
	vec3 specColor = vec3(0.0);
	
	if (LightModel.enabled) {
	    lightColor = lightAmbient();
		lightColor += lightTotalDiffuseFloat(diffuse);
		if (!useTexture) {
			lightColor = min(lightColor, 1.0);
		}
		specColor += lightTotalSpecularFloat(lightApplyAttenuationVectorFloat(pos, lightSpecularFloat(pos, normal)));
	}
	
    gl_FragColor = texColor * vec4(lightColor, 1.0) + vec4(specColor, 0.0);
}
# Aspect Engine

## Overview

Aspect is a cross-platform game engine written in java and built on top of LWJGL. It is designed to make it easy for users to quickly and easily build hardware-accelerated 2D and 3D interactive graphical applications. For more information on getting started with Aspect, please see the tutorials on the [wiki](https://bitbucket.org/vincent_miller/aspect/wiki), or look thorugh demo projects.

## Requirements

You will need an installation of the Java 8 Development Kit to compile and run Aspect projects. You must also have a graphics card complient with OpenGL 2.0 or higher and GLSL 1.3 or higher (some features require more recent versions). In addition, I strongly recommend you download Eclipse, as a workspace is already prepared with the example projects.

## Downloading Aspect

To download Aspect, you can either clone the repository, or download the zip file. Cloning the repository is best, as it will allow you to stay up to date with the most recent version. Downloading the zip is faster, but you will have to come back and redownload it every time.

## Build and Test Projects

The first thing you need to do is build the project "Aspect." This can be done easily in Eclipse. Simply start up Eclipse, and set the repository folder as your workspace. The projects should automatically be available. Click project > build all on the menu bar, and all projects should compile without errors. Try running one of the example projects, such as Aspect Space Game, just to make sure everything is working properly.

## Create Your First Project

The simplest way to create your first project is simply to copy one of the examples. This will ensure that all imported libraries are working correctly. If you wish to start from scratch, create a new Java project. Copy the "native" and "lib" folders from the Aspect main project, and add all the jars in "lib," as well as the Aspect project, to your build path.


Note: Aspect is currently in developement. Nothing is guarrenteed to work, and updates may contain code-breaking changes.
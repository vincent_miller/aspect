/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.example;

import static aspect.resources.Resources.copyVector3;
import aspect.core.AspectRenderer.GeometryType;
import aspect.entity.Entity;
import aspect.entity.behavior.Behavior;
import aspect.physics.dynamics.Force;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.Color;
import aspect.util.Trig;
import aspect.util.Vector3;

/**
 *
 * @author millerv
 */
public class Spring implements Force {

    public Entity anchor;
    public float strength;
    public float length;
    public Vector3 anchorPointTarget;
    public Vector3 anchorPointSource;
    public static ViewModel model;

    public Spring(Entity anchor, Vector3 anchorPointTarget, Vector3 anchorPointSource, float length, float strength) {
        this.anchor = anchor;
        this.strength = strength;
        this.length = length;
        this.anchorPointTarget = anchorPointTarget;
        this.anchorPointSource = anchorPointSource;
    }

    @Override
    public Vector3 getForce(Entity ent) {
        Vector3 anchorPositionSource = anchor.transform.modelMatrix().transformPoint(anchorPointSource);
        Vector3 anchorPositionTarget = ent.transform.modelMatrix().transformPoint(anchorPointTarget);
        
        Vector3 equilibrium = anchorPositionSource.plus(anchorPositionTarget.minus(anchorPositionSource).normalize().times(length));

        Vector3 force = equilibrium.minus(anchorPositionTarget).times(strength);

        ent.rigidBody().impel(force, anchorPositionTarget.minus(ent.transform.position()));
        
        return Vector3.zero();
    }

    public static void create(Entity ent1, Entity ent2, Vector3 anchorPoint1, Vector3 anchorPoint2, float length, float strength) {
        Spring spring = null;
        Entity entity = null;
        if (ent1.rigidBody() != null) {
            spring = new Spring(ent2, anchorPoint1, anchorPoint2, length, strength);
            entity = ent1;
            ent1.rigidBody().addForce(spring);
        }
        if (ent2.rigidBody() != null) {
            spring = new Spring(ent1, anchorPoint2, anchorPoint1, length, strength);
            entity = ent2;
            ent2.rigidBody().addForce(spring);
        }
        
        final Spring s = spring;
        final Entity e = entity;

        ent1.addBehavior(new Behavior() {
            @Override
            public void render() {
                s.render(e);
            }
        });
    }
    
    public void render(Entity other) {
        Vector3 point1 = anchor.transform.modelMatrix().transformPoint(anchorPointSource);
        Vector3 point2 = other.transform.modelMatrix().transformPoint(anchorPointTarget);

        drawSpring(point1, point2, 1.0f);
    }
    
    public static void drawSpring(Vector3 point1, Vector3 point2, float radius) {
        if (model == null) {
            model = springModel(20, 400);
        }
        model.transform.reset();
        model.transform.setPosition(Vector3.divide(point1.plus(point2), 2));
        model.transform.setScale(new Vector3(radius, Vector3.distance(point1, point2), radius));
        model.transform.setUp(point1.minus(point2));
        ShaderProgram.COLOR_UNIFORM.setUniform("color", Color.RED);
        model.render();
    }

    public static MeshRenderer springModel(float coils, int segments) {
        float[] vertices = new float[segments * 3];

        float hstep = 1.0f / segments;
        float astep = Trig.FULL_CIRCLE * (coils / segments);

        for (int i = 0; i < segments; i++) {
            Vector3 vertex = new Vector3(Trig.cos(astep * i), hstep * i - 0.5f, Trig.sin(astep * i));
            copyVector3(vertex, vertices, i * 3, 1);
        }

        return new MeshRenderer(new Mesh(GeometryType.LINE_STRIP, vertices, new float[0], new float[0], new float[0]), new Material(ShaderProgram.COLOR_UNIFORM));
    }
}

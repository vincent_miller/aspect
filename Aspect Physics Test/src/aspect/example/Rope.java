/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.example;

import aspect.entity.Entity;
import aspect.physics.RigidBody;
import aspect.physics.dynamics.Force;
import aspect.util.Vector3;

/**
 *
 * @author millerv
 */
public class Rope implements Force {

    public float length;
    public Entity anchor;

    public Rope(Entity anchor, float length) {
        this.length = length;
        this.anchor = anchor;
    }

    @Override
    public Vector3 getForce(Entity ent) {
        Vector3 displacement = anchor.transform.position().minus(ent.transform.position());
        float distance = displacement.mag();
        if (distance > length) {
            float part = 1.0f;

            RigidBody rb2 = anchor.rigidBody();
            if (rb2 != null) {
                part = rb2.mass / (ent.rigidBody().mass + rb2.mass);
            }

            ent.transform.moveAbsolute(displacement.normalize().times(part * (distance - length)));
            return displacement.normalize().times(ent.rigidBody().mass * ent.rigidBody().velocity.mag2() / length);
        } else {
            return Vector3.zero();
        }
    }

    public static void create(Entity ent1, Entity ent2, float length) {
        if (ent1.rigidBody() != null) {
            ent1.rigidBody().addForce(new Rope(ent2, length));
        }
        if (ent2.rigidBody() != null) {
            ent2.rigidBody().addForce(new Rope(ent1, length));
        }
    }
}

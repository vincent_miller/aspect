/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.example;

import aspect.physics.ConvexCollider;
import aspect.physics.Collision;
import aspect.entity.behavior.Behavior;
import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;
import aspect.entity.Entity;
import aspect.event.KeyEvent;
import aspect.gui.Counter;
import aspect.gui.GUI;
import aspect.input.Input;
import aspect.physics.RigidBody;
import aspect.physics.dynamics.Force;
import aspect.physics.dynamics.UniformGravity;
import aspect.render.DrawableTexture;
import aspect.render.Light;
import aspect.render.PointLight;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import static aspect.resources.Resources.*;
import aspect.time.AbilityTimer;
import aspect.time.Time;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Debug;
import aspect.util.Random;
import aspect.util.Transform;
import aspect.util.Trig;
import aspect.util.Vector2;
import static org.lwjgl.input.Keyboard.*;
import aspect.util.Vector3;
import aspect.world.ListWorld;
import aspect.world.Octree;
import aspect.world.World;

import java.awt.Font;
import java.awt.Graphics2D;
import java.io.File;
import java.util.function.Consumer;

import org.lwjgl.input.Keyboard;

public class PhysicsDemo extends Behavior {

    // private Entity player;

    private UniformGravity gravity;

    private Counter counter;
    private Entity mostRecent = null;
    private Transform cameraParent = new Transform();

    float start = 0;

    private AbilityTimer drop = new AbilityTimer(0.5f, 0.5f);

    private World world1;
    private World world2;

    public static void main(String[] args) throws InterruptedException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        run(1600, 900, false, 0, new PhysicsDemo());
        //run(0, new PhysicsDemo());
        // setPrintFPS(true);
    }

    public void display(int index) {
        counter.setEnabled(true);
        camera.reset();

        if (index == 0) {
            mainWorld = world1;
            camera.setPosition(new Vector3(20));
            camera.yawLeft(Trig.EIGHTH_CIRCLE);
            camera.pitchUp(-0.2f);
            //camera.setParent(cameraParent);
            start = Time.frameTime();
            drop.setInterval(0.5f);
        } else {
            mainWorld = world2;
            camera.setPosition(new Vector3(0, 0, 30));
            camera.setParent(null);
            drop.setInterval(0.2f);
        }
        
        counter.setValue(mainWorld.findAll(Entity.class, (ent) -> ent.getBehavior(BlockTag.class) != null).size());
    }

    @Override
    public void onAdd() {
        world1 = new ListWorld();
        world2 = new ListWorld();

        Material platformMaterial = new Material(Color.BLUE);
        Entity floor = new Entity(box(Color.GREEN, 16.0f, 0.5f, 16.0f));
        floor.setName("floor");

        floor.addBehavior(ConvexCollider.box(16.0f, 0.5f, 16.0f));
        floor.addBehavior(new RigidBody());
        floor.rigidBody().mass = 50f;
        floor.rigidBody().setUniformMOI(floor.rigidBody().mass * 25f);
        //floor.rigidBody().freezePosition = true;
        //floor.rigidBody().freezeRotation = true;
        floor.rigidBody().positionFreezeAxis = Vector3.yAxis();

        floor.rigidBody().addForce((entity) -> {
            Vector3 impulse = entity.rigidBody().angularVelocity.negate().times(Time.deltaTime() * 100);
            entity.rigidBody().impelAngular(impulse);
            return entity.rigidBody().velocity.negate().times(Time.deltaTime() * 10000);
        });

        Entity hook1 = new Entity();
        Entity hook2 = new Entity();
        Entity hook3 = new Entity();
        Entity hook4 = new Entity();
        hook1.transform.setPosition(new Vector3(16, 5, 16));
        hook2.transform.setPosition(new Vector3(-16, 5, 16));
        hook3.transform.setPosition(new Vector3(16, 5, -16));
        hook4.transform.setPosition(new Vector3(-16, 5, -16));

        Spring.create(floor, hook1, new Vector3(8, 0, 8), Vector3.zero(), 5.0f, 0.005f);
        Spring.create(floor, hook2, new Vector3(-8, 0, 8), Vector3.zero(), 5.0f, 0.005f);
        Spring.create(floor, hook3, new Vector3(8, 0, -8), Vector3.zero(), 5.0f, 0.005f);
        Spring.create(floor, hook4, new Vector3(-8, 0, -8), Vector3.zero(), 5.0f, 0.005f);

        world1.add(floor);

        Entity spinner1 = new Entity(box(Color.ORANGE, 2.0f, 0.5f, 8.0f));
        ViewModel spinner1Axis = pipe(new Material(Color.GRAY), 0, 0, 0.125f, 16, 10);
        spinner1Axis.transform.setUp(Vector3.zAxis());
        spinner1.addBehavior(spinner1Axis);
        spinner1.addBehavior(ConvexCollider.box(2.0f, 0.5f, 8.0f));
        spinner1.addBehavior(new RigidBody());
        spinner1.rigidBody().freezePosition = true;
        spinner1.rigidBody().freezeRotation = true;
        spinner1.rigidBody().rotationFreezeAxis = Vector3.zAxis();
        spinner1.transform.setPosition(new Vector3(0, 10, 0));
        world1.add(spinner1);
        spinner1.rigidBody().addForce((entity) -> {
            Vector3 impulse = entity.rigidBody().angularVelocity.negate().times(Time.deltaTime() / 5);
            entity.rigidBody().impelAngular(impulse);
            return Vector3.zero();
        });

        Entity spinner2 = new Entity(box(Color.ORANGE, 2.0f, 0.5f, 8.0f));
        ViewModel spinner2Axis = pipe(new Material(Color.GRAY), 0, 0, 0.125f, 16, 10);
        spinner2Axis.transform.setUp(Vector3.xAxis());
        spinner2.addBehavior(spinner2Axis);
        spinner2.addBehavior(ConvexCollider.box(2.0f, 0.5f, 8.0f));
        spinner2.addBehavior(new RigidBody());
        spinner2.rigidBody().freezePosition = true;
        spinner2.rigidBody().freezeRotation = true;
        spinner2.rigidBody().rotationFreezeAxis = Vector3.xAxis();
        spinner2.rigidBody().setUniformMOI(4.0f);
        spinner2.transform.setPosition(new Vector3(0, 20, 0));
        world1.add(spinner2);
        spinner2.rigidBody().addForce((entity) -> {
            Vector3 impulse = entity.rigidBody().angularVelocity.negate().times(Time.deltaTime() / 5);
            entity.rigidBody().impelAngular(impulse);
            return Vector3.zero();
        });

        PointLight light = new PointLight(Color.WHITE);
        light.transform.setPosition(new Vector3(0, 1, 1));
        Light.ambient = new Color(0.2f);
        setLightingEnabled(true);
        light.light.directional = true;

        gravity = new UniformGravity();
        world1.addForce(gravity);

        counter = new Counter(new Vector2(20, getCanvasHeight() - 20 - GUI.font.getHeight(24)), "Blocks: ", false);
        counter.setTextColor(Color.RED);
        counter.setEnabled(false);
        GUI.add(counter);

        Polyhedra.initialize(false);

        Entity ramp1 = new Entity(box(platformMaterial, 4.0f, 4.0f, 1.0f));
        ramp1.addBehavior(ConvexCollider.box(4.0f, 4.0f, 1.0f));
        ramp1.transform.setPosition(new Vector3(2.0f, 4.0f, 0.0f));
        ramp1.transform.setForward(new Vector3(-1.0f, 1.0f, 0.0f));

        ramp1.addBehavior(new Behavior() {
            @Override
            public void update() {
                if (isKeyDown(KEY_A)) {
                    entity.transform.rotate(Vector3.yAxis(), Time.deltaTime());
                } else if (isKeyDown(KEY_D)) {
                    entity.transform.rotate(Vector3.yAxis(), -Time.deltaTime());
                }

                if (isKeyDown(KEY_W)) {
                    entity.transform.setPosition(entity.transform.position().plus(entity.transform.forward().xz().asXZ().normalize().times(Time.deltaTime() * 4.0f)));
                } else if (isKeyDown(KEY_S)) {
                    entity.transform.setPosition(entity.transform.position().plus(entity.transform.forward().xz().asXZ().normalize().times(Time.deltaTime() * -4.0f)));
                }
            }
        });

        world1.add(new Spawner(new Vector3(0, 30, 0), true));

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 5; j++) {
                Entity ent = new Entity(box(Color.random(), 0.2f, 0.2f, 4.0f));
                ent.addBehavior(ConvexCollider.box(0.2f, 0.2f, 2.0f));
                ent.transform.setPosition(new Vector3((i - 3) * 4, (j - 2) * 4));
                world2.add(ent);
            }
        }

        Entity floor2 = new Entity(rect(Color.random(), 4.0f, 13.0f));
        floor2.addBehavior(new RigidBody());
        floor2.rigidBody().collisions = false;
        floor2.rigidBody().coefficientOfFriction = 0;
        floor2.addBehavior(ConvexCollider.rect(4.0f, 13.0f));
        floor2.transform.setUp(Vector3.xAxis());
        floor2.transform.setForward(Vector3.yAxis().negate());
        floor2.transform.rotate(Vector3.zAxis(), 0.15f);
        floor2.transform.setPosition(new Vector3(8.5f, -12));
        
        Entity floor3 = new Entity(rect(Color.random(), 4.0f, 13.0f));
        floor3.addBehavior(new RigidBody());
        floor3.rigidBody().collisions = false;
        floor3.rigidBody().coefficientOfFriction = 0;
        floor3.addBehavior(ConvexCollider.rect(4.0f, 13.0f));
        floor3.transform.setUp(Vector3.xAxis());
        floor3.transform.setForward(Vector3.yAxis().negate());
        floor3.transform.rotate(Vector3.zAxis(), -0.15f);
        floor3.transform.setPosition(new Vector3(-8.5f, -12));

        world2.add(floor2);
        world2.add(floor3);

        Entity wall1 = new Entity(rect(Color.random(), 30.0f, 30.0f));
        wall1.addBehavior(ConvexCollider.rect(30.0f, 30.0f));
        wall1.transform.setPosition(new Vector3(0, 3, -2));
        world2.add(wall1);

        Entity wall2 = new Entity();
        wall2.addBehavior(ConvexCollider.rect(30.0f, 30.0f));
        wall2.transform.setPosition(new Vector3(0, 3, 2));
        world2.add(wall2);

        world2.addForce(gravity);

        world2.add(new Spawner(new Vector3(0, 15, 0), false));

        addMouseListener((evt) -> {
            if (evt.type) {
                if (evt.button == 0) {
                    Entity selected = getSelected(mainWorld, evt.x, evt.y);
                    if (selected != null) {
                        System.out.println(selected.getName());
                    }
                    mostRecent = selected;
                } else if (evt.button == 1) {
                    Entity selected = getSelected(mainWorld, evt.x, evt.y);
                    if (selected != null && selected.rigidBody() != null) {
                        System.out.println(selected.getName());
                        selected.destroy();
                        if (mostRecent == selected) {
                            mostRecent = null;
                        }
                    }
                }
            }
        });

        addKeyListener(this);
        
        display(0);
    }

    private class Spawner extends Entity {
        private Spawner(Vector3 position, boolean random) {
            addBehavior(new SpawnerBehavior(random));
            //getBehavior(SpawnerBehavior.class).entity = this;
            transform.setPosition(position);
        }
    }

    @Override
    public void update() {
        if (mostRecent != null) {
            Vector3 position = mostRecent.transform.position();
            position.y -= 5.0f;
            cameraParent.setPosition(position);
        } else {
            cameraParent.reset();
        }
        
        camera.orbit(Vector3.yAxis(), Time.deltaTime() * Input.getAxis("X", true));
        camera.moveForward(Time.deltaTime() * Input.getAxis("Y", false) * 10);
    }

    private class SpawnerBehavior extends Behavior {
        private boolean random;

        private SpawnerBehavior(boolean random) {
            addKeyListener(this);
            this.random = random;
        }

        @Override
        public void update() {
            if (mainWorld != entity.container) {
                return;
            }

            if (drop.use()) {
                Entity ent;
                if (random) {
                    ent = Polyhedra.createRandom(new Material(Color.random()));
                } else {
                    ent = RigidBody.box(new Material(Color.random()), 1.0f, 1.0f, 1.0f, 1.0f);
                    ent.rigidBody().velocity.x = Random.Float(-5, 5);
                }
                
                ent.addBehavior(new BlockTag());

                RigidBody rb = new RigidBody();
                rb.setUniformMOI(rb.mass * 0.5f);
                ent.addBehavior(rb);

                ent.transform.setPosition(entity.transform.position());
                ent.transform.setForward(Random.Vector3(1.0f));
                ent.transform.setUp(Random.Vector3(1.0f));
                ent.addBehavior(new Behavior() {
                    @Override
                    public void update() {
                        if (ent.transform.position().y < -50) {
                            ent.destroy();
                        }
                    }

                    @Override
                    public void onEntityDestroy() {
                        counter.increment(-1);
                        if (mostRecent == ent) {
                            mostRecent = null;
                        }
                    }
                });
                entity.container.add(ent);
                counter.increment();
                mostRecent = ent;
            }
        }
    }
    
    private class BlockTag extends Behavior {
        
    }

    @Override
    public void keyEvent(KeyEvent evt) {
        if (evt.type) {
            if (evt.key == KEY_LCONTROL) {
                mainWorld.forEach((entity) -> {
                    if (entity.rigidBody() != null) {
                        entity.destroy();
                    }
                });
            }
        } else if (evt.key == KEY_1) {
            display(0);
        } else if (evt.key == KEY_2) {
            display(1);
        }
    }
}

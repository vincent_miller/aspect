package aspect.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import aspect.core.AspectRenderer.GeometryType;
import aspect.core.Console.LogLevel;
import aspect.core.Console;
import aspect.entity.Entity;
import aspect.physics.ConvexCollider;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.resources.Resources;
import aspect.resources.modeling.FanTriangulator;
import aspect.util.Color;
import aspect.util.Vector3;

public class Polyhedra {
    public static final HashMap<String, Mesh> meshes = new HashMap<String, Mesh>();
    private static final HashMap<String, ConvexCollider> colliders = new HashMap<String, ConvexCollider>();
    private static String[] names;

    private Polyhedra() {
    }

    public static void initialize(boolean useEdges) {
        try {
            HashSet<String> bad = new HashSet<>();
            BufferedReader badreader = new BufferedReader(new FileReader(new File("models/bad_polyhedra.txt")));
            String s;
            while ((s = badreader.readLine()) != null) {
                s = s.trim();
                if (!s.isEmpty()) {
                    bad.add(s);
                }
            }
            badreader.close();

            BufferedReader reader = new BufferedReader(new FileReader(new File("models/polyhedra.txt")));
            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }

                String name = line;

                if (bad.contains(name)) {
                    reader.readLine();
                    reader.readLine();
                    continue;
                }

                line = reader.readLine();

                HashSet<Vector3> allEdges = new HashSet<>();
                ArrayList<Vector3> allVertices = new ArrayList<>(20);
                HashSet<Vector3> allNormals = new HashSet<>();

                String[] parts = line.split(";");

                for (String part : parts) {
                    String[] stuff = part.split(",");
                    Vector3 vertex = new Vector3(Float.parseFloat(stuff[0]), Float.parseFloat(stuff[1]), Float.parseFloat(stuff[2]));
                    allVertices.add(vertex);
                }

                line = reader.readLine();

                ArrayList<Vector3> vertices = new ArrayList<>(100);
                ArrayList<Vector3> normals = new ArrayList<>(100);

                parts = line.split(";");

                int faces = 0;
                for (String part : parts) {
                    String[] stuff = part.split(",");
                    if (stuff.length > 2) {
                        ArrayList<Vector3> face = new ArrayList<>(stuff.length);
                        for (int i = 0; i < stuff.length; i++) {
                            face.add(allVertices.get(Integer.parseInt(stuff[i])));
                        }

                        Vector3 n1 = face.get(1).minus(face.get(0));
                        Vector3 n2 = face.get(2).minus(face.get(0));
                        Vector3 normal = Vector3.cross(n1, n2).normalize();

                        if (Vector3.dot(normal, face.get(0)) < 0) {
                            // normal = normal.negate();
                        }

                        allNormals.add(normal);

                        LinkedList<Vector3> triangles = FanTriangulator.triangulate(face);
                        vertices.addAll(triangles);
                        for (int i = 0; i < triangles.size(); i++) {
                            normals.add(normal);
                        }

                        faces++;
                    }
                }

                float[] vertexData = new float[vertices.size() * 3];
                float[] normalData = new float[normals.size() * 3];

                for (int i = 0; i < vertices.size(); i++) {
                    Vector3 vertex = vertices.get(i);
                    Vector3 normal = normals.get(i);

                    Resources.copyVector3(vertex, vertexData, i * 3, 1);
                    Resources.copyVector3(normal, normalData, i * 3, 1);
                }

                Mesh mesh = new Mesh(GeometryType.TRIANGLES, vertexData, normalData, Resources.copyColorRGBA(Color.WHITE, vertices.size()), new float[vertices.size() * 3]);
                ConvexCollider collider = new ConvexCollider(allVertices.toArray(new Vector3[0]), allNormals.toArray(new Vector3[0]), allEdges.toArray(new Vector3[0]), new int[0][0]);
                meshes.put(name, mesh);
                colliders.put(name, collider);

                Console.log(LogLevel.DEBUG, name + ": " + faces);
            }
            reader.close();

            names = meshes.keySet().toArray(new String[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String[] names() {
        return names;
    }

    public static Entity create(String name, Material material) {
        MeshRenderer mesh = new MeshRenderer(meshes.get(name).copy(), material);
        ConvexCollider collider = colliders.get(name).copy();
        Entity entity = new Entity(mesh);
        entity.addBehavior(collider);
        entity.setName(name);
        return entity;
    }

    public static Entity createRandom(Material material) {
        int index = new Random().nextInt(names.length);
        return create(names[index], material);
    }
}

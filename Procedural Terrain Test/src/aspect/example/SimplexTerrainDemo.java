/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.example;

import aspect.entity.behavior.Behavior;

import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;

import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.event.KeyEvent;
import aspect.render.MultiTexturedMaterial;
import aspect.render.RenderTexture;
import aspect.render.PointLight;
import aspect.render.Light;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.Texture;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import aspect.resources.ModelImportSettings;
import aspect.resources.terrain.BufferedTerrain;
import aspect.resources.terrain.ChunkySimplexTerrain;
import aspect.resources.terrain.SimplexTerrain;
import aspect.resources.terrain.Terrain;

import static aspect.resources.Resources.*;

import aspect.time.AbilityTimer;
import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.SimplexNoise;
import aspect.util.Vector2;

import static org.lwjgl.input.Keyboard.*;

import aspect.util.Vector3;
import aspect.world.ListWorld;
import aspect.world.World;

import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class SimplexTerrainDemo extends Behavior {
    private ChunkySimplexTerrain terrain;
    private Player player;
    private Material ground;
    private AbilityTimer generate = new AbilityTimer(0.0f, 1.0f);
    
    public static void main(String[] args) {
        run(800, 600, false, 60, new SimplexTerrainDemo());
        
        //System.out.println(SimplexNoise.Generate(0, -1));
    }

    @Override
    public void onAdd() {

        player = new Player(Vector3.zero());
        player.setWalkSpeed(30.0f);
        PointLight l = new PointLight(Color.WHITE);
        Light.ambient = Color.BLACK;
        l.light.directional = true;
        l.transform.setPosition(new Vector3(3, 1, 2));
        setLightingEnabled(true);
        Light.ambient = new Color(0.2f);
        
        Texture rock = getTexture("materials/rock.jpg");
        Texture grass = getTexture("materials/grass.jpg");
        Texture ice = getTexture("materials/ice.jpg");
        ShaderProgram multitex = new ShaderProgram(getShader("shaders/multitex.vert", Shader.Type.VERTEX), getShader("shaders/multitex.frag", Shader.Type.FRAGMENT));
        ground = new MultiTexturedMaterial(multitex, rock, grass, ice);
        //ground.shader = ShaderProgram.PHONG_VERTEX;

        terrain = new ChunkySimplexTerrain(64, Vector2.zero(), Vector3.one(), ground, 8.0f);
        
        
        terrain.addLayer(0.001f, 100.0f);
        terrain.addLayer(0.01f, 10.0f);
        terrain.addLayer(0.05f, 1.0f);
        terrain.addLayer(0.5f, 0.2f);
        
        ViewModel treeModel = getModel("models/tree.obj", ModelImportSettings.DEFAULT);
        terrain.addTrees(0.002f, -50f, treeModel);
        
        terrain.rebuild();

        //fbo.postProcess(post);
        
        mainWorld.add(new Entity(terrain));
        Material sky = new Material(getTexture("materials/stars.jpg"), ShaderProgram.TEXTURE);
        sky.shader = ShaderProgram.TEXTURE;
        addSkybox(box(sky, 1.0f, 1.0f, 1.0f));
        
    }

    
    @Override
    public void update() {
        player.update();
        
        Vector3 position = player.transform.position();
        position.y = terrain.interpolateHeight(position.x, position.z) + 1.75f;
        player.transform.setPosition(position);
        
        if (generate.use()) {
            int x = (int)position.x;
            int z = (int)position.z;
            
            terrain.deleteOutsideRange(x, z, 6 * 64);
            
            for (int i = -4; i <= 4; i++) {
                for (int j = -4; j <= 4; j++) {
                    terrain.createChunk(i * 64 + x, j * 64 + z);
                }
            }
        }
    }

    @Override
    public void render() {
        terrain.render();
    }
}

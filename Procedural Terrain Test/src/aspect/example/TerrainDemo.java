/*
 * Copyright (C) 2014 MillerV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package aspect.example;

import aspect.entity.behavior.Behavior;
import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;
import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.event.KeyEvent;
import aspect.render.RenderTexture;
import aspect.render.PointLight;
import aspect.render.Light;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;
import aspect.render.ViewModel;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;
import static aspect.resources.Resources.*;
import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Vector2;
import static org.lwjgl.input.Keyboard.*;
import aspect.util.Vector3;
import aspect.world.ListWorld;
import aspect.world.World;

import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class TerrainDemo extends Behavior {

    private Entity terrain;
    private Entity water;
    private Player player;
    private ViewModel crosshair;
    private ViewModel skybox;
    private World world;
    private RenderTexture fbo;
    private ShaderProgram post;
    private float[][] t;
    private Entity box;

    private static final long seed = 2014;

    public static final Random r = new Random(seed);

    public static void main(String[] args) {
        run(800, 600, false, 60, new TerrainDemo());
    }

    @Override
    public void onAdd() {

        player = new Player(Vector3.zero());
        PointLight l = new PointLight(Color.WHITE);
        Light.ambient = Color.BLACK;
        l.light.directional = true;
        l.transform.setPosition(new Vector3(3, 1, 2));
        setLightingEnabled(true);

        int pow = 8;

        int w = (1 << pow);

        t = new float[w + 1][w + 1];

        diamondSquare(t, w);

        fbo = new RenderTexture(2048, 2048, player.transform);
        
        Shader vert = Shader.loadPrebuilt("texture.vert", Shader.Type.VERTEX);
        Shader frag = getShader("shaders/test.frag", Shader.Type.FRAGMENT);
        post = new ShaderProgram(vert, frag);

        //fbo.postProcess(post);
        
        Material ground = new Material(getTexture("materials/rock.jpg"));
        ground.shader = ShaderProgram.PHONG_VERTEX;

        terrain = new Entity(genMesh(t, ground, 32.0f, 1));
        Material wat = new Material(getTexture("materials/water.jpg"));
        wat.shader = ShaderProgram.TEXTURE;
        wat.shininess = 0.05f;
        water = new Entity(rect(wat, 8.0f, 8.0f, w, w));

        water.transform.setRotation(Vector3.xAxis(), Vector3.zAxis(), Vector3.yAxis());
        water.transform.setPosition(new Vector3(w / 2.0f, 0, w / 2.0f));
        Material sky = new Material(getTexture("materials/stars.jpg"));
        sky.shader = ShaderProgram.TEXTURE;
        skybox = box(sky, 1.0f, 1.0f, 1.0f);

        world = new ListWorld();
        box = new Entity(rect(new Material(fbo), getCanvasWidth(), getCanvasHeight()));

        for (int i = 0; i < 50; i++) {
            int x;
            int z;

            do {
                x = r.nextInt(w);
                z = r.nextInt(w);
            } while (t[x][z] < 0.0f);

            Tree tree = new Tree();
            tree.transform.setPosition(new Vector3(x, t[x][z] + tree.height / 2, z));

            world.add(tree);
        }
    }

    private float getY(float x, float z) {

        int i = (int) x;
        int j = (int) z;

        if (i < 0 || j < 0 || i >= t.length - 1 || j >= t.length - 1) {
            return 0;
        }

        float dx = x - i;
        float dz = z - j;

        Vector3 point1 = new Vector3(i, t[i][j], j);
        Vector3 point2 = new Vector3(i + 1, t[i + 1][j + 1], j + 1);
        Vector3 point3;

        if (dx > dz) {
            point3 = new Vector3(i + 1, t[i + 1][j], j);
        } else {
            point3 = new Vector3(i, t[i][j + 1], j + 1);
        }

        Vector3 normal = normal(point1, point2, point3);

        float a = normal.x;
        float b = normal.y;
        float c = normal.z;
        float d = -(a * point1.x + b * point1.y + c * point1.z);

        return -(a * x + c * z + d) / b;
    }

    private void diamondSquare(float[][] map, int w) {
        map[0][0] = var(0, 0, w);
        map[0][w] = var(0, w, w);
        map[w][0] = var(w, 0, w);
        map[w][w] = var(w, w, w);

        while (w >= 2) {
            step(map, w);
            w /= 2;
        }
    }

    private float var(int x, int z, int w) {

        float v = 0.4f * w;
        // Random r = new Random(((long)x) + (((long)z) << 32));

        return (r.nextFloat() - 0.5f) * v;
        // return 0;
    }

    private void step(float[][] map, int w) {
        for (int i = 0; i < map.length - 1; i += w) {
            for (int j = 0; j < map.length - 1; j += w) {
                diamond(map, w, i, j);

                square(map, w, i, j);
            }
        }
    }

    private void diamond(float[][] map, int w, int i, int j) {
        int x = i + w / 2;
        int y = j + w / 2;

        float mean = (map[i][j] + map[i + w][j] + map[i][j + w] + map[i + w][j + w]) / 4.0f;

        map[x][y] = mean + r.nextFloat() * var(x, y, w);
    }

    private void square(float[][] map, int w, int i, int j) {
        int x = i + w / 2;
        int y = j + w / 2;

        squareMain(map, i, y, w / 2);
        squareMain(map, x, j, w / 2);
        squareMain(map, i + w, y, w / 2);
        squareMain(map, x, j + w, w / 2);

    }

    private void squareMain(float[][] map, int x, int y, int w) {
        int pts = 0;

        float mean = 0.0f;
        if (x - w > 0) {
            mean += map[x - w][y];
            pts++;
        }

        if (y - w > 0) {
            mean += map[x][y - w];
            pts++;
        }

        if (x + w < map.length) {
            mean += map[x + w][y];
            pts++;
        }

        if (y + w < map[0].length) {
            mean += map[x][y + w];
            pts++;
        }

        mean /= pts;

        map[x][y] = mean + var(x, y, w * 2);
    }

    private Vector3 getVertex(float[][] heightmap, int i, int j, float scale) {
        return new Vector3(i * scale, heightmap[i][j], j * scale);
    }

    private Vector3 get2Normals(float[][] heightmap, int i1, int j1, int i2, int j2, float scale) {
        Vector3 v1 = getVertex(heightmap, i1, j1, scale);
        Vector3 v2 = getVertex(heightmap, i1, j2, scale);
        Vector3 v3 = getVertex(heightmap, i2, j2, scale);
        Vector3 v4 = getVertex(heightmap, i2, j1, scale);

        Vector3 normal1 = normal(v1, v2, v3);
        if (normal1.y < 0) {
            normal1 = normal1.negate();
        }

        Vector3 normal2 = normal(v3, v4, v1);
        if (normal2.y < 0) {
            normal2 = normal2.negate();
        }

        return Vector3.add(normal1, normal2);
    }

    private Vector3 getNormal(float[][] heightmap, int i, int j, float scale) {

        Vector3 normal = Vector3.zero();
        int num = 0;

        if (i > 1) {
            if (j > 1) {
                normal = normal.plus(get2Normals(heightmap, i, j, i - 1, j - 1, scale));
                num += 2;
            }

            if (j < heightmap[0].length - 1) {
                normal = normal.plus(get2Normals(heightmap, i, j, i - 1, j + 1, scale));
                num += 2;
            }
        }

        if (i < heightmap.length - 1) {
            if (j > 1) {
                normal = normal.plus(get2Normals(heightmap, i, j, i + 1, j - 1, scale));
                num += 2;
            }

            if (j < heightmap[0].length - 1) {
                normal = normal.plus(get2Normals(heightmap, i, j, i + 1, j + 1, scale));
                num += 2;
            }
        }

        return Vector3.divide(normal, num).normalize();
    }

    private MeshRenderer genMesh(float[][] heightmap, Material m, float texScale, float scale) {
        m.cull = true;
        float[] vertices = new float[(heightmap.length - 1) * (heightmap[0].length - 1) * 6 * 3];
        float[] normals = new float[vertices.length];
        float[] texcoords = new float[(heightmap.length - 1) * (heightmap[0].length - 1) * 6 * 2];

        System.out.println("Start");
        for (int i = 0; i < heightmap.length - 1; i++) {
            for (int j = 0; j < heightmap[0].length - 1; j++) {
                int index = (i * (heightmap.length - 1) + j) * 18;

                int tindex = (i * (heightmap.length - 1) + j) * 12;

                int nindex = index;

                Vector3 v1 = getVertex(heightmap, i, j, scale);
                Vector3 v2 = getVertex(heightmap, i, j + 1, scale);
                Vector3 v3 = getVertex(heightmap, i + 1, j + 1, scale);
                Vector3 v4 = getVertex(heightmap, i + 1, j, scale);

                Vector2 t1 = Vector3.divide(v1, texScale).xz();
                Vector2 t2 = Vector3.divide(v2, texScale).xz();
                Vector2 t3 = Vector3.divide(v3, texScale).xz();
                Vector2 t4 = Vector3.divide(v4, texScale).xz();

                Vector3 n1 = getNormal(heightmap, i, j, scale);
                Vector3 n2 = getNormal(heightmap, i, j + 1, scale);
                Vector3 n3 = getNormal(heightmap, i + 1, j + 1, scale);
                Vector3 n4 = getNormal(heightmap, i + 1, j, scale);

                index += copyVector3(v1, vertices, index, 1);
                index += copyVector3(v2, vertices, index, 1);
                index += copyVector3(v3, vertices, index, 1);
                index += copyVector3(v3, vertices, index, 1);
                index += copyVector3(v4, vertices, index, 1);
                index += copyVector3(v1, vertices, index, 1);

                tindex += copyVector2(t1, texcoords, tindex, 1);
                tindex += copyVector2(t2, texcoords, tindex, 1);
                tindex += copyVector2(t3, texcoords, tindex, 1);
                tindex += copyVector2(t3, texcoords, tindex, 1);
                tindex += copyVector2(t4, texcoords, tindex, 1);
                tindex += copyVector2(t1, texcoords, tindex, 1);

                nindex += copyVector3(n1, normals, nindex, 1);
                nindex += copyVector3(n2, normals, nindex, 1);
                nindex += copyVector3(n3, normals, nindex, 1);
                nindex += copyVector3(n3, normals, nindex, 1);
                nindex += copyVector3(n4, normals, nindex, 1);
                nindex += copyVector3(n1, normals, nindex, 1);
            }
        }

        return new MeshRenderer(new Mesh(GeometryType.TRIANGLES, vertices, normals, copyColorRGB(Color.WHITE, vertices.length / 3), texcoords), m);
    }

    @Override
    public void update() {
        player.update();
        
        Vector3 position = player.transform.position();
        position.y = getY(position.x, position.z) + 1.75f;
        player.transform.setPosition(position);
    }

    @Override
    public void render() {
        clearRenderer();
        //fbo.capture();
        
        skybox.transform.setPosition(player.transform.position());
        skybox.render();
        clearDepthBuffer();
        terrain.render();
        world.render();
        water.render();
        //fbo.stop();
        //fbo.render();
    }
}

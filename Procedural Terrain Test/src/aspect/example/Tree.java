/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.example;

import aspect.core.AspectRenderer;
import static aspect.core.AspectRenderer.GeometryType.QUADS;
import static aspect.core.AspectRenderer.GeometryType.QUAD_STRIP;
import aspect.entity.Entity;
import aspect.render.Material;
import aspect.render.Mesh;
import aspect.render.MeshRenderer;

import static aspect.resources.Resources.*;
import aspect.render.ViewModel;
import aspect.util.Angles;
import aspect.util.Color;
import aspect.util.Matrix4x4;
import aspect.util.Trig;
import aspect.util.Vector3;

import java.io.File;
import java.util.Random;

/**
 *
 * @author MillerV
 */
public class Tree extends Entity {

    public final float height;
    
    public static final int LOD = 6;

    public Tree() {
        Random r = new Random();
        height = 6.0f + r.nextFloat() * 1.0f;
        Material m = new Material(getTexture("materials/bark.jpg"));
        int numBranches = 20 + r.nextInt(10);
        
        float[] vertices = new float[(numBranches + 1) * vertexDataPipe(LOD)];
        float[] normals = new float[(numBranches + 1) * vertexDataPipe(LOD)];
        float[] texCoords = new float[(numBranches + 1) * texCoordDataPipe(LOD)];
        
        int vpos = 0;
        int tpos = 0;
        int npos = 0;
        
        vpos += pipeVertices(0.2f, height, LOD, vertices, vpos);
        tpos += pipeTexCoords(1, 1, LOD, texCoords, tpos);
        npos += pipeNormals(LOD, normals, npos);
        
        for (int i = 0; i < numBranches; i++) {
            float branchHeight = height - r.nextFloat() * 4.0f;
            float branchLength = (height - branchHeight) / 2.0f;
            float branchWidth = branchLength * 0.1f;
            float branchYaw = r.nextFloat() * 360.0f;
            float branchPitch =  75;
            
            int nv = pipeVertices(branchWidth, branchLength, LOD, vertices, vpos);
            int nt = pipeTexCoords(1, 1, LOD, texCoords, tpos);
            int nn = pipeNormals(LOD, normals, npos);
            
            Vector3 pos = new Vector3(Trig.sin(branchYaw), -Trig.cos(branchPitch), Trig.cos(branchYaw));
            pos = pos.times(branchLength / 2.0f); 
            pos.y += branchHeight - height / 2;
            
            Angles ang = Angles.zero();
            ang.pitch = 75;
            ang.yaw = branchYaw;
            
            Matrix4x4 transform = Matrix4x4.TRS(pos, ang, Vector3.one());
            transformPoints(vertices, transform, vpos, vertexDataPipe(LOD));
            transformVectors(normals, transform, npos, vertexDataPipe(LOD));
            
            vpos += nv;
            tpos += nt;
            npos += nn;
        }
        
        addBehavior(new MeshRenderer(new Mesh(QUADS, vertices, normals, copyColorRGB(Color.WHITE, vertices.length / 3), texCoords), m));
    }
}

#version 130
#include <lighting.ase>

uniform bool useTexture;
uniform sampler2D textures[3];

in vec3 normal;
in vec3 ambient;
in vec4 color;
in vec4 position;

in float diffuse[{maxLights}];
in float texvalues[3];

in vec2 texcoord;

void main() {
	vec4 texColor = color;
	
	float f0 = min(max(texvalues[0], 0.0), 1.0);
	float f1 = min(max(texvalues[1], 0.0), 1.0);
	float f2 = min(max(texvalues[2], 0.0), 1.0);

    if (useTexture) {
        texColor = texColor * (
        (texture2D(textures[0], texcoord) * f0) + 
        (texture2D(textures[1], texcoord) * f1) +
        (texture2D(textures[2], texcoord) * f2)
        );
    }
    
    if (texColor.a == 0) {
        discard;
    }
    	
	
	vec3 lightColor = vec3(1.0);
	
	if (LightModel.enabled) {
	    lightColor = ambient;
		lightColor += lightTotalDiffuseFloat(lightApplyAttenuationVectorFloat(position, diffuse));
		if (!useTexture) {
			lightColor = min(lightColor, 1.0);
		}
		//lightColor += lightTotalSpecularFloat(lightApplyAttenuationVectorFloat(position, lightSpecularFloat(position, normal)));
	}

    gl_FragColor = texColor * vec4(lightColor, 1.0);
}
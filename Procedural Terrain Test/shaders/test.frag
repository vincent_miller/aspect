#include <time>
#include <simplex>

in vec2 texcoord;

void main() {
    float noise = simplex_Generate(texcoord.x * 10, texcoord.y * 10, Time.elapsed);
    if (noise > 0) {
    	gl_FragColor = vec4(1, 0, 0, 1);
	} else {
		gl_FragColor = vec4(0, 1, 0, 1);
	}
}
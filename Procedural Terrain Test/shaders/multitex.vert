#version 130
#include <lighting.ase>
#include <matrices.ase>
#include <vertex.ase>

out vec3 normal;
out vec3 ambient;
out vec4 color;
out vec4 position;

out float diffuse[{maxLights}];
out float texvalues[3];

out vec2 texcoord;

void main() {
    position = Matrices.modelview * Vertex;
    gl_Position = Matrices.projection * position;
    normal = normalize(Matrices.normal * Normal);
    texcoord = TexCoord;
    color = Color;
    
    if (Vertex.y < 20) {
	    float f = (Vertex.y + 10.0) / 20.0;
	    texvalues[0] = f;
	    texvalues[1] = 1.0 - f;
	    texvalues[2] = 0.0;
    } else {
    	float f = (Vertex.y - 20.0) / 40.0;
	    texvalues[0] = 1.0 - f;
	    texvalues[1] = 0.0;
	    texvalues[2] = f;
	}

    ambient = lightAmbient();
	
	if (LightModel.enabled) {
	    diffuse = lightDiffuseFloat(position, normal);
    }
}
package aspect.editor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import aspect.render.Light;
import aspect.render.PointLight;
import aspect.util.AspectInputStream;
import aspect.util.AspectOutputStream;
import aspect.world.World;

public class MapIO {
    
    public static void bake(OutputStream out, World world) throws IOException {
        
        JSONObject obj = new JSONObject();
        JSONArray ents = new JSONArray();
        obj.put("entities", ents);
        
        JSONArray geometry = new JSONArray();
        obj.put("geometry", geometry);
        int[] offs = new int[3];
        
        world.forEach((entity) -> {
            
            if (entity instanceof EditorBlock) {
                JSONObject mesh = new JSONObject();
                geometry.add(mesh);
                StringBuilder geom = new StringBuilder();
                EditorBlock blk = (EditorBlock) entity;
                blk.toObjString(geom, offs);
                mesh.put("mesh", geom.toString());
            } else {
                JSONObject ent = new JSONObject();
                
            }
            
        });
        
        
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
        writer.write(obj.toJSONString());
        writer.close();
    }
    
    
    public static void save(OutputStream out, World world) throws IOException {
        AspectOutputStream stream = new AspectOutputStream(out);
        stream.writeInt(world.count());

        try {
            world.forEach((entity) -> {
                try {
                    if (!(entity instanceof EditorComponent)) {
                        System.out.println("Entity " + entity + " not of type EditorComponent.");
                    } else {
                        EditorComponent cmpnt = (EditorComponent) entity;

                        stream.writeUTF(cmpnt.getClass().getName());
                        cmpnt.write(stream);
                    }
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            });
        } finally {
            stream.close();
        }
    }

    public static void load(InputStream open, World world) throws IOException {
        world.clear();
        Light.clear();
        AspectInputStream stream = new AspectInputStream(open);
        int count = stream.readInt();

        try {
            for (int i = 0; i < count; i++) {
                try {
                    String className = stream.readUTF();
                    Class<?> cls = Class.forName(className);
                    if (EditorComponent.class.isAssignableFrom(cls)) {
                        EditorComponent cmpnt = (EditorComponent) cls.getConstructor(AspectInputStream.class).newInstance(stream);
                        world.add(cmpnt);
                    } else {
                        throw new IOException("Entity class must extend EditorComponent");
                    }

                } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException ex) {
                    throw new IOException(ex);
                }
            }
        } finally {
            stream.close();
        }
    }
}

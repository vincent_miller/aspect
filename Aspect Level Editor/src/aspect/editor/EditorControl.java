/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.editor;

import static aspect.core.AspectLauncher.*;
import static aspect.core.AspectRenderer.*;

import aspect.core.AspectLauncher;
import aspect.core.AspectRenderer;
//import aspect.editor.LevelEditor.Tool;
import aspect.entity.Entity;
import aspect.entity.Player;
import aspect.entity.behavior.Behavior;
import aspect.event.KeyEvent;
import aspect.event.MouseEvent;
import aspect.render.PointLight;
import aspect.render.Light;
import aspect.render.Material;
import aspect.render.shader.Shader;
import aspect.render.shader.Shader;
import aspect.render.shader.ShaderProgram;

import static aspect.resources.Resources.*;
import static org.lwjgl.opengl.GL11.GL_COLOR_CLEAR_VALUE;
import static org.lwjgl.opengl.GL11.GL_LINE_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_POLYGON_OFFSET_FILL;
import static org.lwjgl.opengl.GL11.GL_POLYGON_OFFSET_LINE;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetFloat;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glPolygonOffset;

import aspect.time.Time;
import aspect.util.Angles;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Debug;
import aspect.util.Transform;
import aspect.util.Vector2;
import aspect.util.Vector3;
import aspect.world.ListWorld;
import aspect.world.World;

import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.swing.JList;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author vincent
 */
public class EditorControl extends Behavior {

    public boolean update;

    public final BoundingBox builder = BoundingBox.cube(0.0f);
    public ListWorld world;
    public static Material blockMaterial;

    private Transform camera;
    private Angles angles;
    
    private InputStream open;

    public EditorControl() {
        this.camera = new Transform();
        AspectRenderer.camera = this.camera;
        angles = Angles.zero();
    }

    @Override
    public void onAdd() {
        world = new ListWorld();
        blockMaterial = new Material(getTexture("textures/tex.jpg"));
        blockMaterial.setTextureFilter(Material.Filter.MIPMAP, Material.Filter.LINEAR);
        blockMaterial.shader = ShaderProgram.PHONG_FRAGMENT;
        blockMaterial.shininess = 1.0f;

        Entity cube = new EditorBlock(blockMaterial, new BoundingBox(new Vector3(-5, -4, -10), new Vector3(5, -2, -5)));
        world.add(cube);
        cube.setName("Cube 1");

        world.add(new EditorLight(Vector3.zero()));

        // setAmbientLight(Color.BLACK);

        updateList();
        addMouseListener(this);
        addKeyListener(this);

        // MaterialSelector selector = editor.getMaterialSelector();

    }
    
    private void loadResource(String name) {
        String ext = name.substring(name.lastIndexOf('.'));
        if (LevelEditor.loaders.containsKey(ext)) {
            LevelEditor.loaders.get(ext).accept(name);
        }
    }
    

    @Override
    public void update() {

        if (Mouse.isButtonDown(1)) {
            if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
                camera.moveUp(Time.deltaTime() * Mouse.getDY());
                camera.moveRight(Time.deltaTime() * Mouse.getDX());
            } else {
                angles.yaw += Time.deltaTime() * Mouse.getDX() / 4.0f;
                angles.pitch += Time.deltaTime() * Mouse.getDY() / 4.0f;
                camera.setEulerAngles(angles);
            }
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            camera.moveForward(6.0f * Time.deltaTime());
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            camera.moveForward(-6.0f * Time.deltaTime());
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            camera.moveRight(-6.0f * Time.deltaTime());
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            camera.moveRight(6.0f * Time.deltaTime());
        }

        world.update();
        
        if (open != null) {
            try {
                MapIO.load(open, world);
                updateList();
            } catch (IOException e) {
                e.printStackTrace();
            }
            open = null;
        }
        
        String name;
        while ((name = LevelEditor.load.poll()) != null) {
            loadResource(name);
        }
    }

    @Override
    public void mouseEvent(MouseEvent evt) {
        if (evt.wheel != 0) {
            camera.moveForward(evt.wheel / 120f);
        }

        if (evt.button == 0 && evt.type) {
            clearRenderer();
            EditorComponent.selectionMode = true;
            world.render();
            FloatBuffer buff = BufferUtils.createFloatBuffer(3);
            GL11.glReadPixels(evt.x, evt.y, 1, 1, GL11.GL_RGB, GL11.GL_FLOAT, buff);
            Color c = new Color(buff.get(), buff.get(), buff.get());
            EditorComponent block = EditorComponent.select(c);
            if (block != null) {
                System.out.println(block);
                LevelEditor.lstItems.setSelectedValue((block), false);
            } else {
                LevelEditor.lstItems.setSelectedIndices(new int[0]);
            }
            EditorComponent.selectionMode = false;
        }
    }

    @Override
    public void keyEvent(KeyEvent evt) {
        if (evt.key == Keyboard.KEY_RETURN && evt.type && builder.dimensions().mag2() != 0.0f) {
            EditorComponent ent = new EditorBlock(blockMaterial, builder.copy());
            addToWorld(ent);
            builder.max = Vector3.zero();
            builder.min = Vector3.zero();

            LevelEditor.lstItems.setSelectedValue(ent, false);
        } else if (evt.key == Keyboard.KEY_DELETE && evt.type) {
            Entity value = LevelEditor.lstItems.getSelectedValue();
            if (value != null) {
                world.remove(value);
                updateList();
            }
        }
    }

    public void save(OutputStream stream) {
        try {
            MapIO.save(stream, world);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void compile(OutputStream out) {
        try {
            MapIO.bake(out, world);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void open(InputStream stream) {
        open = stream;
    }
    
    public void clear() {
        world.clear();
        Light.clear();
        updateList();
    }

    private void updateList() {
        ArrayList<EditorComponent> lst = new ArrayList<>(50);
        world.forEach((entity) -> {
            lst.add((EditorComponent) entity);
        });
        LevelEditor.lstItems.setListData(lst.toArray(new EditorComponent[0]));
    }

    public void setup2D() {
        LevelEditor.viewXY.setupDraw();
        LevelEditor.viewXZ.setupDraw();
        LevelEditor.viewZY.setupDraw();
    }

    public void drawBounds(BoundingBox bounds, Color color) {
        LevelEditor.viewXY.drawBounds(bounds, color);
        LevelEditor.viewXZ.drawBounds(bounds, color);
        LevelEditor.viewZY.drawBounds(bounds, color);

        Debug.showBounds(bounds, Color.BLUE);
    }
    
    public void addToWorld(EditorComponent ent) {

        world.add(ent);
        ent.setName(ent.getClass().getSimpleName());

        updateList();
    }

    public void paint2D() {
        LevelEditor.viewXY.repaint();
        LevelEditor.viewXZ.repaint();
        LevelEditor.viewZY.repaint();
    }

    @Override
    public void render() {

        setLightingEnabled(LevelEditor.ViewMenu.lighting.isSelected());
        setTexturesEnabled(LevelEditor.ViewMenu.textures.isSelected());
        setWireframe(LevelEditor.ViewMenu.wireframe.isSelected());

        EditorBlock.selectionMode = false;
        setup2D();

        world.render();

        if (builder.dimensions().mag2() > 0.0f) {
            drawBounds(builder, Color.BLUE);
        }

        paint2D();
        

        EditorComponent selected = LevelEditor.lstItems.getSelectedValue();

        if (selected != null) {
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            
            ShaderProgram.lock(ShaderProgram.COLOR_UNIFORM);
            ShaderProgram.COLOR_UNIFORM.setUniform("color", Color.RED);
            boolean w = wireframe();
            setWireframe(true);
            
            selected.render();
            
            ShaderProgram.unlock();
            setWireframe(w);
            
            GL11.glEnable(GL11.GL_DEPTH_TEST);
        }
    }
}

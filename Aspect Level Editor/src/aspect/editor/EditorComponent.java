/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aspect.editor;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import aspect.entity.Entity;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.util.AspectInputStream;
import aspect.util.AspectOutputStream;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Debug;
import static org.lwjgl.opengl.GL11.*;
import static aspect.core.AspectRenderer.*;

/**
 *
 * @author MillerV
 */
public class EditorComponent extends Entity {
    public BoundingBox bounds = BoundingBox.cube(0.0f);
    public Color color = Color.WHITE;
    public ViewModel model;

    private static final Map<Color, EditorComponent> colors = new HashMap<>();
    public static boolean selectionMode = false;

    public boolean valid = false;
    
    public EditorComponent() {
        Color c;
        do {
            c = Color.random(false);
        } while (colors.containsKey(c));

        this.color = c;
        colors.put(c, this);
    }
    
    public EditorComponent(AspectInputStream stream) {
        throw new RuntimeException("Must override read constructor");
    }

    public void revalidate() {
        if (!valid) {
            bounds.revalidate();
            valid = true;
        }
    }
    
    public boolean resizeable() {
    	return false;
    }

    @Override
    public void render() {
        super.render();

        revalidate();

        if (model == null) {
            return;
        }
        
        boolean b = wireframe();

        if (selectionMode) {
        	ShaderProgram.COLOR_UNIFORM.setUniform("color", color);
            ShaderProgram.lock(ShaderProgram.COLOR_UNIFORM);
            setWireframe(false);
        }

        model.transform.setParent(transform);
        model.render();
        
        if (selectionMode) {
            ShaderProgram.unlock();
            setWireframe(b);
        }
        
        render2D(LevelEditor.viewXY);
        render2D(LevelEditor.viewXZ);
        render2D(LevelEditor.viewZY);
    }

    public void render2D(View2D view) {
        view.drawBounds(bounds, color);

        if (LevelEditor.lstItems.getSelectedValue() == this) {
            view.drawDragPoints(bounds, color, resizeable());
        }
    }

    public static EditorComponent select(Color c) {
        return colors.get(c);
    }
    
    public void write(AspectOutputStream stream) throws IOException {
        throw new RuntimeException("Must override write method");
    }
    
    public void write(BufferedWriter writer) throws IOException {
        throw new RuntimeException("Must override write method");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.editor;

import aspect.entity.Entity;
import aspect.render.Material;
import aspect.render.ViewModel;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.resources.modeling.Face;
import aspect.resources.modeling.Solid;
import aspect.resources.modeling.Vertex;

import static aspect.resources.Resources.*;
import static aspect.core.AspectRenderer.*;
import aspect.util.AspectInputStream;
import aspect.util.AspectOutputStream;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Stream;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author MillerV
 */
public class EditorBlock extends EditorComponent {

    public Material material;
    public Solid solid;
    public Vector3[] corners;

    public EditorBlock(Material material, BoundingBox bounds) {
        this.material = material;
        this.bounds = bounds;

        try {
            revalidate();
        } catch (Exception ex) {
            valid = false;
        }
        

        corners = new Vector3[8];
        for (int i = 0; i < corners.length; i++) {
            corners[i] = Vector3.zero();
        }
        
        updateCornersFromBounds();
        
        Face f1 = getFace(0, 1, 2, 3);
        Face f2 = getFace(4, 5, 6, 7);
        Face f3 = getFace(0, 3, 6, 5);
        Face f4 = getFace(4, 7, 2, 1);
        Face f5 = getFace(0, 5, 4, 1);
        Face f6 = getFace(3, 2, 7, 6);
        
        solid = new Solid(f1, f2, f3, f4, f5, f6);
    }
    
    private Face getFace(int i1, int i2, int i3, int i4) {
        return new Face(new Vertex(corners[i1]), new Vertex(corners[i2]), new Vertex(corners[i3]), new Vertex(corners[i4]));
    }
    
    private void updateCornersFromBounds() {
        Vector3 min = bounds.min;
        Vector3 max = bounds.max;
        
        corners[0].set(min.x, min.y, min.z);
        corners[1].set(min.x, min.y, max.z);
        corners[2].set(min.x, max.y, max.z);
        corners[3].set(min.x, max.y, min.z);
        
        corners[4].set(max.x, min.y, max.z);
        corners[5].set(max.x, min.y, min.z);
        corners[6].set(max.x, max.y, min.z);
        corners[7].set(max.x, max.y, max.z);
    }
    
    public EditorBlock(AspectInputStream stream) throws IOException {
        this(EditorControl.blockMaterial, stream.readBoundingBox());
    }

    @Override
    public boolean resizeable() {
        return true;
    }

    public void revalidate() {
        if (!valid) {
            super.revalidate();
            
            /*transform.setPosition(bounds.center());
            Vector3 size = bounds.dimensions();
            model = box(material, size.x, size.y, size.z, size.x, size.y, size.z);*/
            updateCornersFromBounds();
            solid.calculateNormals();
            solid.calculateTexcoords();
            model = solid.buildMesh(material);
            valid = true;
        }
    }
    
    @Override
    public void write(AspectOutputStream stream) throws IOException {
        stream.writeBoundingBox(bounds);
    }
    
    public void toObjString(StringBuilder builder, int[] offs) {
        solid.toObjString(builder, offs);
    }
}

package aspect.editor;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.LinkedList;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import aspect.resources.ArcFile;
import aspect.resources.ArcFile.ArcEntry;

public class ArcTree extends JTree {
    public final DefaultTreeModel model;
    public final DefaultMutableTreeNode root;

    public DefaultMutableTreeNode dragging;

    private ArcFile archive;

    public ArcTree(JPopupMenu mnContext) {
        root = makeNode("[No Archive]", true);
        model = new DefaultTreeModel(root);
        setModel(model);
        setCellRenderer(new NodeRenderer());
        setEditable(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setShowsRootHandles(true);

        MouseAdapter ad = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                int row = getRowForLocation(evt.getX(), evt.getY());
                if (row != -1) {
                    setSelectionRow(row);
                } else {
                    clearSelection();
                }
                if (evt.getClickCount() == 1) {
                    if (SwingUtilities.isRightMouseButton(evt)) {
                        if (row != -1) {
                            mnContext.show(evt.getComponent(), evt.getX(), evt.getY());
                        }
                    } else if (SwingUtilities.isLeftMouseButton(evt)) {
                        if (row != -1) {
                            dragging = getSelectedNode();
                        }
                    }
                } else if (evt.getClickCount() == 2) {
                    if (row != -1) {
                        DefaultMutableTreeNode node = getSelectedNode();
                        if (getInfo(node).name.endsWith(".map")) {
                            LevelEditor.FileMenu.openMap(getPath(node));
                        }
                    }
                }
            }

            @Override
            public void mouseDragged(MouseEvent evt) {
                if (SwingUtilities.isLeftMouseButton(evt)) {
                    if (dragging != null) {
                        int row = getRowForLocation(evt.getX(), evt.getY());
                        if (row != -1) {
                            setSelectionRow(row);
                        } else {
                            clearSelection();
                        }
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                if (SwingUtilities.isLeftMouseButton(evt)) {
                    if (dragging != null) {
                        int row = getRowForLocation(evt.getX(), evt.getY());
                        if (row != -1) {
                            setSelectionRow(row);
                            DefaultMutableTreeNode node = getSelectedNode();
                            if (node != dragging) {
                                if (getInfo(node).isFolder) {
                                    move(dragging, node);
                                } else {
                                    if (node.getParent() != dragging) {
                                        move(dragging, (DefaultMutableTreeNode) node.getParent());
                                    }
                                }
                            }
                        } else {
                            clearSelection();
                        }
                    }

                    dragging = null;
                }
            }
        };
        addMouseListener(ad);
        addMouseMotionListener(ad);
    }

    public void setArchive(ArcFile file) {
        archive = file;
        root.removeAllChildren();
        root.setUserObject(new NodeInfo(file.getName(), true));

        for (ArcEntry entry : file) {
            LinkedList<String> list = new LinkedList<>(Arrays.asList(entry.getName().split("/")));
            addFileToTree(list, "", root);
        }

        model.reload();
    }

    private void addFileToTree(LinkedList<String> parts, String path, DefaultMutableTreeNode parent) {
        String name = parts.removeFirst();
        path += name;
        if (parts.isEmpty()) {
            parent.add(makeNode(name, false));
        } else {
            path += "/";
            for (int i = 0; i < parent.getChildCount(); i++) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent.getChildAt(i);
                NodeInfo info = getInfo(node);
                if (info.name.equals(name) && info.isFolder) {
                    addFileToTree(parts, path, node);
                    return;
                }
            }

            DefaultMutableTreeNode newNode = makeNode(name, true);
            parent.add(newNode);
            addFileToTree(parts, path, newNode);
        }
    }

    private void addNode(DefaultMutableTreeNode newNode, DefaultMutableTreeNode parent) {
        model.insertNodeInto(newNode, parent, parent.getChildCount());
    }

    public void addFolder(String name, DefaultMutableTreeNode parent) {
        addNode(makeNode(name, true), parent);
    }

    public void addEmptyFile(String name, DefaultMutableTreeNode parent) {
        addNode(makeNode(name, false), parent);
    }

    public void addFile(File file, DefaultMutableTreeNode parent) {
        String path = getPath(parent);
        try {
            if (parent.isRoot()) {
                archive.addFile(file, "");
            } else {
                archive.addFile(file, path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        addNode(makeNode(file.getName(), false), parent);
    }

    public void delete(DefaultMutableTreeNode node) {
        try {
            archive.deleteFile(getPath(node));
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.removeNodeFromParent(node);
    }

    public void move(DefaultMutableTreeNode node, DefaultMutableTreeNode newParent) {
        NodeInfo info = getInfo(node);
        String oldpath = getPath(node);
        String newpath = getPath(newParent) + info.name;
        try {
            archive.renameFile(oldpath, newpath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        model.removeNodeFromParent(node);
        addNode(node, newParent);
        node.setUserObject(new NodeInfo(info.name, info.isFolder));
    }

    public void rename(DefaultMutableTreeNode node, String name) {
        String path = getPath(node);
        String newpath = name;
        int i = path.lastIndexOf('/');
        if (i != -1) {
            newpath = path.substring(0, i + 1) + name;
        }

        try {
            archive.renameFile(path, newpath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        node.setUserObject(new NodeInfo(name, getInfo(node).isFolder));
    }

    public DefaultMutableTreeNode getSelectedNode() {
        return (DefaultMutableTreeNode) getLastSelectedPathComponent();
    }

    public String getPath(DefaultMutableTreeNode node) {
        if (node == root) {
            return "";
        }

        Object[] nodes = node.getUserObjectPath();
        String path = "";
        for (int i = 1; i < nodes.length - 1; i++) {
            path += nodes[i] + "/";
        }

        path += getInfo(node).name;

        if (getInfo(node).isFolder) {
            path += "/";
        }

        return path;
    }

    private NodeInfo getInfo(DefaultMutableTreeNode node) {
        return (NodeInfo) node.getUserObject();
    }

    private DefaultMutableTreeNode makeNode(String name, boolean isFolder) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(new NodeInfo(name, isFolder));
        node.setAllowsChildren(isFolder);
        return node;
    }

    private class NodeInfo {
        private String name;
        private boolean isFolder;

        private NodeInfo(String name, boolean isFolder) {

            this.name = name;
            this.isFolder = isFolder;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private class NodeRenderer extends DefaultTreeCellRenderer {
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            if (value instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
                
                if (getInfo(node).isFolder) {
                    setIcon(getOpenIcon());
                } else {
                    setIcon(getIcon(getInfo(node).name));
                }
            }

            return this;
        }

        private Icon getIcon(String name) {
            int i = name.lastIndexOf('.');
            if (i != -1) {
                String ext = name.substring(i);
                try {
                    File tmp = File.createTempFile("icon", ext);
                    FileSystemView view = FileSystemView.getFileSystemView();
                    return view.getSystemIcon(tmp);
                } catch (IOException e) {
                    return getLeafIcon();
                }
            } else {
                return getLeafIcon();
            }
        }
    }
}

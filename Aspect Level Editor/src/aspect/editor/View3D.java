/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.editor;

import aspect.core.AspectLauncher;
import static aspect.core.AspectRenderer.onDisplayResize;
import aspect.entity.Player;
import aspect.entity.behavior.Behavior;
import aspect.util.Vector3;
import java.awt.Canvas;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static java.lang.Thread.yield;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.JPanel;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

/**
 *
 * @author vincent
 */
public class View3D extends Canvas {

    private final Behavior control;

    public View3D(EditorControl control) {
        setMinimumSize(new Dimension(10, 10));
        this.control = control;
        start3D();
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent evt) {
                onDisplayResize();
            }
        });
    }

    private void start3D() {
        new Thread(() -> {
            while (!isDisplayable()) {
                yield();
            }
            AspectLauncher.run(View3D.this, 60, control);
        }).start();

    }
}

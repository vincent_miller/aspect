/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspect.editor;

//import aspect.editor.LevelEditor.Tool;
import aspect.editor.LevelEditor.Tool;
import aspect.resources.Resources;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Vector2;
import aspect.util.Vector3;

import java.awt.BasicStroke;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import org.lwjgl.opengl.Display;

/**
 *
 * @author millerv
 */
public class View2D extends javax.swing.JPanel {

    private BufferedImage image;
    private Graphics2D pen;
    public Axes axes;
    public EditorControl control;

    private DragPoint currentPoint;
    private Point lastMouseClick;
    private Point lastMouseDrag;

    public static float gridSize = 1.0f;
    private int pixelsPerUnit = 16;
    private Point viewPos = new Point(0, 0);

    /**
     * Creates new form View2D
     */
    public View2D(Axes axes, EditorControl control) {
        initComponents();
        this.axes = axes;
        this.control = control;
        setMinimumSize(new Dimension(10, 10));
        MouseAdapter mouse = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                requestFocusInWindow();
                if (LevelEditor.currentTool == Tool.BLOCK) {
                    BoundingBox builder = control.builder;
                    builder.min = snap(getPointInWorld(evt.getPoint()));
                    builder.max = builder.min.copy();
                    EditorComponent cmpnt = LevelEditor.lstItems.getSelectedValue();
                    if (cmpnt != null) {
                        builder.min = axes.copyUnknown(cmpnt.bounds.min, builder.min);
                        builder.max = axes.copyUnknown(cmpnt.bounds.max, builder.max);
                    }

                } else if (LevelEditor.currentTool == Tool.ENTITY) {
                    LevelEditor.control.addToWorld(new EditorLight(getPointInWorld(evt.getPoint())));
                } else {
                    currentPoint = getDragPoint(evt.getPoint());
                }
                lastMouseClick = evt.getPoint();
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent evt) {
                double d = Math.pow(2, -evt.getWheelRotation());
                pixelsPerUnit = (int) (pixelsPerUnit * d);
                viewPos = new Point((int) (viewPos.x * d), (int) (viewPos.y * d));

                pixelsPerUnit = Math.max(pixelsPerUnit, 1);
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                if (LevelEditor.currentTool == Tool.BLOCK) {
                    control.builder.revalidate();
                } else if (currentPoint != null) {
                    EditorComponent cmpnt = LevelEditor.lstItems.getSelectedValue();
                    if (cmpnt.resizeable()) {
                        cmpnt.bounds.min = snap(cmpnt.bounds.min);
                        cmpnt.bounds.max = snap(cmpnt.bounds.max);
                    } else {
                        cmpnt.bounds.setCenter(snap(cmpnt.bounds.center()));
                    }
                    cmpnt.valid = false;
                    currentPoint = null;
                }
                lastMouseDrag = null;
            }

            @Override
            public void mouseMoved(MouseEvent evt) {
                if (LevelEditor.currentTool == Tool.EDIT) {
                    if (LevelEditor.lstItems.getSelectedValue() != null) {
                        DragPoint point = getDragPoint(evt.getPoint());
                        if (point != null) {
                            setCursor(Cursor.getPredefinedCursor(point.getCursorType()));
                        } else {
                            setCursor(Cursor.getDefaultCursor());
                        }
                    } else {
                        setCursor(Cursor.getDefaultCursor());
                    }
                }
            }

            @Override
            public void mouseDragged(MouseEvent evt) {
                EditorComponent cmpnt = LevelEditor.lstItems.getSelectedValue();
                if (LevelEditor.currentTool == Tool.BLOCK) {
                    BoundingBox builder = control.builder;

                    builder.max = snap(getPointInWorld(evt.getPoint()));
                    if (cmpnt != null) {
                        builder.max = axes.copyUnknown(cmpnt.bounds.max, builder.max);
                    }
                } else if (currentPoint != null) {
                    int xmin = (evt.getX() - lastMouseClick.x) * currentPoint.xmin;
                    int ymin = (evt.getY() - lastMouseClick.y) * currentPoint.ymin;
                    int xmax = (evt.getX() - lastMouseClick.x) * currentPoint.xmax;
                    int ymax = (evt.getY() - lastMouseClick.y) * currentPoint.ymax;
                    Dimension minAdd = new Dimension(xmin, ymin);
                    Dimension maxAdd = new Dimension(xmax, ymax);
                    cmpnt.bounds.min = cmpnt.bounds.min.plus(getSizeInWorld(minAdd));
                    cmpnt.bounds.max = cmpnt.bounds.max.plus(getSizeInWorld(maxAdd));

                    if (minAdd.width != 0 || maxAdd.width != 0) {
                        lastMouseClick.x = evt.getX();
                    }

                    if (minAdd.height != 0 || maxAdd.height != 0) {
                        lastMouseClick.y = evt.getY();
                    }

                    cmpnt.valid = false;
                } else {
                    if (lastMouseDrag != null) {
                        viewPos = new Point(viewPos.x + evt.getX() - lastMouseDrag.x, viewPos.y + evt.getY() - lastMouseDrag.y);
                    }

                }
                lastMouseDrag = evt.getPoint();
            }
        };

        addMouseListener(mouse);
        addMouseWheelListener(mouse);
        addMouseMotionListener(mouse);
    }

    private DragPoint getDragPoint(Point point) {
        EditorComponent block = LevelEditor.lstItems.getSelectedValue();
        if (block == null) {
            return null;
        }

        Point center = getPointInView(block.bounds.center());
        Dimension size = getSizeInView(block.bounds.dimensions());

        if (block.resizeable()) {
            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    Point dragPoint = new Point(center.x + x * size.width / 2, center.y + y * size.height / 2);
                    if (point.distance(dragPoint) < 4) {
                        return new DragPoint(x, y);
                    }
                }
            }
        } else {
            if (point.distance(center) < 4) {
                return new DragPoint(0, 0);
            }
        }

        return null;
    }

    public void drawBounds(BoundingBox b, Color c) {
        b = b.copy();
        b.revalidate();

        Point min = getPointInView(b.min);
        Point max = getPointInView(b.max);

        pen.setColor(c.toAWT());
        pen.setStroke(new BasicStroke(2));
        pen.drawRect(min.x, max.y, max.x - min.x, min.y - max.y);
        pen.setStroke(new BasicStroke(1));
    }

    public void drawDragPoints(BoundingBox b, Color c, boolean resizeable) {
        Point center = getPointInView(b.center());
        Dimension size = getSizeInView(b.dimensions());

        if (resizeable) {
            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    drawDragPoint(center.x + x * size.width / 2, center.y + y * size.height / 2);
                }
            }
        } else {
            drawDragPoint(center.x, center.y);
        }
    }

    private void drawDragPoint(int x, int y) {
        pen.fillRect(x - 3, y - 3, 6, 6);
    }

    @Override
    public void paintComponent(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, this);
        }
    }

    public void clear(Color color) {
        pen.setColor(color.toAWT());
        pen.fillRect(0, 0, getWidth(), getHeight());
    }

    public Graphics2D getPen() {
        return pen;
    }

    public void setupDraw() {
        clear(Color.BLACK);
        if (LevelEditor.ViewMenu.grid.isSelected()) {
            drawGrid();
        }

        pen.setColor(Color.RED.toAWT());
        pen.drawString(axes.name() + " View", 15, 20);
    }

    public void drawGrid() {
        pen.setStroke(new BasicStroke(1));

        pen.setColor(new Color(0.5f, 0.5f, 0.5f).toAWT());
        int numx = (int) (scaleToWorld(getWidth()) / gridSize);
        int numy = (int) (scaleToWorld(getHeight()) / gridSize);

        for (int i = -numx / 2; i <= numx / 2; i++) {
            int x = centerX() + (viewPos.x % (int) (pixelsPerUnit * gridSize)) + (int) (scaleToView(i) * gridSize);
            pen.drawLine(x, 0, x, getHeight());
        }

        for (int j = numy / 2; j >= -numy / 2; j--) {
            int y = centerY() + (viewPos.y % (int) (pixelsPerUnit * gridSize)) + (int) (scaleToView(j) * gridSize);
            pen.drawLine(0, y, getWidth(), y);
        }

        pen.setColor(Color.WHITE.toAWT());
        pen.drawLine(centerX() + viewPos.x, 0, centerX() + viewPos.x, getHeight());
        pen.drawLine(0, centerY() + viewPos.y, getWidth(), centerY() + viewPos.y);
    }

    private class DragPoint {

        private int xmin;
        private int ymin;
        private int xmax;
        private int ymax;

        private DragPoint(int x, int y) {
            this.xmin = x == -1 ? 1 : 0;
            this.ymin = y == 1 ? -1 : 0;
            this.xmax = x == 1 ? 1 : 0;
            this.ymax = y == -1 ? -1 : 0;

            if (x == 0 && y == 0) {
                xmin = 1;
                ymin = -1;
                xmax = 1;
                ymax = -1;
            }
        }

        private int getCursorType() {
            if (xmin != 0 && ymin != 0 && xmax != 0 && ymax != 0) {
                return Cursor.MOVE_CURSOR;
            } else if (xmin != 0) {
                if (ymin != 0) {
                    return Cursor.SW_RESIZE_CURSOR;
                } else if (ymax != 0) {
                    return Cursor.NW_RESIZE_CURSOR;
                } else {
                    return Cursor.W_RESIZE_CURSOR;
                }
            } else if (xmax != 0) {
                if (ymin != 0) {
                    return Cursor.SE_RESIZE_CURSOR;
                } else if (ymax != 0) {
                    return Cursor.NE_RESIZE_CURSOR;
                } else {
                    return Cursor.E_RESIZE_CURSOR;
                }
            } else {
                if (ymin != 0) {
                    return Cursor.S_RESIZE_CURSOR;
                } else {
                    return Cursor.N_RESIZE_CURSOR;
                }
            }
        }
    }

    public enum Axes {

        XY {
            @Override
            public Vector2 toView(Vector3 point) {
                return point.xy();
            }

            @Override
            public Vector3 toWorld(Vector2 point) {
                return point.asXY();
            }

            @Override
            public Vector3 copyUnknown(Vector3 source, Vector3 dest) {
                return new Vector3(dest.x, dest.y, source.z);
            }
        },
        ZY {
            @Override
            public Vector2 toView(Vector3 point) {
                return point.zy();
            }

            @Override
            public Vector3 toWorld(Vector2 point) {
                return point.asZY();
            }

            @Override
            public Vector3 copyUnknown(Vector3 source, Vector3 dest) {
                return new Vector3(source.x, dest.y, dest.z);
            }
        },
        XZ {
            @Override
            public Vector2 toView(Vector3 point) {
                return point.xz();
            }

            @Override
            public Vector3 toWorld(Vector2 point) {
                return point.asXZ();
            }

            @Override
            public Vector3 copyUnknown(Vector3 source, Vector3 dest) {
                return new Vector3(dest.x, source.y, dest.z);
            }
        };

        public abstract Vector3 toWorld(Vector2 point);

        public abstract Vector2 toView(Vector3 point);

        public abstract Vector3 copyUnknown(Vector3 source, Vector3 dest);

    }

    public Vector3 snap(Vector3 point) {
        if (!LevelEditor.ViewMenu.grid.isSelected()) {
            return point;
        }

        float x = Math.round(point.x / gridSize) * gridSize;
        float y = Math.round(point.y / gridSize) * gridSize;
        float z = Math.round(point.z / gridSize) * gridSize;
        return new Vector3(x, y, z);
    }

    public Point getPointInView(Vector3 point) {
        Vector2 usedAxes = axes.toView(point);
        int x = centerX() + viewPos.x + scaleToView(usedAxes.x);
        int y = centerY() + viewPos.y - scaleToView(usedAxes.y);
        return new Point(x, y);
    }

    public Vector3 getPointInWorld(Point point) {
        float x = scaleToWorld(point.x - centerX() - viewPos.x);
        float y = -scaleToWorld(point.y - centerY() - viewPos.x);
        Vector2 usedAxes = new Vector2(x, y);
        return axes.toWorld(usedAxes);
    }

    public Dimension getSizeInView(Vector3 size) {
        Vector2 usedAxes = axes.toView(size);
        int x = scaleToView(usedAxes.x);
        int y = scaleToView(usedAxes.y);
        return new Dimension(x, y);
    }

    public Vector3 getSizeInWorld(Dimension size) {
        float x = scaleToWorld(size.width);
        float y = scaleToWorld(size.height);
        Vector2 usedAxes = new Vector2(x, y);
        return axes.toWorld(usedAxes);
    }

    public int centerX() {
        return getWidth() / 2;
    }

    public int centerY() {
        return getHeight() / 2;
    }

    public int scaleToView(float world) {
        return (int) (world * pixelsPerUnit);
    }

    public float scaleToWorld(int view) {
        return view / (float) pixelsPerUnit;
    }

    private void initComponents() {

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING).addGap(0, 400, Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addGap(0, 300, Short.MAX_VALUE));
    }

    private void formComponentResized(java.awt.event.ComponentEvent evt) {
        image = Resources.createImage(getWidth(), getHeight());
        pen = image.createGraphics();
    }
}

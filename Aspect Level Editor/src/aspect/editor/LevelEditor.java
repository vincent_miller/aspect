package aspect.editor;

import static aspect.resources.Resources.getImage;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JList;
import javax.swing.JSplitPane;
import javax.swing.AbstractListModel;

import aspect.core.Console;
import aspect.core.Console.LogLevel;
import aspect.editor.View2D.Axes;
import aspect.resources.ArcFile;
import aspect.resources.Resources;

import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

public class LevelEditor extends JFrame {

    public static JList<EditorComponent> lstItems;
    public static View3D viewXYZ;
    public static View2D viewXY;
    public static View2D viewXZ;
    public static View2D viewZY;
    public static EditorControl control;
    public static Tool currentTool;
    public static JMenuBar menuBar;
    public static JMenu mnFile;
    public static JMenu mnView;
    public static JButton btnEdit;
    public static JButton btnBlock;
    public static JButton btnEntity;
    public static JFileChooser fileChooser;
    public static ArcTree explorer;
    public static ArcFile archive;
    public static JPopupMenu mnContext;
    public static Queue<String> load;
    public static HashMap<String, Consumer<String>> loaders;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                LevelEditor frame = new LevelEditor();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame.
     */
    @SuppressWarnings({ "unchecked", "rawtypes", "serial" })
    public LevelEditor() {
        super("Aspect Level Editor");
        setLookAndFeel("Nimbus");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        getContentPane().setLayout(new BorderLayout(0, 0));

        fileChooser = new JFileChooser();
        load = new LinkedList<>();
        loaders = new HashMap<>();
        initLoaders();

        lstItems = new JList<>();
        lstItems.setModel(new AbstractListModel() {
            String[] values = new String[] {};

            public int getSize() {
                return values.length;
            }

            public Object getElementAt(int index) {
                return values[index];
            }
        });
        lstItems.setPreferredSize(new Dimension(100, lstItems.getHeight()));
        getContentPane().add(lstItems, BorderLayout.EAST);

        mnContext = new JPopupMenu();
        ContextMenu.init(mnContext);
        explorer = new ArcTree(mnContext);

        getContentPane().add(explorer, BorderLayout.WEST);

        JSplitPane splitHorizontal = new JSplitPane();
        getContentPane().add(splitHorizontal, BorderLayout.CENTER);

        control = new EditorControl();
        viewXYZ = new View3D(control);
        viewXY = new View2D(Axes.XY, control);
        viewXZ = new View2D(Axes.XZ, control);
        viewZY = new View2D(Axes.ZY, control);

        JSplitPane splitVertical1 = new JSplitPane();
        splitVertical1.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitVertical1.setLeftComponent(viewXY);
        splitVertical1.setRightComponent(viewZY);
        splitHorizontal.setLeftComponent(splitVertical1);
        splitVertical1.setDividerLocation(200);

        JSplitPane splitVertical2 = new JSplitPane();
        splitVertical2.setEnabled(false);
        splitVertical2.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitVertical2.setLeftComponent(viewXZ);
        splitVertical2.setRightComponent(viewXYZ);
        splitHorizontal.setRightComponent(splitVertical2);
        splitVertical2.setDividerLocation(200);

        splitHorizontal.setDividerLocation(300);

        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.NORTH);

        btnEdit = new JButton();
        btnEdit.setIcon(new ImageIcon(Resources.getImage("cursor_normal", getClass().getResourceAsStream("/aspect/resources/textures/cursor_normal.png"))));
        btnEdit.addActionListener((evt) -> {
            currentTool = Tool.EDIT;
            setCursors(currentTool);
        });
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        panel.add(btnEdit);

        btnBlock = new JButton();
        btnBlock.setIcon(new ImageIcon(Resources.getImage("tool_block", getClass().getResourceAsStream("/aspect/resources/textures/tool_block.png"))));
        btnBlock.addActionListener((evt) -> {
            currentTool = Tool.BLOCK;
            setCursors(currentTool);
        });

        panel.add(btnBlock);

        btnEntity = new JButton();
        btnEntity.setIcon(new ImageIcon(Resources.getImage("tool_entity", getClass().getResourceAsStream("/aspect/resources/textures/tool_entity.png"))));
        btnEntity.addActionListener((evt) -> {
            currentTool = Tool.ENTITY;
            setCursors(currentTool);
        });

        panel.add(btnEntity);

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        mnFile = new JMenu("File");
        FileMenu.init(mnFile);
        menuBar.add(mnFile);

        mnView = new JMenu("View");
        ViewMenu.init(mnView);
        menuBar.add(mnView);

        splitVertical1.addPropertyChangeListener((PropertyChangeEvent evt) -> splitVertical2.setDividerLocation(splitVertical1.getDividerLocation()));

        currentTool = Tool.EDIT;
        setCursors(currentTool);
    }

    private void initLoaders() {
        Consumer<String> imageLoader = img -> {
            InputStream stream;
            try {
                stream = archive.getInputStream(img);
                getImage(img, stream);
            } catch (IOException ex) {
                Console.log(LogLevel.ERROR, ex);
            }
        };

        loaders.put(".jpg", imageLoader);
        loaders.put(".png", imageLoader);
        loaders.put(".gif", imageLoader);
    }

    public View2D getViewXY() {
        return viewXY;
    }

    public View2D getViewYZ() {
        return viewZY;
    }

    public View2D getViewXZ() {
        return viewXZ;
    }

    public View3D getViewXYZ() {
        return viewXYZ;
    }

    public Tool currentTool() {
        return currentTool;
    }

    private void setCursors(Tool tool) {
        viewXY.setCursor(tool.cursor);
        viewZY.setCursor(tool.cursor);
        viewXZ.setCursor(tool.cursor);
    }

    public static class ContextMenu {
        public static JMenuItem newFolder;
        public static JMenuItem newMap;
        public static JMenuItem importFile;
        public static JMenuItem rename;
        public static JMenuItem delete;

        private static void init(JPopupMenu menu) {
            newFolder = new JMenuItem("New Folder");
            newFolder.addActionListener(evt -> {
                DefaultMutableTreeNode node = explorer.getSelectedNode();
                explorer.addFolder(JOptionPane.showInputDialog("Folder Name: ", "New Folder"), node);

            });
            menu.add(newFolder);

            newMap = new JMenuItem("New Map");
            newMap.addActionListener(evt -> {
                DefaultMutableTreeNode node = explorer.getSelectedNode();
                String name = JOptionPane.showInputDialog("Map Name:", "new_map.map");
                FileMenu.currentMap = explorer.getPath(node) + name;
                FileMenu.currentMapCompiled = FileMenu.currentMap.substring(FileMenu.currentMap.lastIndexOf(".") + 1) + ".scn";
                FileMenu.saveMap.setEnabled(true);
                FileMenu.compileMap.setEnabled(true);
                explorer.addEmptyFile(name, node);
            });
            menu.add(newMap);

            importFile = new JMenuItem("Import File");
            importFile.addActionListener(evt -> {
                int i = fileChooser.showOpenDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    DefaultMutableTreeNode node = explorer.getSelectedNode();
                    explorer.addFile(file, node);
                    load.add(explorer.getPath(node) + file.getName());
                }
            });
            menu.add(importFile);

            rename = new JMenuItem("Rename");
            rename.addActionListener(evt -> {
                DefaultMutableTreeNode node = explorer.getSelectedNode();
                if (!node.isRoot()) {
                    String name = JOptionPane.showInputDialog("Rename Item: ", node.getUserObject());
                    if (name != null && !name.isEmpty()) {
                        explorer.rename(node, name);
                    }
                }
            });
            menu.add(rename);

            delete = new JMenuItem("Delete");
            delete.addActionListener(evt -> {
                DefaultMutableTreeNode node = explorer.getSelectedNode();
                if (!node.isRoot()) {
                    explorer.delete(node);
                }
            });
            menu.add(delete);
        }
    }

    public static class ViewMenu {
        public static JCheckBoxMenuItem grid;
        public static JMenuItem increaseGridSize;
        public static JMenuItem decreaseGridSize;
        public static JCheckBoxMenuItem lighting;
        public static JCheckBoxMenuItem textures;
        public static JCheckBoxMenuItem wireframe;

        private static boolean t = true;
        private static boolean l = true;

        private static void init(JMenu menu) {
            grid = new JCheckBoxMenuItem("Grid", true);
            menu.add(grid);

            increaseGridSize = new JMenuItem("Increase Grid Size");
            increaseGridSize.addActionListener((evt) -> View2D.gridSize *= 2);
            menu.add(increaseGridSize);

            decreaseGridSize = new JMenuItem("Decrease Grid Size");
            decreaseGridSize.addActionListener((evt) -> View2D.gridSize /= 2);
            menu.add(decreaseGridSize);

            menu.addSeparator();

            lighting = new JCheckBoxMenuItem("Lighting", true);
            menu.add(lighting);

            textures = new JCheckBoxMenuItem("Textures", true);
            menu.add(textures);

            wireframe = new JCheckBoxMenuItem("Wireframe", false);
            wireframe.addActionListener((evt) -> {
                if (wireframe.isSelected()) {
                    l = lighting.isSelected();
                    t = textures.isSelected();
                    lighting.setSelected(false);
                    lighting.setEnabled(false);
                    textures.setSelected(false);
                    textures.setEnabled(false);
                } else {
                    lighting.setSelected(l);
                    lighting.setEnabled(true);
                    textures.setSelected(t);
                    textures.setEnabled(true);
                }
            });
            menu.add(wireframe);
        }
    }

    public static class FileMenu {
        public static JMenuItem saveMap;
        public static JMenuItem compileMap;

        public static JMenuItem newArc;
        public static JMenuItem openArc;

        private static String currentMap = null;
        private static String currentMapCompiled = null;

        private static void init(JMenu menu) {

            saveMap = new JMenuItem("Save Map");
            saveMap.addActionListener(evt -> {
                if (currentMap != null) {
                    try {
                        String dir = currentMap;
                        int i = dir.lastIndexOf("/");
                        if (i == -1) {
                            dir = "";
                        } else {
                            dir = dir.substring(0, i + 1);
                        }
                        final String d = dir;

                        File temp = new File(new File(currentMap).getName());
                        OutputStream out = new FileOutputStream(temp);
                        control.save(out);
                        archive.addFile(temp, dir);
                        temp.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            saveMap.setEnabled(false);

            menu.add(saveMap);
            
            compileMap = new JMenuItem("Compile Map");
            compileMap.addActionListener(evt -> {
                if (currentMapCompiled != null) {
                    try {
                        String dir = currentMapCompiled;
                        int i = dir.lastIndexOf("/");
                        if (i == -1) {
                            dir = "";
                        } else {
                            dir = dir.substring(0, i + 1);
                        }
                        final String d = dir;

                        File temp = new File(new File(currentMapCompiled).getName());
                        OutputStream out = new FileOutputStream(temp);
                        control.compile(out);
                        archive.addFile(temp, dir);
                        temp.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            compileMap.setEnabled(false);
            menu.add(compileMap);

            menu.addSeparator();

            newArc = new JMenuItem("New Archive");
            newArc.addActionListener(evt -> {
                int i = fileChooser.showSaveDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    archive = ArcFile.create(fileChooser.getSelectedFile());
                    explorer.setArchive(archive);
                }
            });
            menu.add(newArc);

            openArc = new JMenuItem("Open Archive");
            openArc.addActionListener(evt -> {
                int i = fileChooser.showOpenDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    archive = ArcFile.open(fileChooser.getSelectedFile());
                    explorer.setArchive(archive);
                }
            });
            menu.add(openArc);
        }

        public static void openMap(String path) {
            InputStream in;
            try {
                in = archive.getInputStream(path);
                control.open(in);
                currentMap = path;
                FileMenu.currentMapCompiled = FileMenu.currentMap.substring(FileMenu.currentMap.lastIndexOf(".") + 1) + ".scn";
                saveMap.setEnabled(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static enum Tool {
        BLOCK(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR)), EDIT(Cursor.getDefaultCursor()), ENTITY(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

        public final Cursor cursor;

        private Tool(Cursor cursor) {
            this.cursor = cursor;
        }
    }

    /**
     * @param name
     */
    public void setLookAndFeel(String name) {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(LevelEditor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

package aspect.editor;

import java.io.IOException;

import org.lwjgl.opengl.GL11;

import aspect.entity.Entity;
import aspect.render.Billboard;
import aspect.render.PointLight;
import aspect.render.Material;
import aspect.render.Material.Filter;
import aspect.render.Texture;
import aspect.render.shader.ShaderProgram;
import aspect.resources.Resources;
import aspect.util.AspectInputStream;
import aspect.util.AspectOutputStream;
import aspect.util.BoundingBox;
import aspect.util.Color;
import aspect.util.Vector3;
import static aspect.core.AspectRenderer.*;

public class EditorLight extends EditorComponent {
    private PointLight light;
    private boolean built = false;

    public EditorLight(Vector3 position) {

        transform.setPosition(position);
        bounds = BoundingBox.cube(1.0f);

        light = new PointLight(Color.WHITE);
    }

    public EditorLight(AspectInputStream stream) throws IOException {
        this(stream.readVector3());
    }

    @Override
    public void onRemove() {
        light.onRemove();
    }

    @Override
    public void update() {
        light.transform.set(transform);
    }

    @Override
    public void revalidate() {
        transform.setPosition(bounds.center());
        bounds = BoundingBox.cube(0.5f);
        bounds.setCenter(transform.position());
    }

    @Override
    public void write(AspectOutputStream stream) throws IOException {
        stream.writeVector3(transform.position());
    }

    @Override
    public void render() {
        if (!built) {
            Texture texture = Resources.getTexture("icon_light", getClass().getResourceAsStream("/aspect/resources/textures/icon_light.png"));
            Material m = new Material(texture);
            m.shader = ShaderProgram.TEXTURE;
            m.setTextureFilter(Filter.NEAREST, Filter.NEAREST);
            model = new Billboard(Resources.rect(m, 0.5f, 0.5f));
            built = true;
        }
        super.render();
    }
}
